<?php

/**
 * Description of ShView
 *
 * @author tiago
 */
class ShView {

    public static function ajaxModal($label, $url, $id, $msgErro = 'Ops! Tivemos um probleminha, atualize a página.', $htmlOptions = array(), $modelID = 'nova-tax') {
        return CHtml::ajaxLink($label, $url, array(
                    'success' => 'js:function(data){$("#' . $modelID . '").html(data);$("#' . $modelID . '").dialog("open");}',
                    'error' => 'js:function(data){alert("' . $msgErro . '");}',
                        ), $htmlOptions + array(
                    'class' => 'hint',
                    'id' => $id,
        ));
    }

    /**
     * Mantém somente o nome dos parametros no template {{nome}}
     * @param type $template
     * @return string conforme descirção. Usado com angular.js
     */
    public static function simplificaTemplate($template) {
        $parametros =  ShView::extraiParametros($template);
        $codigo = $template;
        foreach ($parametros as $paramID => $valorDefault) {
            $codigo = preg_replace('/{{' . $paramID . ':(.*?)}}/i', '{{' . $paramID . '}}', $codigo);
        }
        return $codigo;
    }

    /**
     * Faz merge do template com os valores dos parametros cadastrados, caso o parâmetro não esteja cadastrado o valor default é incluíod.
     * @param type $template
     * @param type $data
     * @return string codigo sem marcações
     */
    public static function mergeDataToTemplate($template, $content) {
        $parametros = ShView::extraiParametros($template);
        $codigo = $template;

        foreach ($parametros as $paramID => $data) {
            if ($data['isList']) {
                if (isset($content[$paramID]) && is_array($content[$paramID])) {
                    $valor = '[(' . implode('),(', $content[$paramID]['valor']) . ')]';
                } else {
                    $valor = $parametros[$paramID]['valor'];
                }
            } else {

              if(isset($content[$paramID]['valor']) && strlen($content[$paramID]['valor']) > 0){
                $valor = $content[$paramID]['valor'];
              } else {
                $valor = $data['valor'];
              }
            }

            $label = str_replace(array('@', '|'), array('\@', '\|'), $data['labelOriginal']);
            $codigo = preg_replace('/{{' . $label . ':(.*?)}}/i', $valor, $codigo);
            $codigo = preg_replace('/{{' . $label . '}}/i', $valor, $codigo);
        }

        return $codigo;
    }

    /**
     * Faz merge do template com os valores dos parametros cadastrados, caso o parâmetro não esteja cadastrado o valor default é incluíod.
     * @param type $template
     * @param type $data
     * @return string codigo sem marcações
     */
    public static function replaceData($template, $data) {
        $parametros = ShView::extraiParametros($template);
        $codigo = $template;
        foreach ($parametros as $paramID => $paramData) {
            $valorDefault = $paramData['valor'];
            $valor = isset($data[$paramID]) ? $data[$paramID] : $valorDefault;
            $replace = "{{---$valor---}}";
            $codigo = preg_replace('/{{' . $paramID . ':(.*?)}}/i', $replace, $codigo);
        }
        return $codigo;
    }

    /**
     * Extrai parametros com valores default do template
     * @param type $template
     * @return type
     */
    public static function extraiParametros($template) {
        $parametros = $matches = array();
//        $tptId = false;
        preg_match_all('/{{(.*?)}}/i', $template, $matches);
        $data = $matches[1];

        foreach ($data as $d) {

            // Separa label e value
            if (strstr($d, ':')) {
                list($nomeParametro, $valorParametro) = explode(':', $d);
            } else {
                if (!in_array($d, array_keys($parametros))) {
                    $nomeParametro = $d;
                    $valorParametro = '';
                }
            }
            $labelOriginal = $nomeParametro;

            // Verifica tipo
            $dataTipo = array();
            preg_match_all('/\|(.*?)\|/i', $nomeParametro, $dataTipo);
            $tipo = 'str';
            if (isset($dataTipo[1]) && count($dataTipo[1]) > 0) {
                $tipo = $dataTipo[1][0];
                $nomeParametro = str_replace($dataTipo[0][0], '', $nomeParametro);
                if (!in_array($tipo, array('int'))) {
                    $tipo = 'str';
//                    if (strpos($tipo, 'tpt-') !== false) {
//                        list($tipo, $tptId) = explode('-', $tipo);
//                    } else {
//                    }
                }
            }


            // Identifica cardinalidade (1 ou n)
            $isList = false;
            if (substr($nomeParametro, 0, 1) === '@') {
                $nomeParametro = substr($nomeParametro, 1, strlen($nomeParametro) - 1);
                $isList = true;
            }

            $data = array(
                'valor' => $valorParametro,
                'tipo' => $tipo,
                'isList' => $isList,
//                'tptId' => $tptId,
                'labelOriginal' => $labelOriginal,
            );
            $parametros[$nomeParametro] = $data;
        }

        return $parametros;
    }

    /**
     * Monta conteúdo HTML para edição do tipo do parâmetro
     * @param type $id
     * @param type $p
     * @return type
     */
    public static function getTemplateTipo($label, $template, $data) {
        if (in_array($template['tipo'], array('int', 'str'))) {
            $value = isset($data[$label]) ? $data[$label]['valor'] : $template['valor'];
            return Yii::app()->controller->renderPartial('shared.views.Widget.' . $template['tipo'], array(
                        'id' => $label,
                        'isList' => $template['isList'],
                        'value' => $value,
                            ), true);
        } else {
            return 'tipo desconhecido';
        }
    }

    /**
     * Agrupa valores de parametros do tipo template e lista
     * @param type $content
     * @return type
     */
    public static function normalizaTemplateData($template, $data) {
        $parms = ShView::extraiParametros($template);
        foreach ($parms as $id => $p) {
            if (isset($data[$id])) {
                if ($p['isList'] && $p['tipo'] == 'tpt') {
                    $data = $data[$id];
                    $newData = array();
                    foreach ($data as $k => $subData) {
                        foreach ($subData as $k2 => $v) {
                            if (!isset($newData[$k2][$k])) {
                                $newData[$k2][$k] = array();
                            }
                            $newData[$k2][$k] = $v;
                        }
                    }
                    $data[$id] = $newData;
                }
            }
        }
        return $data;
    }

    /**
     * Cria link para processamento na busca
     * @param type $proccess
     * @return type
     */
    public static function makeLinkResol($proccess, $label = null) {
        $label = is_null($label) ? $proccess : $label;
        return CHtml::link("$label", Yii::app()->controller->createUrl('search/ResultEq', array('q' => $proccess)));
    }

	public static function shareLinks($url,$title='O Monitor',$desc='teste'){
		$img = urlencode("http://{$_SERVER['HTTP_HOST']}" . Yii::app()->baseUrl . "/webroot/logo-face.png");

		echo '<a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url='.$url.'" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/16x16/facebook.png" border="0" alt="Facebook"/></a>
		<a href="https://api.addthis.com/oexchange/0.8/forward/gmail/offer?url='.$url.'&title='.$title.'" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/16x16/gmail.png" border="0" alt="Gmail"/></a>
		<a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url='.$url.'&title='.$title.'" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/16x16/twitter.png" border="0" alt="Twitter"/></a>
		<a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url='.$url.'&title='.$title.'" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/16x16/linkedin.png" border="0" alt="LinkedIn"/></a>
		<a href="https://www.addthis.com/bookmark.php?source=tbx32nj-1.0&v=300&url='.$url.'&title='.$title.'" target="_blank"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/16x16/addthis.png" border="0" alt="Addthis"/></a>';
	}


}

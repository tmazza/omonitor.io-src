<?php

class ShSage {

    public static $host = 'http://aleph.sagemath.org/service';

    public static function run($code) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::$host);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, array('accepted_tos' => 'true', 'code' => utf8_encode($code)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }

    public static function isEqual($a, $b, $f) {
        $code = <<<EOT
{$f}
if (corrigi({$a},{$b})):
   print "SIM"
else:
   print "NAO"
EOT;
        $data = ShSage::run($code);
        if($data['success'] && $data['success'] == '1') {
          return isset($data['stdout']) && trim($data["stdout"]) == 'SIM';
        } else {
          return null;
        }
    }

}

<?php

/**
 * Description of Seg
 *
 * @author tiago
 */
class ShSeg {

    /**
     * Controle de acesso Acesso a recurso/modulos da aplicação
     * @param type $operation
     */
    public static function checkAccess($operation) {
        if (!ShSeg::hasAccess($operation)) {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Verifica permissão de usuário de acordo com definições me authAssign
     * @param type $operation
     * @return type
     */
    public static function hasAccess($operation) {
        return Yii::app()->user->checkAccess($operation);
    }

    // Controle de acesso a recuros
    /**
     * Controle de acesso a nodo de tax item
     * @param type $id
     */
    public static function acessoNodo($id) {
        $permissoes = Yii::app()->controller->getPermissoes();
        $permissoesEmNodos = $permissoes['nodos'];
        if (!in_array($id, $permissoesEmNodos)) {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Controle de acesso a tópicos
     */
    public static function acessoTopico($id) {
        $permissoes = Yii::app()->controller->getPermissoes();
        $permissoesEmTopicos = $permissoes['topicos'];
        $meusTopicos = CHtml::listData(Topico::model()->doAutor()->findAll(), 'id', 'id');
        $todosOsTopicos = $meusTopicos + $permissoesEmTopicos;
        if (!in_array($id, $todosOsTopicos)) {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Controle de acesso a arquivo
     */
    public static function acessoArquivo($id) {
        $permissoes = Yii::app()->controller->getPermissoes();
        $permissoesEmArquivos = $permissoes['arquivos'];
        $meusArquivos = CHtml::listData(Arquivo::model()->doAutor()->findAll(), 'id', 'id');
        $todosOsArquivos = $meusArquivos + $permissoesEmArquivos;
        if (!in_array($id, $todosOsArquivos)) {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Controle de acesso a questionários
     */
    public static function acessoQuestionario($id) {
        $permissoes = Yii::app()->controller->getPermissoes();
        $permissoesEmQuestionarios = $permissoes['questionarios'];
        $meusQuestionarios = CHtml::listData(Topico::model()->doAutor()->findAll(), 'id', 'id');
        $todosOsQuestionarios = $meusQuestionarios + $permissoesEmQuestionarios;
        if (!in_array($id, $todosOsQuestionarios)) {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Bloqueia acesso de usuário
     */
    public static function bloqueiaAcesso() {
        Yii::app()->controller->redirect(Yii::app()->controller->createAbsoluteUrl('/site/error', array(
                    'msg' => 'Sem permissão de acesso.',
        )));
    }

}

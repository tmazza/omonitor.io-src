<?php

/**
 * Description of ShEmail
 *
 * @author tiago
 */
class ShEmail {

    public static $responsaveis = array('tiagomdepaula@gmail.com');

    /**
     * Reporta erro para responsável associados
     * @param type $msg
     * @param type $data
     */
    public static function reportaErro($msg, $data = array()) {
        ShEmail::send(self::$responsaveis, "Erro em produção", Yii::app()->name, 'tiagomdepaula@gmail.com', $msg);
    }

    /**
     * Envia email
     * @param type $para
     * @param type $assunto
     * @param type $fromNome
     * @param type $fromEmail
     * @param type $conteudo
     */
    public static function send($para, $assunto, $fromNome, $fromEmail, $conteudo) {
        $name = '=?UTF-8?B?' . base64_encode($fromNome) . '?=';
        $subject = '=?UTF-8?B?' . base64_encode($assunto) . '?=';
        $headers = "From: $name <{$fromEmail}>\r\n" .
                "Reply-To: {$fromEmail}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-type: text/plain; charset=UTF-8";

        if (mail(implode(', ', $para), $subject, utf8_encode(str_replace("\n.", "\n..", $conteudo)), $headers)) {
            return true;
        } else {
            return false;
        }
    }

}

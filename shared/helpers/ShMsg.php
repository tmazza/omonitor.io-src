<?php

/**
 * Description of ShMsg
 *
 * @author tiago
 */
class ShMsg {

    /**
     * Envia mensagem para outro usuário
     * @param type $para
     * @param type $msg
     * @param type $tipo
     */
    public static function enviar($para, $corpo, $tipo = Mensagem::TipoPessoal) {

        if(!is_null(Yii::app()->user->id)){
            $de = Yii::app()->user->id;
        }else{
            $de = -1;
        }
        $msg = new Mensagem();
        $msg->de = $de;
        $msg->para = $para;
        $msg->mensagem = $corpo;
        $msg->tipo = $tipo;
        $msg->lida = 0;
        $msg->entregue = 0;
        $msg->dataEnvio = time();
        if($msg->save()){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Busca todas as mensagens não lidas do usuários
     */
    public static function getNaoLidas() {
        $msgs = Mensagem::model()
                ->with('origem')
                ->queRecebi()
                ->naoLidas()
                ->findAll();
        // Altera status de mensagens não entregues para entregus.
        foreach ($msgs as $msg) {
            if (!$msg->entregue) {
                $msg->setAsEntregue();
            }
        }
        return $msgs;
    }

    /**
     * Ultimas mensagens recebidas
     * @param type $limit
     * @return type
     */
    public static function ultimasRecebidas($limit) {
        return Mensagem::model()
                        ->with('origem')
                        ->lidas()
                        ->queRecebi()
                        ->findAll(array('limit' => $limit, 'order' => 'dataEnvio DESC'));
    }

    /**
     * Retorna string de acordo com a diferença de tempo de $tempoInicial e o tempo atual
     * @param type $tempoInicial
     * @return type
     */
    public static function tempoAteAgora($tempoInicial) {
        $diferenca = time() - $tempoInicial;
        $start_date = new DateTime(date("Y-m-d H:i:s", $tempoInicial));
        $since_start = $start_date->diff(new DateTime(date("Y-m-d H:i:s", time())));
        $anos = $since_start->y . ' years<br>';
        $meses = $since_start->m . ' months<br>';
        $dias = $since_start->d . ' days<br>';

        if ($diferenca < 60) {
            return $since_start->s . ' segundo' . ShMsg::hasPlural($since_start->s);
        } elseif ($diferenca < (60 * 60)) {
            return $since_start->i . ' minuto' . ShMsg::hasPlural($since_start->i);
        } elseif ($diferenca < (60 * 60 * 24)) {
            return $since_start->h . ' hora' . ShMsg::hasPlural($since_start->h);
        } else {
            return $since_start->days . ' dia' . ShMsg::hasPlural($since_start->days);
        }
    }

    /**
     * Caso var seja maior que 1 o sufixo de plural é retornado
     * @param type $teste
     * @param type $sufixo
     * @return type
     */
    public static function hasPlural($teste, $sufixo = 's') {
        return $teste > 1 ? $sufixo : null;
    }

//    // TODO: enviar para todos os elementos do grupo
//    public static function enviarParaGrupo(){
//
//    }
}

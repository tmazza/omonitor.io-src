<?php

/**
 * Description of Code
 *
 * @author tiago
 */
class ShCode {

    public static function setFlashInfo($msg) {
        Yii::app()->user->setFlash(EditorController::INFO_FLASH, $msg);
    }

    public static function setFlashErro($msg) {
        Yii::app()->user->setFlash(EditorController::ERRO_FLASH, $msg);
    }

    public static function setFlashSucesso($msg) {
        Yii::app()->user->setFlash(EditorController::SUCS_FLASH, $msg);
    }

    public static function renderFlashes() {
        $content = '';
        if (Yii::app()->user->hasFlash(EditorController::INFO_FLASH)) {
            $content .= ShCode::flashSkin("info", Yii::app()->user->getFlash(EditorController::INFO_FLASH));
        }
        if (Yii::app()->user->hasFlash(EditorController::SUCS_FLASH)) {
            $content .= ShCode::flashSkin("success", Yii::app()->user->getFlash(EditorController::SUCS_FLASH));
        }
        if (Yii::app()->user->hasFlash(EditorController::ERRO_FLASH)) {
            $content .= ShCode::flashSkin("error", Yii::app()->user->getFlash(EditorController::ERRO_FLASH));
        }
        return $content;
    }

    /**
     * Template base das mesnagens flash.
     * $name
     * @param type $msg
     * @return type
     */
    public static function flashSkin($name, $msg) {
        return "<div class='alert alert-{$name} alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>{$msg}</div>";
    }

    /**
     * Cria link de menu
     * @param type $label
     * @param type $url
     * @param type $outros
     * @return type
     */
    public static function makeItem($label, $url, $outros = array()) {
        return array_merge($outros,array(
            'label' => $label,
            'url' => $url,
          ));
    }

    /**
     * Cria link de menu condicionado a existencia do prametro rt na URL
     * @param type $label
     * @param type $url
     * @param type $outros
     * @return type
     */
    public static function makeItemCondicionado($label, $url, $outros = array()) {
        if (isset($_GET['rt'])) {
            $url = ShCode::interpretaLinkRetorno($_GET['rt']);
            return ShCode::makeItem($label, $url, $outros);
        } else {
            return ShCode::makeItem($label, $url, $outros);
        }
    }

    /**
     * Remove acentos de string
     * @param type $str
     * @return type
     */
    public static function removeAcentos($str) {
        // assume $str esteja em UTF-8
        $map = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e', 'ê' => 'e', 'í' => 'i', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U',
            'Ü' => 'U', 'Ç' => 'C'
        );

        return strtr($str, $map);
    }

    /**
     * Normaliza string para busca
     * @param type $str
     * @return type
     */
    public static function paraBusca($str) {
        return strtolower(ShCode::removeAcentos($str));
    }

    public static function toUrl($str){
      $str = str_replace(' ','-',str_replace('  ',' ',$str));
      $str = preg_replace('[\.|,|;|\/]','',$str);
      $str = str_replace('>','maior',$str);
      $str = str_replace('<','menor',$str);
      return self::paraBusca($str);
    }

    /**
     * Url para controler/action de um módulo dentro do perfil do usuário
     */
    public static function getModUrl($module, $controller = 'default', $action = 'index', $params = array()) {
        $baseModule = 'meuEspaco';
        return Yii::app()->controller->createUrl('/' . $baseModule . '/' . $module . '/' . $controller . '/' . $action, $params);
    }

    /**
     * Gera link de referencia de retorno
     * @return type
     */
    public static function geraLinkRetorno($params = array()) {
        $base = str_replace('/', '.', Yii::app()->controller->module->id) . '.' . Yii::app()->controller->id . '.' . Yii::app()->controller->action->id;
        foreach ($params as $p) {
            $v = Yii::app()->request->getParam($p);
            $base .= '.' . $p . '.' . $v;
        }
        return $base;
    }

    /**
     * Interpreta link de retorno gerado
     * @param type $link
     * @return type
     */
    public static function interpretaLinkRetorno($link) {
        return Yii::app()->controller->createUrl('/' . str_replace(array("."), array("/"), $link));
    }

    // public static function getGoogleAnalytics(){
    //     echo "<script>
    //                     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    //                         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    //               m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    //               })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    //
    //               ga('create', 'UA-67290251-1', 'auto');
    //               ga('send', 'pageview');
    //
    //             </script>";
    // }
}

<script>
    $(document).ready(function() {
        $('#<?= $this->contentID; ?>').sticky({
            topSpacing: <?= $this->top; ?>,
            bottomSpacing: <?= $this->bottom; ?>
        });
    });
</script>
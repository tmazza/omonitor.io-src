<?php

/**
 * Description of Kit
 *
 * @author tiago
 */
class Kit extends CWidget {

    public $contentID;

    public function init() {
        $publicPath = Yii::app()->assetManager->publish(__DIR__ . '/assets/');
        Yii::app()->clientScript->registerScriptFile($publicPath . '/js/kit.js', CClientScript::POS_HEAD);
        parent::init();
    }

    public function run() {
        $this->render('kit');
    }

}

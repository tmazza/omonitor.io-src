<?php

/**
 * Description of SearchBox
 *
 * @author tiago
 */
class StickyContent extends CWidget {

    public $contentID;
    public $top = 0;
    public $bottom = 0;



    public function init() {
        $publicPath = Yii::app()->assetManager->publish(__DIR__ . '/assets/');
        Yii::app()->clientScript->registerScriptFile($publicPath . '/js/jquery.sticky.js', CClientScript::POS_HEAD);
        parent::init();
    }

    public function run() {
        $this->render('_core');
    }

}

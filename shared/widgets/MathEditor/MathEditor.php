<?php

/**
 * Description of MathEditor
 *
 * @author tiago
 */
class MathEditor extends CWidget {

    public $camada;
    private $templates;
    private $tipos;
    protected $controller;

    public function init() {
        $this->controller = Yii::app()->controller;
        $this->setTiposDeCamadas();
        $this->setTemplates();
        $this->incluiArquivos();
    }

    public function run() {
        $this->render('main', array(
            'camada' => $this->camada,
            'tipos' => $this->tipos,
            'templates' => $this->templates,
        ));
    }

    private function setTemplates() {
        $this->templates = TemplatesCodigo::model()->findAll();
    }

    private function setTiposDeCamadas() {
        $this->tipos = CamadaTipo::model()->findAll(array(
            'condition' => "(de_aplicacao = 1 AND  visibilidade_edicao = " . CamadaTipo::VisiEditAplicacao . ") "
            . "OR (user_id = '" . Yii::app()->user->id . "' AND  visibilidade_edicao = " . CamadaTipo::VisiEditPessoal . ")",
            'order' => 'de_aplicacao DESC',
        ));
    }

    private function incluiArquivos() {
        $assetsPath = Yii::app()->assetManager->publish(__DIR__ . '/assets');
        Yii::app()->clientScript->registerScriptFile($assetsPath . '/main.js');
        Yii::app()->clientScript->registerCssFile($assetsPath . '/main.css');
    }

}

<?php
$this->widget('shared.widgets.StickyContent.StickyContent', array(
    'contentID' => 'stick-menu',
));
?>
<div id='stick-menu' style="background: white; z-index: 10; padding-bottom: 4px;">
    <div class="sh-row">
        <div class="medium-12 column">
            <!--CONTROLE-->
            <div id="a-topEdit">
                <div class="sh-row" style="margin-bottom: 3px;">
                    <div class="medium-8 column">
                        <div class="btn-group">
                            <button class="btn btn-sm btn-primary" onclick="saveSelection();
                                    openTopEdit('minhas-camadas');"><i class="fa fa-user"></i> Minhas camadas</button>
                            <button class="btn btn-sm btn-primary" onclick="saveSelection();
                                    openTopEdit('reutilizar-camada');"><i class="fa fa-puzzle-piece"></i> Reutilizar camadas</button>
                            <button class="btn btn-sm btn-primary" onclick="saveSelection();
                                    openTopEdit('templates-de-codigo');"><i class="fa fa-code"></i> Modelos de código</button>
                            <button class="btn btn-sm btn-primary" onclick="saveSelection();
                                    openTopEdit('templates-de-latex');"><i class="fa fa-superscript"></i> LaTex</button>
                        </div>
                    </div>
                    <div class="medium-4 column text-right">
                        <div class="btn-group">
                            <?php
                            echo CHtml::link("<i class='fa fa-repeat'></i> Salvar e continuar editando", '#', array(
                                'onclick' => 'salvaContinua($(this));',
                                'class' => 'btn btn-success btn-sm',
                            ));
                            echo CHtml::link("<i class='fa fa-save'></i> Salvar e finalizar", '#', array(
                                'onclick' => 'salvaFinaliza($(this));',
                                'class' => 'btn btn-success btn-sm',
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <div class="sh-row">
                    <div class="medium-5 column" style="padding-top: 3px;">
                        <?php
                        $this->render('_minhasCamadas', array(
                            'tipos' => $tipos,
                            'deAplicacao' => true,
                            'doUsuario' => false,
                        ));
                        ?>
                    </div>
                    <div class="medium-7 column">
                        <div id="redactor-toolbar"></div>
                    </div>
                </div>
            </div>
            <!--EDIÇÂO-->
            <div id="b-topEdit" style="max-height: 400px; overflow: hidden; overflow-y: auto;">
                <div class="sh-row">
                    <div class="medium-12 column">
                        <!--                        <div class="btn-group left">
                                                    <button class="btn btn-success btn-sm left" title="Salvar e continuar editando"><i class="fa fa-repeat"></i></button>
                                                    <button class="btn btn-success btn-sm left" title="Salvar e finalizar"><i class="fa fa-save"></i></button>
                                                    &nbsp;&nbsp;
                                                </div>-->
                        <button class="btn btn-danger btn-sm right" onclick="toggleTopEdit()"><i class="fa  fa-angle-down"></i></button>
                        <div class="b-topEdit" id="minhas-camadas">
                            <?php
                            $this->render('_minhasCamadas', array(
                                'tipos' => $tipos,
                                'deAplicacao' => false,
                                'doUsuario' => true,
                            ));
                            ?>
                        </div>
                        <div class="b-topEdit" id="reutilizar-camada">
                            <?php
                            $this->render('_reutilzarCamadas', array(
                                'tipos' => $tipos,
                            ));
                            ?>
                        </div>
                        <div class="b-topEdit" id="templates-de-codigo">
                            <?php
                            $this->render('_templatesDeCodigo', array(
                                'templates' => $templates,
                            ));
                            ?>
                        </div>
                        <div class="b-topEdit" id="templates-de-latex">
                            <?php
                            $this->render('_templatesDeLatex', array(
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
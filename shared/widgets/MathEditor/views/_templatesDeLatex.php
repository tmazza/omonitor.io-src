<?php $templates = PubliTemplatesLatex::model()->doAutor()->findAll(); ?>
<div class="sh-row">
    <div class="column medium-10">
        <input type="text" id="s" />
        <ul class="list-group">
            <?php foreach ($templates as $t): ?>
                <li class="lt list-group-item">
                    <?= strip_tags($t->codigo); ?>
                    <span class="to-add hidden"><?= strip_tags($t->codigo); ?></span>
                    <button type="button" class="btn btn-xs btn-primary" onclick="usaLatex($(this),false)">Simple $</button>
                    <button type="button" class="btn btn-xs btn-primary" onclick="usaLatex($(this),true)">Centralizado $$</button>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<?php foreach ($templates as $t): ?>
    <?php
    echo CHtml::ajaxLink($t->nome, $this->controller->createUrl('topico/AjaxTemplateParametros', array('id' => $t->id)), array(
        'update' => '#template-params',
            ), array(
        'class' => 'btn btn-default btn-xs',
    ));
    ?>
<?php endforeach; ?>
<div id="template-params"></div>
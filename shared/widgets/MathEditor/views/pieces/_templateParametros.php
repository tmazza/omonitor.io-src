<?php $params = ShView::extraiParametros($template->template); ?>
<div id="template-parametros">
    <?php foreach ($params as $p => $v): ?>
        <?php $v = isset($data[$p]) ? $data[$p] : $v; ?>
        <?= $p ?>: <input type="text" data-id="<?= $p ?>" class="param" value="<?= $v ?>"><br>
    <?php endforeach; ?>
</div>
<?php if (is_null($target)): ?>
    <button type="button" onclick="usaTemplate('<?= $template->id ?>', false)" >Criar</button>
<?php else: ?>
    <button type="button" onclick="usaTemplate('<?= $template->id ?>', '<?= $target; ?>')" >Atualizar</button>
<?php endif; ?>
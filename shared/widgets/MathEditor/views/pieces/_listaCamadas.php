<div class="sh-row">
    <div class="medium-12 column">
        <div class="text-left">
            <h4 class="text-center"><?= $tipo->nome; ?></h4>
            <ul class="list-group">
                <li class="list-group-item"><b>Reuso</b>: o mesmo conteúdo é utilizado. Alterações locais mudam a versão original e os demais ussos da camada.</li>
                <li class="list-group-item"><b>Duplicar</b>: uma cópia do conteúdo é criada. Alterações locais não mudam a verão original nem as demais cópias.</li>
            </ul>
        </div>
        
        <ul class="list-group">
            <?php foreach ($camadas as $camada): ?>
                <li class="list-group-item">
                    <div class="sh-row">
                        <div class="medium-12 column text-left">
                            <?= $camada->getTipoView($camada, true); ?>
                            <button type="button" class="btn btn-default right" onclick="restoreSelection();duplicarCamada('<?= $camada->id; ?>');">Duplicar</button>
                            <button type="button" class="btn btn-primary right" onclick="restoreSelection();reutilizarCamada('<?= $camada->id; ?>');">Reutilizar</button>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
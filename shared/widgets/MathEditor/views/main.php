<?php
$this->render('_topMenuEdicao', array(
    'tipos' => $tipos,
    'templates' => $templates,
));
?>
<div class="sh-row">
    <div class="medium-12 column">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edicao-conteudo-form',
            'action' => $this->controller->createUrl('topico/SalvarConteudo'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>

        <?php echo $form->hiddenField($camada, 'id'); ?>

        <div class="">
            <?php
            $this->widget('ImperaviRedactorWidget', array(
                'model' => $camada,
                'attribute' => 'conteudo',
                'options' => array(
                    'lang' => 'pt_br',
                    'plugins' => array(
//                        'table',
//                    'imageUpload' =>$this->controller->createUrl('topico/uploadImage'),
                        'bufferbuttons',
//                    'fullscreen',
                        'definedlinks',
                        'video',
                        'advanced',
                    ),
//                    cleanOnPaste: false
//                    pastePlainText: true
//                    allowedTags: ['br', 'strong', 'em']
                    'replaceDivs' => false,
                    'minHeight' => 300,
                    'removeEmpty' => array('p'),
                    'cleanStyleOnEnter' => false,
                    'linebreaks' => true,
                    'pastePlainText' => true,
                    'cleanOnPaste' => true,
                    'focus' => true,
                    'toolbarExternal' => '#redactor-toolbar',
                    // 
                    'startCallback' => 'js: function() { triggetSageInside(); } ',
                    'enterCallback' => 'js:  function(e) { return enterCallback(e); } ',
                    // TODO: não ser no callback, mas sim no template de edição
                    'initCallback' => 'js: function() { $(".camada").prepend("<div class=\"ignore inline right\"><button type=\"button\" class=\"fecha-camada btn btn-xs btn-danger\" onclick=\'removerEstaCamada($(this));\'>x</button></div>"); }',
                    'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
                    'definedLinks' => $this->controller->createUrl('topico/linkParaOutrosTopicos'),
                    'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
//                'clickCallback' => 'js: function() { $(".camada[tipo=\'18\']").attr("contenteditable", "false") }',
                ),
            ));
            ?>
            <?php echo $form->error($camada, 'conteudo'); ?>
        </div>

        <div class="textCenter">
            <?php echo CHtml::submitButton('Salvar e finalizar', array('class' => 'btn btn-success span6')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>
<script>
     $('#s').keyup(function(){
            var str = $(this).val();
            $('.lt').each(function(){
                if($(this).text().indexOf(str) !== -1){
                    $(this).slideDown(0);
                } else {
                     $(this).slideUp(0);
                }
        });
    });
    
    function usaLatex(elem,double){
        var mark = double ? '$$' : '$';
        var str = mark + elem.parent().find('.to-add').text() + mark;
        restoreSelection();
        $('#PubliCamada_conteudo').redactor('insert.html', str, false);        
    }
    
    function salvaContinua(elem) {
        $("#edicao-conteudo-form").attr("action", "<?= $this->controller->createUrl('topico/SalvarConteudo'); ?>/r/n").submit();
        elem.html('Salvando...');
    }

    function salvaFinaliza(elem) {
        $("#edicao-conteudo-form").attr("action", "<?= $this->controller->createUrl('topico/SalvarConteudo'); ?>").submit();
        elem.html('Salvando e finalizando...');
    }

    function reutilizaCamadaTemplate(id, reuso) {
        $('#result-reuso-camadas').html('<img src="/aaa/monitor/webroot/monitor/images/lll.gif" style="width: 100%; height: 5px;" />');
        $.ajax({
            method: "POST",
            dataType: "html",
            url: '<?= $this->controller->createUrl('topico/AjaxLoadConteudoCamada') ?>',
            data: {id: id, reuso: reuso},
        }).done(function(data) {
            $('#PubliCamada_conteudo').redactor('insert.html', data, false);
        }).fail(function() {
            alert("error");
        }).always(function() {
            $('#result-reuso-camadas').html('Selecione um tipo, acima...');
        });
    }

    // TEMPLATES DE CÓDIGO

    // Ao selecionar template cria campos para os parametros em #template-params
    function usaTemplate(id, target) {
        var data = {};
        data['tipo'] = id;
        data['params'] = {};
        $('#template-parametros .param').each(function() {
            var id = $(this).attr('data-id');
            var val = $(this).val();
            data['params'][id] = val;
        });
        if (target) {
            $('#' + target).html(JSON.stringify(data));
             $('#PubliCamada_conteudo').redactor('code.sync');
        } else {
            $.ajax({
                method: "POST",
                url: '<?= $this->controller->createUrl('topico/MergeDataTemplate') ?>',
                data: {id: data['tipo'], data: data['params']}
            }).done(function(msg) {
                restoreSelection();
                $('#PubliCamada_conteudo').redactor('insert.html', msg, false);
                // TODO: não reprocessar todas, somente a incluida
                triggetSageInside();
            }).fail(function() {
                alert("error");
            }).always(function() {
                //            alert("complete");
            });
        }
    }
    function triggetSageInside() {
        sagecell.makeSagecell({inputLocation: ".sage-inside", autoeval: true, hide: ["editor", "language", "evalButton", "permalink", "done", "sessionFiles"]});
    }
    function editarModeloEmUso(data, target) {
        data = JSON.parse(data);
        $('#a-topEdit').slideUp(500);
        $('#b-topEdit').slideDown(500);
        $('.b-topEdit').slideUp(0);
        $('#templates-de-codigo').slideDown(500);
        $('#template-params').html('Carregando...');
        // Monta lista de parâmetros com valores em uso na camada
        $.ajax({
            method: "GET",
            url: '<?= $this->controller->createUrl('topico/AjaxTemplateParametros') ?>',
            data: {id: data['tipo'], data: JSON.stringify(data['params']), target: target}
        }).done(function(msg) {
            $('#template-params').html(msg);
        }).fail(function() {
            alert("error");
        }).always(function() {
//            alert("complete");
        });
    }
</script>
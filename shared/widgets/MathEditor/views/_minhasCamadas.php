<?php foreach ($tipos as $t): ?>
    <?php if (in_array($t->categoria, array('sage', 'texto', 'subtopico'))): ?>
    <?php if ($t->de_aplicacao && $deAplicacao): ?>
<button class='btn btn-default btn-xs' onclick="restoreSelection();insertCamada<?= $t->id; ?>();"><?= $t->nome ?></button>
    <?php elseif(!$t->de_aplicacao && $doUsuario): ?>
<button class='btn btn-default btn-xs' onclick="restoreSelection();insertCamada<?= $t->id; ?>();"><?= $t->nome ?></button>
    <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>

<script type="text/javascript">
<?php foreach ($tipos as $t): ?>
        function insertCamada<?= $t->id; ?>() {
    <?php if (in_array($t->categoria, array('texto', 'subtopico'))): ?>
        <?php if ($t->possui_titulo == '1'): ?>
                    var html = '<br><div class="camada" tipo="<?= $t->id ?>"><div class="apendice" tipo="1">Título <?= $t->nome ?></div>Conteúdo <?= $t->nome ?></div><br><br>';
        <?php else: ?>
                    var html = '<br><div class="camada" tipo="<?= $t->id ?>"><?= $t->nome ?></div><br><br>';
        <?php endif; ?>
    <?php elseif ($t->categoria == 'sage'): ?>
                var html = '<br><div class="camada" contenteditable="false" tipo="<?= $t->id ?>">CONTEÚDO INTERATIVO: <?= $t->nome ?></div><br>';
    <?php endif; ?>
            $('#PubliCamada_conteudo').redactor('insert.html', html, false);
        }
<?php endforeach; ?>
</script>
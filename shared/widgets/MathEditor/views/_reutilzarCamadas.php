<?php
foreach ($tipos as $t) {
    $btnClass = $t->de_aplicacao ? 'btn-primary' : 'btn-default';
    echo CHtml::ajaxLink($t->nome,$this->controller->createUrl('topico/ajaxBuscaCamada', array('id' => $t->id)), array(
        'update' => '#result-reuso-camadas',
        'beforeSend' => 'js: function() { $("#result-reuso-camadas").html("<img style=\'width: 100%;height:5px;\' src=\'' . Yii::app()->baseurl . '/webroot/monitor/images/lll.gif' . '\' />") }',
            ), array(
        'class' => 'btn btn-xs ' . $btnClass,
        'id' => $t->id . '-result-reuso-camadas-tipo',
    ));
}
?>
<div class="sh-row">
    <div class="medium-12 column">
        <div id="result-reuso-camadas">
            <span class="hint">Selecione um tipo acima.</span>
        </div>
    </div>
</div>
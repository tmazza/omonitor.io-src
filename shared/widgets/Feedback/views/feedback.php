<button class="feedback-btn feedback-bottom-right" onclick="capture();">Feedback</button>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'modalFeedback',
    'options'=>array(
        'title'=>'Feedback',
        'autoOpen'=>false,
        'width' =>'600'
    ),
));
?>

<p class="flow-text">Tem alguma sugestão ou encontrou algum erro? Deixe o seu Feedback.</p>
<div class="row">
  <div class="col l12 s12">
    <?php echo CHtml::beginForm( Yii::app()->controller->createUrl('site/feedback'),'',array('id'=>'formFeedback')); ?>
      <input type="hidden" name="destino" value="<?=$autor_id;?>">
      <input type="hidden" name="screenschot_src" id="screenschot_src">
      <div class="form-group">
          <input type="text" name="email" placeholder="Digite seu email" class="form-control">
      </div>

      <div class="form-group">
          <textarea cols="5" rows="9" name="mensagem" id="mensagem"><?=$mensagem;?></textarea>
      </div>
      <button type="button" class="btn btn-info" onclick="enviarFeedback();"><i class="fa fa-envelope-o"></i> Enviar</button>
    <?php echo CHtml::endForm(); ?>
  </div>
</div>

<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<script language="JavaScript">
    function enviarFeedback() {
        var form = $("#formFeedback");
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),

            success: function (response) {
                if (response == 1) {
                    alert("Obrigado pelo seu feedback");
                    $("#modalFeedback").dialog("close");
                } else {
                    alert("Algo deu errado, tente novamente!");
                }
            }
        });
    }
</script>

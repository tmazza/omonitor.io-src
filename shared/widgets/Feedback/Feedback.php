<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 18/06/15
 * Time: 22:56
 */
class Feedback extends CWidget {

    public $mensagem;
    public $autor_id;

    public function init() { }

    public function run() {
        $mensagem = $this->mensagem;
        $this->render('feedback',array("autor_id"=>$this->autor_id,"mensagem"=>$mensagem));
    }

}

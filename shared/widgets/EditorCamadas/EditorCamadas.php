<?php

/**
 * Description of EditorCamadas
 *
 * @author tiago
 */
class EditorCamadas extends CWidget {

    // Camada em edicao
    public $model;
    // Se definedLinks está habilitado
    public $enableLinkTopicos = true;
    // Se camadas de aplicação devem ser carregadas
    public $enableCamadas = true;
    // Nome da action definida no controller que está utilizando o widget
    public $baseAction = 'editor.';
    static $baseAlias = 'shared.widgets.EditorCamadas.';
    ///
    private $camadasNaoFavoritas = array();
    private $camadasFavoritas = array();

    public static function actions() {
        return array(
            'loadLinks' => array(
                'class' => self::$baseAlias . 'actions.LoadLinksTopicos',
                'baseAlias' => self::$baseAlias,
            ),
            'salvaCamada' => array(
                'class' => self::$baseAlias . 'actions.SalvaCamada',
                'baseAlias' => self::$baseAlias,
            ),
            'editInteracao' => array(
                'class' => self::$baseAlias . 'actions.EditInteracao',
                'baseAlias' => self::$baseAlias,
            ),
            'editHtml' => array(
                'class' => self::$baseAlias . 'actions.EditHtml',
                'baseAlias' => self::$baseAlias,
            ),
            'normalizaCode' => array(
                'class' => self::$baseAlias . 'actions.AplicaAlteracaoInteracao',
                'baseAlias' => self::$baseAlias,
            ),
            'loadHelp' => array(
                'class' => self::$baseAlias . 'actions.LoadHelp',
                'baseAlias' => self::$baseAlias,
            ),
            'reusoCamadas' => array(
                'class' => self::$baseAlias . 'actions.ReusoCamadas',
                'baseAlias' => self::$baseAlias,
            ),
            'widgets' => array(
                'class' => self::$baseAlias . 'actions.LoadWidgets',
                'baseAlias' => self::$baseAlias,
            ),
            'loadTemplate' => array(
                'class' => self::$baseAlias . 'actions.LoadTemplate',
                'baseAlias' => self::$baseAlias,
            ),
            'mergeTemplate' => array(
                'class' => self::$baseAlias . 'actions.MergeDataTemplate',
                'baseAlias' => self::$baseAlias,
            ),
            'editTemplate' => array(
                'class' => self::$baseAlias . 'actions.EditTemplate',
                'baseAlias' => self::$baseAlias,
            ),
            'normalizaCodeTemplate' => array(
                'class' => self::$baseAlias . 'actions.AplicaAlteracaoTemplate',
                'baseAlias' => self::$baseAlias,
            ),
            'loadPreview' => array(
                'class' => self::$baseAlias . 'actions.LoadPreview',
                'baseAlias' => self::$baseAlias,
            ),
            'loadCamada' => array(
                'class' => self::$baseAlias . 'actions.LoadCamada',
                'baseAlias' => self::$baseAlias,
            ),
            'macros' => array(
                'class' => self::$baseAlias . 'actions.Macros',
                'baseAlias' => self::$baseAlias,
            ),
        );
    }

    public function init() {
        $this->incluiArquivos();
        $this->loadCamadas();
    }

    public function run() {
        $this->render('main', array(
            'model' => $this->model,
            'favoritas' => $this->camadasFavoritas,
            'naoFavoritas' => $this->camadasNaoFavoritas,
        ));
    }

    private function incluiArquivos() {
        $assetsPath = Yii::app()->assetManager->publish(__DIR__ . '/assets', false, -1, true);
        Yii::app()->clientScript->registerScriptFile($assetsPath . '/main.js');
        Yii::app()->clientScript->registerCssFile($assetsPath . '/main.css');
    }

    private function loadCamadas() {
        if ($this->enableCamadas) {
            $tipos = CamadaTipo::model()->findAll(array(
                'condition' => "(de_aplicacao = 1 AND  visibilidade_edicao = " . CamadaTipo::VisiEditAplicacao . ") "
                . "OR (user_id = '" . Yii::app()->user->id . "' AND  visibilidade_edicao = " . CamadaTipo::VisiEditPessoal . ")",
                'order' => 'de_aplicacao DESC',
            ));
            $tiposFavoritos = CamadaTipoFavorita::model()->doAutor()->findAll();
            $favoritos = CHtml::listData($tiposFavoritos, 'tipo_id', 'tipo_id');
            foreach ($tipos as $t) {
                if (in_array($t->id, $favoritos)) {
                    $this->camadasFavoritas[] = $t;
                } else {
                    $this->camadasNaoFavoritas[] = $t;
                }
            }
        }
    }

}

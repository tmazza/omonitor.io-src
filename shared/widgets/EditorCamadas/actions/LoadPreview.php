<?php

/**
 * Description of EditInteracao
 *
 * @author tiago
 */
class LoadPreview extends CAction {

    public $baseAlias;

    public function run() {
        if (isset($_POST['code'])) {
            $code = $_POST['code'];
            if (isset($code['tipo']) && isset($code['params'])) {
                $widget = Widget::model()->findByPk($code['tipo']);
                $data = $code['params'];
                echo $this->controller->renderPartial($this->baseAlias . 'views.____loadPreview', array(
                    'template' => $widget,
                    'data' => $data,
                        ), true, true);
            }
        }
    }

}

<?php

/**
 * Description of EditInteracao
 *
 * @author tiago
 */
class EditHtml extends CAction {

    public $baseAlias;

    public function run() {
        if (isset($_POST['code']) && isset($_POST['id'])) {
            $this->controller->renderPartial($this->baseAlias . 'views.___editHtml', array(
                'id' => $_POST['id'],
                'code' => base64_decode($_POST['code']),
                    ), false, true);
        } else {
            echo 'Interação não encontrada para edição.';
        }
    }

}

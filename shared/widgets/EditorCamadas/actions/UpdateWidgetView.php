<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NormalizaTemplateData
 *
 * @author tiago
 */
class UpdateWidgetView extends CAction {

    public function run() {
        if (isset($_POST['str'])) {
            $template = $_POST['str'];
            $content = ShView::normalizaTemplateData($template, $_POST);
            $this->controller->widget('shared.widgets.Widget.WidgetView', array(
                'str' => $template,
                'data' => $content,
                'showParametros' => true,
            ));
        }
    }

}

<?php

/**
 * Description of LoadWidgets
 *
 * @author tiago
 */
class LoadTemplate extends CAction {

    public $baseAlias;

    /**
     * Retorna wigets de aplicação e do usuário
     */
    public function run($id = null, $data = null, $target = null) {
        $template = Widget::model()->findByPk($id);
        $data = is_null($data) ? array() : json_decode($data, true);
        echo $this->controller->renderPartial($this->baseAlias . 'views.____templateParametros', array(
            'template' => $template,
            'data' => $data,
            'target' => $target,
            'nova' => true,
                ), true, true);
    }

}

<?php

/**
 * Description of EditInteracao
 *
 * @author tiago
 */
class Macros extends CAction {

    public $baseAlias;

    public function run($rq = 'list') {
        if ($rq == 'new') {
            echo $this->handleCreate();
        } elseif ($rq == 'edit') {
            echo 'Edição na lista de tópicos';
        } else {
            echo $this->handleListMacros();
        }
    }

    private function handleCreate() {
        $model = new LatexMacro;
        $tipos = $model->getMacros();

        if (isset($_POST['LatexMacro'])) {
            $model->attributes = $_POST['LatexMacro'];
            $model->user_id = Yii::app()->user->id;

            if ($model->save()){
                return '<h3>Macro criada</h3>' . $this->handleListMacros();
            }
        }
        echo $this->controller->renderPartial($this->baseAlias . 'views.____formMacros', array(
            'model' => $model,
            'tipos' => $tipos
                ), true, true);
    }

    private function handleListMacros() {
        $macros = LatexMacro::model()->doAutor()->findAll(array('order'=>'tipo'));
        return $this->controller->renderPartial($this->baseAlias . 'views.___listMacros', array(
                    'macros' => $macros,
                        ), true, true);
    }

//    /**
//     * Returns the data model based on the primary key given in the GET variable.
//     * If the data model is not found, an HTTP exception will be raised.
//     * @param integer $id the ID of the model to be loaded
//     * @return LatexMacro the loaded model
//     * @throws CHttpException
//     */
//    private function loadModel($id) {
//        $model = LatexMacro::model()->doAutor()->findByPk($id);
//        if ($model === null)
//            throw new CHttpException(404, 'The requested page does not exist.');
//        return $model;
//    }

}

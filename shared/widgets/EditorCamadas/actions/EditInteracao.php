<?php

/**
 * Description of EditInteracao
 *
 * @author tiago
 */
class EditInteracao extends CAction {

    public $baseAlias;

    public function run() {
        if (isset($_POST['code']) && isset($_POST['id']) && isset($_POST['type'])) {
            if($_POST['type'] == 'sageR'){
                $type = 'r';
            } elseif($_POST['type'] == 'sageSingular'){
                $type = 'singular';
            } else {
                $type = 'sage';
            }
            
            $this->controller->renderPartial($this->baseAlias . 'views.___editInteracao', array(
                'id' => $_POST['id'],
                'type' => $type,
                'code' => base64_decode($_POST['code']),
                    ), false, true);
        } else {
            echo 'Interação não encontrada para edição.';
        }
    }

}

<?php

/**
 * Description of LoadHelp
 *
 * @author tiago
 */
class LoadHelp extends CAction {

    public $baseAlias;

    /**
     * Retorna links para outros tópicos.
     * Utilizado pelo editor para mostrar tópicos como obção de link.
     */
    public function run($tipo = 'latex') {
        if ($tipo == 'reuso') {
            $this->controller->renderPartial($this->baseAlias . 'views.___helpReuso');
        } else {
            $this->controller->renderPartial($this->baseAlias . 'views.___helpLatex');
        }
    }

}

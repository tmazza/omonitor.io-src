<?php

/**
 * Description of LoadWidgets
 *
 * @author tiago
 */
class LoadWidgets extends CAction {

    public $baseAlias;

    /**
     * Retorna wigets de aplicação e do usuário
     */
    public function run() {
        $categorias = WidgetCategoria::model()->findAll("categoria_pai IS NULL");
        echo $this->controller->renderPartial($this->baseAlias . 'views.___widgets', array(
            'categorias' => $categorias,
                ), true, true);
    }
    
}

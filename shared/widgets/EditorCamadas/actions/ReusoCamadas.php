<?php

/**
 * Description of LoadHelp
 *
 * @author tiago
 */
class ReusoCamadas extends CAction {

    public $baseAlias;

    /**
     * Retorna links para outros tópicos.
     * Utilizado pelo editor para mostrar tópicos como obção de link.
     */
    public function run($id) {
        $tipo = CamadaTipo::model()->comAcesso()->findByPk($id);
        if (isset($_POST['term'])) {
            echo $this->controller->renderPartial($this->baseAlias . 'views.____bucasCamadas', array(
                'results' => $this->getCamadas($tipo),
                    ), true);
        } else {
            echo $this->controller->renderPartial($this->baseAlias . 'views.___selecionarCamada', array(
                'tipo' => $tipo,
                'results' => array(),
                    ), true, true);
        }
    }

    private function getCamadas($tipo) {
        if (isset($_POST['term'])) {
            // TODO: sql injection
            $data = Camada::model()->doAutor()->findAll(array(
                'condition' => 'tipo_id = ' . $tipo->id . ' AND conteudo LIKE "%' . $_POST['term'] . '%" AND excluido = 0',
                'limit' => 5,
            ));
        } else {
            $data = array();
        }
        return $data;
    }

}

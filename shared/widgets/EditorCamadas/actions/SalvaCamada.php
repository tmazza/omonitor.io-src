<?php

/**
 * Description of SalvaCamada
 *
 * @author tiago
 */
class SalvaCamada extends CAction {

    public $baseAlias;

    /**
     * Salva conteúdo editado de tópico
     */
    public function run($s = false) {
        if (isset($_POST['PubliCamada']['id']) && isset($_POST['PubliCamada']['conteudo'])) {
            $camadaBase = PubliCamada::model()
                    ->doAutor()
                    ->findByPk($_POST['PubliCamada']['id']);

            $camadaBase->conteudo = $_POST['PubliCamada']['conteudo'];
            // Save de camada analisa conteúdo separando subcamadas
            if (!$camadaBase->save()) {
                throw new Exception("Erro ao salvar camada, entre em contato com o suporte.");
            } else {
                Yii::app()->user->setFlash(EditorController::SUCS_FLASH, 'Conteúdo de "' . $camadaBase->topico->nome . '" salvo.');
                $camadaBase->topico->update(array('lastUpdate')); // Aciona behaviour

                if ($s) {
                    $this->controller->redirect($this->controller->createUrl('edicao/index', array('id' => $camadaBase->topico->id)));
                } else {
                    $this->controller->redirect($this->controller->createUrl('topico/listar', array('f' => $camadaBase->topico->id)));
                }
            }
        } else {
            Yii::app()->user->setFlash(EditorController::ERRO_FLASH, 'Requisição inválida.');
            $this->controller->redirect($this->controller->createUrl('topico/listar'));
        }
    }

}

<?php

/**
 * Description of TesteA
 *
 * @author tiago
 */
class LoadLinksTopicos extends CAction {

    public $baseAlias;

    /**
     * Retorna links para outros tópicos.
     * Utilizado pelo editor para mostrar tópicos como obção de link.
     */
    public function run() {
        $topicos = Topico::model()
                ->doAutor()
                ->findAll();
        $links = array();
        $links[] = array('name' => 'Tópicos...', 'url' => false);
        foreach ($topicos as $top) {
            $links[] = array(
                'name' => $top->nome,
                'url' => "{link-topico:{$top->id}}",
            );
        }
        echo json_encode($links);
    }

}

<?php

/**
 * Description of LoadCamada
 *
 * @author tiago
 */
class LoadCamada extends CAction {

    public $baseAlias;

    public function run() {
        if (isset($_POST['id']) && isset($_POST['reuso'])) {
            $camada = PubliCamada::model()->doAutor()->findByPk($_POST['id'], array('condition' => 'excluido = 0'));
            if (is_null($camada)) {
                echo '';
            } else {
                $reuso = (int) $_POST['reuso'];
                $attrReuso = $reuso ? 'data-reuso=true' : '';
                if ($camada->tipo->categoria == 'sage') {
                    echo Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.sage', array(
                        'attrReuso' => $attrReuso,
                        'modelCamada' => $camada,
                        'attrId' => $reuso,
                            ), true);
                } elseif ($camada->tipo->categoria == 'template') {
                    $data = Camada::getJSON($camada->conteudo);
                    echo Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.template', array(
                        'modelCamada' => $camada,
                        'widget' => Widget::model()->findByPk($data['tipo']),
                        'data' => $data,
                        'attrId' => $reuso,
                            ), true);
                } else {
                    echo Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.default', array(
                        'attrReuso' => $attrReuso,
                        'camada' => $camada,
                        'thisModel' => $camada,
                        'attrId' => $reuso,
                            ), true);
                }
            }
        } else {
            return '';
        }
    }

}

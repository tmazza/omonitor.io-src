<?php

/**
 * Description of EditInteracao
 *
 * @author tiago
 */
class EditTemplate extends CAction {

    public $baseAlias;

    public function run() {
        if (isset($_POST['code']) && isset($_POST['id'])) {
            $data = Camada::getJSON(base64_decode($_POST['code']));
            $widget = Widget::model()->findByPk($data['tipo']);

            if (is_null($widget)) {
                echo 'Widget não encontrado.';
            } else {

                // $params = array();
                // parse_str($data['params'],$params);
                $realData = array();
                foreach ($data['params'] as $key => $value) {
                  $realData[$key] = array('valor' => $value);
                }

                echo $this->controller->renderPartial($this->baseAlias . 'views.____templateParametros', array(
                    'id' => $_POST['id'],
                    'template' => $widget,
                    'data' => $realData,
                    'nova' => false,
                        ), true, true);
            }
        } else {
            echo 'Widget não encontrado.';
        }
    }

}

<?php

/**
 * Description of MergeTemplate
 *
 * @author tiago
 */
class MergeDataTemplate extends CAction {

    public $baseAlias;

    /**
     * Salva conteúdo editado de tópico
     */
    public function run() {
        if (isset($_POST['id']) && isset($_POST['data'])) {

            $camadaTipo = PubliCamadaTipo::model()->findByAttributes(array('categoria' => 'template'));
            $id = $_POST['id'];
            $data = array();
            parse_str($_POST['data'],$data);
            $template = Widget::model()->findByPk($id);

            $code = array(
                'tipo' => $template->id,
                'params' => $data,
            );

            $content = '<br>';
            $content .= '<div class="camada" dataType="template" onclick="edicaoTemplate(event,$(this))" dataCode="' . base64_encode(json_encode($code)) . '" dataId="nova-' . hash('md5', microtime(true)) . '" contenteditable="false" tipo="' . $camadaTipo->id . '">';
            $content .= ucfirst($template->nome) . '(Clique para editar)';
            $content .= '</div><br>';

            echo $content;
        } else {
            echo '<pre>';
            print_r($_POST);
            exit;
        }
    }

}

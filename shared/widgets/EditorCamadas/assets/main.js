var copyData;
function enterCallback(e) {
    e.preventDefault(); //Prevent default browser behavior
    if (window.getSelection) {
        var selection = window.getSelection(),
                range = selection.getRangeAt(0),
                br = document.createElement("br"),
                textNode = document.createTextNode($("<div>&nbsp;</div>").text()); //Passing " " directly will not end up being shown correctly
        range.deleteContents();//required or not?
        range.insertNode(br);
        range.collapse(false);
        range.insertNode(textNode);
        range.selectNodeContents(textNode);

        selection.removeAllRanges();
        selection.addRange(range);
        return false;
    }
}
function toggleTopEdit() {
    $('#a-topEdit').slideToggle(500);
    $('#b-topEdit').slideToggle(500);
}
function openTopEdit(id) {
    $('.b-topEdit').slideUp(0);
    $('#' + id).slideDown(0);
    toggleTopEdit();
}
function filtraCamadas() {
    $('#reuso .camada').each(function() {
        var n = $(this).text().toLowerCase();
        if (n.indexOf($('#s').val().toLowerCase()) === -1) {
            $(this).parent().parent().fadeOut();
        } else {
            $(this).parent().parent().fadeIn();
        }
    });
}
function saveSelection() {
    $('#redactor-editor').redactor('selection.save');
}

function restoreSelection() {
    $('#redactor-editor').redactor('selection.restore');
}
function reutilizarCamada(id) {
    reutilizaCamadaTemplate(id, true);
}
function duplicarCamada(id) {
    reutilizaCamadaTemplate(id, false);
}

function subHeader(id) {
//    $('.sub-header').slideUp();
    $('#' + id).slideToggle();
}

$('#redactor-editor').mouseleave(function() {
    alert('asd');
});


function cutContent(event, elem) {
    event.stopPropagation();
    copyData = elem.clone();
    elem.remove();
    $('.btn-paste').fadeIn();
}

function pasteContent() {
    $('#redactor-editor').redactor('insert.node', copyData);
    $('#redactor-editor').redactor('code.sync');
    copyData = null;
    $('.btn-paste').fadeOut();
}

function lineUp(event, elem) {
    event.stopPropagation();
    elem.before("<br>");
}
function lineDown(event, elem) {
    event.stopPropagation();
    elem.after("<br>");
}
function removeCamada(event, elem) {
    event.stopPropagation();
    if (confirm("Remover camadas e subcamadas?")) {
        elem.remove();
        $('#redactor-editor').redactor('code.sync');
    }
    

}

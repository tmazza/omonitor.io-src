<?php
$options = array();
if ($this->enableLinkTopicos) {
    $options['definedLinks'] = $this->controller->createUrl($this->baseAction . 'loadLinks');
}


$this->widget('ImperaviRedactorWidget', array(
    'model' => $model,
    'attribute' => 'conteudo',
    'options' => array(
'lang' => 'pt_br',
 'plugins' => array(
    'bufferbuttons',
    'definedlinks',
    'video',
    'advanced',
),
 'replaceDivs' => false,
 'minHeight' => 300,
 'removeEmpty' => array('p'),
 'cleanStyleOnEnter' => false,
 'linebreaks' => true,
 'pastePlainText' => true,
 'cleanOnPaste' => true,
 'focus' => true,
 'source' => false,
 'toolbarExternal' => '#edicao-redactor',
 //
'startCallback' => 'js: function() { triggetSageInside(); } ',
 'enterCallback' => 'js:  function(e) { return enterCallback(e); } ',
 // TODO: não ser no callback, mas sim no template de edição
//        'initCallback' => 'js: function() { $(".camada").prepend("<div class=\"ignore inline right\"><button type=\"button\" class=\"fecha-camada btn btn-xs btn-danger\" onclick=\'removerEstaCamada($(this));\'>x</button></div>"); }',
'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
 'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
//                'clickCallback' => 'js: function() { $(".camada[tipo=\'18\']").attr("contenteditable", "false") }',
    ) + $options,
    'htmlOptions' => array(
        'id' => 'redactor-editor',
        'style' => 'width: 100%!important;',
    ),
));
?>
<script>
    $('#s').keyup(function() {
        var str = $(this).val();
        $('.lt').each(function() {
            if ($(this).text().indexOf(str) !== -1) {
                $(this).slideDown(0);
            } else {
                $(this).slideUp(0);
            }
        });
    });

    function usaLatex(elem, double) {
        var mark = double ? '$$' : '$';
        var str = mark + elem.parent().find('.to-add').text() + mark;
        restoreSelection();
        $('#redactor-editor').redactor('insert.html', str, false);
    }

    function salvaContinua(elem) {
        $("#edicao-conteudo-form").attr("action", "<?= $this->controller->createUrl($this->baseAction . 'salvaCamada', array('s' => true)); ?>/r/n").submit();
        elem.html('Salvando...');
    }

    function salvaFinaliza(elem) {
        $("#edicao-conteudo-form").attr("action", "<?= $this->controller->createUrl($this->baseAction . 'salvaCamada'); ?>").submit();
        elem.html('Salvando e finalizando...');
    }

    function reutilizaCamadaTemplate(id, reuso) {
        $('#result-reuso-camadas').html('<img src="/aaa/monitor/webroot/monitor/images/lll.gif" style="width: 100%; height: 5px;" />');
        $.ajax({
            method: "POST",
            dataType: "html",
            url: '<?= $this->controller->createUrl('editor.loadCamada') ?>',
            data: {id: id, reuso: reuso},
        }).done(function(html) {
            restoreSelection();
            $('#redactor-editor').redactor('insert.html', html, false);
            $('#redactor-editor').redactor('code.sync');
            $('#modal-edicao').dialog("close");
            console.log(html);
        }).fail(function() {
            alert("error");
        }).always(function() {
            $('#result-reuso-camadas').html('Selecione um tipo, acima...');
        });
    }

    // TEMPLATES DE CÓDIGO

    // Ao selecionar template cria campos para os parametros em #template-params
    function usaTemplate(id, target) {
        data = getParams(id); 
        if (target) {
            $('#' + target).html(JSON.stringify(data));
            $('#redactor-editor').redactor('code.sync');
        } else {
            $.ajax({
                method: "POST",
                url: '<?= $this->controller->createUrl($this->baseAction . 'mergeTemplate') ?>',
                data: {id: data['tipo'], data: data['params']}
            }).done(function(msg) {
                $('#modal-edicao').dialog("close");
                restoreSelection();
                $('#redactor-editor').redactor('insert.html', msg, false);
            }).fail(function() {
                alert("error");
            }).always(function() {
                //            alert("complete");
            });
        }
    }
    function triggetSageInside() {
        sagecell.makeSagecell({inputLocation: ".sage-inside", autoeval: true, hide: ["editor", "language", "evalButton", "permalink", "done", "sessionFiles"]});
    }
    function editarModeloEmUso(data, target) {
        data = JSON.parse(data);
        $('#a-topEdit').slideUp(500);
        $('#b-topEdit').slideDown(500);
        $('.b-topEdit').slideUp(0);
        $('#templates-de-codigo').slideDown(500);
        $('#template-params').html('Carregando...');
        // Monta lista de parâmetros com valores em uso na camada
        $.ajax({
            method: "GET",
            url: '<?= $this->controller->createUrl('topico/AjaxTemplateParametros') ?>',
            data: {id: data['tipo'], data: JSON.stringify(data['params']), target: target}
        }).done(function(msg) {
            $('#template-params').html(msg);
        }).fail(function() {
            alert("error");
        }).always(function() {
//            alert("complete");
        });
    }
</script>

<br>
<p class="textLeft">
    <button class="btn btn-success" type="button" onclick="aplicaAlteracao('<?= $id ?>', $('#sage-edit-<?= $id ?>').val())">
        Aplicar
    </button>
</p>
<hr>

<script>
    sagecell.makeSagecell({
        inputLocation: "#sage-edit-<?= $id ?>",
        evalButtonText: "Processar",
        hide: ["permalink"],
        languages: ["<?= $type; ?>"]
    });
</script>
<div class="edicao-sage">
    <h4 class="textCenter"><?= ucfirst($type); ?></h4>
    <textarea id="sage-edit-<?= $id ?>">
        <?php echo stripslashes($code); ?>
    </textarea>
</div>
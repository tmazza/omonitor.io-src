<?php if (count($categorias) > 0): ?>
    <ul>
        <?php foreach ($categorias as $c): ?>
            <li><?= $c->label; ?>
                <?php
                $this->renderPartial('shared.widgets.EditorCamadas.views.____renderTree', array(
                    'categorias' => $c->categorias,
                ));
                ?>
                <?php if (count($c->widgets) > 0): ?>
                    <ul class="nav">
                        <?php
                        foreach ($c->widgets as $w) {
                            echo '<li>';
                            echo CHtml::ajaxLink($w->nome, $this->createUrl('editor.loadTemplate', array(
                                        'id' => $w->id,
                                    )), array(
                                'update' => '#template-code',
                                    ), array(
                                'id' => 'open-template-' . hash('md5', microtime(true))
//                                'onClick' => 'return false;',
                            ));
                            echo '</li>';
                        }
                        ?>
                    </ul>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

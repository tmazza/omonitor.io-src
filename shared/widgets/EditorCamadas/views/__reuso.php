<div class="sh-row">
    <div class="columns medium-12 sup-reuso" style="display: none;">
        <div class="sh-row">
            <div class="columns medium-12" id="reuso-camadas">
                <?php
                echo CHtml::ajaxLink('O que é isso <i class="fa fa-question-circle"></i>', $this->controller->createUrl($this->baseAction . 'loadHelp', array('tipo' => 'reuso')), array(
                    'success' => 'js: function(html) { '
                    . '$("#modal-edicao").html(html).dialog("open");'
                    . ' }',
                    'beforeSend' => 'js: function() {'
                    . '$("#modal-edicao").html("<div class=\'textCenter\'><img src=\'' . $this->controller->assetsPath . '/img/loading.gif\'></div>");'
                    . '$("#modal-edicao").dialog({title: "Reaproveitamento de camadas"}).dialog("open");'
                    . '}'
                        ), array(
                    'class' => 'btn btn-xs btn-info',
                    'id' => 'reuso-help',
                ))
                ?>
                <!--<h3 class="hint">Aqui estão todas as camadas que você já criou neste e em outros tópicos</h3>-->

            </div>
        </div>
        <hr>
        <div class="sh-row">
            <div class="columns medium-12 ">
                <?php
                foreach ($favoritas as $f) {
                    echo CHtml::ajaxLink($f->nome, $this->controller->createUrl($this->baseAction . 'reusoCamadas', array(
                                'id' => $f->id,
                            )), array(
                        'beforeSend' => 'js: function(){'
                        . 'saveSelection();'
                        . '$("#modal-edicao").dialog("option", {"title":"Reutilizar ' . $f->nome . '"});'
                        . '$("#modal-edicao").html("<i class=\'fa f-refresh fa-spin\'></i>").dialog("open");'
                        . '}',
                        'success' => 'js: function(html) {'
                        . '$("#modal-edicao").html(html);'
                        . '}',
                            ), array(
                        'class' => 'btn btn-sm btn-primary'
                    ));
                }
                ?>
                <?php
                foreach ($naoFavoritas as $f) {
                    echo CHtml::ajaxLink($f->nome, $this->controller->createUrl($this->baseAction . 'reusoCamadas', array(
                                'id' => $f->id,
                            )), array(
                        'beforeSend' => 'js: function(){'
                        . 'saveSelection();'
                        . '$("#modal-edicao").dialog("option", {"title":"Reutilizar ' . $f->nome . '"});'
                        . '$("#modal-edicao").html("<i class=\'fa f-refresh fa-spin\'></i>").dialog("open");'
                        . '}',
                        'success' => 'js: function(html) {'
                        . '$("#modal-edicao").html(html);'
                        . '}',
                            ), array(
                        'class' => 'btn btn-sm btn-default'
                    ));
                }
                ?>
            </div>
        </div>
        <hr>
        <br>
    </div>
</div>

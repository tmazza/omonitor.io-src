<div class="sh-row">
    <div class="column medium-12">
        <div class="sh-row">
            <div class="column medium-4">
                <?php $nivel2 = array(); ?>
                <?php if (count($categorias) > 0): ?>
                    <ul class="nav">
                        <?php foreach ($categorias as $c): ?>
                            <?php $nivel2[$c->id] = $c->categorias; ?>
                            <li>
                                <?=
                                CHtml::link($c->label . ' <div class="right">&raquo;</div>', '#', array(
                                    'onClick' => "cliqueNivel1({$c->id}); return false;"
                                ));
                                ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="column medium-4">
                <?php $nivel3 = array(); ?>
                <?php foreach ($nivel2 as $id => $categorias): ?>
                    <?php if (count($categorias) > 0): ?>
                        <ul class="nav nivel2" id="cat<?= $id ?>" style="display: none;">
                            <?php foreach ($categorias as $c): ?>
                                <?php $nivel3[$c->id] = $c->widgets; ?>
                                <li>
                                    <?=
                                    CHtml::link($c->label . ' <div class="right">&raquo;</div>', '#', array(
                                        'onClick' => "cliqueNivel2({$c->id}); return false;"
                                    ));
                                    ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="column medium-4">
                <?php foreach ($nivel3 as $id => $widgets): ?>
                    <?php if (count($categorias) > 0): ?>
                        <ul class="nav nivel3" id="subcat<?= $id ?>" style="display: none;">
                            <?php foreach ($widgets as $w): ?>
                                <li>
                                    <?php
                                    echo CHtml::ajaxLink($w->nome, $this->createUrl('editor.loadTemplate', array(
                                                'id' => $w->id,
                                            )), array(
                                        'beforeSend' => 'js: function(){'
                                        . '$("#template-code").html("<img src=\'' . $this->assetsPath . '/img/loading.gif' . '\'/>")'
                                        . '}',
                                        'success' => 'js: function(html){'
                                        . '$("#template-code").html(html);'
                                        . '}',
                                            ), array(
                                        'id' => 'open-template' . $w->id,
                                    ));
                                    ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</div>
<hr>
<div class="sh-row">
    <div class="column medium-12" id="template-code">
    </div>
</div>
<script>
    function cliqueNivel1(id) {
        $('.nivel2,.nivel3').hide(0);
//        $('.nivel3').first().fadeIn();
        $('#cat' + id).fadeIn();
    }
    function cliqueNivel2(id) {
        $('.nivel3').hide(0);
//        $('.nivel3').first().fadeIn();
        $('#subcat' + id).fadeIn();
    }
</script>
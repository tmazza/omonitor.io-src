<div class="sh-row">
    <div class="medium-12 column no-mathjax">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edicao-conteudo-form',
            'action' => $this->controller->createUrl($this->baseAction . 'salvaCamada'),
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));

        echo $form->hiddenField($model, 'id');

        $this->render('__redactor', array(
            'model' => $model,
        ));

        echo $form->error($model, 'conteudo');

//        echo CHtml::submitButton('Salvar e finalizar', array('class' => 'btn btn-success span6'));

        $this->endWidget();
        ?>
    </div>
</div>
<script>
    function edicaoInt(elem) {
        code = elem.attr('dataCode');
        id = elem.attr('dataId');
        type = elem.attr('dataType');
        $.ajax({
            method: "POST",
            url: "<?= $this->controller->createUrl($this->baseAction . 'editInteracao'); ?>",
            data: {code: code, id: id, type: type},
            beforeSend: function() {
                $('#modal-edicao').html('<div class="textCenter"><img src="<?= $this->controller->assetsPath; ?>/img/loading.gif"></div>');
                $('#modal-edicao').dialog({title: 'Edição de interação'}).dialog('open');
            }
        }).done(function(html) {
            $('#modal-edicao').html(html);
        }).fail(function() {
            alert("Erro ao tentar editar camada. Salve seu tópico e atualize a página.");
        });
    }
    function edicaoHtml(elem) {
        code = elem.attr('dataCode');
        id = elem.attr('dataId');
        type = elem.attr('dataType');
        $.ajax({
            method: "POST",
            url: "<?= $this->controller->createUrl($this->baseAction . 'editHtml'); ?>",
            data: {code: code, id: id, type: type},
            beforeSend: function() {
                $('#modal-edicao').html('<div class="textCenter"><img src="<?= $this->controller->assetsPath; ?>/img/loading.gif"></div>');
                $('#modal-edicao').dialog({title: 'Edição de interação'}).dialog('open');
            }
        }).done(function(html) {
            $('#modal-edicao').html(html);
        }).fail(function() {
            alert("Erro ao tentar editar camada. Salve seu tópico e atualize a página.");
        });
    }
    function aplicaAlteracao(id, code) {
        $.ajax({
            method: "POST",
            url: "<?= $this->controller->createUrl($this->baseAction . 'normalizaCode'); ?>",
            data: {code: code},
            beforeSend: function() {
                $('#modal-edicao').html('<div class="textCenter"><img src="<?= $this->controller->assetsPath; ?>/img/loading.gif"></div>');
            }
        }).done(function(code) {
            $('[dataId=' + id + ']').attr('dataCode', code);
            $('#modal-edicao').dialog('close');
            $('#redactor-editor').redactor('code.sync');
        }).fail(function() {
            alert("Erro ao tentar editar camada. Salve seu tópico e atualize a página.");
        });
    }
    function edicaoTemplate(event,elem) {

        code = elem.attr('dataCode');
        id = elem.attr('dataId');
        $.ajax({
            method: "POST",
            url: "<?= $this->controller->createUrl($this->baseAction . 'editTemplate'); ?>",
            data: {code: code, id: id},
            beforeSend: function() {
                $('#modal-edicao').html('<div class="textCenter"><img src="<?= $this->controller->assetsPath; ?>/img/loading.gif"></div>');
                $('#modal-edicao').dialog({title: 'Edição de widget'}).dialog('open');
            }
        }).done(function(html) {
            $('#modal-edicao').html(html);
        }).fail(function() {
            alert("Erro ao tentar editar widget. Salve seu tópico e atualize a página.");
        });
    }
    function aplicaAlteracaoTemplate(id, tipo) {
        data = getParams(tipo);
        $.ajax({
            method: "POST",
            url: "<?= $this->controller->createUrl($this->baseAction . 'normalizaCodeTemplate'); ?>",
            data: {data: data},
            beforeSend: function() {
                $('#modal-edicao').html('<div class="textCenter"><img src="<?= $this->controller->assetsPath; ?>/img/loading.gif"></div>');
            }
        }).done(function(code) {
            $('[dataId=' + id + ']').attr('dataCode', code);
            $('#modal-edicao').dialog('close');
            $('#redactor-editor').redactor('code.sync');
        }).fail(function() {
            alert("Erro ao tentar editar camada. Salve seu tópico e atualize a página.");
        });
    }

    function getParams(tipo) {

        var data = {};
        data['tipo'] = tipo;
        data['params'] =  $('#template-parametros #widget-view form').serialize();
        return data;
    }

</script>

<div class="sh-row">
    <div class="column medium-12">
        <p>
            Aqui você tem todas as camadas que já foram criadas neste e em outros tópicos.
            <br>
            <br>
            Você pode <b class="label label-primary">reutilizar</b> ou <b class="label label-default">duplicar</b> uma camada.
            <br>
            <br>
            A diferença entre essas duas modalidades é a centralização, ou não, da edição do conteúdo da camada.
            <br>
            <br>
            <b>Reuso</b>: a alteração em uma das cópias é replicada em todas as demais.
            <br>
            <b>Duplicar</b>: a alteração em uma das cópias NÃO é replicada nas demais.
            <br>
        </p>
    </div>
</div>
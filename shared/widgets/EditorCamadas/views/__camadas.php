<?php foreach ($tipos as $t): ?>
    <button class='btn btn-default btn-xs' onclick="insertCamada<?= $t->id; ?>();">
        <?= $t->nome ?>
    </button>
<?php endforeach; ?>

<script type="text/javascript">
    var qtdNovas = 0;
<?php foreach ($tipos as $t): ?>
        function insertCamada<?= $t->id; ?>() {
            qtdNovas++;
            restoreSelection();
            html = '';
    <?php if (in_array($t->categoria, array('texto', 'subtopico','html'))): ?>
        <?php if ($t->possui_titulo == '1'): ?>
                    var html = '<br><div class="camada" tipo="<?= $t->id ?>"><div class="apendice" tipo="1">Título <?= $t->nome ?></div>Conteúdo <?= $t->nome ?></div><br><br>';
        <?php else: ?>
                    var html = '<br><div class="camada" tipo="<?= $t->id ?>"><?= $t->nome ?></div><br><br>';
        <?php endif; ?>
    <?php elseif ($t->categoria == 'sage'): ?>
        var html = '<br><div class="camada" onclick="edicaoInt($(this))" dataType="<?= $t->view_name; ?>" dataCode="" dataId="nova-' + qtdNovas + '" contenteditable="false" tipo="<?= $t->id ?>">';
        html += '<?= ucfirst($t->nome) ?> (Clique para editar)';
        html += '</div><br>';
    <?php endif; ?>
             $('#redactor-editor').redactor('insert.html', html, false);
        }
<?php endforeach; ?>
</script>
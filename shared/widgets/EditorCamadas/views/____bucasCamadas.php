<div  id="reuso-results" style="max-height: 200px; overflow: hidden; overflow-y: auto; width: 100%;">
    <table class="table">
        <?php if (count($results) > 0): ?>
            <?php foreach ($results as $r): ?>
                <tr>
                    <td>
                        <?php $str = strip_tags($r->expandeCamadasVisualizacao($r->conteudo)); ?>
                        <?= substr($str, 0, 60) . (strlen($str) > 60 ? '...' : '') ?>
                    </td>
                    <td>
                        <button class="btn btn-primary btn-sm" onclick="reutilizaCamadaTemplate('<?= $r->id ?>', 1)">Reutilizar</button>
                        <button class="btn btn-default btn-sm" onclick="reutilizaCamadaTemplate('<?= $r->id ?>', 0)">Duplicar</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
                <p class="hint">
                    Nenhum resultado.
                </p>
        <?php endif; ?>
    </table>
</div>
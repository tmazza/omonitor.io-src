    
<script>
    sagecell.makeSagecell({
        inputLocation: "#sage-template-<?= $template->id ?>",
        evalButtonText: "Processar",
        autoeval: true,
        hide: ["permalink", "editor", "evalButton"],
    });
</script>
<div class="edicao-sage">
    <textarea id="sage-template-<?= $template->id ?>">
        <?php echo stripslashes(ShView::mergeDataToTemplate($template->code, $data)); ?>
    </textarea>
</div>
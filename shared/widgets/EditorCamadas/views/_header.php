<div id="editor-header">
    <!--Camada não favoritas-->
    <div class="sh-row sub-header" id="nao-favoritas">
        <div class="columns medium-12">
            <?php
            $this->render('__camadas', array(
                'tipos' => $naoFavoritas,
            ));
            ?>
            <br>
            <br>
        </div>
    </div>
    <!--Barra de ediçaõ principal-->
    <div class="sh-row">
        <div class="columns medium-5" style="padding-top: 5px;">
            <?php
            $this->render('__camadas', array(
                'tipos' => $favoritas,
            ));
            ?>
            <button class="btn btn-primary btn-xs" onclick="subHeader('nao-favoritas');">+</button>
        </div>
        <div class="columns medium-2">
            <?php
//            $this->widget('shared.widgets.Teclado.ViewTeclado', array(
//                'inputID' => 'redactor-editor',
//                'template' => 'template4',
//                'tecladoID' => Teclado::Arquimedes,
//                'script' => '_edicaoCamadas',
//            ));
            ?>
        </div>
        <div class="columns medium-5" id="edicao-redactor">            
        </div>
    </div>
</div>
<br><br>
<?php
$this->widget('shared.widgets.StickyContent.StickyContent', array(
    'contentID' => 'editor-header',
    'top' => 0,
));
?>
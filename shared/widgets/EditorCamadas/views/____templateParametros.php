<?php $params = ShView::extraiParametros($template->code); ?>
<div class="sh-row">
    <div class="column medium-12">
        <h4><?= ucfirst($template->nome); ?></h4>
        <div><?= $template->uso; ?></div>
    </div>   
</div>
<div class="sh-row">
    <div class="medium-12">
        &nbsp;&nbsp;<span id='waitLoad' style="display: none;"><?= CHtml::image($this->assetsPath . '/img/loading.gif'); ?></span>
        <div class="right">
            <?php if ($nova): ?>
                <button type="button" class="btn btn-success btn-sm" onclick="usaTemplate('<?= $template->id ?>', false)" >Criar</button>
            <?php else: ?>
                <button type="button" class="btn btn-success btn-sm" onclick="aplicaAlteracaoTemplate('<?= $id; ?>', '<?= $template->id; ?>')" >Aplicar alterações</button>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <br><br>
        <div id="template-parametros">
            <?php
            $this->widget('shared.widgets.Widget.WidgetView', array(
                'str' => $template->code,
                'data' => $data,
                'showParametros' => true,
                'url' => $this->createUrl('edicao/updateWidgetView'),
            ));
            ?>
        </div>
        <br>
        <div class="textLeft">
        </div>

    </div>
</div>
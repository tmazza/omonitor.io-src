<div class="sh-row">
    <div class="sup-teclado columns medium-12 " style="display: none;">
        <div class="textLeft">
            <label>
                <input type="checkbox" checked="true" id="latex-mark"/>
                Incluir marcador LaTex.
            </label>
            <div class="right">
                <?php
                echo CHtml::ajaxLink('Macros', $this->controller->createUrl($this->baseAction . 'macros'), array(
                    'beforeSend' => 'js: function(){'
                    . '$("#modal-edicao").html("<i class=\'fa fa-refresh fa-spin\'></i>");'
                    . '$("#modal-edicao").dialog({title: "Macros latex"}).dialog("open");'
                    . '}',
                    'success' => 'js: function(html) { '
                    . '$("#modal-edicao").html(html).dialog("open");'
                    . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("modal-edicao")]);'
                    . ' }',
                        ), array(
                    'class' => 'btn btn-primary',
                    'id' => 'load-macros-list',
                ));
                ?>
            </div>
        </div>
        <?php
        echo CHtml::ajaxLink('<i class="fa fa-question-circle"></i>', $this->controller->createUrl($this->baseAction . 'loadHelp'), array(
            'success' => 'js: function(html) { '
            . '$("#modal-edicao").html(html).dialog("open");'
            . ' }',
            'beforeSend' => 'js: function() {'
            . '$("#modal-edicao").html("<i class=\'fa fa-refresh fa-spin\'></i>");'
            . '$("#modal-edicao").dialog({title: "Uso do LaTex"}).dialog("open");'
            . '}'
                ), array(
            'class' => 'btn btn-xs btn-info',
            'id' => 'tecladohelp',
        ))
        ?>

        <br>
        <?php
        $this->widget('shared.widgets.Teclado.ViewTeclado', array(
            'inputID' => 'redactor-editor',
            'tecladoID' => Teclado::Latex,
            'script' => '_edicaoCamadas',
        ));
        ?>
    </div>
</div>                
<div id="edicao-de-camdas">
    <div class="sh-row">
        <div class="columns medium-12 medium-centered" style="max-width: 1200px;" id="edicao-box">
            <?php
            $this->render('_header', array(
                'favoritas' => $favoritas,
                'naoFavoritas' => $naoFavoritas,
            ));

            $this->render('_body', array(
                'model' => $model,
            ));
            $this->render('_footer', array(
                'favoritas' => $favoritas,
                'naoFavoritas' => $naoFavoritas,
            ));
            ?>
        </div>
    </div>
</div>
<?php

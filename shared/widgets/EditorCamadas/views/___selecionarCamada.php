<div>
    <hr>
    <div class="sh-row">
        <div class="column medium-4">
            <input id="reuso-search" placeholder="Buscar <?= ucfirst($tipo->nome); ?>..." class="form-control" value="" />
        </div>
        <div class="column medium-4" style="padding-top: 3px;">
            <?php
            echo CHtml::ajaxLink('<i class="fa fa-search"></i> Buscar', $this->createUrl('editor.reusoCamadas', array('id' => $tipo->id)), array(
                'type' => 'POST',
                'update' => '#reuso-results',
                'data' => array('term' => 'js: function() { return $("#reuso-search").val(); }'),
                    ), array(
                'id' => 'ajax-reuso-' . hash('crc32', microtime(true)),
                'class' => 'btn btn-sm btn-primary',
            ));
            ?>
        </div>
        <div class="column medium-offset-3"></div>
    </div>
    <hr>
    <?php
    $this->renderPartial('shared.widgets.EditorCamadas.views.____bucasCamadas', array(
        'results' => $results,
    ));
    ?>
</div>
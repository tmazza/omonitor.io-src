<div class="sh-row">
    <div class="column medium-12">
        <h4 class="textCenter">
            Todo o conteúdo englobado por <code>$</code> será
            interpretado como LaTex após salvar.
        </h4>
        <br>
        <div class="textCenter">
            <?= CHtml::image($this->assetsPath . '/img/proclat.png', 'Processamento do LaTex'); ?>
        </div>
    </div>
</div>
<br>
<p class="textCenter">É possível utilizar outros marcadores dependendo de como LaTex será mostrado.</p>
<div class="sh-row">
    <div class="column medium-6 textCenter">
        <h4>Na mesma linha</h4>
        <br>
        <code>$ LaTex $</code><br>
        ou<br>
        <code>\( LaTex \)</code>
        <br><br>
    </div>
    <div class="column medium-6 textCenter">
        <h4>Centralizado</h4>
        <br>
        <code>$$ LaTex $$</code><br>
        ou<br>
        <code>\[ LaTex \]</code>
        <br><br>
    </div>
</div>

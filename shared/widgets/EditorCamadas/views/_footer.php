<div id="editor-footer" style="display: none;">
    <?php $this->render('__teclado'); ?>
    <?php
    $this->render('__reuso', array(
        'favoritas' => $favoritas,
        'naoFavoritas' => $naoFavoritas,
    ));
    ?>
    <div class="sh-row">
        <div class="textLeft columns medium-12">

            <button class="btn btn-info" onclick="$('.sup-teclado').slideToggle();">
                LaTex<!--<?//= CHtml::image($this->controller->assetsPath . '/img/key.png'); ?>-->
            </button>
            &nbsp;
            <?php
            echo CHtml::ajaxLink('Widgets', $this->controller->createUrl($this->baseAction . 'widgets'), array(
                'beforeSend' => 'js: function() {'
                . '$("#modal-edicao").dialog("option","title", "Widgets");'
                . '$("#modal-edicao").html("<i class=\'fa fa-refresh fa-spin\'></i>").dialog("open");'
                . '}',
                'success' => 'js: function(html) {'
                . '$("#modal-edicao").html(html).dialog("open");'
                . '}',
                    ), array(
                'id' => 'load-widgets' . hash('md5', time()),
                'class' => 'btn btn-primary',
            ));
            ?>
            &nbsp;
            <button class="btn btn-default" onclick="$('.sup-reuso').slideToggle();">
                <?= CHtml::image($this->controller->assetsPath . '/img/recycleicon.png'); ?> camadas
            </button>
            <button class="btn btn-danger btn-paste" style="display: none;" onclick="pasteContent()">
                Colar
            </button>
            <div class="right">
                <div class="btn-group">
                    <?php
                    echo CHtml::link("<i class='fa fa-repeat'></i> Salvar e continuar editando", '#', array(
                        'onclick' => 'salvaContinua($(this));',
                        'class' => 'btn btn-primary btn',
                    ));
                    echo CHtml::link("<i class='fa fa-save'></i> Salvar e finalizar", '#', array(
                        'onclick' => 'salvaFinaliza($(this));',
                        'class' => 'btn btn-primary btn',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'modal-edicao',
    'options' => array(
        'title' => 'Link público de interação',
        'autoOpen' => false,
        'minWidth' => 900,
        'minHeight' => 400,
//        'maxHeight' => 600,
        'modal' => true,
        'overlay' => array(
            'backgroundColor' => '#000',
            'opacity' => '0.5'
        ),
    ),
    'htmlOptions' => array(
    ),
));
echo 'Compartilhar camada.';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script>
    $(window).load(function() {
        ajustaFooter(true);
    });
    $(window).resize(function() {
        ajustaFooter(false);
    });
    function ajustaFooter(ef) {
        pos = $('#editor-header').offset();
        wid = $('#editor-header').width();
        if (ef) {
            $('#editor-footer').css({left: pos.left + 'px', width: (wid) + 'px'}).fadeIn();
        } else {
            $('#editor-footer').css({left: pos.left + 'px', width: (wid) + 'px'});
        }
//        alert(wid);
    }
</script>
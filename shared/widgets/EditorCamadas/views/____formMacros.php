<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'latex-macro-form',
    'enableAjaxValidation' => false,
    'method' => 'GET',
    'action' => CHtml::normalizeUrl('editor.macros', array('rq' => 'new')),
        ));
?>


<?php echo $form->errorSummary($model); ?>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'alias'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textField($model, 'alias', array('size' => 60, 'maxlength' => 255)); ?>
    </div>
</div>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'valor'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->textField($model, 'valor', array('size' => 60, 'maxlength' => 255)); ?>
    </div>
    <div class="column medium-offset-6"></div>
</div>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'tipo'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->dropDownList($model, 'tipo', $tipos, array('empty' => '(Selecione um tipo de Macro)', 'class' => 'medium-4')); ?>
    </div>
    <div class="column medium-offset-6"></div>
</div>

<div class="sh-row">
    <div class="column medium-12">
        <?php
        echo CHtml::ajaxsubmitButton($model->isNewRecord ? 'Criar' : 'Salvar', $this->createUrl('editor.macros', array('rq' => 'new')), array(
            'beforeSend' => 'js: function(){'
            . '$("#modal-edicao").html("<i class=\'fa fa-refresh fa-spin\'></i>");'
            . '$("#modal-edicao").dialog({title: "Macros latex"}).dialog("open");'
            . '}',
            'success' => 'js: function(html) { '
            . '$("#modal-edicao").html(html).dialog("open");'
            . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("modal-edicao")]);'
            . ' }',
                ), array('id' => 'submit-new-macro', hash('md5', microtime(true)), "class" => "btn btn-success"));
        ?>
    </div>
</div>

<?php $this->endWidget(); ?>
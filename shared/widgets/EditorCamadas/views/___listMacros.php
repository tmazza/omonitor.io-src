<?php
echo CHtml::ajaxLink('Nova Macro', $this->createUrl('editor.macros', array('rq' => 'new')), array(
    'beforeSend' => 'js: function(){'
    . '$("#modal-edicao").html("<i class=\'fa fa-refresh fa-spin\'></i>");'
    . '$("#modal-edicao").dialog({title: "Nova macro"}).dialog("open");'
    . '}',
    'success' => 'js: function(html) { '
    . '$("#modal-edicao").html(html).dialog("open");'
    . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("modal-edicao")]);'
    . ' }',
        ), array(
    'class' => 'btn btn-primary btn-sm',
    'id' => 'nova-macro' . hash('md5', microtime(true)),
));
// TODO: botao ajax para todo o editor: Label, url, title, id
?>
<span class="hint right">Edição e exclusão de Macros LaTex em "Tópicos -> LaTex Macros"</span>
<?php if (count($macros) > 0): ?>
    <?php
    foreach ($macros as $macro)
        echo "<div style='display: none;'>" . $macro->getMacroMathJaxSintax($macro->valor, $macro->alias, $macro->tipo) . "<br>" . "</div>";
    ?>
    <table class="table">
        <?= $tipo = null; ?>
        <?php foreach ($macros as $m): ?>
            <?php if ($tipo !== $m->tipo): ?>
                <tr>
                    <td colspan="3" class="textCenter"><b><?= $m->tipo; ?></b></td>
                </tr>
                <tr>
                    <th>Alias</th>
                    <th>Valor</th>
                    <th><!--Preview--></th>
                </tr>
                <?php $tipo = $m->tipo; ?>
            <?php endif; ?>
            <tr>
                <td><?= $m->alias ?></td>
                <td><?= $m->valor ?></td>
                <td><?= $m->getPreview($m->alias) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <h5><span class="hint">Nenhuma Macro criada.</span></h5>
<?php endif; ?>

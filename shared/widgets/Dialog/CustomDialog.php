<?php

/**
 * Description of CustomDialog
 *
 * @author tiago
 */
class CustomDialog extends CWidget {

    public $id;
    public $content = '';
    public $title = null;
    public $theme = 'style1';
    private $themeUrl;

    public function init() {
        $assets = Yii::app()->assetManager->publish(__DIR__ . '/assets');
//        Yii::app()->clientScript->registerCssFile($assets . '/theme/' . $this->theme . '.css');
        $this->themeUrl = $assets;
    }

    public function run() {
        $this->render('main', array(
            'url' => $this->themeUrl,
        ));
    }

}

<?php

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => $this->id,
    'options' => array(
        'title' => $this->title,
        'autoOpen' => false,
        'theme' => 'custom',
//        'themeUrl' => $url,
//        'cssFile' => $this->theme . '.css'
    ),
));
echo $this->content;
$this->endWidget('zii.widgets.jui.CJuiDialog');

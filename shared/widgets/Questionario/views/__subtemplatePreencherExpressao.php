<?php
$this->beginContent('shared.widgets.Questionario.views._temaplateCascaQuestao3'.$this->template, array(
    'q' => $q,
    'seq' => (isset($seq) ? $seq : null),
));
?>
<?= '<b>' .$code . '</b>'; ?>
<?php if(!is_null($q->questao->interacao) && strlen($q->questao->interacao) > 0): ?>

<?php $sageCode = ShView::mergeDataToTemplate(stripslashes($q->questao->interacao),$selecionados); ?>

<div class='sage-auto sage-questao'><?=$sageCode;?></div>
<?php endif;?>

<br><br>
<?= CHtml::hiddenField('selected', json_encode($selecionados), array('id'=>'sel-' . $q->questao->id . (isset($seq) ? '-'.$seq : null))); ?>

<?= CHtml::activeTextField($q, "[{$q->questao->id}]resposta", array(
  'onkeypress'=>'return checkEnter(event)',
  'id' => 'resp-' . $q->questao->id . (isset($seq) ? '-'.$seq : null),
  'style'=>'width:100%;max-width:300px;',
  'placeholder'=>'Resposta...',
)) ?>
<a onclick='$(this).next().slideToggle();' href='#!' class="uk-button" data-delay='0' data-position='right' data-tooltip="Teclado">
  <i class='uk-icon uk-icon-keyboard-o'></i> Teclado
</a>

<div style="display:none;">
  <?php
  $this->widget('shared.widgets.Teclado.ViewTeclado', array(
      'inputID' => 'resp-' . $q->questao->id . (isset($seq) ? '-'.$seq : null),
      'tecladoID' => Teclado::Arquimedes,
      'template' => 'template2',
  ));
  ?>
</div>
<?php $this->endContent(); ?>
<script>
sagecell.makeSagecell({
  inputLocation: " .sage-auto",
  autoeval: true,
  evalButtonText: "Resolver",
  hide:["editor","language","evalButton","permalink","done","sessionFiles"]
});
</script>

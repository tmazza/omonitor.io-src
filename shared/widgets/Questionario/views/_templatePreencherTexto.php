<?php
$this->beginContent('shared.widgets.Questionario.views._temaplateCascaQuestao3'.$this->template, array(
    'q' => $q,
));
$selecionados = array();
?>
<b><?= $q->questao->enunciado ?></b>
<br><br>
<?php if(!is_null($q->questao->interacao) && strlen($q->questao->interacao) > 0): ?>
<div class='sage-auto sage-questao'><?=stripslashes($q->questao->interacao);?></div>
<?php endif;?>
<br><br>
<?= CHtml::hiddenField('selected', json_encode($selecionados), array('id'=>'sel-' . $q->questao->id)); ?>
<?=  CHtml::activeTelField($q, "[{$q->questao->id}]resposta", array('id' => 'resp-' . $q->questao->id))?>
<?php $this->endContent(); ?>

<?php
$this->beginContent('shared.widgets.Questionario.views._temaplateCascaQuestao3', array(
    'q' => $q,
));
?>
<b><?= $q->questao->enunciado ?></b>
<br><br>
<ul class="nav nav-pills nav-stacked">
    <?php foreach ($q->questao->alternativas as $a): ?>
        <li>
            <div class="text-left">
                <input <?= is_array($q->resposta) && in_array($a->id, array_keys($q->resposta)) ? 'checked="checked"' : '' ?> class="" name="RespostaForm[<?= $q->questao->id; ?>][resposta][<?= $a->id; ?>]" data-altId="<?=$a->id?>" type="checkbox">
                <b><?= $a->valor; ?></b>
            </div>
        </li>
    <?php endforeach; ?>
</ul>
<?php $this->endContent(); ?>

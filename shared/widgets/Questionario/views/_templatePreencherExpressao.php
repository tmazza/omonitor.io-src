<div id='qst-<?=$q->questao->id;?>'>
<?php
$params = ShView::extraiParametros($q->questao->enunciado);
if(count($params) > 0) {
  $sequencias = $q->questao->getSequencias();
  foreach ($sequencias as $id => $variaveis) {
    $selecionados = CHtml::listData($variaveis,'variavel_id',function($i) { return array('valor'=>$i->valor);  });
    $code = ShView::mergeDataToTemplate($q->questao->enunciado,$selecionados);
     $this->render('shared.widgets.Questionario.views.__subtemplatePreencherExpressao',array(
       'q' => $q,
       'code' => $code,
       'selecionados' => $selecionados,
       'seq' => $id,
     ));
  }
} else  {
  if(get_class($this) == 'ResolverQuestionario'){
    $this->render('shared.widgets.Questionario.views.__subtemplatePreencherExpressao',array(
      'q' => $q,
      'code' => $q->questao->enunciado,
      'selecionados' => array(),
    ));
  } else {
    $this->renderPartial('shared.widgets.Questionario.views.__subtemplatePreencherExpressao',array(
      'q' => $q,
      'code' => $q->questao->enunciado,
      'selecionados' => array(),
    ));
  }
}
?>
</div>

<div class="camada" style="border:1px solid #aaa;">
    <?php if (!is_null($q->questao->dica) && strlen($q->questao->dica) > 0): ?>
        <a href="#!" onclick="$('#dica<?= $q->questao->id ?>').fadeIn();">Dica</a>
    <?php endif; ?>
    <div class="">
        <div class="" id="questao<?= $q->questao->id ?>">
            <?= $content ?>
        </div><!-- /.tab-pane -->
    </div>

    <?php $idUpdate = "verifica-questao" . $q->questao->id . (isset($seq) ? '-' . $seq : null); ?>

    <div class="box-footer">
        <div id="<?=$idUpdate;?>">
            <?php if ($q->hasErrors()): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?= CHtml::error($q, 'resposta'); ?>
                </div>
            <?php else: ?>
                <?php if (!is_null($q->correta)): ?>
                    <?php
                    Yii::app()->controller->renderPartial('shared.actions.views.correcaoQuestao', array(
                        'q' => $q->questao,
                        'taxaAcerto' => $q->correta,
                    ));
                    ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php
        // Define script a ser usado para pegar a resposta
        $tipo = $q->questao->tipo;
        $script = 'js: JSON.stringify($("#resp-' . $q->questao->id . (isset($seq) ? '-'.$seq : null) . '").val())';
        $script2 = 'js: JSON.stringify($("#sel-' . $q->questao->id . (isset($seq) ? '-'.$seq : null). '").val())';
        if ($tipo == Questao::CincoAlternativas) {
            $script = 'js: JSON.stringify($("#questao' . $q->questao->id . ' input[type=\'radio\']:checked").val())';
        } elseif ($tipo == Questao::VerdadeiroOuFalseo) {
            $script = 'js: function(){ var selected = [];
$("#questao' . $q->questao->id . ' input[type=checkbox]:checked").each(function() {
   if ($(this).is(":checked")) {
       selected.push($(this).attr("data-altId"));
   }
}); return JSON.stringify(selected)}';
        }
        if(!is_object($this->questionario)){
          echo '--->' . time();
          exit;
        }
        // Link para verificação de resposta
        echo CHtml::ajaxLink('Verificar', Yii::app()->controller->createUrl('CorrigirQuestao'), array(
            'update' => '#'.$idUpdate,
            'beforeSend' => 'js: function() { $("#' . $idUpdate . '").html("Corrigindo...") }',
            'data' => array(
              'id' => $q->questao->id,
              'resp' => $script,'sel'=>$script2,
              'qstId' => $this->questionario->id,
            ),
                ), array(
            'class' => 'btn grey darken-3 right',
            'id' => 'verif-qst-' . $idUpdate,
        ));
        ?>
    </div>
</div>
<br>
<br>
<?php if (!is_null($q->questao->dica) && strlen($q->questao->dica) > 0): ?>
  <div id="dica<?= $q->questao->id ?>" class="modal" style="display:none;">
    <div class="modal-content" style="padding:20px;">
      <p class='flow-text'><?=$q->questao->dica?></p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="" onclick="$('#dica<?= $q->questao->id ?>').fadeOut();">Ok</a>
    </div>
  </div>
<?php endif; ?>

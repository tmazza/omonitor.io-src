<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <!--<li class=""><a href="#gabarito<?//= $q->questao->id ?>" data-toggle="tab">Gabarito</a></li>-->
        <?php if (!is_null($q->questao->dica) && strlen($q->questao->dica) > 0): ?>
            <li class=""><a href="#dica<?= $q->questao->id ?>" data-toggle="tab">Dica</a></li>
        <?php endif; ?>
        <li class="active"><a href="#questao<?= $q->questao->id ?>" data-toggle="tab">Questão</a></li>
        <!--<li class="pull-left header"><i class="fa fa-question-circle"></i></li>-->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="questao<?= $q->questao->id ?>">
            <?= $content ?>
        </div><!-- /.tab-pane -->
        <?php if (!is_null($q->questao->dica) && strlen($q->questao->dica) > 0): ?>
            <div class="tab-pane" id="dica<?= $q->questao->id ?>">
                <?= $q->questao->dica; ?>
            </div>
        <?php endif; ?>

    </div>

    <div class="box-footer">
        <div id="verifica-questao<?= $q->questao->id; ?>">
            <?php if ($q->hasErrors()): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?= CHtml::error($q, 'resposta'); ?>
                </div>
            <?php else: ?>
                <?php if (!is_null($q->correta)): ?>
                    <?php
                    Yii::app()->controller->renderPartial('shared.actions.views.correcaoQuestao', array(
                        'q' => $q->questao,
                        'taxaAcerto' => $q->correta,
                    ));
                    ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php
        // Define script a ser usado para pegar a resposta
        $tipo = $q->questao->tipo;
        $script = 'js: JSON.stringify($("#resp-' . $q->questao->id . '").val())';
        $script2 = 'js: JSON.stringify($("#sel-' . $q->questao->id . '").val())';
        if ($tipo == Questao::CincoAlternativas) {
            $script = 'js: JSON.stringify($("#questao' . $q->questao->id . ' input[type=\'radio\']:checked").val())';
        } elseif ($tipo == Questao::VerdadeiroOuFalseo) {
            $script = 'js: function(){ var selected = [];
$("#questao' . $q->questao->id . ' input[type=checkbox]:checked").each(function() {
   if ($(this).is(":checked")) {
       selected.push($(this).attr("data-altId"));
   }
}); return JSON.stringify(selected)}';
        }
        if(!is_object($this->questionario)){
          echo '--->' . time();
          exit;
        }
        // Link para verificação de resposta
        echo CHtml::ajaxLink('Verificar', $this->url, array(
            'update' => '#verifica-questao' . $q->questao->id,
            'beforeSend' => 'js: function() { $("#verifica-questao' . $q->questao->id . '").html("Corrigindo...") }',
            'data' => array(
              'id' => $q->questao->id,
              'resp' => $script,'sel'=>$script2,
              'qstId' => $this->questionario->id,
            ),
                ), array(
            'class' => 'btn btn-sm btn-primary',
            'id' => 'verif-qst-' . $q->questao->id,
        ));
        ?>
    </div>
</div>

<?php
echo CHtml::beginForm('','POST',['class'=>'uk-form']);

foreach ($questoes as $q) {
    if ($q->questao->tipo == Questao::CincoAlternativas) {
        $this->render('_templateCincoAlternativas', array('q' => $q));
    } elseif ($q->questao->tipo == Questao::VerdadeiroOuFalseo) {
        $this->render('_templateVouF', array('q' => $q));
    } elseif ($q->questao->tipo == Questao::EntradaSimples) {
        $this->render('_templatePreencherTexto', array('q' => $q));
    } elseif ($q->questao->tipo == Questao::EntradaExpressao) {
        $this->render('_templatePreencherExpressao', array('q' => $q));
    }
}

//echo CHtml::submitButton('Verificar todas', array('class' => 'btn btn-primary'));
echo CHtml::endForm();
?>

<script>
  function checkEnter(e){
    e = e || event;
    var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
    return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
  }
</script>
<style>
.sage-auto2 img {
  max-width: 100%;
  display: block;
  margin: 0 auto;
}
</style>

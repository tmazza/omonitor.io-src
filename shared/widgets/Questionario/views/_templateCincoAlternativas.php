<?php
$this->beginContent('shared.widgets.Questionario.views._temaplateCascaQuestao3'.$this->template, array(
    'q' => $q,
));
?>
<b><?= $q->questao->enunciado ?></b>
<br><br>
<?php if(!is_null($q->questao->interacao) && strlen($q->questao->interacao) > 0): ?>
<div class='sage-auto sage-questao'><?=stripslashes($q->questao->interacao);?></div>
<?php endif;?>
<br><br>
<?=
CHtml::activeRadioButtonList($q, '[' . $q->questao->id . ']resposta', CHtml::listData($q->questao->alternativas, 'id', 'valor'), array(
    'id' => 'resp-' . $q->questao->id,
));
?>
<?php $this->endContent(); ?>

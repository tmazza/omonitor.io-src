<?php

/**
 *
 *
 * @author tiago
 */
class ResolverQuestionario extends CWidget {

    public $questionarioId = null;
    public $questionario = null;
    public $questoes = array();
    public $verDesempenho = false;
    public $url = null;
    public $template = null;
    public function init() {
      if(is_null($this->url)){
        $this->url = Yii::app()->controller->createUrl('questionario/CorrigirQuestao');
      }
      if(!is_null($this->questionarioId)){
        $this->montaQuestionario();
      }

    }

    public function run() {
        $this->render('main', array(
            'questoes' => $this->questoes,
        ));
    }

    private function montaQuestionario(){
      $questionario = $this->getQuestionario($this->questionarioId);

      $verDesempenho = false;
      if (isset($_POST['RespostaForm'])) {
          $respostas = $questionario->constroiForm($_POST['RespostaForm']);
      } else {
          $respostas = $questionario->constroiForm();
      }
      $this->questionario = $questionario;
      $this->questoes = $respostas;

    }

    /**
     * Busca questionaio $id. Validando permissão de acesso.
     * @param type $id
     */
    private function getQuestionario($id) {
        $questionario = Questionario::model()->findByPk($id);
        if (is_null($questionario)) {
            Yii::app()->user->setFlash('msg-e', 'Questionário não encontrada');
            Yii::app()->controller->redirect(Yii::app()->controller->createUrl('/questionarios/index'));
        }
        return $questionario;
    }


}

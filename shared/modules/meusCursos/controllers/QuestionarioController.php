<?php

/**
 * Description of QuestionarioController
 *
 * @author tiago
 */
class QuestionarioController extends MeusCursosController {

    public function actions() {
        return array(
            'CorrigirQuestao' => 'shared.actions.CorrigirQuestao',
            'salvarResposta' => false,
        );
    }

    public function actionIndex($id) {
        $this->setRawLayout();
        $questionario = $this->getQuestionario($id);

        $verDesempenho = false;
        if (isset($_POST['RespostaForm'])) {
            $respostas = $questionario->constroiForm($_POST['RespostaForm']);
        } else {
            $respostas = $questionario->constroiForm();
        }

        if (isset($_POST['RespostaForm'])) {
            if ($this->salvarTentativaEmQuestionario($questionario, $respostas)) {
                $verDesempenho = true;
//                $this->corrigiQuestionario($questionario);
            }
        }

        $params = array(
            'questionario' => $questionario,
            'questoes' => $respostas,
            'verDesempenho' => $verDesempenho,
        );

        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('index', $params, true, true);
        } else {
            $this->render('index', $params);
        }
    }

    private function salvarTentativaEmQuestionario($questionario, $respostas) {
        $valid = true;
        foreach ($respostas as $i => $item) {
            if (isset($_POST['RespostaForm'][$i])) {
                $item->attributes = $_POST['RespostaForm'][$i];
            }
            $valid = $item->validate() && $valid;
        }
        if ($valid) {
            foreach ($respostas as $r) {
//                if (MCAlunoQuestaoQuestionario::novo($questionario, $r)) {
//                    echo '<pre>';
//                    print_r('Ops!');
//                    exit;
//                }
            }
            ShCode::setFlashSucesso('Respostas salvas. (TODO)');
            return true;
        }
    }

    private function corrigiQuestionario($questionario) {
        $respostas = MCAlunoQuestaoQuestionario::model()
                ->noQuestionario($questionario->id)
                ->doAluno()
                // TODO: status não deveria ser entregue? como vai ficar?
                ->findAll();
//                ->findAll("status = " . MCAlunoQuestaoQuestionario::Salva);
        foreach ($respostas as $r) {
            $r->taxaAcerto = $r->questao->taxaAcerto(unserialize($r->resposta));
            $r->status = MCAlunoQuestaoQuestionario::Corrigida;
            $r->update(array('taxaAcerto', 'status'));
        }
        ShCode::setFlashSucesso('Corrigida');
    }

}

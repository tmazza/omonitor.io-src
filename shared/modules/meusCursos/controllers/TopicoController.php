<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TopicoController
 *
 * @author tiago
 */
class TopicoController extends MeusCursosController {

    public function actionVer($id) {
        $this->redirect($this->createUrl('leitura/index', array('id' => $id)));
    }

}

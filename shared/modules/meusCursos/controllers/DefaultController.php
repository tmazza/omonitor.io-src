<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends MeusCursosController {

    public function actionIndex() {
        $this->setRawLayout();
        $cursos = $this->getCursosMatriculados();
        $meusCursosID = CHtml::listData($cursos, 'id', 'id');

        $this->render('index', array(
            'cursos' => $cursos,
        ));
    }

    public function actionVer($id) {
        $curso = $this->getCursosMatriculado($id);
        $this->render('ver', array(
            'curso' => $curso,
        ));
    }

    public function actionAula($id) {
        $this->setRawLayout();
        $aula = $this->getAula($id);
        $this->render('aula', array(
            'aula' => $aula,
        ));
    }

    public function getLabel($data, $icon, $class = 'default', $title = '') {
        $qtd = count($data);
        if ($qtd > 0) {
            if ($qtd === 1) {
                $label = '';
            } else {
                $label = '&nbsp;&nbsp;&nbsp;(' . $qtd . ')';
            }
            echo "&nbsp;<span title='{$title}' data-toggle='tooltip' class='label label-{$class}'><i class='fa fa-{$icon}'></i>{$label}</span>";
        }
    }

}

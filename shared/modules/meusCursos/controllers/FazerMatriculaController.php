<?php

/**
 * Description of FazerMatricula
 *
 * @author tiago
 */
class FazerMatriculaController extends MeusCursosController {

    /**
     * Lista cursos.
     * @param type $id
     */
    public function actionIndex($id) {
        $curso = $this->getCurso($id);
        $matriculados = array_keys($this->getCursosMatriculados());

        if (in_array($curso->id, $matriculados)) {
            Yii::app()->user->setFlash(self::INFO_FLASH, 'Você já está matriculado no curso ' . $curso->nome);
            $this->redirect($this->createUrl('default/ver', array('id' => $curso->id)));
        }

        $this->render('matricula', array(
            'curso' => $curso,
        ));
    }

    /**
     * Realiza matrícula e curso com matrícula liberada
     * @param type $id
     */
    public function actionCursoLivre($id) {
        $curso = $this->getCurso($id);
        if ($curso->formaDeMatricula == Curso::MatriculaLivre) {
            $this->realizaMatricula($curso);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Curso ' . $curso->nome . ' não tem matrícula aberta.');
            $this->redirect($this->createUrl('default/index'));
        }
    }

    /**
     * Mayttícula em curso com senha
     */
    public function actionCursoComSenha() {
        if (isset($_POST['id']) && isset($_POST['senha'])) {
            $curso = $this->getCurso($_POST['id']);
            if ($curso->senhaDeMatricula === $_POST['senha']) {
                $this->realizaMatricula($curso);
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Senha incorreta.');
                $this->redirect($this->createUrl('FazerMatricula/index', array('id' => $curso->id)));
            }
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Informe a senha.');
            $this->redirect($this->createUrl('default/index'));
        }
    }

    /**
     * Realiza matrícula de usuário em curso.
     * @param type $curso
     */
    private function realizaMatricula($curso) {
        if ($curso->matriculaAluno(Yii::app()->user->id)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Matrícula realizada.');
            $this->redirect($this->createUrl('default/ver', array('id' => $curso->id)));
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao fazer matrícula no Curso ' . $curso->nome . '.');
            Yii::log('Falha ao realizar matrícula em curso. CURSO: ' . $curso->id . ':' . $curso->nome . '. ALUNO: ' . Yii::app()->user->id);
            $this->redirect($this->createUrl('default/index'));
        }
    }

}

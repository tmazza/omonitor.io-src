<?php

class SiteController extends MeusCursosController {

    /**
     * Abre camada
     * @param type $id
     */
    public function actionExpandeCamada($id) {
        $camada = Camada::model()
                ->with('topico', 'tipo')
                ->findByPk($id);
        echo $camada->getTipoView($camada, true, true);
    }

}

<?php

/**
 * Description of BuscarController
 *
 * @author tiago
 */
class BuscarController extends MeusCursosController {

    public function actionListar() {
        $todosOsCursos = Curso::model()->with('autor')->findAll("publicado = 1");
        $cursos = $this->getCursosMatriculados();
        $meusCursosID = CHtml::listData($cursos, 'id', 'id');
        $this->render('listar', array(
            'matriculas' => $meusCursosID,
            'cursos' => $todosOsCursos,
        ));
    }

}

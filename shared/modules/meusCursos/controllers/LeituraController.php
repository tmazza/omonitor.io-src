<?php

/**
 * Description of LeituraController
 *
 * @author tiago
 */
class LeituraController extends MeusCursosController {

    public function actionIndex($id) {
        $topico = $this->getTopico($id);
        $leitura = $topico->getLeitura();
        $params = array(
            'topico' => $topico,
            'leitura' => $leitura,
        );
        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('index', $params, true, true);
        } else {
            $this->render('index', $params);
        }
    }

}

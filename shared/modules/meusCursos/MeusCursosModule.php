<?php

class MeusCursosModule extends CWebModule {

    public $layoutName = 'main';
    public $layoutRaw = 'raw';

    public function init() {
        $this->setImport(array(
            'meusCursos.components.*',
            'meusCursos.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            'fazermatricula.*' => false,
            'leitura.*' => false,
            'questionario.*' => false,
            'materialapoio.index' => false,
            'site.*' => false,
            'buscar.*' => false,
            'topico.*' => false,
        );
    }

}

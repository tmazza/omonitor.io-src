<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class MeusCursosController extends EditorController {

    protected function beforeAction($action) {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutName;
        return parent::beforeAction($action);
    }

    protected function setRawLayout() {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutRaw;
    }

    /**
     * Todos os cursos poblucados
     * @param type $id
     * @return type
     */
    protected function getCurso($id) {
        $curso = Curso::model()->findByPk((int) $id, "publicado = 1");
        if (is_null($curso)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Curso não encontrado');
            $this->redirect($this->createUrl('default/index'));
        }
        return $curso;
    }

    /**
     *
     * @return type
     */
    protected function getCursosMatriculados() {
        return Curso::model()->with('matriculas')
                        ->findAll(array(
                            'condition' => "matriculas_matriculas.aluno_id = '" . Yii::app()->user->id . "'",
                            'index' => 'id',
        ));
    }

    /**
     * Curso em que usuário logado possui matrícula
     * @param type $id
     * @return type
     */
    protected function getCursosMatriculado($id) {
        $curso = Curso::model()
                ->with('matriculas')
                ->findByPk((int) $id, array(
            'condition' => "matriculas_matriculas.aluno_id = '" . Yii::app()->user->id . "'",
        ));
        if (is_null($curso)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Curso não encontrado');
            $this->redirect($this->createUrl('default/index'));
        }
        return $curso;
    }

    /**
     * Busca tópico $id. Validando permissão de acesso.
     * @param type $id
     */
    protected function getTopico($id) {
        // ShSeg::acessoTopico($id);
        $topico = Topico::model()->findByPk($id);
        if (is_null($topico)) {

            $colab = PubliColaborador::model()->find([
                'condition' => "user_id = " . Yii::app()->user->id 
                    . " AND topico_id = " . (int) $id
                    . " " . ($permissao ? "AND permissao = " . $permissao : ''),
            ]);

            if(is_null($colab)){
                Yii::app()->user->setFlash('alert-error', 'Leitura não encontrada.');
                $this->redirect($this->createUrl('default/index'));
            } else {
                $topico = $colab->topico;
            }

        }
        return $topico;
    }

    /**
     * Busca questionaio $id. Validando permissão de acesso.
     * @param type $id
     */
    protected function getQuestionario($id) {
        ShSeg::acessoQuestionario($id);
        $questionario = Questionario::model()->findByPk($id);
        if (is_null($questionario)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não encontrada');
            $this->redirect($this->createUrl('default/index'));
        }
        return $questionario;
    }

    /**
     * Busca arquivo $id. Validando permissão de acesso.
     * @param type $id
     */
    protected function getArquivo($id) {
        ShSeg::acessoArquivo($id);
        $arquivo = Arquivo::model()->findByPk($id);
        if (is_null($arquivo)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Arquivo não encontrada');
            $this->redirect($this->createUrl('default/index'));
        }
        return $arquivo;
    }

/**
     * Busca aula do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */

    public function getAula($id) {
        $aula = CursoAula::model()
                ->findByPk((int) $id);
        if (is_null($aula)) {
            Yii::app()->user->setFlash('alert-error', 'Aula não econtrada.');
            $this->redirect($this->createUrl('default/index'));
        }
        return $aula;
    }

}

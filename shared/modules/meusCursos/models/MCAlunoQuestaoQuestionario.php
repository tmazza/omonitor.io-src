<?php

/**
 * This is the model class for table "aluno_questao_questionario".
 *
 * The followings are the available columns in table 'aluno_questao_questionario':
 * @property string $aluno_id
 * @property integer $questionario_id
 * @property integer $questao_id
 * @property string $resposta
 * @property double $taxaAcerto
 *
 * The followings are the available model relations:
 * @property User $aluno
 * @property Questao $questao
 * @property Questionario $questionario
 */
class MCAlunoQuestaoQuestionario extends CActiveRecord {

    const Salva = 1;
    const Entregue = 2;
    const Corrigida = 3;

    public function tableName() {
        return 'aluno_questao_questionario';
    }

    public static function getTableName() {
        return 'aluno_questao_questionario';
    }

    public function rules() {
        return array(
            array('aluno_id, questionario_id, questao_id', 'required'),
            array('questionario_id, questao_id', 'numerical', 'integerOnly' => true),
            array('taxaAcerto', 'numerical'),
            array('aluno_id', 'length', 'max' => 100),
            array('resposta', 'safe'),
            array('aluno_id, questionario_id, questao_id, resposta, taxaAcertom status', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'aluno' => array(self::BELONGS_TO, 'User', 'aluno_id'),
            'questao' => array(self::BELONGS_TO, 'Questao', 'questao_id'),
            'questionario' => array(self::BELONGS_TO, 'Questionario', 'questionario_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'aluno_id' => 'Aluno',
            'questionario_id' => 'Questionario',
            'questao_id' => 'Questao',
            'resposta' => 'Resposta',
            'taxaAcerto' => 'Taxa Acerto',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('aluno_id', $this->aluno_id, true);
        $criteria->compare('questionario_id', $this->questionario_id);
        $criteria->compare('questao_id', $this->questao_id);
        $criteria->compare('resposta', $this->resposta, true);
        $criteria->compare('taxaAcerto', $this->taxaAcerto);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Cria nova resposta de aluno em questão de querstionário
     * TODO: caso já exista o registro?
     * @param type $questionario
     * @param type $resposta
     * @return type
     */
    public static function novo($questionario, $resposta) {
        $aqq = new MCAlunoQuestaoQuestionario();
        $aqq->aluno_id = Yii::app()->user->id;
        $aqq->questionario_id = $questionario->id;
        $aqq->questao_id = $resposta->questao->id;
        $aqq->resposta = serialize($resposta->resposta);
        $aqq->status = self::Salva;
        return $aqq->save();
    }

    public function scopes() {
        return array(
            'doAluno' => array(
                'condition' => "aluno_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    public function noQuestionario($id) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "questionario_id = {$id}",
        ));
        return $this;
    }

}

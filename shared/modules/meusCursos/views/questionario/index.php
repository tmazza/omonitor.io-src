<?php
$this->pageTitle = $questionario->nome;
?>
<div class="content">
    <?php
    $this->widget('shared.widgets.Questionario.ResolverQuestionario', array(
        'questoes' => $questoes,
        'questionario' => $questionario,
        'verDesempenho' => $verDesempenho,
    ));
    ?>
</div>

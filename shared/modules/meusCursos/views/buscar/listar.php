<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
);
?>
<?php $value = isset($_GET['sc']) ? $_GET['sc'] : null; ?>
<input id="s-curso" class="form-control" placeholder="Buscar curso..." value="<?= $value ?>" />
<br>
<ul class="large-block-grid-3 medium-block-grid-2 small-block-grid-1">
    <?php foreach ($cursos as $c): ?>
        <li>
            <div class="box">
                <div class="box-header">
                    <h4 class="textCenter sc"><?= $c->nome; ?></h4>
                </div>
                <div class="box-body">
                    <?= $c->apresentacao; ?>
                    <hr>
                    <span class="hint">Professor(a):</span> <?= $c->autor->perfil->nome; ?>
                    <br>
                    <span class="hint">Matrícula:</span> <?= $c->getLabelformaDeMatricula(); ?>
                </div>
                <div class="box-footer">
                    <?php if (in_array($c->id, $matriculas)): ?>
                        <div class="textRight">
                            <div class="left">Já matriculado.</div>
                            <?= CHtml::link('Entrar', $this->createUrl('default/ver', array('id' => $c->id)), array('class' => 'btn btn-default btn-sm')); ?>
                        </div>
                    <?php else: ?>
                        <div class="textRight">
                            <?= CHtml::link('Realizar matrícula', $this->createUrl('FazerMatricula/index', array('id' => $c->id)), array('class' => 'btn btn-primary btn-sm')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<script>
    
    $(document).ready(function(){
        var txt = $('#s-curso').val();
        searchCurso(txt);
    });
    $('#s-curso').keyup(function() {
        var txt = $(this).val();
        searchCurso(txt);
    });
    function searchCurso(term) {
        $('.sc').each(function() {
            if ($(this).text().toLowerCase().indexOf(term.toLowerCase()) !== -1) {
                $(this).parent().parent().parent().show();
            } else {
                $(this).parent().parent().parent().hide();
            }
        });
    }
</script>
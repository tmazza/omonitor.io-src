<br>
<br>
<?php $qtd = count($aula->questionarios); ?>
<?php if ($qtd > 1): ?>
    <ul class="nav nav-tabs" id="qsts">
        <?php foreach ($aula->questionarios as $q): ?>
            <li>
                <?php
                echo CHtml::ajaxLink($q->nome, ShCode::getModUrl('meusCursos', 'questionario', 'index', array('id' => $q->id)), array(
                    'success' => 'js: function(html) { '
                    . '$("#view-questionario").html(html); '
                    . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("view-questionario")]);'
                    . '}',
                    'beforeSend' => 'js: function() {$("#view-questionario").html("<i class=\'fa fa-refresh fa-spin\'></i>");}',
                ));
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<div id="view-questionario">

    <?php
    $questionario = $aula->questionarios[0];
    $params = array(
        'questionario' => $questionario,
        'questoes' => $questionario->constroiForm(),
        'verDesempenho' => false,
    );
    $this->renderPartial('/questionario/index', $params);
    ?>
</div>
<script>
    $(document).ready(function() {
        $("#qsts a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $("#qsts li:eq(0) a").tab('show');
    });
</script>
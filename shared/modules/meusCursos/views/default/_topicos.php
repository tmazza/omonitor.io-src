<?php
$qtd = count($aula->topicos);
?>
<br>
<?php if ($qtd > 1): ?>
    <ul class="nav nav-tabs" id="topicos">
        <?php foreach ($aula->topicos as $id => $t): ?>
            <li>
                <?php
                echo CHtml::ajaxLink($t->nome, ShCode::getModUrl('meusCursos', 'leitura', 'index', array('id' => $t->id)), array(
                    'success' => 'js: function(html) { '
                    . '$("#show-read").html(html); '
                    . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("show-read")]);'
                    . '}',
                    'beforeSend' => 'js: function() {$("#show-read").html("<i class=\'fa fa-refresh fa-spin\'></i>");}',
                ));
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<div id="show-read">
    <?php
    $topico = $aula->topicos[0];
    $this->renderPartial('/leitura/index', array(
        'topico' => $topico,
        'leitura' => $topico->getLeitura(),
    ));
    ?>
</div>
<script>
    $(document).ready(function() {
        $("#topicos a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $("#topicos li:eq(0) a").tab('show');
    });
</script>
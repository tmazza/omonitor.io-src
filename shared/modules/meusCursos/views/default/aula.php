<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/ver', array('id' => $aula->curso->id))),
);
?>
<div class="content">
    <div class="box">
        <div class="box-header">
            <div class="pull-right">
                <h4>
                    <span class="label label-default">
                        125/500
                        <?= CHtml::image($this->assetsPath . '/img/Moins16.png') ?>
                    </span>
                </h4>
            </div>
            <h3>
                <?= CHtml::link($aula->curso->nome, $this->createUrl('default/ver', array('id' => $aula->curso->id))); ?>
                &raquo
                <?= $aula->nome; ?>
            </h3>
        </div>
        <div class="box-body">
            <ul class="nav nav-tabs" id="contents">
                <?php if (!is_null($aula->urlVideo)): ?>
                    <li>
                        <a href="#tabVideo">
                            Vídeo
                            &nbsp;&nbsp;
                            <?= $this->getLabel($aula->urlVideo, 'youtube-play', 'danger', 'Vídeos'); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if ($aula->temTopicos()): ?>
                    <?php
                    $qtdTop = count($aula->topicos);
                    $titulo = $qtdTop == 1 ? $aula->topicos[0]->nome : 'Conteúdo';
                    ?>
                    <li>
                        <a href="#tabTopicos">
                            <?= $titulo; ?>
                            &nbsp;&nbsp;
                            <?= $this->getLabel($aula->topicos, 'files-o', 'primary', 'Artigos'); ?>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if ($aula->temQuestionarios()): ?>
                    <li>
                        <a href="#tabQst">
                            Questionários
                            &nbsp;&nbsp;
                            <?= $this->getLabel($aula->questionarios, 'question-circle', 'success', 'Questionários'); ?>               
                        </a>
                    </li>
                <?php endif; ?>
                <?php if ($aula->temArquivos()): ?>
                    <li>
                        <a href="#tabArq">
                            Material de apoio
                            &nbsp;&nbsp;
                            <?= $this->getLabel($aula->arquivos, 'cloud-download', 'default', 'Arquivos'); ?>
                        </a>
                    </li>
                <?php endif; ?>
            </ul>
            <div class="tab-content">
                <?php if (!is_null($aula->urlVideo)): ?>
                    <div id="tabVideo" class="tab-pane fade">
                        <?php
                        $this->renderPartial('_videos', array(
                            'aula' => $aula,
                        ));
                        ?>
                    </div>
                <?php endif; ?>
                <?php if ($aula->temTopicos()): ?>
                    <div id="tabTopicos" class="tab-pane fade">
                        <?php $this->renderPartial('_topicos', array('aula' => $aula)); ?>
                    </div>
                <?php endif; ?>
                <?php if ($aula->temQuestionarios()): ?>
                    <div id="tabQst" class="tab-pane fade">
                        <?php $this->renderPartial('_questionarios', array('aula' => $aula)); ?>
                    </div>
                <?php endif; ?>
                <?php if ($aula->temArquivos()): ?>
                    <div id="tabArq" class="tab-pane fade">
                        <?php $this->renderPartial('_arquivos', array('aula' => $aula)); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#contents a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $("#contents li:eq(0) a").tab('show');
    });
</script>
<style>
    .ui-corner-all {
        border-radius: 0px;
    }
    .ui-widget-header {
        background: #3C8DBC;
        color: white;
    }
    .ui-dialog {
        z-index: 1000000!important;
    }
</style>
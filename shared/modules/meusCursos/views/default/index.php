<section class="content">
    <div class="sh-row">
        <div class="columns medium-9">
            <h3>Matrículas</h3> 
            <?php if (count($cursos) > 0): ?>
                <?php foreach ($cursos as $c): ?>
                    <div class="box">
                        <div class="box-body">
                            <h4><?= $c->nome; ?> <span class="hint"><?= $c->autor->perfil->nome; ?></span></h4>
                            <div class="textRight">
                                <?= CHtml::link('Entrar', $this->createUrl('default/ver', array('id' => $c->id)), array('class' => 'btn btn-default btn-sm')); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="box">
                    <div class="box-body">
                        <h4>
                            Você não está matriculado em nenhum curso. Comece &rarr;
                        </h4>
                    </div>
                </div>
            <?php endif; ?>

        </div>
        <div class="columns medium-3">
            <h3>Outros cursos</h3>
            <div class="box">
                <div class="box-body textCenter">
                    <?php echo CHtml::beginForm($this->createUrl('buscar/listar'), 'GET'); ?>
                    <?php echo CHtml::textField('sc', '', array('class' => 'form-control', 'placeholder' => 'Buscar curso...')); ?>                                       
                    <br>
                    <?php echo CHtml::submitButton('Buscar curso', array('class' => 'btn btn-primary')); ?>
                    <?php echo CHtml::endForm(); ?>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>
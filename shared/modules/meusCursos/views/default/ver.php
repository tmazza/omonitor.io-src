<?php $this->pageTitle = $curso->nome; ?>
<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
);
?>
<table class="table">
    <?php foreach ($curso->aulas as $aula): ?>
        <tr>
            <td><h4><?= ucfirst($aula->nome); ?></h4></td>
            <td class="textRight">
                <?= $this->getLabel($aula->urlVideo, 'youtube-play', 'danger', 'Vídeos'); ?>
                <?= $this->getLabel($aula->topicos, 'files-o', 'primary', 'Artigos'); ?>
                <?= $this->getLabel($aula->questionarios, 'question-circle', 'success', 'Questionários'); ?>
                <?= $this->getLabel($aula->arquivos, 'cloud-download', 'default', 'Arquivos'); ?>
            </td>
            <td class="textRight">
                <?= CHtml::link("Aprender", $this->createUrl('default/aula', array('id' => $aula->id)), array('class' => 'btn btn-primary')); ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<?php

if ($curso->formaDeMatricula == Curso::MatriculaLivre) {
    $this->renderPartial('_matriculaLivre', array('curso' => $curso));
} elseif ($curso->formaDeMatricula == Curso::MatriculaPorSenhaDeAcesso) {
    $this->renderPartial('_matriculaComSenha', array('curso' => $curso));
} elseif ($curso->formaDeMatricula == Curso::MatriculaPorSolicitacao) {
    echo 'Por solicitacao';
} else {
    echo 'TODO';
}
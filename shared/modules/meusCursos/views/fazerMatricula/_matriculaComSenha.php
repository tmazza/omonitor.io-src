<?= CHtml::beginForm($this->createUrl('FazerMatricula/cursoComSenha')); ?>
<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $curso->nome; ?></h3>
    </div>
    <div class="box-body">
        Professor: <?= $curso->autor->perfil->nome; ?>
    </div>
    <div class="box-footer">
        <div class="sh-row">
            <div class="medium-6 columns">
                <?= CHtml::hiddenField('id', $curso->id); ?>
                <?= CHtml::passwordField('senha'); ?>
            </div>
            <div class="medium-2 columns">
                <?=
                CHtml::submitButton('Realizar matrícula', array(
                    'class' => 'btn btn-primary'
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<?= CHtml::endForm(); ?>
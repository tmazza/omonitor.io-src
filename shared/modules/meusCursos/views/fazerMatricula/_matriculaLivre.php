<div class="box box-solid box-primary">
    <div class="box-header">
        <h3 class="box-title"><?= $curso->nome; ?></h3>
    </div>
    <div class="box-body">
        Professor: <?= $curso->autor->perfil->nome; ?>
    </div>
    <div class="box-footer textRight">
        <?= CHtml::link('Realizar matrícula', $this->createUrl('FazerMatricula/cursoLivre', array('id' => $curso->id)), array(
            'class' => 'btn btn-primary'
            )); ?>
    </div>
</div>
<?php

/**
 * Description of MonitorController
 *
 * @author tiago
 */
class PublicacoesController extends EditorController {

    // Definidas em PublicacoesModule
    public $layoutComMenu;
    public $layoutSemMenu;

    protected function beforeAction($action) {
        // Define layouts da aplicação
        $this->setLayouts();
        return parent::beforeAction($action);
    }

    /**
     * Baseado nas configurações do módulo define os layouts em uso.
     * Utiliza o layout dos módulo pai.
     */
    private function setLayouts() {
        $this->layoutComMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutComMenu;
        $this->layoutSemMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutSemMenu;
        $this->layout = $this->layoutSemMenu;
    }

    /**
     * Busca tópico do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getTopico($id,$permissao=false) {
        $topico = PubliTopico::model()
                ->doAutor()
                ->findByPk((int) $id);
        if (is_null($topico)) {

            $colab = PubliColaborador::model()->find([
                'condition' => "user_id = " . Yii::app()->user->id 
                    . " AND topico_id = " . (int) $id
                    . " " . ($permissao ? "AND permissao = " . $permissao : ''),
            ]);

            if(is_null($colab)){
                Yii::app()->user->setFlash('alert-error', 'Tópico não econtrado.');
                $this->redirect($this->createUrl('topico/listar'));
            } else {
                $topico = $colab->topico;
            }
        }
        return $topico;
    }

}

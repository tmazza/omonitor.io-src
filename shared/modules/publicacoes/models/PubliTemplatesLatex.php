<?php

/**
 * This is the model class for table "templates_latex".
 *
 * The followings are the available columns in table 'templates_latex':
 * @property string $id
 * @property string $codigo
 * @property integer $tipo
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class PubliTemplatesLatex extends CActiveRecord {

    const TipoDolar = 1;
    const TipoConchetes = 2;
    
    
    public function tableName() {
        return 'templates_latex';
    }

    public static function getTableName() {
        return 'templates_latex';
    }

    public function rules() {
        return array(
            array('id, codigo', 'required'),
            array('tipo,user_id', 'numerical', 'integerOnly' => true),
            array('id', 'length', 'max' => 64),
            array('id, codigo, tipo, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Codigo',
            'tipo' => 'Tipo',
            'user_id' => 'Autor',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
            'daAplicacao' => array(
                'condition' => "user_id IS NULL",
            ),
        );
    }

}

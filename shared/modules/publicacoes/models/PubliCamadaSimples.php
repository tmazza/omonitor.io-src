<?php

/**
 * This is the model class for table "camada".
 *
 * The followings are the available columns in table 'camada':
 * @property integer $id
 * @property string $conteudo
 * @property integer $tipo_id
 * @property integer $last_update
 * @property integer $topico_id
 * @property string $user_id
 * @property integer $excluido
 *
 * The followings are the available model relations:
 * @property UserAutor $autor
 * @property Topico $topico
 * @property CamadaApendice[] $camadaApendices
 * @property Questao[] $questaos
 */
class PubliCamadaSimples extends CActiveRecord {

    public function tableName() {
        return 'camada';
    }

    public static function getTableName() {
        return 'camada';
    }

    public function rules() {
        return array(
            array('tipo_id, topico_id, user_id', 'required'),
            array('tipo_id,user_id, last_update, topico_id, excluido', 'numerical', 'integerOnly' => true),
            array('id, conteudo, tipo_id, last_update, topico_id, user_id, excluido', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'tipo' => array(self::BELONGS_TO, 'PubliCamadaTipo', 'tipo_id'),
            'topico' => array(self::BELONGS_TO, 'PubliTopico', 'topico_id'),
            'camadaApendices' => array(self::HAS_MANY, 'PubliCamadaApendice', 'camada_id'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            )
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'conteudo' => 'Conteudo',
            'tipo_id' => 'Tipo',
            'last_update' => 'Last Update',
            'topico_id' => 'Topico',
            'user_id' => 'Autor',
            'excluido' => 'Excluido',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('conteudo', $this->conteudo, true);
        $criteria->compare('tipo_id', $this->tipo_id);
        $criteria->compare('last_update', $this->last_update);
        $criteria->compare('topico_id', $this->topico_id);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('excluido', $this->excluido);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

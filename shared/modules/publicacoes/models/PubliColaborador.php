<?php

/**
 * This is the model class for table "topico_colaborador".
 *
 * The followings are the available columns in table 'topico_colaborador':
 * @property integer $topico_id
 * @property integer $user_id
 * @property integer $permissao
 * @property integer $lastOpen
 */
class PubliColaborador extends CActiveRecord
{

    const PermissaoVer = 1;
    const PermissaoEditar = 2;

    const StatusPendente = 0;
    const StatusAceita = 1;
    const StatusRevogada = 2;

    public function tableName()
    {
        return 'topico_colaborador';
    }

        public static function getTableName()
    {
        return 'topico_colaborador';
    }

        
    public function rules()
    {
        return array(
            array('topico_id, user_id, permissao, lastOpen', 'required'),
            array('topico_id, user_id, permissao, lastOpen', 'numerical', 'integerOnly'=>true),
            array('topico_id, user_id, permissao, lastOpen, status', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'user' => [self::BELONGS_TO,'User','user_id'],
            'topico' => [self::BELONGS_TO,'PubliTopico','topico_id'],
        );
    }

    public function attributeLabels()
    {
        return array(
            'topico_id' => 'Topico',
            'user_id' => 'User',
            'permissao' => 'Permissao',
            'lastOpen' => 'Last Open',
        );
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getLabelPermissao($num)
    {
        switch ($num) {
            case self::PermissaoVer: return 'Ver';
            case self::PermissaoEditar: return 'Ver e Editar (em breve)';
            default: return '?';
        }
    }

}
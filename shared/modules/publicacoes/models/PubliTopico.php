<?php

/**
 * This is the model class for table "topico".
 *
 * The followings are the available columns in table 'topico':
 * @property integer $id
 * @property string $nome
 */
class PubliTopico extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'topico';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('nome', 'length', 'max' => 200),
            array('id, nome, emCriacao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            // TODO: remover relacionamento camada substituido por camadaRaiz
            'camadaRaiz' => array(self::HAS_ONE, 'PubliCamada', 'topico_id', 'condition' => 'tipo_id = 0'), // Camada tópico/raiz
            'relacionados' => array(self::HAS_MANY, 'PubliTopicosRelacionados', 'pai_id'),
            // Tipos de camadas
            'interacoes' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('tipo'), 'condition' => 'tipo.categoria = "sage"'), // Camada sage/interação
            'interacoesComDestaque' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('isDestaque' => array('alias' => 'dest'), 'tipo'), 'condition' => 'tipo.categoria = "sage" AND dest.valor="S"', 'alias' => 'cam'), // Camada sage/interação
            'subtopicos' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('tipo'), 'condition' => 'tipo.categoria = "subtopico"'), // Camada sage/interação        
            'colaboradores' => [self::HAS_MANY,'PubliColaborador','topico_id',
                'condition' => 'status != ' . PubliColaborador::StatusRevogada,
            ],
            'user' => [self::BELONGS_TO,'User','user_id'],
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = " . Yii::app()->user->id,
            )
        );
    }

    /**
     * Exclui tópico
     * @param type $pk
     */
    public static function excluir($pk) {
        self::atulizaFlag($pk, 'excluido', true);
    }

    /**
     * Publica tópico
     * @param type $pk
     */
    public static function publicar($pk) {
        self::atulizaFlag($pk, 'publicado', true);
    }

    /**
     * Despublica tópico
     * @param type $pk
     */
    public static function despublicar($pk) {
        self::atulizaFlag($pk, 'publicado', false);
    }

    /**
     * Destaca tópico
     * @param type $pk
     */
    public static function colocarDestaque($pk) {
        self::atulizaFlag($pk, 'comDestaque', true);
    }

    /**
     * Remove destaque de tópico
     * @param type $pk
     */
    public static function removerDestaque($pk) {
        self::atulizaFlag($pk, 'comDestaque', false);
    }
    /**
     * Marca tópico como rascunho
     * @param type $pk
     */
    public static function marcarRascunho($pk) {
        self::atulizaFlag($pk, 'emCriacao', true);
    }

    /**
     * Desmarca tópico como rascunho
     * @param type $pk
     */
    public static function desmarcarRascunho($pk) {
        self::atulizaFlag($pk, 'emCriacao', false);
    }

    /**
     * Atualiza $attr para $estado de tópico com id == $pk
     * @param type $pk
     * @param type $attr
     * @param type $estado
     */
    public static function atulizaFlag($pk, $attr, $estado) {
        Topico::model()->doAutor()->updateByPk($pk, array(
            $attr => $estado,
        ));
    }

    /**
     * Relaciona dois tópicos
     * @param Topico $aRelacionar
     * @return bool se foi criado
     */
    public function relacionarCom($aRelacionar, $tipo = 'simples') {
        $relacao = new PubliTopicosRelacionados();
        $relacao->pai_id = $this->id;
        $relacao->filho_id = $aRelacionar->id;
        $relacao->user_id = Yii::app()->user->id;
        $isRelacaoSalva = $relacao->save();
        if ($tipo == 'reflexivo') {
            return $isRelacaoSalva && $aRelacionar->relacionarCom($this);
        } else {
            return $isRelacaoSalva;
        }
    }

    /**
     * Desfaz relação entre dois tópicos
     * @param Topico $aRelacionar
     */
    public function desrelacionarDe($aDesrelacionar, $tipo = 'simples') {
        PubliTopicosRelacionados::model()->deleteAllByAttributes(array(
            'pai_id' => $this->id,
            'filho_id' => $aDesrelacionar->id,
            'user_id' => Yii::app()->user->id,
        ));
        if ($tipo == 'reflexivo') {
            $aDesrelacionar->desrelacionarDe($this);
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'user_id' => 'Autor',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Topico the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'exclusaoLogica' => array(
                'class' => 'ExclusaoLogicaBehavior',
            ),
            'dataAtualizacao' => array(
                'class' => 'LastUpdateBehavior',
            ),
        );
    }

}

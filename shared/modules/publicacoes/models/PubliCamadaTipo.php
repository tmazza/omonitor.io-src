<?php

/**
 * This is the model class for table "camada_tipo".
 *
 * The followings are the available columns in table 'camada_tipo':
 * @property integer $id
 * @property integer $nome
 * @property integer $escondido
 * @property integer $recursivo
 * @property string $view_name
 */
class PubliCamadaTipo extends CamadaTipo {

    protected function beforeSave() {
        $this->cor_borda_em_edicao = $this->cor_fundo;
        return true;
    }

}

<?php

include_once Yii::getPathOfAlias('shared.vendors') . '/simple_html_dom.php';

/**
 * @property integer $id
 * @property string $conteudo
 * @property integer $tipo
 */
class PubliCamada extends CActiveRecord {

    // Lista de camadas em uso
    private $camadasEmUso = array();
    protected $pathViewCamadas = 'shared.views.subCamadas.';

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('tipo_id', 'required'),
            array('conteudo', 'default', 'setOnEmpty' => true, 'value' => null),
            array('tipo_id', 'numerical', 'integerOnly' => true),
            array('id, conteudo, tipo', 'safe', 'on' => 'search'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Camada the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Para cada camada encontrada no primeiro nivel de conteúdo um novo registro
     * é criado, caso a camada possa um id o registro com esse id é atualizado.
     * @return type
     * @throws Exception
     */
    protected function beforeSave() {
        if (!is_null($this->conteudo)) {
            // Remove divs que possuam a 'class' ignore
            $this->conteudo = $this->removeConteudoIgnorado($this->conteudo);
            $this->conteudo = $this->removeQuebraDeLinhaDupla($this->conteudo);
            // Atualiza conteúdo com subcamadas trocados por referencia para os registros
            $this->conteudo = $this->salvaCamadasFilhas($this->conteudo);
        }
        // Exclui camadas removidas do conteúdo
        $this->aplicaExclusaoDeCamadas();
        // Atualiza tempo de atualização
        $this->last_update = time();
        return parent::beforeSave();
    }

    private function removeQuebraDeLinhaDupla($html) {
        return preg_replace('#(<br */?>\s*)+#i', '<br />', $html);
    }

    protected function removeConteudoIgnorado($conteudo) {
        $html = str_get_html($conteudo);
        if (is_object($html)) {
            $possiveisCamadas = $html->find('div.ignore');
            foreach ($possiveisCamadas as $ignore) {
                $ignore->outertext = '';
            }
            return $html->outertext;
        } else {
            return $conteudo;
        }
    }

    /**
     * Para cada camada encontrada, no primeiro nivel de conteúdo, ou um novo registro
     * de camada é criado ou um existente é atualizado. Basea-se pelo atributo id na
     * tag com a class camada.
     * @param type $conteudo
     * @return type
     */
    private function salvaCamadasFilhas($conteudo) {
        $html = str_get_html($conteudo);
        if (is_object($html)) {
            $possiveisCamadas = $html->childNodes();
            $cms = 0;
            foreach ($possiveisCamadas as $camada) {
                if (isset($camada->class) && $camada->class == 'camada') {
                    $cms++;
                    if (isset($camada->id)) {
                        $modelCamada = $this->atualizaCamada($camada);
                    } else {
                        $modelCamada = $this->criaCamada($camada);
                    }
                    if (is_null($modelCamada)) {
                        $camada->outertext = '<div class="camada-e">' . $camada->innertext . '</div>';
                        Yii::log('Camada com erro criada. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                    } else {

//                            
                        // Conteúdo dentro da sage é ignorado, deve ser editado nas principais interações.
                        if (in_array($modelCamada->tipo->categoria, array(
                                'texto', 
                                'subtopico', 
                                'topico', 
                                'template', 
                                'sage',
                                'html'))) {
                            $modelCamada->last_update = time();
                            // Atualiza apendices das camada | e retira referência do conteúdooriginal
                            $camada->innertext = $this->editaApendices($camada->innertext, $modelCamada->id);
                            if ($modelCamada->tipo->categoria == 'template') {
                                // Verifica JSON e salva em conteúdo
                                if (isset($camada->attr['datacode'])) {
                                    $modelCamada->conteudo = json_encode(Camada::getJSON(base64_decode($camada->attr['datacode'])));
                                }
                            } elseif ($modelCamada->tipo->categoria == 'sage') {
                                if (isset($camada->attr['datacode'])) {
                                    $modelCamada->conteudo = base64_decode($camada->attr['datacode']);
                                }
                            } elseif($modelCamada->tipo->categoria == 'html') {
                                $p = new CHtmlPurifier();
                                $p->options = array(
                                    'URI.AllowedSchemes'=>array(
                                      'http' => true,
                                      'https' => true,
                                    ),
                                    'HTML.Allowed' => 'p,a[href|target],strong,em,br,iframe[src|scrolling|width|height|style]',
                                    'HTML.SafeIframe' => true,
                                    'URI.SafeIframeRegexp' => '%^(http|https)://(localhost|www.geogebra.org)%',
                                );
                                $modelCamada->conteudo = $p->purify(CHtml::decode($camada->innertext));
                            } else {
                                // Processa filhos e atualizada conteúdo e ultima edição de camada
                                $modelCamada->conteudo = $this->salvaCamadasFilhas($camada->innertext);
                            }

                            $modelCamada->update(array('conteudo', 'last_update'));
                        }
                        // Retorna identificador da camada
                        $camada->outertext = "<div class='camada' id='{$modelCamada->id}'></div>";
                        // Adiciona camada a lista de camdas pertencentes ao tópico
                        $this->camadasEmUso[] = $modelCamada->id;
                    }
                }
            }
            $conteudoCamada = $html->outertext;
        } else {
            $conteudoCamada = $conteudo;
        }
        $this->extraiLatex($conteudoCamada);
        return $conteudoCamada;
    }

    /**
     * Cria nova camada com atributo conteúdo NULL
     * @param type $camada
     * @return type
     * Camadas semd definição de tipo ou com tipo inválido não sofrem alterações.
     */
    private function criaCamada($camada) {
        // Valida existencia e valor do tipo definido para a camada
        if (!isset($camada->tipo)) {
            Yii::log('Tipo de camada não especificado. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_WARNING, 'publicacoes.camada');
        } else {
            $tipo = (int) $camada->tipo;
            if (!$this->isTipoValido($tipo)) {
                Yii::log('Tipo de camada inválido. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_WARNING, 'publicacoes.camada');
            } else {
                $novaCamada = new PubliCamadaSimples();
                $novaCamada->tipo_id = $camada->tipo;
                $novaCamada->user_id = Yii::app()->user->id;
                $novaCamada->topico_id = $this->topico_id;
                $novaCamada->conteudo = NULL;
                if ($novaCamada->save()) {
                    return $novaCamada;
                } else {
                    Yii::log('Falha ao criar camada. AUTOR: ' . Yii::app()->user->id . '. TIPO: ' . $camada->tipo, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                }
            }
        }
        return null;
    }

    /**
     * Atualiza camada do conteúdo.
     * @param type $camada
     * @return type
     * @throws Exception
     */
    private function atualizaCamada($camada) {
        $modelCamada = PubliCamadaSimples::model()
                ->doAutor()
                ->findByPk((int) $camada->id);
        if (is_null($modelCamada)) {
            Yii::log('Camada com ID inválido não encontrada para atualização. AUTOR: ' . Yii::app()->user->id . '. TIPO: ' . $camada->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
        }

        return $modelCamada;
    }

    /**
     * Busca no conteÃºdo da camada os apendices associados.
     * 
     * @param type $conteudoCamada
     * @return type
     */
    private function editaApendices($conteudoCamada, $camadaID) {
        if (!is_null($conteudoCamada)) {
            // Atualiza apendices das camada
            $htmlCamada = str_get_html($conteudoCamada);
            if (is_object($htmlCamada)) {
                $possiveisApendices = $htmlCamada->childNodes();
                foreach ($possiveisApendices as $apendice) {
                    if (isset($apendice->class) && $apendice->class == 'apendice') {
                        if (isset($apendice->tipo) && $this->isTipoApendiceValido($apendice->tipo)) {
                            $this->processaApendice($apendice, $camadaID);
                        }
                        $apendice->outertext = '';
                    }
                }
            }
            return $htmlCamada->innertext;
        } else {
            return $conteudoCamada;
        }
    }

    /**
     * Cria ou atualiza apendice de camada
     * @param type $apendice
     * @param type $camadaID
     */
    private function processaApendice($apendice, $camadaID) {
        if (isset($apendice->id)) {
            CamadaApendice::model()->doAutor()->updateAll(array(
                'valor' => $apendice->plaintext,
                    ), array(
                'condition' => "camada_id = '{$camadaID}'",
            ));
        } else {
            // Salva novo registor de apendice
            $novoApendice = new CamadaApendice();
            $novoApendice->valor = $apendice->plaintext;
            $novoApendice->tipo = $apendice->tipo;
            $novoApendice->camada_id = $camadaID;
            $novoApendice->user_id = Yii::app()->user->id;
            if (!$novoApendice->save()) {
                Yii::log("Falha ao criar apêndice.", CLogger::LEVEL_ERROR, 'publicacoes.camada');
            }
        }
    }

    /**
     * Busca no texto expressões latex delimitadas por $ ... $ ou por \[ ... \]
     * Gera hash para expressões encontradas e caso ainda não estajam salva, um
     * registro em TemplatesLatex é criado
     * @param type $string
     */
    private function extraiLatex($string) {
        $latex = array();
        $str = str_replace('$$', '$', $string);

        // Expressões delimitas por $ ... $
        $matchs1 = array();
        preg_match_all('/\$([^\$]*)\$/U', $str, $matchs1);
        if (count($matchs1[1]) > 0) {
            $latex[] = $matchs1[1];
        }

        // Expressões delimitas por \[ ... \]
        $matchs2 = array();
        preg_match_all('/\[([^\]]*)\]/', $str, $matchs2);
        if (count($matchs2[1]) > 0) {
            $m2 = array();
            foreach ($matchs2[1] as $m) {
                // Último caracter deve ser \ TODO: corrigir expressão de busca
                $last = substr($m, strlen($m) - 1, strlen($m));
                if ($last == '\\') {
                    $m2[] = substr($m, 0, strlen($m) - 1);
                }
            }
            $latex[] = $m2;
        }
        // TODO: tempo gasto ???
        if (count($latex) > 0) {
            $finds = array();
            foreach ($latex as $la) {
                foreach ($la as $exp) {
                    $hash = hash('md5', $exp);
                    if (!isset($finds[$hash])) {
                        $finds[$hash] = $exp;
                    }
                }
            }
            foreach ($finds as $h => $f) {
                $template = PubliTemplatesLatex::model()->findByPk(array(
                    'id' => $h,
                    'user_id' => Yii::app()->user->id,
                ));
                if (is_null($template)) {
                    $template = new PubliTemplatesLatex();
                    $template->id = $h;
                    $template->codigo = $f;
                    $template->user_id = Yii::app()->user->id;
                    $template->save();
                }
            }
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'camada';
    }

    public static function getTableName() {
        return 'camada';
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'topico' => array(self::BELONGS_TO, 'PubliTopico', 'topico_id'),
            'tipo' => array(self::BELONGS_TO, 'PubliCamadaTipo', 'tipo_id'),
            'titulo' => array(self::HAS_ONE, 'PubliCamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoTitulo),
            'descricao' => array(self::HAS_ONE, 'PubliCamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoResumoInteracao),
            'isDestaque' => array(self::HAS_ONE, 'PubliCamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoInteracaoComDestaque),
            'apendices' => array(self::HAS_MANY, 'PubliCamadaApendice', 'camada_id'),
        );
    }

    /**
     * Realiza exclusao lógica de camadas não utilizadas.
     * São consideradas não utilizadas as camadas do tópico não encontradas em $camadasEmUSo
     */
    private function aplicaExclusaoDeCamadas() {
        $criteria = new CDbCriteria();
        $criteria->condition = "tipo_id != 0 AND topico_id = " . $this->topico_id;
        $criteria->addNotInCondition('id', array_unique($this->camadasEmUso));
        PubliCamada::model()->doAutor()->updateAll(array('excluido' => true), $criteria);
    }

    private function isTipoApendiceValido($tipo) {
        return ($tipo == CamadaApendice::TipoTitulo || $tipo == CamadaApendice::TipoParametroDeCodigo); // OR ...
    }

    private function isTipoValido($tipo) {
        $tipos = PubliCamadaTipo::model()->findAll(array(
            'index' => 'id',
            'condition' => "(user_id = '" . Yii::app()->user->id . "' "
            . "OR de_aplicacao = 1)",
        ));
        return in_array($tipo, array_keys($tipos));
    }

    public function behaviors() {
        return array(
            'exclusaoLogica' => array(
                'class' => 'ExclusaoLogicaBehavior',
            ),
        );
    }

    /**
     * Prepara camadas para edicao
     * @param type $conteudo
     * @return type
     * @throws Exception
     */
    public function expandeCamadasEdicao($conteudo, $attrId = true) {
        if (!is_null($conteudo) && $conteudo != '') {
            $html = str_get_html($conteudo);
            $camadas = $html->find('div.camada');
            // Restrutura documento
            foreach ($camadas as &$camada) {
                if (!isset($camada->id)) {
                    Yii::log('Classe camada salva, sem id associado. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                } else {
                    $modelCamada = PubliCamada::model()->findByPk($camada->id);
                    if (is_null($modelCamada)) {
                        Yii::log('Camada não encontrada. AUTOR: ' . Yii::app()->user->id . '. ID: ' . $modelCamada->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                    } else {
                        $attrReuso = ($modelCamada->topico_id != $this->topico_id) ? ' data-reuso=true ' : '';

                        if ($modelCamada->tipo->categoria == 'sage') {
                            $camada->outertext = Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.sage', array(
                                'attrReuso' => $attrReuso,
                                'modelCamada' => $modelCamada,
                                'attrId' => $attrId,
                            ), true);
                        } elseif ($modelCamada->tipo->categoria == 'template') {
                            $data = Camada::getJSON($modelCamada->conteudo);
                            $camada->outertext = Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.template', array(
                                'modelCamada' => $modelCamada,
                                'widget' => Widget::model()->findByPk($data['tipo']),
                                'data' => $data,
                                'attrId' => $attrId,
                            ), true);
                        } elseif($modelCamada->tipo->categoria == 'html') {
                            $modelCamada->conteudo = CHtml::encode($modelCamada->conteudo);
                            $camada->outertext = Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.default', array(
                                'attrReuso' => $attrReuso,
                                'camada' => $modelCamada,
                                'thisModel' => $this,
                                'attrId' => $attrId,
                            ), true);

                        } else {
                            $camada->outertext = Yii::app()->controller->renderPartial('shared.views.edicaoCamadas.default', array(
                                'attrReuso' => $attrReuso,
                                'camada' => $modelCamada,
                                'thisModel' => $this,
                                'attrId' => $attrId,
                            ), true);
                        }
                    }
                }
            }
            return $html->outertext;
        }
    }

    // Manipulação de camadas
    /**
     * Prepara camadas para visualização.
     * @param type $conteudo
     * @return type
     * Em situações de erro o conteúdo da camada não é processado e um log é salvo.
     */
    public function expandeCamadasEdicaoInteracoes($conteudo) {
        if (!is_null($conteudo) && $conteudo != '') {
            $html = str_get_html($conteudo);
            $camadas = $html->find('div.camada');

            // Restrutura documento
            foreach ($camadas as &$camada) {
                if (!isset($camada->id)) {
                    Yii::log('Classe camada salva, sem id associado. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                } else {
                    $modelCamada = PubliCamada::model()->findByPk($camada->id);
                    if (is_null($modelCamada)) {
                        Yii::log('Camada não encontrada. AUTOR: ' . Yii::app()->user->id . '. ID: ' . $modelCamada->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                    } else {
                        // TODO: remover tags de apendices!!!
                        if (in_array($modelCamada->tipo->categoria, array('texto', 'subtopico'))) {
                            $camada->outertext = $this->getTipoView($modelCamada, true, true);
                        } elseif ($modelCamada->tipo->categoria == 'sage') {
                            $camada->outertext = Yii::app()->controller->renderPartial('publicacoes.views.subCamadas.' . $modelCamada->tipo->view_name, array(
                                'camada' => $modelCamada,
                                    ), true);
                        } else {
                            Yii::log('Camada processada de categoria desconhecida.' . $modelCamada->id, CLogger::LEVEL_ERROR, 'camada.expande');
                            // TODO:
                        }
                    }
                }
            }
            return $html->outertext;
        }
    }

    /**
     * Monta conteúdo para visualização baseado no tipo da camada.
     *  -- Escondido: se o conteúdo deve vir carregado aberto ou como um link ajax 
     *  -- Recursivo: se as subcamadas serão montadas ou não.
     * @param type $camada - CActiveRecord Camada
     * @param type $forceOpen - Anula comportamento de Escondido
     * @return string (html)
     * @throws Exception - Tipo de camada não cadastrado.
     */
    public function getTipoView($camada, $forceOpen = false, $edicao = null, $proccessOut = false) {
        if (is_null($camada->tipo)) {
            throw new Exception("Camada cadastrada com tipo inváilido!", 500);
        }

        $conteudoEscondido = $camada->tipo->escondido;
        if ($conteudoEscondido && !$forceOpen) {
            return $this->getConteudoEscondido($camada);
        } else {
            if ($camada->tipo->recursivo) {
                // Processa sub camadas da camada
                if (!is_null($edicao) && $edicao === true) {
                    $conteudo = $camada->expandeCamadasEdicaoInteracoes($camada->conteudo);
                } else {
                    $conteudo = $camada->expandeCamadasVisualizacao($camada->conteudo);
                }
            } else {
                // Camada não possui sub camadas
                $conteudo = $camada->conteudo;
            }
            return Yii::app()->controller->renderPartial($this->pathViewCamadas . $camada->tipo->view_name, array(
                        'camada' => $camada,
                        'camada_id' => $camada->id,
                        'class' => 'camada', // attr html_class removido de camada_tipo
                        'conteudo' => $conteudo,
                            ), true, $proccessOut);
        }
    }

}

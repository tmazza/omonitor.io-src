<?php

/**
 * Description of LinkSimNao
 *
 * @author tiago
 */
class ListaCamadas extends CWidget {

    public $tipo;
    private $camadas = array();
    private $modelTipo = array();

    public function init() {
        $this->camadas = Camada::model()->buscaConteudoDoTipo($this->tipo);

        $camadaTipo = PubliCamadaTipo::model()->doAutor()->findByPk($this->tipo);
        if (is_null($camadaTipo)) {
            $camadaTipo = PubliCamadaTipo::model()->findByPk($this->tipo, "de_aplicacao = 1");
        }
        $this->modelTipo = $camadaTipo;
    }

    public function run() {
        $this->render('main', array(
            'camadas' => $this->camadas,
            'tipo' => $this->modelTipo,
        ));
    }

}

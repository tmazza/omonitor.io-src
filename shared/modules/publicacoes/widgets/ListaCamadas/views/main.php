<div class="content">
    <div class="box box-solid box-primary">
        <div class="box-header">
            <h3><b><?= $tipo->nome; ?></b></h3>
        </div>
    </div>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $camadas,
        'itemView' => '_tipo',
    ));
    ?>
</div>
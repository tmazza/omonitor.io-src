<div class="box box-solid">
    <div class="box-body">
        <?= $data->getTipoView($data, true); ?>
    </div>
    <?php if (isset($data->topico->nome)): ?>
        <div class="box-footer">
            <span class="hint">Origem: </span><?= $data->topico->nome; ?>
        </div>
    <?php endif; ?>
</div>
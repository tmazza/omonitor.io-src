<?php

/**
 * Description of LinkSimNao
 *
 * @author tiago
 */
class LinkSimNao extends CWidget {

    public $boolean = true;
    // Texto quando $boolean true
    public $textoTrue = 'sim';
    // Texto quando $boolean false
    public $textoFalse = 'não';
    // Link quando $boolean true
    public $urlTrue = '#';
    // Link quando $boolean false
    public $urlFalse = '#';
    // Texto de link quando $boolean true
    public $textoLinkTrue = '';
    // Texto de link quando $boolean false
    public $textoLinkFalse = '';

    public function init() {
//        $assetsPath = Yii::getPathOfAlias('publicacoes.widgets.LinkSimNao.assets');
//        $assetsUrl = Yii::app()->assetManager->publish($assetsPath, true);
//        Yii::app()->clientScript->registerScriptFile($assetsUrl . '/js/LinkSimNao.js');
    }

    public function run() {
        $this->render('linkSimNao');
    }

}

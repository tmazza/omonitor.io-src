<?php if ($this->boolean): ?>
    <span class="green bold"><?= $this->textoTrue; ?></span>,
    <?= CHtml::link($this->textoLinkTrue, $this->urlTrue, array('class' => '')); ?>
<?php else: ?>
    <span class="red bold"><?= $this->textoFalse; ?></span>,
    <?= CHtml::link($this->textoLinkFalse, $this->urlFalse, array('class' => '')); ?>
<?php endif; ?>
<script>
//    $('.hide-on-hover, .show-on-hover').parent().on('hover, ready', function() {
//        $(this).find('.show-on-hover').css('display', 'block');
//        $(this).find('.hide-on-hover').css('display', 'none');
//    });
//    $('.show-on-hover').parent().on('mouseleave, ready', function() {
//        $(this).find('.show-on-hover').css('display', 'none');
//        $(this).find('.hide-on-hover').css('display', 'block');
//    });
</script>
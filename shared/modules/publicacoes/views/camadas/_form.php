<div class="sh-row">
    <div class="medium-8 columns">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'camada-tipo-_form-form',
            'enableAjaxValidation' => false,
        ));
        ?>

        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'nome'); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo $form->textField($model, 'nome'); ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'nome'); ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'texto_ao_criar'); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo $form->textField($model, 'texto_ao_criar'); ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'texto_ao_criar'); ?>
            </div>
        </div>
        <div class="sh-row">
            <div class="medium-12 columns">
                <span class="hint">Quando criado, na edição, a camada já virá com o texto.</span>
            </div>
        </div>
        <br>

        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'escondido'); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo $form->dropDownList($model, 'escondido', array('0' => 'Não', '1' => 'Sim')); ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'escondido'); ?>
            </div>
        </div>
        <div class="sh-row">
            <div class="medium-12 columns">
                <span class="hint">Se o conteúdo da camada deve ser com texto ou substituído por um botão que ao ser clicado mostra o texto.</span>
            </div>
        </div>
        <br>
        <div id='conteudo-para-escondido'>

            <div class="sh-row">
                <div class="medium-3 columns">
                    <?php echo $form->labelEx($model, 'texto_link_expandir'); ?>
                </div>
                <div class="medium-4 columns">
                    <?php echo $form->textField($model, 'texto_link_expandir'); ?>
                </div>
                <div class="medium-5 columns">
                    <?php echo $form->error($model, 'texto_link_expandir'); ?>
                </div>
            </div>
            <div class="sh-row">
                <div class="medium-12 columns">
                    <span class="hint">Título do botão que abre o conteúdo. Este é substituído pelo título da camada, caso possua título.</span>
                </div>
            </div>
            <br>       

            <div class="sh-row">
                <div class="medium-3 columns">
                    <?php echo $form->labelEx($model, 'em_modal'); ?>
                </div>
                <div class="medium-4 columns">
                    <?php echo $form->dropDownList($model, 'em_modal', array('0' => 'Não', '1' => 'Sim')); ?>
                </div>
                <div class="medium-5 columns">
                    <?php echo $form->error($model, 'em_modal'); ?>
                </div>
            </div>
            <div class="sh-row">
                <div class="medium-12 columns">
                    <span class="hint">Se o conteúdo deve ser colocado dentro do texto ("abrindo" conteúdo) ou não.</span>
                </div>
            </div>
            <br>       
        </div>
        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'possui_titulo'); ?>
            </div>
            <div class="medium-4 columns">
                <?php echo $form->dropDownList($model, 'possui_titulo', array('0' => 'Não', '1' => 'Sim')); ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'possui_titulo'); ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'cor_fundo'); ?>
            </div>
            <div class="medium-4 columns">
                <?php
                $this->widget('shared.extensions.colorpicker.EColorPicker', array(
                    'model' => $model,
                    'attribute' => 'cor_fundo',
                    'onChangeCustom' => '$("#camada-preview").css({"backgroundColor": "#" + $("#PubliCamadaTipo_cor_fundo").val()});'
                        )
                );
                ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'cor_fundo'); ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="medium-3 columns">
                <?php echo $form->labelEx($model, 'cor_texto'); ?>
            </div>
            <div class="medium-4 columns">
                <?php
                $this->widget('shared.extensions.colorpicker.EColorPicker', array(
                    'model' => $model,
                    'attribute' => 'cor_texto',
                    'onChangeCustom' => '$("#camada-preview").css({"color": "#" + $("#PubliCamadaTipo_cor_texto").val()});'
                        )
                );
                ?>
            </div>
            <div class="medium-5 columns">
                <?php echo $form->error($model, 'cor_texto'); ?>
            </div>
        </div>


        <div class="sh-row">
            <div class="medium-12 columns">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- form -->

    <div class="medium-4 columns">
        <h4>Preview de camada</h4>
        <div class="camada" id='camada-preview' style="padding: 12px">
            Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.
        </div>
    </div>
</div><!-- form -->

<script>
    $(document).ready(function() {
        updateCor();
        controleCampos();
    });

    $("#CamadaTipo_escondido").change(function() {
        controleCampos();
    });

    $('#CamadaTipo_cor_fundo').on('blur', function() {
        updateCor();
    });
    $('#CamadaTipo_cor_texto').on('blur',function() {
        updateCor();
    });

    function updateCor() {
        $("#camada-preview").css({"color": "#" + $("#CamadaTipo_cor_texto").val()});
        $("#camada-preview").css({"backgroundColor": "#" + $("#CamadaTipo_cor_fundo").val()});
    }

    function controleCampos() {
        var esc = $("#CamadaTipo_escondido option:selected").val();
        if (esc === "0") {
            $('#conteudo-para-escondido').slideUp(0);
        } else {
            $('#conteudo-para-escondido').slideDown();
        }

    }
</script>
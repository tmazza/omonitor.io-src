<?php
/* @var $this LatexMacroController */
/* @var $model LatexMacro */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'latex-macro-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="sh-row">
		<div class="column medium-2">
			<?php echo $form->labelEx($model,'alias'); ?>
		</div>
		<div class="column medium-10">
			<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
		</div>
	</div>

    	<div class="sh-row">
		<div class="column medium-2">
			<?php echo $form->labelEx($model,'valor'); ?>
		</div>
		<div class="column medium-10">
			<?php echo $form->textField($model,'valor',array('size'=>60,'maxlength'=>255)); ?>
		</div>
	</div>
    
	<div class="sh-row">
		<div class="column medium-2">
			<?php echo $form->labelEx($model,'tipo'); ?>
		</div>
		<div class="column medium-10">
			<?php echo $form->dropDownList($model,'tipo',$tipos, array('empty' => '(Selecione um tipo de Macro)','class'=>'medium-4')); ?>
		</div>
	</div>

	<div class="sh-row">
		<div class="column medium-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar',array("class"=>"btn btn-success")); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
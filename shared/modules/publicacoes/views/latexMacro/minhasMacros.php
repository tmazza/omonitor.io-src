<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 08/07/15
 * Time: 00:01
 */
$this->pageTitle = 'Macros de Latex';
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('topico/listar')),
    ShCode::makeItem('Nova Macro', $this->createUrl('LatexMacro/create')),
);
?>
<?php if (count($macros) > 0): ?>
   <?php
        foreach($macros as $macro)
            echo "<div style='display: none;'>".$macro->getMacroMathJaxSintax($macro->valor,$macro->alias,$macro->tipo)."<br>"."</div>";
    ?>
    <table class="table">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Alias</th>
                <th>Valor</th>
                <th><!--Preview--></th>
                <th></th>
            </tr>
        </thead>
    <tbody>
    <?php foreach ($macros as $m): ?>
           <tr>
               <td><?=$m->tipo?></td>
               <td><?=$m->alias?></td>
               <td><?=$m->valor?></td>
               <td><?=$m->getPreview($m->alias)?></td>
               <td><?php
                       echo Chtml::link("Editar",$this->createUrl('LatexMacro/update',array('id'=>$m->id)),array("class"=>"btn btn-primary"))." ".
                            Chtml::link("Excluir",$this->createUrl('LatexMacro/delete',array('id'=>$m->id)),array("class"=>"btn btn-danger"));
                   ?>
               </td>

           </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h5><span class="hint">Nenhuma Macro criada.</span></h5>
<?php endif; ?>

<?php

echo CHtml::ajaxLink("<i data-toggle='tooltip' title='Remover de favoritos' class='fa fa-star' style='color: #ffcc33'></i>", $this->createUrl('conteudo/desfavoritar', array(
            'id' => $t->id
        )), array(
    'beforeSend' => 'js: function(){ $("#fav-' . $t->id . '").html("<i class=\'fa fa-refresh fa-spin\'></i>"); }',
    'success' => 'js: function(html) { $("#fav-' . $t->id . '").html(html); }',
        ), array(
    'id' => 'desfavoritar-' . $t->id,
));


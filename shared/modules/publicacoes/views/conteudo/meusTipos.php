<?php
$this->pageTitle = 'Tipos de camadas';
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('topico/listar')),
    ShCode::makeItem('Novo tipo', $this->createUrl('camadas/nova'))
);
?>
<table class="table">
    <tr>
        <th>
            <?php
            echo CHtml::link('<i class="fa fa-question-circle"></i>', '#', array(
                'onClick' => '$("#hide-info").slideToggle(); return false;',
            ));
            ?>
        </th>
        <th colspan="3">
    <div id="hide-info" style="display: none;">
        Camadas marcadas como favoritas ficarão visíveis na edição. As demais aparecerão ao clicar em:  <button class="btn btn-xs btn-primary">+</button>
    </div>
</th>
</tr>
<?php foreach ($tiposDaAplicacao as $tc): ?>
    <tr>
        <td id="fav-<?= $tc->id; ?>">
            <?php
            if (in_array($tc->id, $fave)) {
                $this->renderPartial('_favorito', array(
                    't' => $tc,
                ));
            } else {
                $this->renderPartial('_naoFavorito', array(
                    't' => $tc,
                ));
            }
            ?>
        </td>
        <td colspan="2">
            <?= $tc->nome; ?>
        </td>
        <td>
            <?= CHtml::link('Ver todos', $this->createUrl('conteudo/VerTipo', array('tipo' => $tc->id))) ?>
        </td>
    </tr>    
<?php endforeach; ?>

<?php foreach ($tiposDoAutor as $tc): ?>
    <tr>
        <td id="fav-<?= $tc->id; ?>">
            <?php
            if (in_array($tc->id, $fave)) {
                $this->renderPartial('_favorito', array(
                    't' => $tc,
                ));
            } else {
                $this->renderPartial('_naoFavorito', array(
                    't' => $tc,
                ));
            }
            ?>
        </td>
        <td>
            <?= $tc->nome; ?>
        </td>
        <td>
            <?php if ($tc->categoria == 'codigo'): ?>
                <?= CHtml::link('Editar', $this->createUrl('camadas/editarCodigo', array('id' => $tc->id))) ?>
            <?php else: ?>
                <?= CHtml::link('Editar', $this->createUrl('camadas/editar', array('id' => $tc->id))) ?>
            <?php endif; ?>
        </td>
        <td>
            <?= CHtml::link('Ver todos', $this->createUrl('conteudo/VerTipo', array('tipo' => $tc->id))) ?>
        </td>

    </tr>    
<?php endforeach; ?>

</table>
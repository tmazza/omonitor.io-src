<?php

$this->pageTitle = 'Camadas do tipo ';
$this->breadcrumbs += array(
    'Minhas camadas' => '#',
);
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('conteudo/meusTipos')),
);

$this->widget('shared.modules.publicacoes.widgets.ListaCamadas.ListaCamadas', array(
    'tipo' => $tipo,
));

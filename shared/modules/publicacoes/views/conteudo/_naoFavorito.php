<?php

echo CHtml::ajaxLink("<i data-toggle='tooltip' title='Marcar como favorito' class='fa fa-star-o' style='color: #ffcc33'></i>", $this->createUrl('conteudo/favoritar', array(
            'id' => $t->id
        )), array(
    'beforeSend' => 'js: function(){ $("#fav-' . $t->id . '").html("<i class=\'fa fa-refresh fa-spin\'></i>"); }',
    'success' => 'js: function(html) { $("#fav-' . $t->id . '").html(html); }',
        ), array(
    'id' => 'favoritar-' . $t->id,
));



<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'templates-codigo-_form-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<div class="sh-row">
    <div class="medium-12 column">
        <?php echo $form->error($model, 'nome'); ?><br>
        <?php echo $form->error($model, 'template'); ?><br>
    </div>
</div>
<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'nome', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'template'); ?>
    </div>
    <div class="medium-7 column">
        <textarea name="TemplatesCodigo[template]" id="TemplatesCodigo-template"  class="sage"><?= stripslashes($model->template); ?></textarea>
        <?php
        echo CHtml::ajaxLink('Carregar preview', $this->createUrl('template/preview'), array(
            'update' => '#preview-template',
            'type' => 'POST',
            'data' => array('code' => 'js: $("#TemplatesCodigo-template").val() '),
            'beforeSend' => 'js: function() { $("#preview-template").html("Carregando...") } ',
                ), array('class' => 'btn btn-sm btn-primary', 'id' => 'main-update'))
        ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<?php $this->endWidget(); ?>
<hr>
<div id="preview-template"></div>
<hr>

<script>
function getParametros(){
   data = {};
   $('#params input').each(function(){
       data[$(this).attr('data-id')] = $(this).val();
   });
   return JSON.stringify(data);
}
</script>
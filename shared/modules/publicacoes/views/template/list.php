<?php
$this->pageTitle = 'Widgets';
$this->menuContexto = array(
    ShCode::makeItem('Novo widget', $this->createUrl('template/novo')),
);
?>
<?php if (count($templates) > 0): ?>
    <ul class="nav">
        <?php
        foreach ($templates as $t) {
            echo '<li>';
            echo CHtml::link($t->nome, $this->createUrl('template/editar', array('id' => $t->id)));
            echo '</li>';
        }
        ?>
    </ul>
<?php else: ?>
    <p class="hint">Nenhum widgets criado.</p>
<?php endif; ?>

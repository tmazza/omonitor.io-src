<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('template/list')),
);
?>
<div class="sh-row">
    <div class="medium-12 column">
        <?php
        $this->renderPartial('_form', array(
            'model' => $template,
        ));
        ?>
    </div>
</div>
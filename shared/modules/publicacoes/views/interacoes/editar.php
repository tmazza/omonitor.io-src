<?php
$this->pageTitle = $topico->nome;
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    $topico->nome => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)),
    'Interações',
);
if (count($topico->interacoes) > 0){
    $this->menuContexto[] = array('label' => 'Cancelar edição de interações', 'url' => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)), 'itemOptions' => array('confirm' => 'As alterações relizadas nas interações deste tópico não serão salvas!'));
    $this->menuContexto[] = array('label' => 'Salvar conteúdo editado', 'url' => '#', 'itemOptions' => array('onclick' => '$("#edicao-interacoes-form").submit()', 'class' => 'green'));
} else {
    $this->menuContexto[] = array('label' => 'Voltar para menu de edição', 'url' => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)));
}
?>
<?php if (count($topico->interacoes) > 0): ?>
    <?php echo CHtml::beginForm('', 'POST', array('id' => 'edicao-interacoes-form')); ?>
    <div>
        <?php echo $conteudo; ?>
    </div>
    <div class="textCenter">
        <?php echo CHtml::submitButton('Salvar', array('class' => 'btn btn-success span6')); ?>
    </div>
    <?php echo CHtml::endForm(); ?>
<?php else: ?>
    <h5>Este tópico não possue interações. Para incluir interações acesse o <?= CHtml::link('Editor de Conteúdo', $this->createUrl('topico/editar', array('id' => $topico->id))) ?></h5>
<?php endif; ?>
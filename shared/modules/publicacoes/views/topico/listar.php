<?php
$this->pageTitle = 'Tópicos';
$this->breadcrumbs += array(
    'Tópicos'
);
$this->menuContexto = array(
    ShCode::makeItem('Novo tópico', $this->createUrl('topico/novo')),
    ShCode::makeItem('Tipos de conteúdo', $this->createUrl('conteudo/meusTipos')),
    ShCode::makeItem('LaTex Macros', $this->createUrl('latexMacro/MinhasMacros')),
);
?>
<div id="topicosLista">
    <div class="sh-row">
        <?php
        echo CHtml::textField("filtroTopicos", isset($_GET['f']) ? $_GET['f'] : null, array(
            'placeholder' => 'Locallizar tópico...',
            'id' => 's',
            'class' => 'form-control input-lg'));
        ?>
    </div>
    <?php
    $this->renderPartial('_listaTopicos', array(
        'topicos' => $topicos,
        'colab' => false,
    ));
    ?>
</div>
<?php
$this->pageTitle = 'Novo tópico';
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    'Novo',
);

$this->menuContexto = array(
    array('label' => "Cancelar", 'url' => array('topico/listar')),
);
?>
<?php $this->renderPartial('_form', array('model' => $model)); ?>
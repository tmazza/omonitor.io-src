<?php
$this->pageTitle = 'Tópicos compartilhados comigo';
$this->breadcrumbs += array(
    'Tópicos'
);
$this->menuContexto = array();
?>
<div id="topicosLista">
    <div class="sh-row">
        <?php
        echo CHtml::textField("filtroTopicos", isset($_GET['f']) ? $_GET['f'] : null, array(
            'placeholder' => 'Locallizar tópico...',
            'id' => 's',
            'class' => 'form-control input-lg'));
        ?>
    </div>
    <?php
    $this->renderPartial('_listaTopicos', array(
        'topicos' => $topicos,
        'colab' => true,
        'colabs' => $colabs,
    ));
    ?>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'topico-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->textField($model, 'nome', array('class' => 'medium-12')); ?>
    </div>
    <div class="column medium-6">
        <?php echo $form->error($model, 'nome'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'span6 btn btn-success')); ?>
    </div>
</div>
<?php echo $form->error($model, 'nome'); ?>
<?php $this->endWidget(); ?>

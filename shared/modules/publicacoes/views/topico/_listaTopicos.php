<?php if (count($topicos) > 0): ?>  
    <br><br>
    <ul class="small-block-grid-1 large-block-grid-1 medium-block-grid-1">
        <?php foreach ($topicos as $top): ?>
		<?php if(!is_null($top)): ?>
            <li>
                <div class="box">
                    <div class="box-header">
                        <?php
                        echo $top->publicado == '1' ? '<i data-toggle="tooltip" title="Publicado" class="fa fa-eye green"></i>' : '<i data-toggle="tooltip" title="Não publicado" class="fa fa-eye-slash red"></i>';
                        echo $top->comDestaque == '1' ? '<i data-toggle="tooltip" title="Com destaque" class="fa fa-star" style="color: #ffcc33"></i>' : '<i class="fa fa-star-o" data-toggle="tooltip" title="Sem destaque" style="color: #ffcc33"></i>';
                        echo $top->emCriacao == '1' ? '<i data-toggle="tooltip" title="Em edição" class="fa fa-edit red"></i>' : '<i data-toggle="tooltip" title="Finalizado" class="fa fa-check green"></i>';
                        ?>
                        <?php if(!$colab || ($colab && isset($colabs[$top->id])) && $colabs[$top->id]->permissao == PubliColaborador::PermissaoEditar): ?>
                            <?= CHtml::link('<i class="fa fa-edit"></i> Editar', $this->createUrl('edicao/index', array('id' => $top->id)), array('class' => 'btn btn-sm btn-default')) ?>
                            <?php if (count($top->interacoes) > 0): ?>
                                &nbsp;<?= CHtml::link('<i class="fa fa-edit"></i> interações', $this->createUrl('interacoes/editar', array('id' => $top->id)), array('class' => 'btn btn-sm btn-default')) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="box-tools pull-right">
                            <span class="hint">Editado em: <?= date("d/m/Y H:i:s", $top->lastUpdate); ?></span>
                            &nbsp;
                            <?= CHtml::link('Ver', ShCode::getModUrl('meusCursos', 'leitura', 'index', array('id' => $top->id, 'rt' => ShCode::geraLinkRetorno(array('f')))), array('class' => 'btn btn-sm btn-default')); ?>
                              
                            <?php if(!$colab): ?>
                                <?php echo CHtml::link('Incluir colaborador <i class="fa fa-share-alt"></i>', 
                                    ShCode::getModUrl('publicacoes', 'compartilhar', 'index', array(
                                        'id' => $top->id, 
                                        'rt' => ShCode::geraLinkRetorno(array('f')))), array(
                                    'class' => 'btn btn-sm btn-default',
                                )); ?>
                                &nbsp;
                            <?php endif; ?>

                            <?= CHtml::link('Visualizar no site <b title="Abrirá em outra janela">&#8599;</b>', Yii::app()->baseUrl . '/topico/ver/id/' . $top->id, array('target' => '_blank', 'class' => 'btn btn-default btn-sm')); ?>
                            
                            <?php if(!$colab): ?>
                                <button type="button" class="btn btn-sm btn-primary" onclick="$(this).parent().parent().parent().find('.hd-op').slideToggle();"><i class="fa fa-gear"></i></button>
                            <?php endif; ?>
                        </div>
                        <h3 class="s"><?= $top->nome ?></h3>
                        <span class="i hidden"><?= $top->id ?></span>
                    </div>

                    <?php if(!$colab): ?>
                        <div class="box-body hd-op">
                            <ul class="nav nav-pills nav-stacked">
                                <?php if ($this->module->habilitaRelacionamento): ?>
                                    <li>
                                        <?= CHtml::link('Tópicos relacionados', $this->createUrl('relacionados/editar', array('id' => $top->id))); ?>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <?= CHtml::link('Alterar título', $this->createUrl('topico/atualizarNome', array('id' => $top->id)), array()); ?>
                                </li>
                                <li>
                                    <?=
                                    CHtml::link('Excluir', $this->createUrl('topico/excluir', array('id' => $top->id)), array(
                                        'confirm' => "Confirma exclusão do tópico '{$top->nome}' ?",
                                        'id' => 'excluir-' . $top->id,
                                    ));
                                    ?>
                                </li>
                            </ul>
                        </div>
                        <div class="box-footer">
                            <div class="sh-row">
                                <div class="medium-12 columns"><span class="hint">Publicado?</span>
                                    <?php
                                    $this->widget('publicacoes.widgets.LinkSimNao.LinkSimNao', array(
                                        'boolean' => $top->publicado == '1',
                                        'urlTrue' => $this->createUrl('topico/despublicar', array('id' => $top->id)),
                                        'urlFalse' => $this->createUrl('topico/publicar', array('id' => $top->id)),
                                        'textoLinkTrue' => '<i class="fa fa-eye-slash"></i> despublicar',
                                        'textoLinkFalse' => '<i class="fa fa-eye"></i> publicar',
                                    ));
                                    ?>
                                </div>
                            </div>
                            <div class="sh-row">
                                <div class="medium-12 columns"><span class="hint">Destaque?</span>
                                    <?php
                                    $this->widget('publicacoes.widgets.LinkSimNao.LinkSimNao', array(
                                        'boolean' => $top->comDestaque == '1',
                                        'urlFalse' => $this->createUrl('topico/comDestaque', array('id' => $top->id)),
                                        'urlTrue' => $this->createUrl('topico/semDestaque', array('id' => $top->id)),
                                        'textoLinkTrue' => '<i class="fa fa-star-o"></i>remover destaque',
                                        'textoLinkFalse' => '<i class="fa fa-star"></i>destacar',
                                    ));
                                    ?>
                                </div>
                            </div>
                            <div class="sh-row">
                                <div class="medium-12 columns"><span class="hint">Finalizado?</span>
                                    <?php
                                    $this->widget('publicacoes.widgets.LinkSimNao.LinkSimNao', array(
                                        'boolean' => $top->emCriacao == '0',
                                        'urlTrue' => $this->createUrl('topico/emCriacao', array('id' => $top->id)),
                                        'urlFalse' => $this->createUrl('topico/jaCriado', array('id' => $top->id)),
                                        'textoLinkFalse' => '<i class="fa fa-check"></i>marcar como criado',
                                        'textoLinkTrue' => '<i class="fa fa-edit"></i>marcar como esboço',
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
			<?php if(isset($top->user)): ?>
                        Autor: <?=$top->user->nome?>
			<?php endif; ?>

                    <?php endif; ?>
                </div>
            </li>
	<?php endif; ?>
        <?php endforeach; ?>
</ul>
<?php else: ?>
    <h4 class='hint'>Nenhum tópico de sua autoria.</h4>    
<?php endif; ?>
<script>
    $(document).ready(filtraTop('.i'));
    $('#s').keyup(function() {
        filtraTop('.s');
    });

    function filtraTop(classe) {
        $(classe).each(function() {
            var n = $(this).text().toLowerCase();
            if (normalizaStr(n).indexOf(normalizaStr($('#s').val().toLowerCase())) === -1) {
                $(this).parent().parent().parent().fadeOut(0);
            } else {
                $(this).parent().parent().parent().fadeIn(0);
            }
        });
    }

    function normalizaStr(str) {
        str = str.replace(/[àáâãäå]/, "a");
        str = str.replace(/[eéèëê]/, "e");
        str = str.replace(/[iíìïî]/, "i");
        str = str.replace(/[oóòõöô]/, "o");
        str = str.replace(/[uúùüû]/, "u");
        str = str.replace(/[ç]/, "c");

        // o resto

        return str.replace(/[^a-z0-9]/gi, '');
    }

</script>
<style>
    .columns {
        /*        border: 1px solid #ddd;*/
    }
    .topico {
        margin: 10px 0px;
        border: 1px solid #ddd;
    }
    .hd-op {
        display: none;
    }
</style>


<?php
$this->breadcrumbs += array(
    $topico->nome => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)),
    'Menu de edição',
);
$this->menuContexto = array(
    array('label' => 'Voltar para lista de tópicos', 'url' => array('topico/listar'), 'items' => array()),
    array('label' => 'Editar conteúdo', 'url' => array('edicao/index', 'id' => $topico->id), 'items' => array()),
    array('label' => 'Editar interações', 'url' => array('interacoes/editar', 'id' => $topico->id), 'items' => array()),
    array('label' => 'Tópicos relacionados', 'url' => array('relacionados/editar', 'id' => $topico->id), 'items' => array()),
);
?>
<h1><span class="hint">Tópico:</span> <?= $topico->nome; ?></h1>
<?= CHtml::link('Visualizar no site <span class="f16">&#8599;</span>',  Yii::app()->baseUrl . '/topico/ver/id/' . $topico->id); ?>
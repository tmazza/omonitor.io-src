<?php

$this->pageTitle = 'Atualizar título: ' . $model->nome;
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    'Atualizar',
);

$this->menuContexto = array(
    array('label' => "Cancelar", 'url' => array('topico/listar')),
);
?>
<?php $this->renderPartial('_form', array('model' => $model)); ?>
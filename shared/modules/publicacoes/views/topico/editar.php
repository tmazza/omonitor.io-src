<?php
$topico = $camada->topico;
$this->pageTitle = $topico->nome;
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    $topico->nome => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)),
    'Editar',
);
$this->menuContexto[] = array('label' => 'Cancelar edição de conteúdo', 'url' => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)), 'linkOptions' => array('confirm' => 'As alterações relizadas no conteúdo deste tópico não serão salvas!'));

$this->widget('shared.widgets.MathEditor.MathEditor', array(
    'camada' => $camada,
));
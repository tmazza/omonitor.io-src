<?php
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    $model->nome => $this->createUrl('topico/menuEdicao', array('id' => $model->id)),
    'Compartilhar',
);
$this->menuContexto[] = array('label' => 'Cancelar', 'url' => $this->createUrl('compartilhar/index', array('id' => $model->id)));
?>
<?=CHtml::beginForm()?>
	Compartilhar com <?=CHtml::dropDownList('user','',CHtml::listData($users,'id','nome'),[
		'prompt' => 'Selecione...',
	])?>
	<br>
	Permissão 
	<?=CHtml::dropDownList('permissao','',[
		PubliColaborador::PermissaoVer => PubliColaborador::getLabelPermissao(PubliColaborador::PermissaoVer),
		#PubliColaborador::PermissaoEditar => PubliColaborador::getLabelPermissao(PubliColaborador::PermissaoEditar),
	],[
	]);?>
	<br><br>
	<?=CHtml::submitButton('Compartilhar',[])?>
<?=CHtml::endForm()?>
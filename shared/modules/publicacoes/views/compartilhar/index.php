<?php
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    $model->nome => $this->createUrl('topico/menuEdicao', array('id' => $model->id)),
    'Compartilhar',
);
$this->menuContexto[] = array('label' => 'Voltar', 'url' => $this->createUrl('topico/listar'));
$this->menuContexto[] = array('label' => 'Adicionar colaborador', 'url' => $this->createUrl('compartilhar/add', array('id' => $model->id)));


$colaboradores = $model->colaboradores;
if(count($colaboradores) > 0){
	foreach ($colaboradores as $c) {
		echo '<b>' .$c->user->nome . '</b> pode <b>' . PubliColaborador::getLabelPermissao($c->permissao) . '</b>';
		echo CHtml::link(' Retirar permissão',$this->createUrl('compartilhar/remove',[
			'id' => $c->id,
		]));
		echo '<br>';
	}
} else {
	echo 'Nenhum colaborador neste tópico.';
}
?>


<?php
$this->breadcrumbs += array(
    'Tópicos' => $this->createUrl('topico/listar'),
    $topico->nome => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)),
    'Relacionados',
);
$this->menuContexto[] = array('label' => 'Voltar para menu de edição', 'url' => $this->createUrl('topico/menuEdicao', array('id' => $topico->id)));
?>

<h1><?= $topico->nome; ?><span class="hint">:topicos relacionados</span></h1>
<?php
$relacionados = CHtml::listData($topico->relacionados, 'filho_id', 'pai_id');

echo '<table class="table">';
echo "<tr><th>Outros tópicos</th><th>Relacionado?</th></tr>";
foreach ($todosOsTopicos as $t) {
    echo "<tr>";
    echo "<td>{$t->nome}</td>";
    echo "<td>";
    $this->widget('publicacoes.widgets.LinkSimNao.LinkSimNao', array(
        'boolean' => in_array($t->id, array_keys($relacionados)),
        'urlTrue' => $this->createUrl('relacionados/desfazer', array('pai' => $topico->id, 'filho' => $t->id,)),
        'urlFalse' => $this->createUrl('relacionados/criar', array('pai' => $topico->id, 'filho' => $t->id,)),
        'textoLinkTrue' => ' desfazer',
        'textoLinkFalse' => ' relacionar',
    ));
    echo "</td>";
    echo "</tr>";
}
echo '</table>';

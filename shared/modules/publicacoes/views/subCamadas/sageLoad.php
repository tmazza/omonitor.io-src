<div class="edicao-sage">
    <?php if ($camada->tipo->view_name == 'sageLoad'): ?>
        <h4 class="textCenter">Interação</h4>
        <select  name="sagePrincipal[<?php echo $camada->id; ?>]"  >
            <option value="N" <?php echo is_null($camada->isDestaque) ? null : ($camada->isDestaque->valor == 'S' ? 'selected' : null); ?>>Somente parte do conteúdo</option>
            <option value="S" <?php echo is_null($camada->isDestaque) ? null : ($camada->isDestaque->valor == 'S' ? 'selected' : null); ?>>Com destaque</option>
        </select>
        <input 
            type="text" 
            name="sageTitle[<?php echo $camada->id; ?>]"  
            class="edit-sage-title" 
            placeholder="Titulo padrão" 
            value = "<?php echo is_null($camada->titulo) ? null : $camada->titulo->valor; ?>" />
        <textarea 
            style="width: 99%"
            name="sageResumo[<?php echo $camada->id; ?>]"  
            placeholder="Texto explicativo para interação, visivel somente nas 'principais interações'" 
            ><?php echo is_null($camada->descricao) ? null : $camada->descricao->valor; ?></textarea>
        <br>
    <?php elseif ($camada->tipo->view_name == 'questao'): ?>
        <h4 class="textCenter">Questão</h4>
    <?php endif; ?>
    <div class="sage-em-edicao" id="edit-sage-<?php echo $camada->id; ?>">
        <textarea class="sage-edit" name="sagecell[<?php echo $camada->id; ?>]"><?php echo stripslashes($camada->conteudo); ?></textarea>
    </div>
</div>
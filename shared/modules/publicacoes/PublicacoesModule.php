<?php

class PublicacoesModule extends CWebModule {

    // Se é possível relacionar tópicos
    public $habilitaRelacionamento = true;
    // Layout mais utilizado
    public $layoutSemMenu = 'column1';
    // Utilizado no menu edição
    public $layoutComMenu = 'raw';

    public function init() {
        $this->setImport(array(
            'publicacoes.components.*',
            'publicacoes.models.*',
            'publicacoes.helpers.*',
            'shared.behaviors.*',
            'shared.extensions.imperavi-redactor-widget.*'
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            'topico.*' => 'edicaoTopico',
            'relacionados.*' => 'edicaoTopico',
            'interacoes.*' => 'edicaoTopico',
            'conteudo.*' => 'edicaoTopico',
            'camadas.*' => 'edicaoTopico',
            'compartilhar.*' => 'edicaoTopico',
            'template.*' => false,
            'edicao.*' => false,
            'latexmacro.*' => false,
        );
    }

}

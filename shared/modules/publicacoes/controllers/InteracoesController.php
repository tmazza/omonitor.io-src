<?php

/**
 * Description of InteracoesController
 *
 * @author tiago
 */
class InteracoesController extends PublicacoesController {

    /**
     * Edição de interações
     * @param type $id
     */
    public function actionEditar($id) {
        $topico = $this->getTopico($id);
        if (isset($_POST['sagecell'])) {
            $sages = $_POST['sagecell'];
            $titulos = isset($_POST['sageTitle']) ? $_POST['sageTitle'] : array();
            $destaques = isset($_POST['sagePrincipal']) ? $_POST['sagePrincipal'] : array();
            $resumos = isset($_POST['sageResumo']) ? $_POST['sageResumo'] : array();

            $qtd = 0;
            foreach ($sages as $id => $sage) {
                $qtd += PubliCamada::model()
                        ->doAutor()
                        ->updateByPk($id, array(
                    'conteudo' => addslashes($sage),
                    'last_update' => time(),
                ));
                // Trata título da camada
                $this->trataApendice($titulos, $id, CamadaApendice::TipoTitulo);
                $this->trataApendice($resumos, $id, CamadaApendice::TipoResumoInteracao);
                $this->trataApendice($destaques, $id, CamadaApendice::TipoInteracaoComDestaque);
            }
            if ($qtd > 0) {
                Yii::app()->user->setFlash('alert-success', 'Interações de tópico "' . $topico->nome . '" atualizada(s).');
            } else {
                Yii::app()->user->setFlash('alert', 'Nenhuma interação atualizada.');
            }
            $this->redirect($this->createUrl('topico/listar', array(
                        'f' => $topico->id,
            )));
        }

        $conteudo = $topico->camadaRaiz->expandeCamadasEdicaoInteracoes($topico->camadaRaiz->conteudo);

        $this->render('editar', array(
            'topico' => $topico,
            'conteudo' => $conteudo,
        ));
    }

    /**
     * Cria, atualiza, exclui apendices de camadas sendo editadas nas interações
     * @param type $dados
     * @param type $camadaID
     * @param type $tipo
     */
    private function trataApendice($dados, $camadaID, $tipo) {
        if (isset($dados[$camadaID]) && $dados[$camadaID] !== '') {
            $valor = $dados[$camadaID];
            $modelTitulo = PubliCamadaApendice::model()->findByAttributes(array(
                'camada_id' => $camadaID,
                'tipo' => $tipo,
            ));
            if (is_null($modelTitulo)) {
                $novoApendice = new PubliCamadaApendice();
                $novoApendice->user_id = Yii::app()->user->id;
                $novoApendice->camada_id = $camadaID;
                $novoApendice->tipo = $tipo;
                $novoApendice->valor = $valor;
                $novoApendice->save();
            } else {
                $modelTitulo->valor = $valor;
                $modelTitulo->update(array('valor'));
            }
        } else {
            PubliCamadaApendice::model()->deleteAllByAttributes(array(
                'camada_id' => $camadaID,
                'tipo' => $tipo,
            ));
        }
    }

}

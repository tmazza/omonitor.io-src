<?php

/**
 * Description of Camadas
 *
 * @author tiago
 */
class CamadasController extends PublicacoesController {

    public function actionNova() {
        $tipoCamada = new PubliCamadaTipo();

        if (isset($_POST['PubliCamadaTipo'])) {
            $tipoCamada->attributes = $_POST['PubliCamadaTipo'];
            $tipoCamada->user_id = Yii::app()->user->id;
            $tipoCamada->editavel = 1;
            $tipoCamada->recursivo = 1;
            $tipoCamada->visibilidade_edicao = CamadaTipo::VisiEditPessoal;
            $tipoCamada->view_name = 'default';
            $tipoCamada->categoria = 'texto';
            if ($tipoCamada->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tipo criado.');
                $this->redirect($this->createUrl('conteudo/meusTipos'));
            }
        }

        $this->render('nova', array(
            'model' => $tipoCamada,
        ));
    }

    /**
     * Edição de tipo de camada. 
     * Forma de apresentação e texto default
     * @param type $id
     */
    public function actionEditar($id) {
        $tipoCamada = $this->getTipoCamada($id);

        if (isset($_POST['CamadaTipo'])) {
            $tipoCamada->attributes = $_POST['CamadaTipo'];
            if ($tipoCamada->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tipo atualizado.');
                $this->redirect($this->createUrl('conteudo/meusTipos'));
            }
        }

        $this->render('editar', array(
            'model' => $tipoCamada,
        ));
    }

    public function actionEditarCodigo($id) {
        $tipoCamada = $this->getTipoCamada($id);

        if (isset($_POST['codigo'])) {
            $tipoCamada->codigo = addslashes($_POST['codigo']);
            if ($tipoCamada->update(array('codigo'))) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Código atualizado.');
                $this->redirect($this->createUrl('conteudo/meusTipos'));
            }
        }

        $this->render('editarCodigo', array(
            'model' => $tipoCamada,
        ));
    }

    /**
     * Busca tipo de camada do autor
     * @param type $id
     * @return type
     */
    private function getTipoCamada($id) {
        $tipoCamada = PubliCamadaTipo::model()->doAutor()->findByPk($id);
        if (is_null($tipoCamada)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Tipo de camada não encontrado.');
            $this->redirect($this->createUrl('topico/listar'));
        }
        return $tipoCamada;
    }

}

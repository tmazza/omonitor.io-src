<?php

/**
 * Description of TemplateController
 *
 * @author tiago
 */
class TemplateController extends PublicacoesController {

    public function actionList() {
        $templates = TemplatesCodigo::model()->doAutor()->findAll();
        $this->render('list', array(
            'templates' => $templates,
        ));
    }

    public function actionNovo() {
        $template = new TemplatesCodigo();

        if (isset($_POST['TemplatesCodigo'])) {
            $this->salvaTemplate($template, $_POST['TemplatesCodigo']);
        }

        $this->render('novo', array('template' => $template));
    }

    public function actionEditar($id) {
        $template = TemplatesCodigo::model()->doAutor()->   findByPk($id);

        if (isset($_POST['TemplatesCodigo'])) {
            $this->salvaTemplate($template, $_POST['TemplatesCodigo']);
        }

        $this->render('editar', array('template' => $template));
    }

    private function salvaTemplate(&$template, $data) {
        $template->nome = $data['nome'];
        $template->template = addslashes($data['template']);
        $template->user_id = Yii::app()->user->id;
        if ($template->validate()) {
            if ($template->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Template criado.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Erro ao criar template.');
            }
            $this->redirect($this->createUrl('template/list'));
        }
    }

    public function actionPreview() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->renderPartial('_preview', array(
                'template' => $_POST['code'],
                'data' => $data,
                'conteudo' => ShView::mergeDataToTemplate($_POST['code'], $data),
                    ), true, true);
        }
    }

}

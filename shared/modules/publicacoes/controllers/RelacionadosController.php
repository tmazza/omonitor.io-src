<?php

/**
 * Edição de relacacionamento entre tópicos.
 * @author tiago
 */
class RelacionadosController extends PublicacoesController {

    protected function beforeAction($action) {
        if($this->module->habilitaRelacionamento){
            return parent::beforeAction($action);
        } else {
            ShSeg::bloqueiaAcesso();
        }
    }

    /**
     * Lista tópicos relacionados/à relacionar
     * @param type $id
     */
    public function actionEditar($id) {
        $topico = $this->getTopico($id);
        $todosOsTopicos = PubliTopico::model()
                ->doAutor()
                ->findAll("id != " . $id);
        $this->render('editar', array(
            'topico' => $topico,
            'todosOsTopicos' => $todosOsTopicos,
        ));
    }

    /**
     * Cria um nova relação
     * @param type $pai
     * @param type $filho
     * @param type $tipo - simples ou reflexivo. Simples: $pai -> $filho. Reflexivo: Simples + ($filho -> $pai)
     */
    public function actionCriar($pai, $filho, $tipo = 'simples') {
        $tiposValidos = array('reflexivo', 'simples');
        $tipo = in_array($tipo, $tiposValidos) ? $tipo : 'simples';

        $modelPai = $this->getTopico($pai);
        $modelFilho = $this->getTopico($filho);

        if ($modelPai->relacionarCom($modelFilho, $tipo)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópicos relacionados.');
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao criar relação.');
        }

        $this->redirect($this->createUrl('relacionados/editar', array(
                    'id' => $modelPai->id,
        )));
    }

    /**
     * Desfaz relação existente.
     * @param type $pai
     * @param type $filho
     * @param type $tipo
     */
    public function actionDesfazer($pai, $filho, $tipo = 'simples') {
        $tiposValidos = array('reflexivo', 'simples');
        $tipo = in_array($tipo, $tiposValidos) ? $tipo : 'simples';

        $modelPai = $this->getTopico($pai);
        $modelFilho = $this->getTopico($filho);

        $modelPai->desrelacionarDe($modelFilho, $tipo);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópicos desrelacionados.');

        $this->redirect($this->createUrl('relacionados/editar', array(
                    'id' => $modelPai->id,
        )));
    }

}

<?php

class LatexMacroController extends PublicacoesController
{


	/**
	 * painel de Macros do Usuario
	 */
	public function actionMinhasMacros()
	{
		$macros = LatexMacro::model()->doAutor()->findAll();
		$this->render("minhasMacros",array("macros"=>$macros));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new LatexMacro;
		$tipos = $model->getMacros();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LatexMacro']))
		{
			$model->attributes=$_POST['LatexMacro'];
			$model->user_id = Yii::app()->user->id;

			if($model->save())
				$this->redirect(array('minhasMacros'));
		}

		$this->render('create',array(
			'model'=>$model,'tipos'=>$tipos
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$tipos = $model::getMacros();


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LatexMacro']))
		{
			$model->attributes=$_POST['LatexMacro'];
			if($model->save())
				$this->redirect(array('minhasMacros'));
		}

		$this->render('update',array(
			'model'=>$model,'tipos'=>$tipos
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('minhasMacros'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LatexMacro');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LatexMacro the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LatexMacro::model()->doAutor()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LatexMacro $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='latex-macro-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

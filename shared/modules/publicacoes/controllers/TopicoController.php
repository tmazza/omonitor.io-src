<?php
class TopicoController extends PublicacoesController {

    protected function beforeAction($action) {
        if ($this->action->id == 'editar') {
            $this->processaLatex = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lista tópicos do autor para edição
     */
    public function actionListar() {
        $topicos = PubliTopico::model()->doAutor()->findAll();
        $this->render('listar', array(
            'topicos' => $topicos,
        ));
    }


    /**
     * Lista tópicos do autor para edição
     */
    public function actionListarComp() {
        $colabs = PubliColaborador::model()->findAll([
            'condition' => 'user_id =  '. Yii::app()->user->id . ' AND status != ' . PubliColaborador::StatusRevogada,
            'index' => 'topico_id',
        ]);
        $topicos = [];
        foreach ($colabs as $c) {
            $topicos[] = $c->topico;
        }
        $this->render('listarComp', array(
            'topicos' => $topicos,
            'colabs' => $colabs,
        ));
    }

    /**
     * Cria novo tópico.
     * Cria registro em Topico e em Camada
     */
    public function actionNovo() {
        $model = new PubliTopico;
        if (isset($_POST['PubliTopico'])) {
            $model->attributes = $_POST['PubliTopico'];
            $model->user_id = Yii::app()->user->id;
            if ($model->validate()) {
                $camadaID = $this->salvaNovoTopico($model);
                if ($camadaID === false) {
                    $this->redirect($this->createUrl('topico/listar'));
                } else {
                    $this->redirect($this->createUrl('edicao/index', array('id' => $model->id)));
                }
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Edição de conteúdo do tópico
     */
    public function actionEditar($id) {
        $topico = $this->getTopico($id);
        $camada = $topico->camadaRaiz;
        $camada->conteudo = $camada->expandeCamadasEdicao($camada->conteudo);
        $this->render('editar', array(
            'camada' => $camada,
        ));
    }

    /**
     * Carrega camadas
     */
    public function actionAjaxLoadConteudoCamada() {
        if (isset($_POST['id']) && isset($_POST['reuso'])) {
            $camada = PubliCamada::model()->doAutor()->findByPk($_POST['id']);
            if (is_null($camada)) {
                echo '';
            } else {
                $attrID = $_POST['reuso'] == 'true' ? 'id="' . $camada->id . '"' : '';
                $attrTipo = 'tipo="' . $camada->tipo_id . '"';


                // TODO: inificar templates de ediçaõ aqui no modelo PubliCamada
                if ($camada->tipo->categoria === 'sage') {
                    $content = "<div class='camada' contenteditable='false'  {$attrTipo} {$attrID}>";
                    $content .= "CONTEÚDO INTERATIVO: edição no original.";
                } else {
                    $content = "<div class='camada' {$attrTipo} {$attrID}>";
                    $content .= $camada->expandeCamadasEdicao($camada->conteudo);
                }
                $content .= "</div><br>";
                echo $content;
            }
        } else {
            return '';
        }
    }

    public function actionAjaxBuscaCamada($id) {
        $tipo = PubliCamadaTipo::model()->findByPk($id, "(de_aplicacao =1 OR user_id = '" . Yii::app()->user->id . "')");
        echo $this->renderPartial('shared.widgets.MathEditor.views.pieces._listaCamadas', array(
            'camadas' => $tipo->camadasDoAutor,
            'tipo' => $tipo,
                ), true);
    }

    /**
     * Cria formulario para preenchimento dos campos
     * @param type $id
     * @param type $data
     */
    public function actionAjaxTemplateParametros($id, $data = null, $target = null) {
        $template = TemplatesCodigo::model()->findByPk($id);
        $data = is_null($data) ? array() : json_decode($data, true);
        echo $this->renderPartial('shared.widgets.MathEditor.views.pieces._templateParametros', array(
            'template' => $template,
            'data' => $data,
            'target' => $target,
                ), true);
    }

    public function actionMergeDataTemplate() {
        if (isset($_POST['id']) && isset($_POST['data'])) {
            $camadaTipo = PubliCamadaTipo::model()->findByAttributes(array('categoria' => 'template'));
            $id = $_POST['id'];
            $data = $_POST['data'];
            $template = TemplatesCodigo::model()->findByPk($id);
            echo $this->renderPartial('shared.views.edicaoCamadas.template', array(
                'tipo' => $camadaTipo->id,
                'template' => $template,
                'data' => $data,
                'code' => ShView::mergeDataToTemplate(stripslashes($template->template), $data),
                    ), true);
        }
    }

    /**
     * Salva conteúdo editado de tópico
     */
    public function actionSalvarConteudo($r = 's') {
        if (isset($_POST['PubliCamada']['id']) && isset($_POST['PubliCamada']['conteudo'])) {
            $camadaBase = PubliCamada::model()
                    ->doAutor()
                    ->findByPk($_POST['PubliCamada']['id']);

            $camadaBase->conteudo = $_POST['PubliCamada']['conteudo'];
            // Save de camada analisa conteúdo separando subcamadas
            if (!$camadaBase->save()) {
                throw new Exception("Erro ao salvar camada, entre em contato com o suporte.");
            } else {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Conteúdo de "' . $camadaBase->topico->nome . '" salvo.');
                $camadaBase->topico->update(array('lastUpdate')); // Aciona behaviour
                if ($r === 's') {
                    if (count($camadaBase->topico->interacoes) > 0) {
                        $this->redirect($this->createUrl('interacoes/editar', array('id' => $camadaBase->topico->id)));
                    } else {
                        $this->redirect($this->createUrl('topico/listar', array('f' => $camadaBase->topico->id)));
                    }
                } else {
                    $this->redirect($this->createUrl('topico/editar', array('id' => $camadaBase->topico->id)));
                }
            }
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Requisição inválida.');
            $this->redirect($this->createUrl('topico/listar'));
        }
    }

    /**
     * Menu de edição de tópico
     * @param type $id
     */
    public function actionMenuEdicao($id) {
        $this->render('menuEdicao', array(
            'topico' => $this->getTopico($id),
        ));
    }

    /**
     * Atuliza nome do tópico
     * @param type $id
     */
    public function actionAtualizarNome($id) {
        $model = $this->getTopico($id);
        if (isset($_POST['PubliTopico']['nome'])) {
            $model->nome = $_POST['PubliTopico']['nome'];
            $model->save(true, array('nome', 'lastUpdate'));
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Nome de tópico atualizado para "' . $model->nome . '".');
            $this->redirect($this->createUrl('topico/listar'));
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Publica tópico
     * @param type $id
     */
    public function actionPublicar($id) {
        PubliTopico::model()->publicar($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico publicado.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Despublica tópico
     * @param type $id
     */
    public function actionDespublicar($id) {
        PubliTopico::model()->despublicar($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico despublicado.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Marca tópico como destacado
     * @param type $id
     */
    public function actionComDestaque($id) {
        PubliTopico::model()->colocarDestaque($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico destacado.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Retira detaque para tópico
     * @param type $id
     */
    public function actionEmCriacao($id) {
        PubliTopico::model()->marcarRascunho($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico marcado como esboço.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Marca tópico como destacado
     * @param type $id
     */
    public function actionJaCriado($id) {
        PubliTopico::model()->desmarcarRascunho($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico marcado como finalizado.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Retira detaque para tópico
     * @param type $id
     */
    public function actionSemDestaque($id) {
        PubliTopico::model()->removerDestaque($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Destaque removido de tópico.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Exclui tópico. (TODO: exclusao de topico_tax, camada, camada_apendice. Sendo somente a topico_tax exclusao real, demais lógica)
     * @param type $id
     */
    public function actionExcluir($id) {
        PubliTopico::model()->excluir($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico excluído.');
        $this->redirect($this->createUrl('topico/listar'));
    }

    /**
     * Retorna links para outros tópicos.
     * Utilizado pelo editor para mostrar tópicos como obção de link.
     */
    public function actionLinkParaOutrosTopicos() {
        $topicos = Topico::model()
                ->doAutor()
                ->findAll();
        $links = array();
        $links[] = array('name' => 'Tópicos...', 'url' => false);
        foreach ($topicos as $top) {
            $links[] = array(
                'name' => $top->nome,
                'url' => "{link-topico:{$top->id}}",
            );
        }
        echo json_encode($links);
    }

    /**
     * Salva novo tópico
     * @param type $model
     */
    private function salvaNovoTopico($model) {
        $transaction = Yii::app()->db->beginTransaction();
        $error = !$model->save();

        if (!$error) {
            $camadaID = $this->criaCamada($model->id);
            $error = !is_numeric($camadaID);
        }

        if ($error) {
            $transaction->rollback();
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Não foi possível criar o tópico "' . $model->nome . '"');
            return false;
        } else {
            $transaction->commit();
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tópico "' . $model->nome . '" criado.');
            return $camadaID;
        }
    }

    /**
     * Cria nova camada
     * @param type $topicoID
     * @return type
     */
    private function criaCamada($topicoID) {
        $camada = new PubliCamada();
        $camada->tipo_id = 0; // Raiz(Tópico)
        $camada->user_id = Yii::app()->user->id;
        $camada->topico_id = $topicoID;
        if (!$camada->save()) {
            return $camada->getErrors();
        } else {
            return $camada->id;
        }
    }

}

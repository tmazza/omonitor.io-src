<?php

class EdicaoController extends PublicacoesController {

    public function actions() {
        return array(
            'editor.' => array(
                'class' => 'shared.widgets.EditorCamadas.EditorCamadas',
            ),
            'loadType' => array(
                'class' => 'shared.actions.ParseString'
            ),
            'updateWidgetView' => array(
                'class' => 'shared.actions.UpdateWidgetView'
            ),
        );
    }

    public function actionIndex($id) {
        $topico = $this->getTopico($id);
        if(is_null($topico)){
            Yii::app()->user->setFlash('alert-error', 'Você não tem permissão de edição neste tópico.');
            $this->redirect($this->createUrl('topico/listar'));
        } else {
            $camada = $topico->camadaRaiz;
            $camada->conteudo = $camada->expandeCamadasEdicao($camada->conteudo);
            $this->render('index', array(
                'model' => $camada,
            ));
        }
    }

}

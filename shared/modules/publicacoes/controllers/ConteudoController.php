<?php

/**
 * Description of Explorar
 *
 * @author tiago
 */
class ConteudoController extends PublicacoesController {

    public function actions() {
        return array(
            'VerTipo' => 'shared.modules.publicacoes.components.actions.ListaCamadasAction',
        );
    }

    /**
     * Lista tipos de conteúdos
     */
    public function actionIndiceRemissivo() {
        $tipos = CamadaTipo::model()->findAll("recursivo = 0");
        $this->render('indiceRemissivo', array(
            'tipos' => $tipos,
        ));
    }

    /**
     * Lista todos os conteúdos do tipo $tipo criados pelo autor
     */
    public function actionVerTipo($tipo) {
        $this->layout = $this->layoutComMenu;
        $this->render('camadasDoTipo', array(
            'tipo' => $tipo,
        ));
    }

    /**
     * LIsta tipos de camadas do autor
     */
    public function actionMeusTipos() {
        $tiposDoAutor = PubliCamadaTipo::model()
                ->doAutor()
                ->editaveis()
                ->findAll("visibilidade_edicao = " . CamadaTipo::VisiEditPessoal);

        $tiposApp = PubliCamadaTipo::model()
                ->editaveis()
                ->findAll("de_aplicacao = 1");


        $favoritas = CamadaTipoFavorita::model()->doAutor()->findAll();
        $this->render('meusTipos', array(
            'tiposDoAutor' => $tiposDoAutor,
            'tiposDaAplicacao' => $tiposApp,
            'fave' => CHtml::listData($favoritas, 'tipo_id', 'tipo_id'),
        ));
    }

    public function actionFavoritar($id) {
        if (CamadaTipoFavorita::favoritar($id)) {
            echo $this->renderPartial('_favorito', array('t' => CamadaTipo::model()->comAcesso()->findByPk($id)), true, true);
        } else {
            echo $this->renderPartial('_naoFavorito', array('t' => CamadaTipo::model()->comAcesso()->findByPk($id)), true, true);
        }
    }

    public function actionDesfavoritar($id) {
        if (CamadaTipoFavorita::desfavoritar($id)) {
            echo $this->renderPartial('_naoFavorito', array('t' => CamadaTipo::model()->comAcesso()->findByPk($id)), true, true);
        } else {
            echo $this->renderPartial('_favorito', array('t' => CamadaTipo::model()->comAcesso()->findByPk($id)), true, true);
        }
    }

}

<?php
class CompartilharController extends PublicacoesController {

    protected function beforeAction($action) {
        return parent::beforeAction($action);
    }

    public function actionIndex($id){
        $model = $this->getTopico($id);
        $this->render('index',[
            'model' => $model,
        ]);
    }

    public function actionAdd($id){
        $model = $this->getTopico($id);
        $users = User::model()->findAll("tipo IN ('autor','admin')");

        if(isset($_POST['user']) && isset($_POST['permissao'])){
            $colab = new PubliColaborador;
            $colab->user_id = $_POST['user'];
            $colab->permissao = $_POST['permissao'];
            $colab->topico_id = $model->id;
            $colab->status = 0;
            $colab->lastOpen = time() - 60 * 5;
            $colab->save();
            $this->redirect($this->createUrl('compartilhar/index',[
                'id' => $model->id,
            ]));
        }

        $this->render('add',[
            'model' => $model,
            'users' => $users,
        ]);
    }   

    public function actionRemove($id)
    {

        $colab = PubliColaborador::model()->findByPk((int)$id);
        $model = $this->getTopico($colab->topico_id);
        if(!is_null($colab)){
            $colab->status = PubliColaborador::StatusRevogada;
            $colab->update(['status']);
        }
        $this->redirect($this->createUrl('compartilhar/index',[
            'id' => $model->id,
        ]));
    }

}

<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends PublicacoesController {

    public function actionIndex() {
        $this->redirect($this->createUrl('topico/listar'));
    }

    public function actionTeste() {
        $camadas = Camada::model()->doAutor()->findAll("excluido = 0 and tipo_id != 3");

        $expressoes = array();
        foreach ($camadas as $c) {
            $str = str_replace('$$', '$', $c->conteudo);

            // Expressões delimitas por $ ... $
            $matchs1 = array();
            preg_match_all('/\$([^\$]*)\$/U', $str, $matchs1);
            if (count($matchs1[1]) > 0) {
                $expressoes[] = $matchs1[1];
            }

            // Expressões delimitas por \[ ... \]
            $matchs2 = array();
            preg_match_all('/\[([^\]]*)\]/', $str, $matchs2);
            if (count($matchs2[1]) > 0) {
                $m2 = array();
                foreach ($matchs2[1] as $m) {
                    // Último caracter deve ser \ TODO: corrigir expressão de busca
                    $last = substr($m, strlen($m) - 1, strlen($m));
                    if ($last == '\\') {
                        $m2[] = substr($m, 0, strlen($m) - 1);
                    }
                }
                $expressoes[] = $m2;
            }
        }

        $l = array();
        foreach ($expressoes as $c) {
            foreach ($c as $g) {
                $hash = hash('md5', $g);
                if (!isset($l[$hash])) {
                    $l[$hash] = $g;
                }
            }
        }

        foreach ($l as $h => $f) {
            $template = PubliTemplatesLatex::model()->findByPk(array(
                'id' => $h,
                'user_id' => Yii::app()->user->id,
            ));
            if (is_null($template)) {
                $template = new PubliTemplatesLatex();
                $template->id = $h;
                $template->codigo = $f;
                $template->user_id = Yii::app()->user->id;
                $template->save();
            }
        }
        $this->render('teste');
    }

}

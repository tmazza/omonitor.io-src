<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class DefaultController extends ControleAcessoController {

    public function actionIndex() {
        $users = new CActiveDataProvider(SegUser::model()->with(array('items' => array('index' => 'name'))), array('pagination' => array('pageSize' => 100)));
        $itens = new CActiveDataProvider(SegAuthitem::model());
        $this->render('index', array(
            'users' => $users,
            'itens' => $itens,
        ));
    }
    
}

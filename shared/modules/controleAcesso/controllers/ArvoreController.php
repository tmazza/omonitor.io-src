<?php

/**
 * Description of ArvoreController
 *
 * @author tiago
 */
class ArvoreController extends ControleAcessoController {

    /**
     * Lista todas as permissões cadastradas
     */
    public function actionVer() {
        $adminItem = SegAuthitem::model()->findByPk('admin');
        $data = array($this->makeItemNode($adminItem));
        $this->render('ver', array(
            'data' => $data,
        ));
    }

    /**
     * Criação de nova regra. Obrigatoriamente conectada à regra admin
     */
    public function actionNovaRegra() {
        $this->testaSalvaItem(CAuthItem::TYPE_ROLE);
        $this->render('nova', array(
            'type' => CAuthItem::TYPE_ROLE,
        ));
    }

    /**
     * Criação de nova tarefa. Obrigatoriamente conectada à regra
     */
    public function actionNovaTarefa() {
        $this->testaSalvaItem(CAuthItem::TYPE_TASK);
        $itens = CHtml::listData(SegAuthitem::model()->findAll("type = " . CAuthItem::TYPE_ROLE . " OR type = " . CAuthItem::TYPE_TASK), 'name', 'name');
        $this->render('nova', array(
            'type' => CAuthItem::TYPE_TASK,
            'itens' => $itens,
        ));
    }
    /**
     * Criação de nova Operacao.  Obrigatoriamente conectada à tarefa
     */
    public function actionNovaOperacao() {
        $this->testaSalvaItem(CAuthItem::TYPE_OPERATION);
        $itens = CHtml::listData(SegAuthitem::model()->findAll("type = " . CAuthItem::TYPE_TASK), 'name', 'name');
        $this->render('nova', array(
            'type' => CAuthItem::TYPE_OPERATION,
            'itens' => $itens,
        ));
    }

    private function testaSalvaItem($type) {
        if (isset($_POST['item']) && isset($_POST['nome'])) {
            Yii::app()->authManager->createAuthItem($_POST['nome'], $type);
            if (Yii::app()->authManager->addItemChild($_POST['item'], $_POST['nome'])) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Item criado.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao criar item.');
            }
            $this->redirect($this->createUrl('arvore/ver'));
        }
    }

    /**
     * Monta um nodo da árvore. Aplica recursivamente ao filhos dos nodos
     * TODO: controle de loops que podem, mas não deviam, ter sido inseridos
     */
    private function makeItemNode($item) {
        $node = array();

        $usuariosNoNodo = CHtml::listData($item->users, 'username', 'username');

        $node['text'] = ''
                . $item->name
                . ' <span class="users-nodo hint">' . implode(', ', $usuariosNoNodo) . '</span>'
                . ' <span class="mini-box ' . $this->getClassType($item->type) . '"></span>';
        $node['children'] = array();
        foreach ($item->filhos as $f) {
            $node['children'][] = $this->makeItemNode($f);
        }
        $node['expanded'] = count($usuariosNoNodo) > 0 || $item->name == 'admin';

        return $node;
    }

    /**
     * Define classe html de acordo com o tipo de authitem
     * @param type $id
     * @return string
     */
    private function getClassType($id) {
        switch ($id) {
            case CAuthItem::TYPE_OPERATION: return 'operacao';
            case CAuthItem::TYPE_TASK: return 'tarefa';
            case CAuthItem::TYPE_ROLE: return 'regra';
        }
    }

}

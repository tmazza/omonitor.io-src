<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioController
 *
 * @author tiago
 */
class UsuarioController extends ControleAcessoController {

    public $currentUser;

    public function actionVer($username) {
        $permissoes = new CActiveDataProvider(SegAuthassignment::model()->doUsuario($username));
        $this->render('ver', array(
            'username' => $username,
            'permissoes' => $permissoes,
        ));
    }

    public function actionNovaPermissao($username) {
        $this->currentUser = $username;
        $items = new CActiveDataProvider(SegAuthitem::model());
        $this->render('novaPermissao', array(
            'username' => $username,
            'itens' => $items,
        ));
    }

    public function actionRevoke($username, $itemname) {
        if (Yii::app()->authManager->revoke($itemname, $username)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Ok.');
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Não retirou.');
        }
        $this->redirect($this->createUrl('usuario/ver', array('username' => $username)));
    }

    public function actionAssign($username, $itemname) {
        if (Yii::app()->authManager->assign($itemname, $username)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Ok.');
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Não retirou.');
        }
        $this->redirect($this->createUrl('usuario/NovaPermissao', array('username' => $username)));
    }

}

<?php
$this->menuContexto = array(
    ShCode::makeItem('Árvore de permissões', $this->createUrl('arvore/ver')),
);
?>
<br><br>
<h1>Usuários</h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users,
    'columns' => array(
        array(
            'type' => 'html',
            'value' => 'CHtml::link($data->username, Yii::app()->controller->createUrl("usuario/ver", array("username" => $data->username)))',
        ),
        array(
            'type' => 'html',
            'header' => 'Qtd. permissões',
            'value' => 'implode(", ", array_keys($data->items))',
        ),
        'email',
        'tipo',
    ),
));
?>
<br><br>
<h1>Itens</h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $itens, 
    'columns' => array(
        'name',
        'type',
        'description',
        array(
            'type' => 'html',
            'header' => 'Usuários com esta permissão',
            'value' => 'implode(", ", array_keys(CHtml::listData($data->users, "username", "username")))',
        ),
    ),
));
?>

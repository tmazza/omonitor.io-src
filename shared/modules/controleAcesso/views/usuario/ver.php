<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
    ShCode::makeItem('Nova permissão', $this->createUrl('usuario/NovaPermissao', array('username' => $username))),
);
?>
<h1><?= $username ?><span class="hint"> #permissões</span></h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $permissoes,
    'columns' => array(
        'item.name',
        'item.description',
        array(
            'type' => 'html',
            'header' => 'Retirar permissão',
            'value' => 'CHtml::link("Retirar", Yii::app()->controller->createUrl("usuario/revoke", array("username" => $data->userid, "itemname" => $data->item->name)))',
        ),
    ),
));

<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('usuario/ver', array('username' => $username))),
);
?>
<h1><?= $username ?><span class="hint"> #nova permissão</span></h1>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $itens,
    'columns' => array(
        'name',
        'description',
        array(
            'type' => 'html',
            'header' => 'Adicionar permissão',
            'value' => 'CHtml::link("Adicionar", Yii::app()->controller->createUrl("usuario/assign", array("username" => $this->grid->controller->currentUser, "itemname" => $data->name)))',
        ),
    ),
));

<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
    ShCode::makeItem('Nova regra', $this->createUrl('arvore/novaRegra')),
    ShCode::makeItem('Nova tarefa', $this->createUrl('arvore/novaTarefa')),
    ShCode::makeItem('Nova operação', $this->createUrl('arvore/novaOperacao')),
);
?>
<h1>Níveis de permissões</h1>
<div class="sh-row">
    <div class="medium-12 columns">
        <div class="sh-row">
            <div class="medium-6 columns">
                <h4>Estrutura</h4>
                <hr>
                Dois níveis de regras e quantos níveis forem necessários de tarefas para agrupar operações.
                Cada operação deve representar o acesso/execução de um ou mais action.
            </div>
            <div class="medium-6 columns">
                <h4>Legenda</h4>
                <hr>
                <ul style="list-style: none;">
                    <li><span class="mini-box regra"></span> Regra</li>
                    <li><span class="mini-box tarefa"></span> Tarefa</li>
                    <li><span class="mini-box operacao"></span> Operacao</li>
                    <li><span class="hint">Em cinza: usuários cadastrados na permissão.</span></li>
                </ul>
            </div>
        </div>
        <div class="sh-row">
            <div class="medium-12 columns">
                <h4>Árvore de permissões</h4>
                <?php
                $this->widget('CTreeView', array(
                    'id' => 'menu-treeview',
                    'data' => $data,
                    'control' => '#treecontrol',
                    'animated' => 'fast',
                    'htmlOptions' => array(
                        'class' => 'treeview-red'
                    )
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<style>
    <!--
    .mini-box {
        width: 8px;
        height: 8px;
        display: inline-block;
        vertical-align: top;
        border-radius: 20px;
    }
    .operacao {
        background: red;
    }
    .tarefa {
        background: yellowgreen;
    }
    .regra {
        background: yellow;
    }
    -->
</style>

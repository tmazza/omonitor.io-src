<?php

$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('arvore/ver')),
);

echo CHtml::beginForm();

echo CHtml::label('Item pai: ', 'item-pai');
if ($type == CAuthItem::TYPE_ROLE) {
    echo CHtml::textField('item', 'admin', array('readonly' => true, 'id' => 'item-pai'));
} else {
    echo CHtml::dropDownList('item', null, $itens, array('id' => 'item-pai'));
}

echo CHtml::label('Nome: ', 'nome-item');
echo CHtml::textField('nome', null, array('id' => 'nome-item'));

echo CHtml::submitButton('Criar');

echo CHtml::endForm();

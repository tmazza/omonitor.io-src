<?php

/**
 * This is the model class for table "seg_authassignment".
 *
 * The followings are the available columns in table 'seg_authassignment':
 * @property string $itemname
 * @property string $userid
 * @property string $bizrule
 * @property string $data
 */
class SegAuthassignment extends CActiveRecord {

    public function tableName() {
        return 'seg_authassignment';
    }

    public static function getTableName() {
        return 'seg_authassignment';
    }

    public function rules() {
        return array(
            array('itemname, userid', 'required'),
            array('itemname, userid', 'length', 'max' => 64),
            array('bizrule, data', 'safe'),
            array('itemname, userid, bizrule, data', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'item' => array(self::BELONGS_TO, 'SegAuthitem', 'itemname'),
        );
    }

    public function attributeLabels() {
        return array(
            'itemname' => 'Itemname',
            'userid' => 'Userid',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
        );
    }

    public function doUsuario($username) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "userid = '{$username}'"
        ));
        return $this;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

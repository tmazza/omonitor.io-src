<?php

/**
 * This is the model class for table "seg_authitem".
 *
 * The followings are the available columns in table 'seg_authitem':
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $bizrule
 * @property string $data
 *
 * The followings are the available model relations:
 * @property User[] $users
 * @property SegAuthitemchild[] $segAuthitemchildren
 * @property SegAuthitemchild[] $segAuthitemchildren1
 */
class SegAuthitem extends CActiveRecord {

    public function tableName() {
        return 'seg_authitem';
    }

    public static function getTableName() {
        return 'seg_authitem';
    }

    public function rules() {
        return array(
            array('name, type', 'required'),
            array('type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 64),
            array('description, bizrule, data', 'safe'),
            array('name, type, description, bizrule, data', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'users' => array(self::MANY_MANY, 'User', 'seg_authassignment(itemname, userid)'),
            'segAuthitemchildren' => array(self::HAS_MANY, 'SegAuthitemchild', 'parent'),
            'segAuthitemchildren1' => array(self::HAS_MANY, 'SegAuthitemchild', 'child'),
            'filhos' => array(self::MANY_MANY, 'SegAuthitem', SegAuthitemchild::getTableName() . '(parent,child)'),
        );
    }

    public function attributeLabels() {
        return array(
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('bizrule', $this->bizrule, true);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password - sha256
 * @property string $tipo
 */
class SegUser extends AplicationActiveRecord {

    const TipoAdmin = 'admin';
    const TipoAutor = 'autor';
    const TipoAluno = 'aluno';

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->password = CPasswordHelper::hashPassword($this->password);
        }
        return parent::beforeSave();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return string the associated database table name
     */
    public static function getTableName() {
        return 'user';
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'autor' => array(self::HAS_ONE, 'Autor', 'username'),
            'aluno' => array(self::HAS_ONE, 'Aluno', 'username'),
            'admin' => array(self::HAS_ONE, 'Admin', 'username'),
            'permissoes' => array(self::MANY_MANY, 'TaxItem', TaxUser::getTableName() . '(tax_item_id, user_id)'),
            'matriculas' => array(self::MANY_MANY, 'Curso', CursoAluno::getTableName() . '(curso_id,aluno_id)'),
            'items' => array(self::MANY_MANY, 'SegAuthitem', SegAuthassignment::getTableName() . '(userid,itemname)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'tipo' => 'Tipo',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Incluir permissão de acesso para usuário no nodo $itemID
     * TODO: excluir árvore filha de permissões ?
     * @param type $itemID
     */
    public function addPermissao($itemID) {
        $permissao = new TaxUser();
        $permissao->user_id = $this->username;
        $permissao->tax_item_id = $itemID;
        $permissao->save();
    }

    /**
     * Retirar a permissão de acesso do usuário nodo $itemID
     * TODO: retirar permissão de nodos filhos?
     * @param type $itemID
     */
    public function removePermissao($itemID) {
        return TaxUser::model()->deleteAll(array(
                    'condition' => "user_id = '{$this->username}' AND tax_item_id = {$itemID}"
                )) > 0;
    }

    /**
     * Retorna model referente ao perfil do usuário (aluno | autor)
     * @return type
     */
    public function getPerfil() {
        return $this->{$this->tipo};
    }

}

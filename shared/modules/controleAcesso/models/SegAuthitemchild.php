<?php

/**
 * This is the model class for table "seg_authitemchild".
 *
 * The followings are the available columns in table 'seg_authitemchild':
 * @property string $parent
 * @property string $child
 *
 * The followings are the available model relations:
 * @property SegAuthitem $parent0
 * @property SegAuthitem $child0
 */
class SegAuthitemchild extends CActiveRecord {

    public function tableName() {
        return 'seg_authitemchild';
    }

    public static function getTableName() {
        return 'seg_authitemchild';
    }

    public function rules() {
        return array(
            array('parent, child', 'required'),
            array('parent, child', 'length', 'max' => 64),
            array('parent, child', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'parent0' => array(self::BELONGS_TO, 'SegAuthitem', 'parent'),
            'child0' => array(self::BELONGS_TO, 'SegAuthitem', 'child'),
        );
    }

    public function attributeLabels() {
        return array(
            'parent' => 'Parent',
            'child' => 'Child',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('parent', $this->parent, true);
        $criteria->compare('child', $this->child, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

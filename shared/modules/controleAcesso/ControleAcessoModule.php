<?php

class ControleAcessoModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'controleAcesso.components.*',
            'controleAcesso.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => 'atualizarPermissao',
            // Controller arvore
            'arvore.*' => 'atualizarPermissao',
            // Controller item 
            'item.*' => 'atualizarPermissao',
            // Controller usuario
            'usuario.*' => 'atualizarPermissao',            
        );
    }
}

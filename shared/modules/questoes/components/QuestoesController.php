<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class QuestoesController extends EditorController {

    public $layoutBase;
    public $layoutPrincipal;

    protected function beforeAction($action) {
        $this->layoutBase = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutBase;
        $this->layoutPrincipal = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutPrincipal;
        $this->layout = $this->layoutPrincipal;
        return parent::beforeAction($action);
    }

    /**
     * Busca questionaio $id. Validando permissão de acesso.
     * @param type $id
     */
    protected function getQuestionario($id) {
        $questionario = Questionario::model()->findByPk($id);
        if (is_null($questionario)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não encontrada');
            $this->redirect($this->createUrl('default/index'));
        }
        return $questionario;
    }

}

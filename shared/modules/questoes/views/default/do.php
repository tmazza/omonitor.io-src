<?php $this->pageTitle = $qst->nome; ?>
<?php
$this->menuContexto = array(
  ShCode::makeItem('Voltar',$this->createUrl('default/index')),
);
?>
<div class="content">
    <?php
    $this->widget('shared.widgets.Questionario.ResolverQuestionario', array(
        'questionarioId' => $qst->id,
        'verDesempenho' => false,
        'url' => Yii::app()->controller->createUrl('default/CorrigirQuestao'),
    ));
    ?>
</div>

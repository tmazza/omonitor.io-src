<?php
$this->pageTitle = 'Questionários';
?>
<?php if($showHelp): ?>
<hr>
<div class="hint">
  Teste seus conhecimentos! <br>
  Resolva os questionários quantas vezes quiser e obtenha os gabaritos na hora.
  <br>
  <?//=CHtml::link('Não mostar mais.')?>
</div>
<hr>
<?php endif; ?>

<table class='table'>
<?php foreach($qsts as $q): ?>
  <?php
  $data = $q->meuDesempenho();
  $prog = $data['respondido'] * 100;
  $succ = $data['desempenho'] * 100;
  ?>
  <tr>
    <td><?=CHtml::link($q->nome,$this->createUrl('do',array('id'=>$q->id)));?></td>
    <td>

      <?php if($prog < 100): ?>
        Completado: <?=number_format($prog,2,',','.')?>%
      <?php else: ?>
        <?php
        $color = '#f00';
        if($succ > 30) $color = '#0ff';
        if($succ > 60) $color = '#00f';
        if($succ > 90) $color = '#0f0';
        ?>
        <span style="color: <?=$color;?>">
          Acertos: <?=number_format($succ,2,',','.')?>%
        </span>
      <?php endif; ?>
      <br>
  </td>
  <tr>
<?php endforeach; ?>
</table>

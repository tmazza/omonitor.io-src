<?php

class QuestoesModule extends CWebModule {

    public $layoutBase = 'raw';
    public $layoutPrincipal = 'main';


    public function init() {
        $this->setImport(array(
            'questoes.components.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
        );
    }

}

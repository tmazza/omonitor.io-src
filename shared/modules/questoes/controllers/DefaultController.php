<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends QuestoesController {

    public $url;
    public $controller;
    public $questionario;
    
    public function beforeAction($action){
      $this->url = $this->createUrl('/');
      $this->controller = $this;
      return parent::beforeAction($action);
    }


    public function actions() {
        return array(
            'CorrigirQuestao' => array(
              'class' => 'shared.actions.CorrigirQuestao',
              'salvarResposta' => true,
            ),
            'updateOpcao' => array(
              'class' => 'shared.actions.QuestaoUpdateOpcao',
            ),
        );
    }

    public function actionIndex() {
      $qsts = Questionario::model()->findAll();
      $this->render('index',array('qsts' => $qsts,'showHelp' => true));
    }

    public function actionDo($id){
      $questionario = $this->getQuestionario($id);
      $this->questionario = $questionario;
      $this->render('do', array('qst'=>$questionario));
    }



}

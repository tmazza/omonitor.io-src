<?php

/**
 * This is the model class for table "questao".
 *
 * The followings are the available columns in table 'questao':
 * @property integer $id
 * @property string $titulo
 * @property string $conteudo
 * @property integer $tipo
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property UserAutor $autor
 * @property QuestaoAlternativa[] $questaoAlternativas
 * @property QuestaoGabarito[] $questaoGabaritos
 */
class QstQuestao extends CActiveRecord {

    public function tableName() {
        return 'questao';
    }

    public static function getTableName() {
        return 'questao';
    }

    public function rules() {
        return array(
            array('tipo, enunciado, user_id', 'required'),
            array('tipo,user_id', 'numerical', 'integerOnly' => true),
            array('tipo', 'in', 'range' => array_keys(Questao::getTipos())),
            array('comentarioGabarito, dica,peso', 'safe'),
            array('id, tipo, enunciado, comentarioGabarito, dica, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'alternativas' => array(self::HAS_MANY, 'QstQuestaoAlternativa', 'questao_id'),
            // Questões de preenchimento
            'gabarito' => array(self::HAS_ONE, 'QstQuestaoAlternativa', 'questao_id'),
            'questaoGabaritos' => array(self::HAS_MANY, 'QstQuestaoGabarito', 'questao_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tipo' => 'Tipo',
            'enunciado' => 'Enunciado',
            'comentarioGabarito' => 'Comentario Gabarito',
            'dica' => 'Dica',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

    public static function getTipos() {
        return Questao::getTipos();
    }

    /**
     * Salva nova questões criando estrutura de alternativas e gabaritos necessarios
     * @return boolean
     */
    public function salvaQuestao() {
        if (!$this->save()) {
            return false;
        }
        if ($this->tipo == Questao::CincoAlternativas) {
            return $this->criaCincoAlternativas();
        } elseif ($this->tipo == Questao::VerdadeiroOuFalseo) {
            return $this->criaVerdadeiroOuFalso();
        }
        return true;
    }

    /**
     * Criar cinco alternativas para questões
     */
    private function criaCincoAlternativas() {
        $success = true;
        $label = "A";
        for ($i = 1; $i <= 5; $i++) {
            $success = $success && $this->adicionarNovaAlternativa($label, null, false);
            $label++;
        }
        return $success;
    }

    /**
     * Cria nova alternativa para a questão
     * @param type $chave
     * @param type $valor
     * @param type $verdadeira
     * @return type
     */
    public function adicionarNovaAlternativa($chave, $valor, $verdadeira) {
        $alternativa = new QstQuestaoAlternativa();
        $alternativa->chave = $chave;
        $alternativa->valor = $valor;
        $alternativa->verdadeira = $verdadeira;
        $alternativa->user_id = Yii::app()->user->id;
        $alternativa->questao_id = $this->id;
        return $alternativa->save();
    }

    private function criaVerdadeiroOuFalso() {
        return true;
    }

    public function getVariavelOptions($id){
          $opcoes = QuestaoVariavel::model()->doAutor()->findAll(array(
            'condition' => "questao_id = " . $this->id . " AND variavel_id = '{$id}'",
            'order' => 'sequencia DESC',
            'index' => 'sequencia',
          ));
          return $opcoes;
    }

}

<?php

class QuestionariosModule extends CWebModule {

    public $layoutBase = 'raw';
    public $layoutPrincipal = 'main';


    public function init() {
        $this->setImport(array(
            'questionarios.components.*',
            'questionarios.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Questionario
            'questionario.listar' => 'lerQuestionario',
            'questionario.imprimir' => 'lerQuestionario',
            'questionario.novo' => 'criarQuestionario',
            'questionario.novaquestao' => 'atualizarQuestionario',
            'questionario.editar' => 'atualizarQuestionario',
            'questionario.configurar' => 'atualizarQuestionario',
            'questionario.conectarquestao' => 'atualizarQuestionario',
            'questionario.desconectarquestao' => 'atualizarQuestionario',
            'questionario.excluir' => 'atualizarQuestionario',
            // Controller Questao
            'questao.listar' => 'lerQuestao',
            'questao.nova' => 'criarQuestao',
            'questao.editar' => 'atualizarQuestao',
            'questao.responder' => 'responderQuestao',
            'questao.excluir' => 'excluirQuestao',

            // Controller Alternativa
            'alternativa.*' => 'atualizarQuestao',
            'variaveis.*' => 'atualizarQuestao',
        );
    }

}

<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class QuestionariosController extends EditorController {

    public $layoutBase;
    public $layoutPrincipal;

    protected function beforeAction($action) {
        $this->layoutBase = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutBase;
        $this->layoutPrincipal = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutPrincipal;
        $this->layout = $this->layoutPrincipal;
        return parent::beforeAction($action);
    }

    /**
     * Busca curso do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getQuestionario($id) {
        $questionario = QstQuestionario::model()
                ->doAutor()
                ->findByPk($id);
        if (is_null($questionario)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não econtrado.');
            $this->redirect($this->createUrl('questionario/listar'));
        }
        return $questionario;
    }

    /**
     * Busca um questão do autor
     * @param type $id
     * @return type
     */
    protected function getQuestao($id) {
        $questao = QstQuestao::model()
                ->doAutor()
                ->findByPk($id);
        if (is_null($questao)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não econtrado.');
            $this->redirect($this->createUrl('questao/listar'));
        }
        return $questao;
    }

}

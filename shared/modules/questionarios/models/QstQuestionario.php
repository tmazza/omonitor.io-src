<?php

/**
 * This is the model class for table "questionario".
 *
 * The followings are the available columns in table 'questionario':
 * @property integer $id
 * @property string $nome
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class QstQuestionario extends CActiveRecord {

    public function tableName() {
        return 'questionario';
    }

    public static function getTableName() {
        return 'questionario';
    }

    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('nome', 'length', 'max' => 256),
            array('nome, user_id,publicado', 'safe'),
            array('publicado', 'in', 'range' => array(0,1)),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'questoes' => array(self::MANY_MANY, 'QstQuestao', QstQuestionarioQuestao::getTableName() . '(questionario_id,questao_id)'),
        );
    }

    public function attributeLabels() {
        return array(
          'publicado' => 'Publicado?',
            'nome' => 'Nome',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function criaConexao($questao) {
        $conexao = new QstQuestionarioQuestao();
        $conexao->questionario_id = $this->id;
        $conexao->questao_id = $questao->id;
        $conexao->user_id = Yii::app()->user->id;
        return $conexao->save();
    }

    public function desfazConexao($questao) {
        return QstQuestionarioQuestao::model()->deleteAll(array(
                    'condition' => "user_id = '" . Yii::app()->user->id . "' "
                    . "AND questionario_id = {$this->id} "
                    . "AND questao_id = {$questao->id}"
                )) > 0;
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

}

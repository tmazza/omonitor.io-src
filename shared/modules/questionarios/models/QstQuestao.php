<?php

/**
 * This is the model class for table "questao".
 *
 * The followings are the available columns in table 'questao':
 * @property integer $id
 * @property string $titulo
 * @property string $conteudo
 * @property integer $tipo
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property UserAutor $autor
 * @property QuestaoAlternativa[] $questaoAlternativas
 * @property QuestaoGabarito[] $questaoGabaritos
 */
class QstQuestao extends CActiveRecord {

    public function tableName() {
        return 'questao';
    }

    public static function getTableName() {
        return 'questao';
    }

    public function rules() {
        return array(
            array('tipo, enunciado, user_id', 'required'),
            array('tipo,user_id,excluido', 'numerical', 'integerOnly' => true),
            array('tipo', 'in', 'range' => array_keys(Questao::getTipos())),
            array('comentarioGabarito, dica,peso', 'safe'),
            array('id, tipo, enunciado, comentarioGabarito, dica, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'alternativas' => array(self::HAS_MANY, 'QstQuestaoAlternativa', 'questao_id'),
            // Questões de preenchimento
            'gabarito' => array(self::HAS_ONE, 'QstQuestaoAlternativa', 'questao_id'),
            'questaoGabaritos' => array(self::HAS_MANY, 'QstQuestaoGabarito', 'questao_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tipo' => 'Tipo',
            'enunciado' => 'Enunciado',
            'comentarioGabarito' => 'Comentario Gabarito',
            'dica' => 'Dica',
            'user_id' => 'Autor',
            'excluido' => 'Excluido',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

    public static function getTipos() {
        return Questao::getTipos();
    }

    /**
     * Salva nova questões criando estrutura de alternativas e gabaritos necessarios
     * @return boolean
     */
    public function salvaQuestao() {
        if (!$this->save()) {
            return false;
        }
        if ($this->tipo == Questao::CincoAlternativas) {
            return $this->criaCincoAlternativas();
        } elseif ($this->tipo == Questao::VerdadeiroOuFalseo) {
            return $this->criaVerdadeiroOuFalso();
        }
        return true;
    }

    /**
     * Criar cinco alternativas para questões
     */
    private function criaCincoAlternativas() {
        $success = true;
        $label = "A";
        for ($i = 1; $i <= 5; $i++) {
            $success = $success && $this->adicionarNovaAlternativa($label, null, false);
            $label++;
        }
        return $success;
    }

    /**
     * Cria nova alternativa para a questão
     * @param type $chave
     * @param type $valor
     * @param type $verdadeira
     * @return type
     */
    public function adicionarNovaAlternativa($chave, $valor, $verdadeira) {
        $alternativa = new QstQuestaoAlternativa();
        $alternativa->chave = $chave;
        $alternativa->valor = $valor;
        $alternativa->verdadeira = $verdadeira;
        $alternativa->user_id = Yii::app()->user->id;
        $alternativa->questao_id = $this->id;
        return $alternativa->save();
    }

    private function criaVerdadeiroOuFalso() {
        return true;
    }

    public function getVariavelOptions($id){
          $opcoes = QuestaoVariavel::model()->doAutor()->findAll(array(
            'condition' => "questao_id = " . $this->id . " AND variavel_id = '{$id}'",
            'order' => 'sequencia DESC',
            'index' => 'sequencia',
          ));
          return $opcoes;
    }


    public function getSequencias(){
      $opcoes = QuestaoVariavel::model()->findAll(array(
        'condition' => "questao_id = " . $this->id,
        'order' => 'sequencia DESC, variavel_id ASC',
      ));

      $sequencias = array();
      $atual = null;
      foreach ($opcoes as $o) {
        if($o->sequencia != $atual){
          $atual = $o->sequencia;
          $sequencias[$atual] = array();
        }
        $sequencias[$atual][$o->variavel_id] = $o;
      }
      return $sequencias;
    }


    /**
     * Exclui Questao
     * @param type $pk
     */
    public static function excluir($pk) {
        self::atulizaFlag($pk, 'excluido', true);
        self::desconetarQuestaoDosQuestionarios($pk);
    }

    /**
     * Atualiza $attr para $estado de Questao com id == $pk
     * @param type $pk
     * @param type $attr
     * @param type $estado
     */
    public static function atulizaFlag($pk, $attr, $estado) {
        Questao::model()->doAutor()->updateByPk($pk, array(
            $attr => $estado,
        ));
    }

    public function behaviors() {
        return array(
            'exclusaoLogica' => array(
                'class' => 'ExclusaoLogicaBehavior',
            ),
        );
    }

    /*
     * Busca os questionarios aos quais a questao esta conectada e a desconecta deles.
     */
    public static function desconetarQuestaoDosQuestionarios($pk){
        $questao = Questao::model()->findByPk($pk);
        $qst_questionarios = QstQuestionarioQuestao::model()->findAllByAttributes(array("questao_id"=>$pk));

        if(!is_null($qst_questionarios)){
            foreach($qst_questionarios as $qst_questionario){
                $questionario = QstQuestionario::model()->findByPk($qst_questionario->questionario_id);
                $questionario->desfazConexao($questao);
            }

        }
    }

}

<?php

/**
 * This is the model class for table "questao_alternativa".
 *
 * The followings are the available columns in table 'questao_alternativa':
 * @property integer $id
 * @property string $chave
 * @property string $valor
 * @property integer $questao_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Questao $questao
 * @property UserAutor $autor
 */
class QstQuestaoAlternativa extends CActiveRecord {

    public function tableName() {
        return 'questao_alternativa';
    }

    public static function getTableName() {
        return 'questao_alternativa';
    }

    public function rules() {
        return array(
            array('questao_id, user_id', 'required'),
            array('questao_id,user_id', 'numerical', 'integerOnly' => true),
            array('chave', 'length', 'max' => 100),
            array('verdadeira', 'boolean'),
            array('valor', 'safe'),
            array('id, chave, valor, questao_id, user_id, verdadeira', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'questao' => array(self::BELONGS_TO, 'Questao', 'questao_id'),
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'chave' => 'Chave',
            'valor' => 'Valor',
            'questao_id' => 'Questao',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
       

}

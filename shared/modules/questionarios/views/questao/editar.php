<?php
$this->pageTitle = 'Edição de questão';
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('questao/listar')),
    ShCode::makeItem('Editar alternativas', $this->createUrl('alternativa/editar', array('id' => $questao->id)))
);
?>
<div class="sh-row">
    <div class="columns medium-12">
        <?php $this->renderPartial('_formQuestao', array('model' => $questao)); ?>
    </div>
</div>

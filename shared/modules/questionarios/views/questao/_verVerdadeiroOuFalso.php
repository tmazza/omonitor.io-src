<div class="sh-row">
    <div class="medium-12 columns">
        <ul class="nav nav-pills nav-stacked">
            <?php foreach ($questao->alternativas as $alternativa): ?>
                <?php if ($alternativa->verdadeira): ?>
                    <li class="green"><b><?= $alternativa->valor; ?></b></li>
                <?php else: ?>
                    <li class="red"><b><?= $alternativa->valor; ?></b></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
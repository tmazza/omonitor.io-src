<?php
$this->pageTitle = 'Nova questão';
$this->menuContexto = array(
    ShCode::makeItemCondicionado('Cancelar', $this->createUrl('questao/listar')),
);
?>
<div class="sh-row">
    <div class="columns medium-12">
        <?= $this->renderPartial('_formQuestao', array('model' => $model)); ?>
    </div>
</div>

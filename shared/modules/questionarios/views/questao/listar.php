<?php
$this->pageTitle = 'Banco de questões';
$this->menuContexto = array(
    ShCode::makeItemCondicionado('Nova questão', $this->createUrl('questao/nova')),
);
?>
<div class="content">
    <ul class="medium-block-grid-1 small-block-grid-1">
        <?php
        foreach ($questoes as $q) {
            echo '<li>' . $this->renderPartial('_questao', array('data' => $q), true) . '</li>';
        }
        ?>
    </ul>
</div>
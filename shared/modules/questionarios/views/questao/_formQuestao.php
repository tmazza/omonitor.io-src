<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'novo-questionario',
    'enableAjaxValidation' => false,
        ));
?>

<?php if ($model->isNewRecord): ?>
    <div class="sh-row">
        <div class="columns medium-2">
            <?php echo $form->labelEx($model, 'tipo'); ?>
        </div>
        <div class="columns medium-4">
            <?php echo $form->dropDownList($model, 'tipo', QstQuestao::getTipos(), array('prompt' => 'Selecione...')); ?>
        </div>
        <div class="columns medium-4">
            <?php echo $form->error($model, 'tipo'); ?>
        </div>
    </div>
<?php else: ?>
    <div class="sh-row">
        <div class="columns medium-2">
            Tipo:
        </div>
        <div class="columns medium-4">
            <?= Questao::getLabelTipo($model->tipo); ?>
        </div>
        <div class="columns medium-offset-4"></div>
    </div>
    <br>
<?php endif; ?>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'peso'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->dropDownList($model, 'peso', Questao::getPesos(), array('prompt' => 'Selecione...')); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->error($model, 'peso'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="columns medium-12">
        <?php echo $form->labelEx($model, 'enunciado'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12">
        <?php
        $this->widget('ImperaviRedactorWidget', array(
            'model' => $model,
            'attribute' => 'enunciado',
            'options' => array(
                'lang' => 'pt_br',
                'plugins' => array(
                    'bufferbuttons',
//                            'definedlinks',
                    'video',
                ),
                'replaceDivs' => false,
                'minHeight' => 140,
                'removeEmpty' => array(),
                'cleanStyleOnEnter' => true,
                'linebreaks' => true,
                'pastePlainText' => true,
                'cleanOnPaste' => true,
                'focus' => true,
                'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
//                        'definedLinks' => $this->createUrl('topico/linkParaOutrosTopicos'),
                'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
            ),
        ));
        ?>
    </div>
</div>

<b>Interação</b> <span class='hint'>(Para não utilizar deixe em branco)</span><br>
<textarea class='sage-edit' name='QstQuestao[interacao]'><?=stripslashes($model->interacao);?></textarea>
<br><br>
<div class="sh-row">
    <div class="columns medium-6">
        <?php echo $form->labelEx($model, 'dica'); ?>
        <?php
        $this->widget('ImperaviRedactorWidget', array(
            'model' => $model,
            'attribute' => 'dica',
            'options' => array(
                'lang' => 'pt_br',
                'plugins' => array(
                    'bufferbuttons',
//                            'definedlinks',
                    'video',
                ),
                'replaceDivs' => false,
                'minHeight' => 140,
                'removeEmpty' => array(),
                'cleanStyleOnEnter' => true,
                'linebreaks' => true,
                'pastePlainText' => true,
                'cleanOnPaste' => true,
                'focus' => true,
                'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
//                        'definedLinks' => $this->createUrl('topico/linkParaOutrosTopicos'),
                'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
            ),
        ));
        ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->labelEx($model, 'comentarioGabarito'); ?>
        <?php
        $this->widget('ImperaviRedactorWidget', array(
            'model' => $model,
            'attribute' => 'comentarioGabarito',
            'options' => array(
                'lang' => 'pt_br',
                'plugins' => array(
                    'bufferbuttons',
//                            'definedlinks',
                    'video',
                ),
                'replaceDivs' => false,
                'minHeight' => 140,
                'removeEmpty' => array(),
                'cleanStyleOnEnter' => true,
                'linebreaks' => true,
                'pastePlainText' => true,
                'cleanOnPaste' => true,
                'focus' => true,
                'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
//                        'definedLinks' => $this->createUrl('topico/linkParaOutrosTopicos'),
                'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
            ),
        ));
        ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo $form->error($model, 'enunciado'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo $form->error($model, 'dica'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo $form->error($model, 'comentarioGabarito'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

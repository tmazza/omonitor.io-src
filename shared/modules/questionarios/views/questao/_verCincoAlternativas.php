<div class="sh-row">
    <div class="medium-12 columns">
        <ul class="nav nav-pills">
            <?php foreach ($questao->alternativas as $alternativa): ?>
                <?php if ($alternativa->verdadeira): ?>
                    <li class="green"><b><?= $alternativa->chave; ?>) <?= $alternativa->valor; ?></b></li>
                    <?php else: ?>
                    <li><?= $alternativa->chave; ?>) <?= $alternativa->valor; ?><br></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

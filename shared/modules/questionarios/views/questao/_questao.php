<div class="box">
    <div class="box-header with-border">
        <strong>#Q-<?=str_pad($data->id,3,'0',STR_PAD_LEFT);?></strong>
        | <b><?php echo '"' . substr(strip_tags($data->enunciado),0,120) .'..."'; ?></b>
        | <?=Questao::getLabelTipo($data->tipo);?>
        <div class="pull-right">
            <?= CHtml::link('<i class="fa fa-edit"></i> Enunciado', $this->createUrl('questao/editar', array('id' => $data->id)), array('class' => 'btn btn-primary btn-xs')) ?>
            /
            <?=
            CHtml::link('<i class="fa fa-edit"></i> Alternativas e gabarito', $this->createUrl('alternativa/editar', array('id' => $data->id)), array(
                'class' => 'btn btn-primary btn-xs',
            ))
            ?>
            /
            <?= CHtml::link('<i class="fa fa-times"></i> Excluir questão', $this->createUrl('questao/excluir', array('id' => $data->id)), array('class' => 'btn btn-default btn-xs','onClick'=>'return confirm("Tem certeza que deseja excluir a questão?");' )) ?>
        </div>
    </div>
    <?= CHtml::link('Ver', '#!', array('class'=>'btn btn-default','onClick'=>'$(this).parent().find(".ttt").slideToggle();' )) ?>
    <div class="box-body ttt" style="display:none;">
        <?php echo $data->enunciado; ?>
        <?php if(!is_null($data->interacao) && strlen($data->interacao) > 0): ?>
          <?php
          $sequencias = $data->getSequencias();
          if(count($sequencias) > 0) {
            $variaveis = array_shift($sequencias);
            $parametros = CHtml::listData($variaveis,'variavel_id',function($i) { return array('valor'=>$i->valor);  });
            echo "<div class='sage-auto qst-sage'>" . stripslashes(ShView::mergeDataToTemplate($data->interacao,$parametros)) . "</div>";
          } else {
            echo "<div class='sage-auto qst-sage'>" . stripslashes($data->interacao) . "</div>";
          }
          ?>
        <?php endif;?>
    </div>
    <div class="box-footer ttt" style="background: #f5f5f5;display:none;">
        <?php
        if ($data->tipo == Questao::CincoAlternativas) {
            $this->renderPartial('/questao/_verCincoAlternativas', array('questao' => $data));
        } elseif ($data->tipo == Questao::VerdadeiroOuFalseo) {
            $this->renderPartial('/questao/_verVerdadeiroOuFalso', array('questao' => $data));
        } elseif (in_array($data->tipo, array(Questao::EntradaSimples, Questao::EntradaExpressao))) {
            $this->renderPartial('/questao/_verEntradaSimples', array('questao' => $data));
        }
        ?>
    </div>
</div>

<?php
$this->pageTitle = $questionario->nome;
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('questionario/listar')),
    ShCode::makeItem('Editar questionário', $this->createUrl('questionario/configurar', array('id' => $questionario->id))),
    ShCode::makeItem('Excluir questionário', $this->createUrl('questionario/excluir', array('id' => $questionario->id))),
);
?>
<h4>Questões no questionário.</h4>
<ul class="medium-block-grid-4 small-block-grid-1">
    <?php
    $noQuestionario = array();
    foreach ($questionario->questoes as $questao) {
        $noQuestionario[] = $questao->id;
        echo '<li>' . $this->renderPartial('_questao', array(
            'id' => $questionario->id,
            'data' => $questao,
            'add' => false,
                ), true) . '</li>';
    }
    ?>
</ul>
<hr>
<h4>Outras questões disponíveis.</h4>
<?php echo CHtml::textField("filtroTopicos", "", array('placeholder' => 'Localizar questão...', 'id' => 's', 'class' => 'form-control column medium-4')); ?>
<br><br>

<ul class="medium-block-grid-4 small-block-grid-1">
    <?php
    foreach ($questoes as $questao) {
        if (!in_array($questao->id, $noQuestionario)) {
            echo '<li>' . $this->renderPartial('_questao', array(
                'id' => $questionario->id,
                'data' => $questao,
                'add' => true,
                    ), true) . '</li>';
        }
    }
    ?>
</ul>
<script>
    $('#s').keyup(function() {
        $('.s').each(function() {
            var n = $(this).text().toLowerCase();
            if (n.indexOf($('#s').val().toLowerCase()) === -1) {
                $(this).parent().parent().fadeOut();
            } else {
                $(this).parent().parent().fadeIn();
            }
        });
    });
</script>

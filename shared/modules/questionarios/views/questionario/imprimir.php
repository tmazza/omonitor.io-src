<h1><?= $questionario->nome; ?></h1>
<?php
foreach ($questionario->questoes as $questao) {
    echo '<h4>' . $questao->titulo . '</h4>';
    if ($questao->tipo == QstQuestao::CincoAlternativas) {
        $this->renderPartial('/questao/_verCincoAlternativas', array('questao' => $questao));
    } elseif($questao->tipo == QstQuestao::VerdadeiroOuFalseo){
        $this->renderPartial('/questao/_verVerdadeiroOuFalso', array('questao' => $questao));
    }
}
<div class="box" id="qst-<?= $data->id ?>">
    <div class="box-header">
        <div class="pull-right">
            <?php
            if ($add) {
                echo CHtml::link('Incluir', $this->createUrl('questionario/ConectarQuestao', array(
                            'id' => $id,
                            'contID' => $data->id,
                        )), array('class' => 'btn btn-xs btn-info'));
            } else {
                echo CHtml::ajaxLink('Remover', $this->createUrl('questionario/DesconectarQuestao', array(
                            'id' => $id,
                            'contID' => $data->id,
                        )), array(
                    'beforeSend' => 'js: function() { $("#qst-' . $data->id . '").append("<div class=\'overlay\'><i class=\'fa fa-refresh fa-spin\'></i></div>"); }',
                    'success' => 'js: function(html) { '
                    . 'if(html === "1"){'
                    . '$("#qst-' . $data->id . '").slideUp().remove();'
                    . '} else {'
                    . 'alert(html);'
                    . '$("#qst-' . $data->id . ' .overlay").remove();'
                    . '  }'
                    . '} ',
                        ), array(
                    'class' => 'btn btn-xs btn-danger',
                    'id' => 'remover' . $data->id));
            }
            ?>
        </div>
        <br>
    </div>
    <div class="box-body <?= $add ? 's' : ''; ?>" style="display: block; height: 100px; background: transparent; overflow: hidden; overflow-y: auto;" class="box-body">
        <?php echo $data->enunciado; ?>
    </div>
</div>
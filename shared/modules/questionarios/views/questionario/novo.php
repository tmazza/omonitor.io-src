<?php
$this->pageTitle = 'Novo questionário';
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('questionario/listar')),
);
?>
<div class="sh-row">
    <div class="columns medium-12">
        <p class="hint">
            Configurações de tempo de resolução, quantidade de tentativas, etc, 
            serão definidas no quando o questionário for utilizado.
            <br>
            <br>
        </p>
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>

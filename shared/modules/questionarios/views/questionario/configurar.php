<?php
$this->pageTitle = 'Editar questionário ' . $model->nome;
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('questionario/editar', array('id' => $model->id))),
);
?>
<div class="sh-row">
    <div class="columns medium-12">
        <p class="hint">
            Configurações de tempo de resolução, quantidade de tentativas, etc, 
            serão definidas quando o questionário for utilizado em curso. Você pode usar o mesmo questionário em vários cursos.
            <br>
            <br>
        </p>
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>

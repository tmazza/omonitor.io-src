<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'novo-questionario',
    'enableAjaxValidation' => false,
        ));
?>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->textField($model, 'nome'); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'nome'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'publicado'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->dropDownList($model, 'publicado',array(
          0 => 'Não',
          1 => 'Sim',
        )); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'publicado'); ?>
    </div>
</div>
<br>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

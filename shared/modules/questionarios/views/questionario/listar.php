<?php
$this->pageTitle = 'Questionários';
$this->menuContexto = array(
    ShCode::makeItem('Novo questionário', $this->createUrl('questionario/novo')),
);
?>
<div class="sh-row">
    <div class="column medium-12">
        <ul class="nav">
            <?php foreach ($questionarios as $q): ?>
                <li><?= CHtml::link($q->nome, $this->createUrl('questionario/editar', array('id' => $q->id))) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

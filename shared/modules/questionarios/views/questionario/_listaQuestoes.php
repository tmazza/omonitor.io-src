<?php echo CHtml::textField("filtroTopicos", "", array('placeholder' => 'Localizar questão...', 'id' => 's', 'class' => 'column medium-4')); ?>
<br><br>
<?php $questoesIDS = CHtml::listData($questionario->questoes, 'id', 'id'); ?>
<?php if (count($questoes) > 0): ?>
    <ul class="medium-block-grid-4">
        <?php foreach ($questoes as $qst): ?>
            <li>
                <div class="box">
                    <div class="box-header">
                        <?php
                        if (in_array($qst->id, $questoesIDS)) {
                            echo CHtml::link('Remover', $this->createUrl('questionario/DesconectarQuestao', array(
                                        'id' => $questionario->id,
                                        'contID' => $qst->id,
                                    )), array('class' => 'btn btn-xs btn-danger'));
                        } else {
                            echo CHtml::link('Incluir', $this->createUrl('questionario/ConectarQuestao', array(
                                        'id' => $questionario->id,
                                        'contID' => $qst->id,
                                    )), array('class' => 'btn btn-xs btn-info'));
                        }
                        ?>
                    </div>
                    <div class="box-body s">
                        <?= $qst->enunciado; ?>
                    </div>
                </div>

            </li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <h4 class='hint'>Nenhum questão criada.</h4>    
<?php endif; ?>

<script>
    $('#s').keyup(function() {
        $('.s').each(function() {
            var n = $(this).text().toLowerCase();
            if (n.indexOf($('#s').val().toLowerCase()) === -1) {
                $(this).parent().parent().fadeOut();
            } else {
                $(this).parent().parent().fadeIn();
            }
        });
    });
</script>
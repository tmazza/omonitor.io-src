<table class="table" id="parametros" style="background: #f4f4f4;">
  <?php $sequencias = $q->getSequencias(); ?>
  <?php
  if(count($sequencias) > 0){
    $firstKey = array_shift(array_keys($sequencias));
    $labels = array_keys($sequencias[$firstKey]);
    echo '<tr><th>' . implode('</th><th>',$labels) . '</th></tr>';
    foreach ($sequencias as $seq => $variaveis) {
      echo '<tr id="seq-'.$seq.'"><td>';
      echo implode('</td><td>',array_map(function($a) { return $a->valor; }, $variaveis));
      echo '</td><td>';
      echo  CHtml::ajaxLink("x", $this->createUrl('variaveis/deleteOpt',array('q'=>$q->id,'s'=>$seq)), array(
        'success' => 'js: function(removido) {
            if(removido) {
              $("#seq-'.$seq.'").remove();
            } else {
              alert("Não foi possível excluir, tente atualizar a página.");
            }
        }',
        'error' => 'js: function() { alert("Falha ao excluir, tente atualizar a página."); }',
      ),array(
        'class' => 'btn btn-xs btn-danger',
        'id' => 'deleteOpt'.$seq,
        'confirm' => "Certeza?"
      ));

      echo '</td></tr>';
    }
  }
  ?>
</table>

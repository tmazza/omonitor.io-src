<div class="sh-row">
    <div class="columns medium-2 textCenter">
        Verdadeira? <?php echo CHtml::checkBox("novaAfirmacaoGabarito[{$id}]", $alt->verdadeira, array('class' => 'inline')); ?>
    </div>
    <div class="columns medium-10">
        <?php echo CHtml::textField("novaAfirmacao[{$id}]", $alt->valor, array('class' => 'medium-12 columns')); ?>
    </div>
</div>
<div class="loadNovaAfirmacao"></div>
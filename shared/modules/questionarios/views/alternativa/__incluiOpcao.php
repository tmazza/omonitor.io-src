&nbsp;&nbsp;&nbsp;
<?=CHtml::beginForm('','post',array('style'=>'display:inline-block;'));?>
<?=CHtml::hiddenField("questao_id", $q->id);?>
<table>
  <?php $labels = array_keys($params); ?>
  <tr><td><?=implode('</td><td>',$labels)?></td></tr>
  <?php $campos = array_map(function ($v) { return CHtml::textField("valor[{$v}]", null, array('required'=>'true','style'=>'width:100px;')); },$labels); ?>
  <tr><td><?=implode('</td><td>',$campos)?></td></tr>
</table>
&nbsp;
<?=CHtml::ajaxSubmitButton("Incluir", $this->createUrl('variaveis/newOpt'),array(
  'success' => 'js: function(data) {
      if(data === "FALSE"){
        alert("Falha ao criar, tente atualizar a página.");
      } else {
        data = JSON.parse(data);
        $("#parametros").prepend("<tr><td>" + data.join("</td><td>") + "</td><td>---</td></tr>");
      }
  }',
  'error' => 'js: function() {alert("NOP!");}',
),array(
  'class' => 'btn btn-success btn-xs',
  'id' => 'newOpt',
));
?>
<?=CHtml::endForm();?>

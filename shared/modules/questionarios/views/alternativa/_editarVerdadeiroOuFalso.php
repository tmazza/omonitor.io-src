<?php $alternativas = $questao->alternativas; ?>
<div class="sh-row">
    <div class="columns medium-12">
        <?=
        CHtml::ajaxLink('Nova afirmação', $this->createUrl('alternativa/novaAfirmacao'), array(
            'beforeSend' => 'js: function() { $(".loadNovaAfirmacao").html("Criando..."); }',
            'success' => 'js: function(data) { $(".loadNovaAfirmacao").html(data).removeClass("loadNovaAfirmacao"); }',
            'error' => 'js: function(data) { $(".loadNovaAfirmacao").html("Erro ao criar, tente atualizar a página."); }',
                ), $htmlOptions = array(
            'class' => 'btn btn-info btn-sm',
                )
        );
        ?>
        <br><br>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edica-questao',
            'enableAjaxValidation' => false,
        ));
        ?>
        <?php foreach ($alternativas as $alt): ?>
            <?php $this->renderPartial('_VFEditarAfirmacao', array('alt' => $alt)); ?>

        <?php endforeach; ?>
        <div class="loadNovaAfirmacao"></div>

        <br><br>
        <div class="sh-row">
            <div class="columns medium-2">
                <?php echo CHtml::submitButton($questao->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?php
$this->pageTitle = 'Edição de alternativas';
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('questao/listar')),
    ShCode::makeItem('Editar pergunta', $this->createUrl('questao/editar', array('id' => $questao->id))),
);
?>
<hr>
<b>Enunciado:</b>
<?= $questao->enunciado; ?>
<hr>
<?php
if ($questao->tipo == Questao::CincoAlternativas) {
    $this->renderPartial('_editarCincoAlternativas', array(
        'questao' => $questao,
    ));
} elseif ($questao->tipo == Questao::VerdadeiroOuFalseo) {
    $this->renderPartial('_editarVerdadeiroOuFalso', array(
        'questao' => $questao,
    ));
} elseif (in_array($questao->tipo, array(Questao::EntradaSimples, Questao::EntradaExpressao))) {
    $this->renderPartial('_editarEntradaSimples', array(
        'questao' => $questao,
    ));
}

<b>Variáveis:</b><br>
<div class="sh-row">
    <div class="columns medium-12">
        <?php $this->renderPartial('__parametros', array('q' => $questao)); ?>
    </div>
</div>
<hr>
<?php $alternativas = $questao->alternativas; ?>
<div class="sh-row">
    <div class="columns medium-12">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edica-questao',
            'enableAjaxValidation' => false,
        ));
        ?>
        <div class="sh-row">
            <div class="columns medium-1">
                <?= CHtml::label('Gabarito: ', 'gabarito') ?>
            </div>
            <div class="columns medium-11">
                <?php $gab = is_null($questao->gabarito) ? null : $questao->gabarito->valor; ?>
                <?= CHtml::textField('gabarito', $gab, array('id' => 'gabarito', 'class'=>'form-control input-lg')) ?>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <div class='sh-row'>
          <div class='columns medium-12'>
            <b>Função de correção:</b> <a href='#!' onclick="$(this).next().slideToggle();">Editar</a>
            <div style="display: none;">
              <?php
              if(is_null($questao->gabarito)){
                $func = <<<EOT
def corrigi(resposta,gabarito):
    return resposta == gabarito
EOT;
              } else {
                $func = stripslashes($questao->gabarito->chave);
              }
              ?>
              <?= CHtml::textArea('funcComp', $func,array('class'=>'sage')) ?>
            </div>
            <hr>
          </div>
        </div>
        <hr>
        <div class="sh-row">
            <div class="columns medium-12">
                <?php echo CHtml::submitButton($questao->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

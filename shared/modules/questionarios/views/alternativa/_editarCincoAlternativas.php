<?php
$alternativas = $questao->alternativas;
?>
<div class="sh-row">
    <div class="columns medium-12">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edica-questao',
            'enableAjaxValidation' => false,
        ));
        ?>
        <div class="sh-row">
            <div class="columns medium-1">
                Gabarito?
            </div>                                
            <div class="columns medium-offset-11">
            </div>
        </div>
        <?php foreach ($alternativas as $alt): ?>
            <div class="sh-row">
                <div class="columns medium-1 textCenter">
                    <?= CHtml::radioButton('gabarito', $alt->verdadeira, array('value' => $alt->id)) ?>
                </div>                                
                <div class="columns medium-1">
                    <?= $alt->chave; ?>)
                </div>                                
                <div class="columns medium-6">
                    <?php echo CHtml::activeTextField($alt, "[$alt->id]valor", array('class' => 'medium-12 columns')); ?>
                </div>
                <div class="columns medium-4">
                    <?php echo $form->error($alt, 'valor'); ?>
                </div>
            </div>
        <?php endforeach; ?>
        <br>
        <div class="sh-row">
            <div class="columns medium-12">
                <?php echo CHtml::submitButton($questao->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
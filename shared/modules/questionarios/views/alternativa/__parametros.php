<?php
$params = ShView::extraiParametros($q->enunciado);
asort($params);
if(count($params) > 0){
  $this->renderPartial('__incluiOpcao',array('params'=>$params,'q'=>$q));
  $this->renderPartial('__edicaoOpcao',array('params'=>$params,'q'=>$q));

} else {
 echo '<span class="hint">Nenhum parâmetro encontado.</span>';
}

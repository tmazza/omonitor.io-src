<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuestionarioController
 *
 * @author tiago
 */
class QuestionarioController extends QuestionariosController {

    /**
     * Lista questionários do autor
     */
    public function actionListar() {
        $questionarios = QstQuestionario::model()->doAutor()->findAll();
        $this->render('listar', array(
            'questionarios' => $questionarios,
        ));
    }

    public function actionImprimir($id) {
        $this->layout = 'questionarios.views.layouts.imprimir';
        $questionario = $this->getQuestionario($id);
        $this->render('imprimir', array(
            'questionario' => $questionario,
        ));
    }

    /**
     * Criação de novo questionário
     */
    public function actionNovo() {
        $model = new QstQuestionario();

        if (isset($_POST['QstQuestionario'])) {
            $model->attributes = $_POST['QstQuestionario'];
            $model->user_id = Yii::app()->user->id;
            if ($model->save()) {
                $this->redirect($this->createUrl('questionario/editar', array('id' => $model->id)));
            }
        }

        $this->render('novo', array(
            'model' => $model,
        ));
    }

    /**
     * Visualizaxao de questionário
     * @param type $id
     */
    public function actionEditar($id) {
        $questionario = $this->getQuestionario($id);
        $questoes = QstQuestao::model()->doAutor()->findAll();
        $this->render('editar', array(
            'questionario' => $questionario,
            'questoes' => $questoes,
        ));
    }

    /**
     * Conecta uma questão ao questionário
     */
    public function actionConectarQuestao($id, $contID) {
        $questionario = $this->getQuestionario($id);
        $questao = $this->getQuestao($contID);
        if ($questionario->criaConexao($questao)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Questão incluída.');
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questão não foi incluída.');
        }
        $this->redirect($this->createUrl('questionario/editar', array('id' => $questionario->id)));
    }

    /**
     * Conecta uma questão ao questionário
     */
    public function actionDesconectarQuestao($id, $contID) {
        $questionario = $this->getQuestionario($id);
        $questao = $this->getQuestao($contID);
        if ($questionario->desfazConexao($questao)) {
            echo '1';
        } else {
            echo ('Questão não foi removida.');
        }
    }

    /**
     * Configuração do formulário
     * @param type $id
     */
    public function actionConfigurar($id) {
        $questionario = $this->getQuestionario($id);

        if (isset($_POST['QstQuestionario'])) {
            $questionario->nome = $_POST['QstQuestionario']['nome'];
            $questionario->publicado = $_POST['QstQuestionario']['publicado'];
            if ($questionario->save()) {
                ShCode::setFlashSucesso('Questionário atualizado.');
                $this->redirect($this->createUrl('questionario/editar', array('id' => $questionario->id)));
            }
        }

        $this->render('configurar', array(
            'model' => $questionario,
        ));
    }

    public function actionExcluir($id){
      $questionario = $this->getQuestionario($id);
      CursoAulaQuestionario::model()->deleteAll(array(
        'condition' => "questionario_id = {$questionario->id} AND user_id = " . Yii::app()->user->id,
      ));
      QuestaoResposta::model()->deleteAll(array(
        'condition' => "questionario_id = {$questionario->id}",
      ));
      QuestionarioQuestao::model()->deleteAll(array(
        'condition' => "questionario_id = {$questionario->id} AND user_id = " . Yii::app()->user->id,
      ));
      if($questionario->delete()){
        Yii::app()->user->setFlash(self::SUCS_FLASH,'Questionário excluído.');
      } else {
        Yii::app()->user->setFlash(self::ERRO_FLASH,'Falha ao excluir questionário.');
      }
      $this->redirect($this->createUrl('/meuEspaco/questionarios/questionario/listar'));
    }

}

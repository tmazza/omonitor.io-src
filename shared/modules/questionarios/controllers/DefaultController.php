<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends QuestionariosController {

    public function actionIndex() {
        $this->redirect($this->createUrl('questionario/listar'));
    }
    
}

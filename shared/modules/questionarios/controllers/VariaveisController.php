<?php

/**
 * Description of VariaveisController
 *
 * @author tiago
 */
class VariaveisController extends QuestionariosController {


  public function actionDeleteOpt($q,$s){
    echo (int) QuestaoVariavel::excluir($q,$s);
  }

  // public function actionUpdateOpt(){
  //   if(isset($_POST['questao_id']) && isset($_POST['variavel_id']) && isset($_POST['sequencia']) && isset($_POST['valor'])) {
  //     $option = QuestaoVariavel::model()->findByAttributes(array(
  //       'user_id' => Yii::app()->user->id,
  //       'questao_id' => (int) $_POST['questao_id'],
  //       'variavel_id' => $_POST['variavel_id'],
  //       'sequencia' => (int) $_POST['sequencia'],
  //     ));
  //     if(is_null($option)){
  //       echo 'FALSE';
  //     } else {
  //       $option->valor = $_POST['valor'];
  //       echo $option->update(array('valor')) ? $option->valor : 'FALSE';
  //     }
  //   }
  // }

  public function actionNewOpt(){
    if(isset($_POST['questao_id']) && isset($_POST['valor'])) {
      $questaoId = (int) $_POST['questao_id'];
      $valores = (array) $_POST['valor'];
      $sequencia = QuestaoVariavel::novaSequencia($questaoId);

      $transaction = Yii::app()->db->beginTransaction();
      $erro = false;
      foreach ($valores as $id => $v) {
        $model = new QuestaoVariavel();
        $model->questao_id = $questaoId;
        $model->variavel_id = $id;
        $model->valor = $v;
        $model->user_id = Yii::app()->user->id;
        $model->sequencia = $sequencia;
        if(!$model->save()){
          print_r($model->getErrors());
          $erro = true;
        }
      }

      if($erro){
        $transaction->rollback();
        echo 'FALSE';
      } else {
        $transaction->commit();
        echo json_encode(array_values($valores));
      }
    }
  }

}

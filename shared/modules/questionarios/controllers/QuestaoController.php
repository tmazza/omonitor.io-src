<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QuestionarioController
 *
 * @author tiago
 */
class QuestaoController extends QuestionariosController {

    protected function beforeAction($action) {
        if ($this->action->id == 'editar') {
            $this->processaLatex = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lista questões do autor
     */
    public function actionListar() {
        $questoes = QstQuestao::model()->doAutor()->findAll(array('order'=>'id DESC'));
        $this->render('listar', array(
            'questoes' => $questoes,
        ));
    }

    /**
     * Cria nova questão
     */
    public function actionNova($rt = FALSE) {
        $model = new QstQuestao();

        if (isset($_POST['QstQuestao'])) {
            $model->attributes = $_POST['QstQuestao'];
            $model->user_id = Yii::app()->user->id;
            $model->interacao = addslashes(isset($_POST['QstQuestao']['interacao']) ? $_POST['QstQuestao']['interacao'] : '');
            if ($model->validate()) {
                if ($model->salvaQuestao()) {
                    Yii::app()->user->setFlash(self::SUCS_FLASH, 'Questão criada.');
                } else {
                    Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questão não foi criada.');
                }
                if ($rt) {
                    $this->redirect(ShCode::interpretaLinkRetorno($rt));
                } else {
                    $this->redirect($this->createUrl('alternativa/editar', array('id' => $model->id)));
                }
            }
        }

        $this->render('nova', array(
            'model' => $model,
        ));
    }

    /**
     * Mostra questao
     * @param type $id
     */
    public function actionEditar($id) {
        $model = $this->getQuestao($id);
        if (isset($_POST['QstQuestao'])) {
            $tipo = $model->tipo;
            $model->attributes = $_POST['QstQuestao'];
            $model->user_id = Yii::app()->user->id;
            $model->interacao = addslashes(isset($_POST['QstQuestao']['interacao']) ? $_POST['QstQuestao']['interacao'] : '');
            $model->tipo = $tipo;
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Edição salva.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao editar.');
            }
            $this->redirect($this->createUrl('questao/listar', array('id' => $model->id)));
        }
        $this->render('editar', array(
            'questao' => $model,
        ));
    }

    public function actionExcluir($id) {
        QstQuestao::model()->excluir($id);
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Questão excluída.');
        $this->redirect($this->createUrl('questao/listar'));
    }

    /*
     * Responder uma questao
     */

    public function actionResponder($id) {
        $questao = $this->getQuestao($id);
        //var_dump($questao);die();
        $resposta = null;
        $correcao = null;

        if (isset($_POST['resposta'])) {
            $resposta = $_POST['resposta'];
            $correcao = "correta"; //correta, errada
        }

        $this->render('responder', array('questao' => $questao, 'resposta' => $resposta, 'correcao' => $correcao));
    }

}

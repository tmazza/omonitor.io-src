<?php

/**
 * Description of AlternativaController
 *
 * @author tiago
 */
class AlternativaController extends QuestionariosController {

    public function actionEditar($id) {
        $questao = $this->getQuestao($id);

        if ($questao->tipo == Questao::CincoAlternativas) {
            $this->trataEicaoCincoAlternativas($questao);
        } elseif ($questao->tipo == Questao::VerdadeiroOuFalseo) {
            $this->trataEicaoVerdadeiroOuFalso($questao);
        } elseif (in_array($questao->tipo, array(Questao::EntradaSimples, Questao::EntradaExpressao))) {
            $this->trataEicaoEntradaSimples($questao);
        }

        $this->render('editar', array(
            'questao' => $questao,
        ));
    }

    /**
     * Edição de questões com cinco alternativas
     */
    public function trataEicaoCincoAlternativas($questao) {
        $items = $questao->alternativas;
        if (isset($_POST['QstQuestaoAlternativa'])) {
            $postAlternativas = $_POST['QstQuestaoAlternativa'];
            $valid = true;
            foreach ($items as $item) {
                if (isset($postAlternativas[$item->id])) {
                    $item->attributes = $postAlternativas[$item->id];
                }
                $valid = $item->validate() && $valid;
                if (!$valid) {
                    echo '<pre>';
                    print_r($item->getErrors());
                }
            }
            if ($valid) {
                if (isset($_POST['gabarito'])) {
                    $gabarito = $_POST['gabarito'];
                    $itemsID = CHtml::listData($items, 'id', 'id');
                    if (in_array($gabarito, $itemsID)) {
                        foreach ($items as $alt) {
                            $alt->verdadeira = ($alt->id == $gabarito);
                            $alt->update('valor','verdadeira');
                        }
                        $this->redirect($this->createUrl('questao/listar', array('id' => $questao->id)));
                    }
                }
            }
        }
    }

    /**
     *
     */
    private function trataEicaoVerdadeiroOuFalso($questao) {
        if (isset($_POST['novaAfirmacao'])) {
            $novasAfirmacoes = $_POST['novaAfirmacao'];

            // Para cada afirmação cria registro. Campo verdadeira é definido de acordo com o vetor nameAfi em $_POST
            foreach ($novasAfirmacoes as $k => $afi) {
                if (strlen($afi) > 0) {
                    $verdadeira = isset($_POST['novaAfirmacaoGabarito'][$k]) && $_POST['novaAfirmacaoGabarito'][$k] == '1';
                    $questao->adicionarNovaAlternativa(null, $afi, $verdadeira);
                }
            }
        }
        $items = $questao->alternativas;
        if (isset($_POST['QstQuestaoAlternativa'])) {
            $valid = true;
            foreach ($items as $item) {
                if (isset($_POST['QstQuestaoAlternativa'][$item->id])) {
                    $item->attributes = $_POST['QstQuestaoAlternativa'][$item->id];
                }
                $valid = $item->validate() && $valid;
            }
            if ($valid) {
                foreach ($items as $item) {
                    $item->save();
                }
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Alternativas salvas.');
                $this->redirect($this->createUrl('questao/listar', array('id' => $questao->id)));
            }
        }
    }

    private function trataEicaoEntradaSimples($questao) {
        if (isset($_POST['gabarito']) && isset($_POST['funcComp'])) {
            if(is_null($questao->gabarito)){
              $resultado = $questao->adicionarNovaAlternativa(addslashes($_POST['funcComp']), $_POST['gabarito'], true);
            } else {
              $gabarito = $questao->gabarito;
              $gabarito->chave = addslashes($_POST['funcComp']);
              $gabarito->valor = $_POST['gabarito'];
              $resultado = $gabarito->update(array('chave','valor'));
            }
            if ($resultado) {
                ShCode::setFlashSucesso("Gabarito atualizado.");
            } else {
                ShCode::setFlashErro("Gabarito não foi atualizado.");
            }
            $this->redirect($this->createUrl('questao/listar', array('id' => $questao->id)));
        }
    }

    /**
     * Cria nova afirmação
     */
    public function actionNovaAfirmacao() {
        $alt = new QstQuestaoAlternativa();
        $id = hash('crc32', microtime(true));
        echo $this->renderPartial('_VFNovaAfirmacao', array(
            'alt' => $alt,
            'id' => $id,
                ), true);
    }


}

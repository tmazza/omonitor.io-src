<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class GerenciamentoCursosController extends EditorController {

    protected function beforeAction($action) {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.column1';
        return parent::beforeAction($action);
    }
    
    
    /**
     * Busca curso do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getCurso($id) {
        $curso = GenCurCurso::model()
                ->doAutor()
                ->findByPk($id);
        if (is_null($curso)) {
            Yii::app()->user->setFlash('alert-error', 'Tópico não econtrado.');
            $this->redirect($this->createUrl('curso/listar'));
        }
        return $curso;
    }


}

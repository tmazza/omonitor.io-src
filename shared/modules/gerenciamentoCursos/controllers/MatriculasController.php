<?php

/**
 * Description of MatriculasController
 *
 * @author tiago
 */
class MatriculasController extends GerenciamentoCursosController {

    public function actionListar($id) {
        $curso = $this->getCurso($id);
                
        $this->render('listar', array(
            'matriculas' => $curso->matriculas,
        ));
    }

}

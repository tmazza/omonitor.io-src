<?php

/**
 * Description of TurmaController
 *
 * @author tiago
 */
class CursoController extends GerenciamentoCursosController {

    /**
     * Lista cursos do autor
     */
    public function actionListar() {
        $cursos = new CActiveDataProvider(GenCurCurso::model()->doAutor());
        $this->render('listar', array(
            'cursos' => $cursos,
        ));
    }

    /**
     * Criação de novo curso
     */
    public function actionNovo() {
        $curso = new GenCurCurso();

        if (isset($_POST['GenCurCurso'])) {
            $curso->attributes = $_POST['GenCurCurso'];
            $curso->user_id = Yii::app()->user->id;
            if ($curso->validate()) {
                $curso->criarNovoCurso();
                // TODO: redirect para curso
                $this->redirect($this->createUrl('curso/editar', array('id' => $curso->id)));
            }
        }

        $this->render('novoCurso', array(
            'model' => $curso,
        ));
    }

    /**
     * Apresnetacao do curso
     * @param type $id
     */
    public function actionEditar($id) {
        $curso = $this->getCurso($id);
        $this->render('editar', array(
            'curso' => $curso,
        ));
    }

    /**
     * Conecta nova aula ao curso
     */
    public function actionNovaAula($id) {
        $curso = $this->getCurso($id);

        if (isset($_POST['nomeAula'])) {
            $curso->novaAula($_POST['nomeAula']);
            $this->redirect($this->createUrl('curso/editar', array('id' => $curso->id)));
        }

        $this->render('novaAula', array(
            'curso' => $curso,
        ));
    }

    /**
     * Configuração de forma de acesso ao curso
     * @param type $id
     */
    public function actionConfigurar($id) {
        $curso = $this->getCurso($id);

        if (isset($_POST['GenCurCurso'])) {
            $curso->attributes = $_POST['GenCurCurso'];
            if ($curso->update(array('nome', 'formaDeMatricula', 'senhaDeMatricula', 'publicado'))) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Configurações do curso atualizadas.');
                $this->redirect($this->createUrl('curso/editar', array('id' => $curso->id)));
            }
        }

        $this->render('configurar', array(
            'curso' => $curso,
        ));
    }

    public function actionOrdemAulas($id){
      $curso = $this->getCurso($id);

      if(isset($_POST['ordem'])){

        $ordem = $_POST['ordem'];

        foreach ($ordem as $aulaId => $o) {
          CursoAula::model()->updateAll([
            'ordem'=>$o,
          ],"user_id = " . Yii::app()->user->id . " AND id = " . (int) $aulaId );
        }

        $this->redirect($this->createUrl('curso/editar', array('id' => $curso->id)));

      }

      $this->render('ordemAulas', array(
        'curso' => $curso,
      ));
    }


}

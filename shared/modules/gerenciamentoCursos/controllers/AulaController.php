<?php

/**
 * Description of AulaController
 *
 * @author tiago
 */
class AulaController extends GerenciamentoCursosController {

    public function actionAddVideo($id) {
        $aula = $this->getAula($id);

        if (isset($_POST['urlVideo'])) {

            if ($aula->addVideo($_POST['urlVideo'])) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Vídeo incluído.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Vídeo não pode ser incluído.');
            }
            $this->redirect($this->createUrl('curso/editar', array('id' => $aula->curso->id)));
        }

        $this->render('addVideo', array(
            'aula' => $aula,
        ));
    }

    /**
     * Lista tópicos do autor e possibilita criação de novo para utilizar no curso
     * @param type $id
     */
    public function actionNovoConteudo($id) {
        $aula = $this->getAula($id);
        $topicos = Topico::model()->doAutor()->findAll();
        $this->render('novoConteudo', array(
            'aula' => $aula,
            'topicos' => $topicos,
        ));
    }

    /**
     * Conecta um conteúdo à árvore
     * @param type $id
     * @param type $contID
     */
    public function actionConetarConteudo($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->conectarConteudo($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Conteúdo adicionado à ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Conteúdo não foi conectado.');
        }
        $this->redirect($this->createUrl('aula/novoConteudo', array('id' => $aula->id)));
    }

    /**
     * Desconecta um conteúdo da árvore
     * @param type $id
     * @param type $contID
     */
    public function actionDesconetarConteudo($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->desconectarConteudo($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Conteúdo removido de ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Conteúdo não foi removido.');
        }
        $this->redirect($this->createUrl('aula/novoConteudo', array('id' => $aula->id)));
    }

    /**
     * Lista arquivos do autor e possibilita upaload de novo arquivo
     * @param type $id
     */
    public function actionNovoMaterialDeApoio($id) {
        $aula = $this->getAula($id);
        $arquivos = Arquivo::model()->doAutor()->findAll();
        $this->render('novoMaterialDeApoio', array(
            'aula' => $aula,
            'arquivos' => $arquivos,
        ));
    }

    /**
     * Conecta material de apoio à árvore
     * @param type $id
     * @param type $contID
     */
    public function actionConetarMaterialDeApoio($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->conectarMaterialDeApoio($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Material de apoio adicionado à ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Material de apoio não foi conectado.');
        }
        $this->redirect($this->createUrl('aula/novoMaterialDeApoio', array('id' => $aula->id)));
    }

    /**
     * Desconecta um material de apoio da árvore
     * @param type $id
     * @param type $contID
     */
    public function actionDesconetarMaterialDeApoio($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->desconectarMaterialDeApoio($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Material de apoio  removido de ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Material de apoio  não foi removido.');
        }
        $this->redirect($this->createUrl('aula/novoMaterialDeApoio', array('id' => $aula->id)));
    }

    /**
     * Lista tópicos do autor e possibilita criação de novo para utilizar no curso
     * @param type $id
     */
    public function actionNovoQuestionario($id) {
        $aula = $this->getAula($id);
        $questionarios = Questionario::model()->doAutor()->findAll();
        $this->render('novoQuestionario', array(
            'aula' => $aula,
            'questionarios' => $questionarios,
        ));
    }

    /**
     * Conecta questionario à árvore
     * @param type $id
     * @param type $contID
     */
    public function actionConetarQuestionario($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->conectarQuestionario($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Questionário adicionado à ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não foi conectado.');
        }
        $this->redirect($this->createUrl('aula/novoQuestionario', array('id' => $aula->id)));
    }

    /**
     * Desconecta um questionario
     * @param type $id
     * @param type $contID
     */
    public function actionDesconetarQuestionario($id, $contID) {
        $aula = $this->getAula($id);
        if ($aula->desconectarQuestionario($contID)) {
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Questionário removido de ' . $aula->nome);
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Questionário não foi removido.');
        }
        $this->redirect($this->createUrl('aula/novoQuestionario', array('id' => $aula->id)));
    }

    /**
     * Busca aula do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    public function getAula($id) {
        $aula = CursoAula::model()
                ->doAutor()
                ->findByPk((int) $id);
        if (is_null($aula)) {
            Yii::app()->user->setFlash('alert-error', 'Aula não econtrada.');
            $this->redirect($this->createUrl('curso/listar'));
        }
        return $aula;
    }

    public function actionExcluir($id){
      $aula = $this->getAula($id);
      $aula->desconectarTudo();
      if($aula->delete()){
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Aula removida.');
      } else {
        Yii::app()->user->setFlash(self::ERRO_FLASH, 'Erro ao remover aula. Entre em contato com suporte.');
      }
      $this->redirect($this->createUrl('curso/editar', array('id' => $aula->curso_id)));
    }

}

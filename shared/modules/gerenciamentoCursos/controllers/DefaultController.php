<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends GerenciamentoCursosController {

    public function actionIndex() {
        $this->redirect($this->createUrl('curso/listar'));
    }

}

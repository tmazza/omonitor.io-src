<?php

/**
 * This is the model class for table "turma".
 *
 * The followings are the available columns in table 'turma':
 * @property integer $id
 * @property string $nome
 * @property integer $inicioMatricula
 * @property integer $fimMatricula
 * @property integer $publicado
 * @property integer $lastUpdate
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class GenCurCurso extends CActiveRecord {

    protected function beforeSave() {
//        $this->senhaDeMatricula = CPasswordHelper::hashPassword($this->senhaDeMatricula);
        return parent::beforeSave();
    }

    public function tableName() {
        return 'curso';
    }

    public static function getTableName() {
        return 'curso';
    }

    public function rules() {
        return array(
            array('nome, user_id, formaDeMatricula', 'required'),
            array('inicioMatricula,user_id, fimMatricula, publicado, lastUpdate', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 256),
            array('id, nome, formaDeMatricula, senhaDeMatricula, inicioMatricula, fimMatricula, publicado, lastUpdate, user_id', 'safe'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'aulas' => array(self::HAS_MANY, 'CursoAula', 'curso_id','order'=>'ordem ASC'),
            'matriculas' => array(self::MANY_MANY, 'User', CursoAluno::getTableName() . '(aluno_id,curso_id)'),
        );
    }

    /**
     * Cria novo curso.
     */
    public function criarNovoCurso() {
        $this->save();
    }

    /**
     * Cria nodo de entrada em tax item para curso
     */
    private function criaNodoCurso() {
        $nodoCurso = new GenCurTaxItem();
        $nodoCurso->nome = 'Raiz: ' . $this->nome;
        $nodoCurso->tipo = GenCurTaxItem::TipoCurso;
        $nodoCurso->user_id = Yii::app()->user->id;
        $nodoCurso->save();
        return $nodoCurso;
    }

    /**
     * Cria nova aula
     * @param type $cursoID
     * @param type $nomeAula
     */
    public function novaAula($nomeAula) {
        $aula = new CursoAula();
        $aula->nome = $nomeAula;
        $aula->curso_id = $this->id;
        $aula->user_id = Yii::app()->user->id;
        return $aula->save();
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'inicioMatricula' => 'Início Matricula',
            'fimMatricula' => 'Fim Matricula',
            'publicado' => 'Publicado',
            'lastUpdate' => 'Última atualização',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    public function behaviors() {
        return array(
            'dataAtualizacao' => array(
                'class' => 'LastUpdateBehavior',
            ),
        );
    }

}

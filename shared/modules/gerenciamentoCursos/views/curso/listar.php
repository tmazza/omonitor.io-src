<?php
$this->menuContexto = array(
    ShCode::makeItem('Novo curso', $this->createUrl('curso/novo')),
);
?>
<h1>Cursos</h1>
<div class="sh-row">
    <div class="column medium-12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $cursos,
            'itemsCssClass' => 'table',
            'columns' => array(
                array(
                    'type' => 'html',
                    'name' => 'nome',
                    'value' => 'CHtml::link($data->nome, Yii::app()->controller->createUrl("curso/editar", array("id" => $data->id)))',
                ),
                array(
                    'name' => 'lastUpdate',
                    'value' => 'date("d/m/Y", $data->lastUpdate)',
                ),
                array(
                    'type' => 'html',
                    'header' => 'matrículas',
                    'value' => 'CHtml::link("Gerenciar matrículas", Yii::app()->controller->createUrl("matriculas/listar", array("id" => $data->id)))',
                ),
            ),
        ));
        ?>
    </div>
</div>

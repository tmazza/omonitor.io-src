<div class="box box-primary">
  <div class="box-header" style="cursor:pointer" onclick="$(this).next().slideToggle();">
    <?= $aula->nome ?>
    <div class="btn-group" style="float:right">
      <?php echo CHtml::link(is_null($aula->urlVideo) ? 'Incluir vídeo' : 'Editar vídeo', $this->createUrl('aula/addVideo', array('id' => $aula->id)),['class'=>'btn btn-default btn-sm']); ?>
      <?php echo CHtml::link('Tópicos', $this->createUrl('aula/novoConteudo', array('id' => $aula->id)),['class'=>'btn btn-primary btn-sm']); ?>
      <?php echo CHtml::link('questionário', $this->createUrl('aula/novoQuestionario', array('id' => $aula->id)),['class'=>'btn btn-default btn-sm']); ?>
      <?php echo CHtml::link('Material de apoio', $this->createUrl('aula/novoMaterialDeApoio', array('id' => $aula->id)),['class'=>'btn btn-default btn-sm']); ?>
      <?php echo CHtml::link('x', $this->createUrl('aula/excluir', array('id' => $aula->id)),['class'=>'btn btn-danger btn-sm','confirm'=>'Confirma exclusão de aula?']); ?>
    </div>
  </div>
  <div class="box-body" style="display:none;">
          <?php if(!is_null($aula->urlVideo)): ?>
          <?php echo $aula->getIframe('300px','260px'); ?>
          <?php endif; ?>

          <h4>conteúdo</h4>
          <?php
          foreach ($aula->topicos as $t) {
              echo $t->nome . '<br>';
          }
          ?>
          <h4>Questionário</h4>
          <?php
          foreach ($aula->questionarios as $t) {
              echo $t->nome . '<br>';
          }
          ?>
          <h4>Material de apoio</h4>
          <?php
          foreach ($aula->arquivos as $t) {
              echo $t->nome . '<br>';
          }
          ?>
  </div>
</div>

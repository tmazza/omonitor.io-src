<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('curso/listar')),
);
?>
<div class="sh-row">
    <div class="columns medium-12">
        <h1>Novo curso</h1>
        <?php $this->renderPartial('_formNovoCurso', array('model' => $model)); ?>
    </div>
</div>

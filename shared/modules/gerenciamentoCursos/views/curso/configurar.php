<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('curso/editar', array('id' => $curso->id))),
);
?>
<h1><?= $curso->nome; ?> <span class="hint">#configurações</span></h1>
<?php $this->renderPartial('_formNovoCurso', array('model' => $curso)); ?>

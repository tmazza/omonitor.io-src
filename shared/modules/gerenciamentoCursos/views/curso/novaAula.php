<div class="sh-row">
    <div class="columns medium-12">
        <h1>Nova aula de <?= $curso->nome; ?></h1>
        <?php echo CHtml::beginForm(); ?>


        <div class="sh-row">
            <div class="columns medium-2">
                <?php echo CHtml::label('Nome: ', 'nomeAula'); ?>
            </div>
            <div class="columns medium-4">
                <?php echo CHtml::textField('nomeAula', ''); ?>
            </div>
            <div class="columns medium-2">
                <?php echo CHtml::submitButton('Incluir', array('class' => 'btn btn-success right')); ?>
            </div>
            <div class="columns medium-offset-4">
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>

<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('curso/editar',['id'=>$curso->id])),
);
$this->pageTitle = 'Edição de ordem ' . $curso->nome;
?>
<?=CHtml::beginForm();?>
  <?php foreach ($curso->aulas as $a): ?>
    <input name='ordem[<?=$a->id?>]' value="<?=$a->ordem?>" size='2' style='width:40px;' type='number'/>
    <?=$a->nome?><br>
  <?php endforeach; ?>
  <br><br>
  <?=CHtml::submitButton("Atualizar",['class'=>'btn btn-primary']);?>
<?=CHtml::endForm();?>

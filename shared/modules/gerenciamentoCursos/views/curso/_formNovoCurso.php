<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'novo-curso',
    'enableAjaxValidation' => false,
        ));
?>


<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->textField($model, 'nome'); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'nome'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'publicado'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->dropDownList($model, 'publicado',[0=>'Não',1=>'Sim']); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'publicado'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'formaDeMatricula'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->dropDownList($model, 'formaDeMatricula', Curso::formasDeMatricula()); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'formaDeMatricula'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12x box box-solid">
        <ul>
            <li>Livre: qualquer aluno pode se matricular.</li>
            <li>Por solicitação: alunos solicitam ao professor a matrícula.</li>
            <li>Por senha de acesso: somente os alunos que possuirem a senha podem realizar a matrícula.</li>
            <li>Com pagamento: somente os alunos que pagarem pelo terão acesso.</li>
            <li>Com pagamento e free trial: aberto a todos alunos para matrícula, mas após periodo de tempo aluno é desmatribulado se não pagar.</li>
        </ul>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-2">
        <?php echo $form->labelEx($model, 'senhaDeMatricula'); ?>
    </div>
    <div class="columns medium-4">
        <?php echo $form->passwordField($model, 'senhaDeMatricula'); ?>
    </div>
    <div class="columns medium-6">
        <?php echo $form->error($model, 'senhaDeMatricula'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

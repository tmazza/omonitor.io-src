<?php
$this->menuContexto = array(
    ShCode::makeItem('Voltar para lista de cursos', $this->createUrl('curso/listar')),
    ShCode::makeItem('Nova aula', $this->createUrl('curso/novaAula', array('id' => $curso->id))),
    ShCode::makeItem('Configurar', $this->createUrl('curso/configurar', array('id' => $curso->id))),
    ShCode::makeItem('Editar ordem', $this->createUrl('curso/ordemAulas', array('id' => $curso->id))),
);
$this->pageTitle = $curso->nome;
?>
<?php
foreach ($curso->aulas as $aula) {
    $this->renderPartial('_edicaoDeAula', array(
        'aula' => $aula,
    ));
}
?>

<?php
$this->pageTitle = $aula->curso->nome . ' ' . $aula->nome;
$curso = $aula->curso;
$this->menuContexto = array(
    ShCode::makeItem('Voltar para o curso', $this->createUrl('curso/editar', array('id' => $curso->id))),
    ShCode::makeItem('Adicionar questionário', '#'),
    ShCode::makeItem('Adicionar material de apoio', $this->createUrl('../arquivos/upload/novo'))
);
?>
<div class="sh-row">    
    <div class="medium-12 column">    
        <h4>Conectados à aula</h4>
        <?php
        if ($aula->temTopicos()) {
            foreach ($aula->topicos as $t) {
                echo $t->nome . ' - ';
                echo CHtml::link('Desonectar da aula', $this->createUrl('aula/DesconetarConteudo', array(
                            'id' => $aula->id,
                            'contID' => $t->id,
                        )), array('class' => 'btn btn-danger'));
                echo '<br>';
            }
        } else {
            echo '<p class="hint">Nenhum tópico conectado.</p>';
        }
        ?>
    </div>
</div>
<div class="sh-row">    
    <div class="medium-12 column">    
        <h4>Outros tópicos</h4>
        <?php
        $this->renderPartial('_listaTopicos', array(
            'aula' => $aula,
            'topicos' => $topicos,
        ));
        ?>  
    </div>
</div>
<?php
$curso = $aula->curso;
$this->menuContexto = array(
    ShCode::makeItem('Voltar para o curso', $this->createUrl('curso/editar', array('id' => $curso->id))),
    ShCode::makeItem('Adicionar conteúdo', '#'),
    ShCode::makeItem('Adicionar material de apoio', '#')
);
?>
<h1><?= $aula->nome; ?> <span class="hint">#questionários</span></h1>

<div class="sh-row">    
    <div class="medium-3 column">    
        <h5>Novo arquivo</h5>
        <?php echo CHtml::link('Incluir novo questionario', $this->createUrl('../questionarios/questionario/novo'), array('class' => 'btn btn-success')); ?>
    </div>
    <div class="medium-9 column">    
        <h5>Arquivos disponíveis</h5>
        <?php
        $this->renderPartial('_listaQuestionarios', array(
            'aula' => $aula,
            'questionarios' => $questionarios,
        ));
        ?>  
    </div>
</div>
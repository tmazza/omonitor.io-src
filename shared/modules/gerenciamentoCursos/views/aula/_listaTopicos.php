<div class="sh-row">
    <?php echo CHtml::textField("filtroTopicos", "", array('placeholder' => 'Filtrar...', 'id' => 's', 'class' => 'column medium-6')); ?>
</div>
<?php $topicosConectadosIDS = CHtml::listData($aula->topicos, 'id', 'id'); ?>
<?php if (count($topicos) > 0): ?>
    <?php foreach ($topicos as $top): ?>
        <?php if (!in_array($top->id, $topicosConectadosIDS)) : ?>
            <div class="sh-row topico">
                <div class="medium-3 columns">
                    <?php
                    echo CHtml::link('Conectar à aula', $this->createUrl('aula/ConetarConteudo', array(
                                'id' => $aula->id,
                                'contID' => $top->id,
                            )), array('class' => 'btn btn-info'));
                    ?>
                </div>
                <div class="medium-9 columns">
                    <h4><b><?= $top->nome ?></b></h4>
                </div>
                <div class="medium-offset-2 columns"></div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
    <h4 class='hint'>Nenhum tópico.</h4>    
<?php endif; ?>
<style>
    .topico {
        margin: 16px 0px;
    }
</style>

<script>
    $('#s').keyup(function() {
        $('.topico h4').each(function() {
            var n = $(this).text().toLowerCase();
            if (n.indexOf($('#s').val().toLowerCase()) === -1) {
                $(this).parent().parent().hide();
            } else {
                $(this).parent().parent().show();
            }
        });
    });
</script>
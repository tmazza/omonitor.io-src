<div class="sh-row">
    <?php echo CHtml::textField("filtroTopicos", "", array('placeholder' => 'Filtrar...', 'id' => 's', 'class' => 'column medium-6')); ?>
</div>
<?php
$materialDeApoioIDS = CHtml::listData($aula->arquivos, 'id', 'id');
?>
<?php if (count($arquivos) > 0): ?>
    <?php foreach ($arquivos as $arq): ?>
        <div class="sh-row topico">
            <div class="medium-3 columns">
                <?php
                if (in_array($arq->id, $materialDeApoioIDS)) {
                    echo CHtml::link('Desconectar da aula', $this->createUrl('aula/desconetarMaterialDeApoio', array(
                                'id' => $aula->id,
                                'contID' => $arq->id,
                            )), array('class' => 'btn btn-danger'));
                } else {
                    echo CHtml::link('Conectar à aula', $this->createUrl('aula/conetarMaterialDeApoio', array(
                                'id' => $aula->id,
                                'contID' => $arq->id,
                            )), array('class' => 'btn btn-info'));
                }
                ?>
                <?php
                ?>
            </div>
            <div class="medium-9 columns">
                <h4><b><?= $arq->showNome(); ?></b></h4>
            </div>
            <div class="medium-offset-2 columns"></div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <h4 class='hint'>Nenhum material de apoio.</h4>    
<?php endif; ?>
<style>
    .topico {
        margin: 16px 0px;
    }
</style>

<script>
    $('#s').keyup(function() {
        $('.topico h4').each(function() {
            var n = $(this).text().toLowerCase();
            if (n.indexOf($('#s').val().toLowerCase()) === -1) {
                $(this).parent().parent().hide();
            } else {
                $(this).parent().parent().show();
            }
        });
    });
</script>
<?php

class GerenciamentoCursosModule extends CWebModule {

    // TODO: CRUDO operation, tasks, rules

    public function init() {
        $this->setImport(array(
            'gerenciamentoCursos.components.*',
            'gerenciamentoCursos.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Curso
            'curso.listar' => 'lerCurso',
            'curso.novo' => 'criarCurso',
            'curso.editar' => 'atualizarCurso',
            'curso.novaaula' => 'atualizarCurso',
            'curso.*' => 'atualizarCurso',
            // Controller Aula
            'aula.*' => 'atualizarCurso',
            // Controller Matriculas
            'matriculas.*' => 'lerCurso',
        );
    }
}

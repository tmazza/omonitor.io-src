<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class LinksController extends NotebookController {

    public function actionIndex($all = true) {
        $links = SearchLinks::model()->doAutor()->findAll();
        $this->render('index', array('links' => $links));
    }

    public function actionRemover($id){
      if(SearchLinks::remover($id)){
        Yii::app()->user->setFlash(self::SUCS_FLASH,'Link removido.');
      } else {
        Yii::app()->user->setFlash(self::ERRO_FLASH,'Link não foi removido.');
      }
      $this->redirect($this->createUrl('links/index'));
    }

}

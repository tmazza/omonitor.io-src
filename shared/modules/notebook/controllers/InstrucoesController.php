<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class InstrucoesController extends NotebookController {

    public function actions() {

        return array(
            'loadType' => array(
                'class' => 'shared.actions.ParseString'
            ),
            'updateWidgetView' => array(
                'class' => 'shared.actions.UpdateWidgetView'
            ),
        );
    }

    public function actionIndex() {
        $insts = InstrucaoUser::model()->doAutor()->findAll();
        $this->render('index', array('insts' => $insts));
    }
    
    
    public function actionNova() {
        $model = new InstrucaoUser();
        
        if(isset($_POST['InstrucaoUser'])){
			$model->attributes = $_POST['InstrucaoUser'];
			$model->template = addslashes($_POST['InstrucaoUser']['template']);
			$model->user_id = Yii::app()->user->id;
			if($model->save()){
				Yii::app()->user->setFlash(self::SUCS_FLASH, 'Criada.');
				$this->redirect($this->createUrl('instrucoes/index'));
			}			
		}
        
        $this->render('novo', array('model' => $model));
    }
    
	public function actionPreview() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->renderPartial('_preview', array(
                'template' => $_POST['code'],
                'data' => $data,
                    ), true, true);
        }
    }

}

<?php

class NotebookModule extends CWebModule {

    // Layout mais utilizado
    public $layoutSemMenu = 'column1';
    // Utilizado no menu edição
    public $layoutComMenu = 'column2';

    public function init() {
        $this->setImport(array(
            'notebook.components.*',
            'shared.behaviors.*',
            'shared.extensions.imperavi-redactor-widget.*'
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            'links.*' => false,
        );
    }

}

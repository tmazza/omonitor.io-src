
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'codes-codigo-_form-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<div class="sh-row">
    <div class="medium-12 column">
        <?php echo $form->error($model, 'descricao'); ?><br>
        <?php echo $form->error($model, 'template'); ?><br>
        <?php echo $form->error($model, 'alias'); ?><br>
    </div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'alias'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'alias', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'descricao'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'descricao', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'template'); ?>
    </div>
    <div class="medium-7 column">
        <textarea name="InstrucaoUser[template]" id="TemplatesCodigo-code"  class="sage"><?= stripslashes($model->template); ?></textarea>
        <?php
        echo CHtml::ajaxLink('Carregar preview', $this->createUrl('instrucoes/preview'), array(
            'update' => '#preview-template',
            'type' => 'POST',
            'data' => array('code' => 'js: $("#TemplatesCodigo-code").val() '),
            'beforeSend' => 'js: function() { $("#preview-code").html("Carregando...") } ',
                ), array('class' => 'btn btn-sm btn-primary', 'id' => 'main-update'))
        ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<?php $this->endWidget(); ?>
<hr>
<div id="preview-template"></div>
<hr>

<script>
    function getParametros() {
        data = {};
        $('#params input').each(function() {
            data[$(this).attr('name')] = $(this).val();
        });
        return JSON.stringify(data);
    }
</script>

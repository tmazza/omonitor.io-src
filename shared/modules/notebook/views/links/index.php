<?php
$this->pageTitle = 'Links favoritos';
if(count($links) > 0){
	echo '<table class="table">';
	foreach($links as $l){
		echo '<tr><td>';
		echo CHtml::link($l->valor,Yii::app()->baseUrl.'/do?q='.urlencode($l->valor));
		echo '</td><td>';
		echo CHtml::link('Remover', $this->createUrl('links/remover',array('id'=>$l->id)), array('style' => 'float: right;'));
		echo '</td></tr>';
	}
	echo '</table>';
} else {
	echo '<h3 class="hint">Nenhum link salvo.</h3>';
	?>
	<a href="#" onclick="$('#entenda-links').slideToggle();">Entenda</a>
	<div style="display: none;" id='entenda-links'>
		<hr>
		<div class="sh-row">
			  <div class="medium-4 columns">
			  	Sempre que quiser salvar algum resultado obtido no OM.
					Clique no ícone circulado em vermelho e o resultado ficará salvo aqui.
					<br><br>
					<?=CHtml::link('Ir para O Monitor', Yii::app()->baseUrl.'/do?q=')?>
				</div>
				<div class="medium-8 columns">
			    <img src="<?=$this->assetsPath; ?>/img/eg/links.png" style="border: 1px solid #ddd;" />
			  </div>
		</div>
	</div>

	<?php
}
?>

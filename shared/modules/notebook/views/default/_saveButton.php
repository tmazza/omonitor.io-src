<?php

$ajaxBehaviour = array(
    'beforeSend' => 'js: function() {'
    . '$("#note-' . $note->id . '").append("<div class=\'overlay\'><i class=\'fa fa-save fa-spin\'></i></div>");'
    . '}',
    'data' => array('id' => $note->id, 'code' => 'js: function() { return $("#note-code-' . $note->id . '").val(); }'),
    'success' => 'js: function(data) { '
    . '$("#note-' . $note->id . ' .overlay").remove();'
    . 'alert(data); '
    . '}',
);
echo CHtml::ajaxLink('Salvar', $this->createUrl('default/save'), $ajaxBehaviour, array(
    'id' => 'save-code-' . $note->id,
    'class' => 'btn btn-success btn-sm',
));

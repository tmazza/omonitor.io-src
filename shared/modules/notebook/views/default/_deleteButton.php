<?php

echo CHtml::ajaxLink('Excluir', $this->createUrl('default/delete', array('id' => $note->id)), array(
    'beforeSend' => 'js: function() {'
    . '$("#note-' . $note->id . '").append("<div class=\'overlay\'><i class=\'fa fa-trash-o fa-spin\'></i></div>");'
    . '}',
    'success' => 'js: function(){ $("#note-' . $note->id . '").slideUp(); }',
        ), array(
    'class' => 'btn btn-danger btn-xs',
    'id' => 'delete-' . $note->id,
    'confirm' => 'Confirma exclusão?',
));

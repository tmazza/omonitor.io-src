<?php $this->pageTitle = 'Interações'; ?>
<?php if($showHelp): ?>
<hr>
<div class="hint">
  Crie e compartilhe suas próprias interações!
  <br>
  Você pode utilizar
  <?=CHtml::link('Sage','http://doc.sagemath.org/html/en/tutorial/tour.html');?>,
  <?=CHtml::link('Phyton','http://wiki.python.org.br/')?>,
  <?=CHtml::link('R','https://www.r-project.org/')?> ou <?=CHtml::link('Singular','http://www.singular.uni-kl.de/');?>.
  <br>
  Não sabe programar ou não conhece nenhuma destas linguagens?
  Use os exemplos e tente modificar.<br>
  Sinta-se à vontade e modifique os exemplos o quanto quiser. Divirta-se!
  <br>
  <?//=CHtml::link('Não mostar mais.','')?>
</div>
<hr>
<?php endif; ?>
<div class="">
    <br>
    <div class="pull-right">
        <div class="btn-group">
            <?php $fav = (isset($_GET['all']) && $_GET['all'] == '0') ? true : false; ?>
            <?php echo CHtml::link('Todas', $this->createUrl('default/index'), array('class' => 'btn btn-default btn-sm ' . ($fav ? '' : 'active'))); ?>
            <?php echo CHtml::link('Favoritos', $this->createUrl('default/index', array('all' => 0)), array('class' => 'btn btn-default btn-sm ' . ($fav ? 'active' : ''))); ?>
        </div>
    </div>
      Criar:
      <div class="btn-group">
          <?php
          $ajaxBehaviour = array(
              'beforeSend' => 'js: function(){  '
              . '$("<div id=\'loading-note\'><div class=\'box\'><br><br><div class=\'overlay\'><i class=\'fa fa-refresh fa-spin\'></i></div></div></div>").prependTo("#notes-list"); '
              . '}',
              'success' => 'js: function(html){  '
              . '$("" + html + "").prependTo("#notes-list"); '
              . '$("#loading-note").remove();'
              . '}',
          );
          echo CHtml::ajaxLink('Sage', $this->createUrl('default/novaInteracao', array('tipo' => Notebook::Sage)), $ajaxBehaviour, array('class' => 'btn btn-primary btn-sm'));
          echo CHtml::ajaxLink('Singular', $this->createUrl('default/novaInteracao', array('tipo' => Notebook::Singular)), $ajaxBehaviour, array('class' => 'btn btn-primary btn-sm'));
          echo CHtml::ajaxLink('R', $this->createUrl('default/novaInteracao', array('tipo' => Notebook::R)), $ajaxBehaviour, array('class' => 'btn btn-primary btn-sm'));
          echo CHtml::ajaxLink('Python', $this->createUrl('default/novaInteracao', array('tipo' => Notebook::Python)), $ajaxBehaviour, array('class' => 'btn btn-primary btn-sm'));
          ?>
      </div>
    <br><br>
    <?=CHtml::link('Exemplos', '',array('class'=>'btn btn-default '));?>
    <br><br>
</div>
<br>
<div id="notes-list">
    <?php if (count($notes) > 0): ?>
        <?php
        $count = 0;
        foreach ($notes as $n) {
            if ($count < 5) {
                $view = '_interacao';
            } else {
                $view = '_interacaoEscondida';
            }
            $this->renderPartial($view, array(
                'note' => $n,
            ));
            $count++;
        }
        ?>
    <?php else: ?>
        <h3 class="hint">Nenhuma anotação. Clique na linguagem que deseja, acima, para começar.</h3>
    <?php endif; ?>
</div>
<style>
    <!--
    .sagecell .CodeMirror-scroll {
        min-height: 10em;
        max-height: 30em;
    }
    .exp-btn {
        font-size: 18px;
        display: block;
        width: 100%;
        height: 100%;
    }
    .exp-btn:hover {
        background: #ddd;
    }
    -->
</style>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'share-note',
    'options' => array(
        'title' => 'Link público de interação',
        'autoOpen' => false,
        'minWidth' => 600,
        'minHeight' => 300,
        'modal'=>true,
        'overlay' => array(
            'backgroundColor' => '#000',
            'opacity' => '0.5'
        ),
    ),
    'htmlOptions' => array(
    ),
));
echo 'Compartilhar camada.';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<style>
    .ui-corner-all {
        border-radius: 0px;
    }
    .ui-widget-header {
        background: #3C8DBC;
        color: white;
    }
    .ui-dialog {
        z-index: 1000000!important;
    }
</style>

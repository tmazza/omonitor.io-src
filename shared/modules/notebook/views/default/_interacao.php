<script>
    sagecell.makeSagecell({
        inputLocation: '#note-code-<?= $note->id ?>',
        evalButtonText: 'Atualizar',
        autoeval: false,
        hide: ['permalink'],
        languages: ["<?= $note->getLanguage(); ?>"],
    });
</script>
<div class="box" id="note-<?= $note->id; ?>">
    <div class="box-header">
        <div class="pull-right">

            <?php
            $img = CHtml::image($this->assetsPath . '/img/share.gif');
            echo CHtml::ajaxLink($img, $this->createUrl('default/share', array('id' => $note->id)), array(
                'success' => 'js: function(html) { '
                . '$("#share-note").dialog("open"); '
                . '$("#share-note").html(html); '
                . 'return false; }',
                    ), array('id' => 'share-' . $note->id));
            ?>
            <span id='fav-<?= $note->id ?>' style="font-size: 18px;">
                <?php if ($note->favorito): ?>
                    <?php $this->renderPartial('_favorito', array('note' => $note)); ?>
                <?php else: ?>
                    <?php $this->renderPartial('_naoFavorito', array('note' => $note)); ?>
                <?php endif; ?>
            </span>
        </div>
        <h4>
            <?= ucfirst($note->getLanguage()); ?>
        </h4>
    </div>
    <div class="box-body" id='code-edit-<?= $note->id ?>'>
        <?php
//        $this->widget('shared.widgets.TecladoDeComandos.TecladoDeComandos', array(
//            'inputID' => 'code-edit-' . $note->id . ' div textarea',
//            'model' => TecladoComandos::model(),
//        ));
        ?>
        <textarea id='note-code-<?= $note->id; ?>'><?php echo CHtml::decode(stripslashes($note->codigo)); ?></textarea>
    </div>
    <div class="box-footer">
        <br><br>
        <div class="pull-right">
            <?php $this->renderPartial('_saveButton', array('note' => $note)); ?>
        </div>
        <?php $this->renderPartial('_deleteButton', array('note' => $note)); ?>
        <br><br>
    </div>
</div>
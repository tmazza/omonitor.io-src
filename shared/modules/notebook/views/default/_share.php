<?php if ($note->sharing): ?>
    <?php $link = $note->linkPublico; ?>
    <?= CHtml::link('Abri no site &#8599;', $link, array('target' => '_blank')); ?>
    <br>
    Link publico: 
    <code onclick="$(this).select();"><?= $link ?></code>
    <br><br>
    <a href="#" class="btn btn-primary btn-xs" onclick="$('#send-to').slideDown();
                return false;">Enviar para... </a>
    <div id="send-to" style="display: none;">
        <span class="hint">Pressione Ctrl para selecionar mais de destinatário.</span>
        <?php echo CHtml::beginForm($this->createUrl('default/send')); ?>
        <?php
        echo CHtml::hiddenField('id', $note->id);
        $this->widget('shared.extensions.select2.ESelect2', array(
            'name' => 'destinos',
            'data' => $users,
            'htmlOptions' => array(
                'multiple' => 'multiple',
                'class' => 'medium-12',
                'placeholder' => 'Para: '
            ),
        ));
        ?>
        <?php echo CHtml::submitButton('Enviar', array('class' => 'btn btn-success btn-sm')); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
<?php else: ?>
    Interação não está sendo compartilhada. 
    <?=
    CHtml::ajaxLink('Gerar link público', $this->createUrl('default/ParaPublica', array('id' => $note->id)), array(
        'update' => '#share-note',
            ), array(
        'id' => 'gerarLink-' . $note->id,
    ));
    ?>
<?php endif; ?>
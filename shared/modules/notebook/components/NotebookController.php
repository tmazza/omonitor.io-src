<?php

/**
 * Description of MonitorController
 *
 * @author tiago
 */
class NotebookController extends EditorController {

    // Definidas em PublicacoesModule
    public $layoutComMenu;
    public $layoutSemMenu;

    protected function beforeAction($action) {
        // Define layouts da aplicação
        $this->setLayouts();
        return parent::beforeAction($action);
    }

    /**
     * Baseado nas configurações do módulo define os layouts em uso.
     * Utiliza o layout dos módulo pai.
     */
    private function setLayouts() {
        $this->layoutComMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutComMenu;
        $this->layoutSemMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutSemMenu;
        $this->layout = $this->layoutSemMenu;
    }

}
<?php

/**
 * Description of MonitorController
 *
 * @author tiago
 */
class InstituicaoController extends EditorController {

    public function beforeAction($action) {
        $modules = explode('/', $this->getModule()->id);
        $this->layout = $modules[0] . '.views.layouts.column1';
        return parent::beforeAction($action);
    }


}

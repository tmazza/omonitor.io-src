<?php

/**
 * Description of TemplateController
 *
 * @author tiago
 */
class WidgetsController extends InstituicaoController {

    public function actions() {

        return array(
            'loadType' => array(
                'class' => 'shared.actions.ParseString'
            ),
            'updateWidgetView' => array(
                'class' => 'shared.actions.UpdateWidgetView'
            ),
        );
    }

    public function actionIndex() {
        $categorias = WidgetCategoria::model()->findAll("categoria_pai IS NULL");
        $this->render('index', array(
            'categorias' => $categorias,
        ));
    }

    public function actionNovo() {
        $model = new Widget();

        if (isset($_POST['Widget'])) {
            $this->salvaTemplate($model, $_POST['Widget']);
        }
        $cats = CHtml::listData(WidgetCategoria::model()->findAll(), 'id', 'label');
        $this->render('novo', array('model' => $model, 'cats' => $cats));
    }

    public function actionEditar($id) {
        $model = Widget::model()->findByPk($id);

        if (isset($_POST['Widget'])) {
            $this->salvaTemplate($model, $_POST['Widget']);
        }
        $cats = CHtml::listData(WidgetCategoria::model()->findAll(), 'id', 'label');
        $this->render('editar', array('model' => $model, 'cats' => $cats));
    }

    private function salvaTemplate(&$model, $data) {
        $model->attributes = $data;
        $model->code = addslashes($data['code']);
        if ($model->validate()) {
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Widget criado.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Erro ao criar widget.');
            }
            $this->redirect($this->createUrl('widgets/index'));
        }
    }

    public function actionPreview() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->renderPartial('_preview', array(
                'template' => $_POST['code'],
                'data' => $data,
              ), true, true);
        }
    }

    public function actionNovaCategoria() {
        $model = new WidgetCategoria();

        if (isset($_POST['WidgetCategoria'])) {
            $model->attributes = $_POST['WidgetCategoria'];
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Categoria criada.');
                $this->redirect($this->createUrl('widgets/index'));
            }
        }
        $cats = CHtml::listData(WidgetCategoria::model()->findAll(), 'id', 'label');

        $this->render('novaCategoria', array(
            'model' => $model,
            'cats' => $cats,
        ));
    }

}

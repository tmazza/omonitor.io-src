<?php
/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends InstituicaoController{
    
    public function actionIndex() {
        $this->redirect($this->createUrl('perfil/index'));
    }
    
}

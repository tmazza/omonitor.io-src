<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class InstrucoesController extends InstituicaoController {

    public function actionIndex() {
        $inst = Instrucao::model()->findAll(array('order' => 'id DESC'));
        $this->render('list', array(
            'inst' => $inst,
        ));
    }

    public function actionNova() {
        $model = new Instrucao();

        if (isset($_POST['Instrucao'])) {
            $model->attributes = $_POST['Instrucao'];
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Instrução crida.');
                $this->redirect($this->createUrl('instrucoes/index'));
            }
        }

        $this->render('nova', array(
            'model' => $model,
        ));
    }

    public function actionEditar($id) {
        $model = $this->getInstrucao($id);
        if (isset($_POST['Instrucao'])) {
            $model->attributes = $_POST['Instrucao'];
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Instrução crida.');
                $this->redirect($this->createUrl('instrucoes/index'));
            }
        }

        $this->render('nova', array(
            'model' => $model,
        ));
    }

    public function actionTemplates($id) {
        $model = $this->getInstrucao($id);
        $this->render('listTemplates', array(
            'model' => $model,
        ));
    }

    public function actionNovoTemplate($id) {
        $instrucao = $this->getInstrucao($id);
        $model = new InstrucaoCodigo();
        if (isset($_POST['InstrucaoCodigo'])) {
            $model->attributes = $_POST['InstrucaoCodigo'];
            $model->template = addslashes($_POST['InstrucaoCodigo']['template']);
            $model->instrucao_id = $instrucao->id;
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Instrução criada.');
                $this->redirect($this->createUrl('instrucoes/index'));
            }
        }
        $this->render('novoTemplate', array(
            'instrucao' => $instrucao,
            'model' => $model,
        ));
    }

    public function actionEditarTemplate($id, $id2,$r='s') {
        $instrucao = $this->getInstrucao($id);
        $model = $this->getTemplateInstucao($id2);

        if (isset($_POST['InstrucaoCodigo'])) {
            $model->attributes = $_POST['InstrucaoCodigo'];
            $model->template = addslashes($_POST['InstrucaoCodigo']['template']);
            $model->instrucao_id = $instrucao->id;
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Instrução atualizada.');

                if($r == 's'){
                  $this->redirect($this->createUrl('instrucoes/index'));
                }
            }
        }
        $this->render('novoTemplate', array(
            'instrucao' => $instrucao,
            'model' => $model,
            'id' => $id,
            'id2' => $id2,
        ));
    }

    protected function getInstrucao($id) {
        $inst = Instrucao::model()->findByPk($id);
        if (is_null($inst)) {
            Yii::app()->user->setFlash('alert-error', 'Instrução não econtrado.');
            $this->redirect($this->createUrl('instrucoes/index'));
        }
        return $inst;
    }

    protected function getTemplateInstucao($id) {
        $inst = InstrucaoCodigo::model()->findByPk($id);
        if (is_null($inst)) {
            Yii::app()->user->setFlash('alert-error', 'Comando de instrução não econtrado.');
            $this->redirect($this->createUrl('instrucoes/index'));
        }
        return $inst;
    }

    public function actionNovoApelido($id) {
        $instrucao = $this->getInstrucao($id);
        $model = new InstrucaoNome();
        if (isset($_POST['InstrucaoNome'])) {
            $model->attributes = $_POST['InstrucaoNome'];
            $model->instrucao_id = $instrucao->id;
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Apelido criado.');
                $this->redirect($this->createUrl('instrucoes/index'));
            }
        }
        $this->render('novoApelido', array(
            'instrucao' => $instrucao,
            'model' => $model,
        ));
    }

    public function actionEditarApelido($id, $id2) {
        $instrucao = $this->getInstrucao($id);
        $model = $this->getApelidoInstucao($id2);

        if (isset($_POST['InstrucaoNome'])) {
            $model->attributes = $_POST['InstrucaoNome'];
            $model->instrucao_id = $instrucao->id;
            if ($model->save()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Instrução atualizada.');
                $this->redirect($this->createUrl('instrucoes/index'));
            }
        }
        $this->render('novoApelido', array(
            'instrucao' => $instrucao,
            'model' => $model,
        ));
    }

    public function actionExcluirApelido($id, $id2) {
        $instrucao = $this->getInstrucao($id);
        $model = $this->getApelidoInstucao($id2);
        $model->delete();
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Apelido excluído.');
        $this->redirect($this->createUrl('instrucoes/index'));
    }

    public function actionExcluirTemplate($id, $id2) {
        $instrucao = $this->getInstrucao($id);
        $model = $this->getTemplateInstucao($id2);
        $model->delete();
        Yii::app()->user->setFlash(self::SUCS_FLASH, 'Template excluído.');
        $this->redirect($this->createUrl('instrucoes/index'));
    }

    protected function getApelidoInstucao($id) {
        $inst = InstrucaoNome::model()->findByPk($id);
        if (is_null($inst)) {
            Yii::app()->user->setFlash('alert-error', 'Apelido de instrução não econtrado.');
            $this->redirect($this->createUrl('instrucoes/index'));
        }
        return $inst;
    }

    public function actionPreview() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->renderPartial('_preview', array(
                'template' => $_POST['code'],
                'data' => $data,
                'conteudo' => ShView::mergeDataToTemplate($_POST['code'], $data),
                    ), true, true);
        }
    }

}

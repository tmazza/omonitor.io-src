<?php

/**
 * Description of TecladoController
 *
 * @author tiago
 */
class TecladoController extends InstituicaoController {

    public function actionIndex() {
        $teclados = Teclado::model()->deAplicacao()->findAll();
        $this->render('listar', array(
            'teclados' => $teclados,
        ));
    }

    public function actionEditar($id) {
        $teclado = Teclado::model()->findByPk($id);
        $this->render('editar', array(
            'teclado' => $teclado,
        ));
    }

    public function actionNovaTecla($id) {
        $model = new TecladoTecla();

        if (isset($_POST['TecladoTecla'])) {
            $model->attributes = $_POST['TecladoTecla'];
            if ($model->save()) {
                ShCode::setFlashSucesso("Tecla criada");
                $this->redirect($this->createUrl('teclado/editar', array('id' => $id)));
            }
        }

        $categorias = CHtml::listData(TecladoCategoria::model()->findAll("teclado_id = {$id}"), 'id', 'nome');

        $this->render('nova', array(
            'id' => $id,
            'model' => $model,
            'categorias' => $categorias,
        ));
    }

    public function actionEditarTecla($id) {
        $model = TecladoTecla::model()->findByPk($id);

        if (isset($_POST['TecladoTecla'])) {
            $model->attributes = $_POST['TecladoTecla'];
            if ($model->save()) {
                ShCode::setFlashSucesso("Tecla atualizada.");
                $this->redirect($this->createUrl('teclado/editar', array('id' => $model->categoria->teclado->id)));
            }
        }

        $categorias = CHtml::listData(TecladoCategoria::model()->findAll("teclado_id = " . $model->categoria->teclado->id), 'id', 'nome');

        $this->render('nova', array(
            'id' => $id,
            'model' => $model,
            'categorias' => $categorias,
        ));
    }

    /**
     * Edição de ordem das teclas de uma categoria
     */
    public function actionOrdemTeclas($id){
      $categoria = TecladoCategoria::model()->findByPk($id);

      if(isset($_POST['ordem'])){
        foreach ($_POST['ordem'] as $id => $o) {
          TecladoTecla::model()->updateByPk((int) $id,array(
            'ordem' => $o,
          ));
        }
      }

      $this->render('ordemTeclas',array(
        'categoria' => $categoria,
      ));
    }


    public function actionExcluirTecla($id) {
        $tecla = TecladoTecla::model()->findByPk($id);
        if (!is_null($tecla)) {
            $tecla->delete();
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Tecla excluída.');
            $this->redirect($this->createUrl('teclado/editar', array('id' => $tecla->categoria->teclado->id)));
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Tecla não encontrada.');
            $this->redirect($this->createUrl('teclado/index'));
        }
    }

    public function actionEditarCategorias($id) {
        $categorias = TecladoCategoria::model()->findAll("teclado_id = {$id}");
        $this->render('editarCategorias', array(
            'id' => $id,
            'categorias' => $categorias,
        ));
    }

    public function actionNovaCategoria($id, $rt = false) {
        $model = new TecladoCategoria();

        if (isset($_POST['TecladoCategoria'])) {
            $model->attributes = $_POST['TecladoCategoria'];
            $model->teclado_id = $id;
            if ($model->save()) {
                ShCode::setFlashSucesso("Categoria criada");
                if ($rt) {
                    $this->redirect(ShCode::interpretaLinkRetorno($rt));
                } else {
                    $this->redirect($this->createUrl('teclado/editar', array('id' => $id)));
                }
            }
        }

        $this->render('novaCategoria', array(
            'id' => $id,
            'model' => $model,
        ));
    }

    public function actionEditarCategoria($id) {
        $model = TecladoCategoria::model()->findByPk($id);

        if (isset($_POST['TecladoCategoria'])) {
            $model->attributes = $_POST['TecladoCategoria'];
            if ($model->save()) {
                ShCode::setFlashSucesso("Categoria Atualizada.");
                $this->redirect($this->createUrl('teclado/editar', array('id' => $model->teclado_id)));
            }
        }

        $this->render('editarCategoria', array(
            'id' => $id,
            'model' => $model,
        ));
    }

    public function actionExcluirCategoria($id) {
        $cat = TecladoCategoria::model()->findByPk($id);
        if (!is_null($cat)) {
            TecladoTecla::model()->deleteAll("categoria_id = {$cat->id}");
            $cat->delete();
            Yii::app()->user->setFlash(self::SUCS_FLASH, 'Categoria excluída.');
            $this->redirect($this->createUrl('teclado/editar', array('id' => $cat->teclado->id)));
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Categoria não encontrada.');
            $this->redirect($this->createUrl('teclado/index'));
        }
    }

}

<?php

/**
 * Description of OrganizacaoController
 *
 * @author tiago
 */
class PerfilController extends InstituicaoController {

    private $paginasParaEdicao = array('apresentacao', 'modoDeUsar', 'plataformas', 'referencias', 'parceiros', 'como_usar', 'comentarios');

    public function beforeAction($action) {
        $this->processaLatex = false;
        if (!ShSeg::hasAccess('organizacao')) {
            $this->redirect($this->createUrl('instrucoes/index'));
        }
        return parent::beforeAction($action);
    }

    public function actionIndex($pagina = null) {
        $pagina = is_null($pagina) ? 'apresentacao' : $pagina;
        if (in_array($pagina, $this->paginasParaEdicao)) {
            $org = InstOrganizacao::model()->findByAttributes(array(
                'orgID' => Yii::app()->user->orgID,
            ));
            if (!is_null($org)) {
                $this->render('index', array(
                    'org' => $org,
                    'attr' => $pagina,
                ));
            } else {
                Yii::app()->user->setFlash('alert-error', 'Dados da organização não encontrados.');
                $this->redirect($this->createUrl('autor/index'));
            }
        } else {
            Yii::app()->user->setFlash('alert-error', 'Requisição inválida.');
            $this->redirect($this->createUrl('autor/index'));
        }
    }

    public function actionEditar($pagina) {
        $this->processaLatex = false;
        $pagina = is_null($pagina) ? 'apresentacao' : $pagina;
        if (in_array($pagina, $this->paginasParaEdicao)) {
            $org = InstOrganizacao::model()->findByAttributes(array(
                'orgID' => Yii::app()->user->orgID,
            ));
            if (!is_null($org)) {
                if (isset($_POST['InstOrganizacao'][$pagina])) {
                    $org->{$pagina} = $_POST['InstOrganizacao'][$pagina];
                    if ($org->validate()) {
                        $org->update(array('nome', $pagina));
                        Yii::app()->user->setFlash('alert-success', 'Conteúdo de "' . InstOrganizacao::model()->getAttributeLabel($pagina) . '" atualizado.');
                        $this->redirect($this->createUrl('perfil/index/pagina/' . $pagina));
                    }
                }

                $this->render('editar', array(
                    'model' => $org,
                    'attr' => $pagina,
                ));
            } else {
                Yii::app()->user->setFlash('alert-error', 'Dados da organização não encontrados.');
                $this->redirect($this->createUrl('autor/index'));
            }
        } else {
            Yii::app()->user->setFlash('alert-error', 'Requisição inválida.');
            $this->redirect($this->createUrl('autor/index'));
        }
    }

    public function actionEditarProcessamentoBusca() {
        $org = InstOrganizacao::model()->findByAttributes(array(
            'orgID' => Yii::app()->user->orgID,
        ));

        if (isset($_POST['cod'])) {
            $org->equationResults = serialize(addslashes($_POST['cod']));
            if ($org->update(array('equationResults'))) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Salvo.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao salvar.');
            }
        }

        $this->render('procBusca', array(
            'cod' => unserialize($org->equationResults),
        ));
    }

    public function actionEditarTraducoes() {
        $org = InstOrganizacao::model()->findByAttributes(array(
            'orgID' => Yii::app()->user->orgID,
        ));

        if (isset($_POST['cod'])) {
            $org->traducoes = serialize(addslashes($_POST['cod']));
            if ($org->update(array('traducoes'))) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Salvo.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao salvar.');
            }
        }

        $this->render('traducoes', array(
            'cod' => unserialize($org->traducoes),
        ));
    }

    public function actionPreview() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->renderPartial('_preview', array(
                'template' => $_POST['code'],
                'data' => $data,
                'conteudo' => ShView::mergeDataToTemplate($_POST['code'], $data),
                    ), true, true);
        }
    }

}

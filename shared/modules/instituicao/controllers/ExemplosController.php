<?php
/**
 * Description of DefaultController
 *
 * @author tiago
 */
class ExemplosController extends InstituicaoController{

    public function actionIndex() {
  		$categorias = ExemplosSearchCategoria::model()->findAll("pai_id IS NULL");
  		$this->render('index', array('categorias' => $categorias));
    }

  public function actionNovaCategoria(){
		$model = new ExemplosSearchCategoria();

		if(isset($_POST['ExemplosSearchCategoria'])){
			$model->attributes = $_POST['ExemplosSearchCategoria'];
			if($model->save()){
				Yii::app()->user->setFlash(self::SUCS_FLASH, 'Categoria criada.');
				$this->redirect($this->createUrl('exemplos/index'));
			}
		}

		$this->render('novaCat', array(
			'model' => $model,
		));
	}

	public function actionEditCategoria($id){
		$model = ExemplosSearchCategoria::model()->findByPk($id);
		if(isset($_POST['ExemplosSearchCategoria'])){
			$model->attributes = $_POST['ExemplosSearchCategoria'];
			if($model->save()){
				Yii::app()->user->setFlash(self::SUCS_FLASH, 'Categoria criada.');
				$this->redirect($this->createUrl('exemplos/index'));
			}
		}

		$this->render('novaCat', array(
			'model' => $model,
		));
	}

	public function actionEditarOrdem($id){
		$model = ExemplosSearchCategoria::model()->findByPk($id);
		if(isset($_POST['ordem']) && is_array($_POST['ordem'])){
		    foreach ($_POST['ordem'] as $e => $o) {
          ExemplosSearch::model()->updateByPk((int) $e,array(
            'ordem' => $o,
          ));
		    }
		}

		$this->render('ordem', array(
			'model' => $model,
		));
	}

    public function actionExcCategoria($id){
		$model = ExemplosSearchCategoria::model()->findByPk($id);
    if(count($model->filhas) > 0){
      foreach ($model->filhas as $f) {
        $f->pai_id = null;
        $f->update(array('pai_id'));
      }
    }

		if($model->delete()){
			Yii::app()->user->setFlash(self::SUCS_FLASH, 'Categoria excluído.');
		} else {
			Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao categoria.');
		}
		$this->redirect($this->createUrl('exemplos/index'));
	}

    public function actionNovoExemplo(){
		$model = new ExemplosSearch();
		$categorias = ExemplosSearchCategoria::model()->findAll();
		if(isset($_POST['ExemplosSearch'])){
			$model->attributes = $_POST['ExemplosSearch'];
			if($model->save()){
				Yii::app()->user->setFlash(self::SUCS_FLASH, 'Exemplo criado.');
				$this->redirect($this->createUrl('exemplos/index'));
			}
		}

		$this->render('novoEx', array(
			'model' => $model,
			'categorias' => CHtml::listData($categorias, 'id', 'nome'),
		));
	}


    public function actionEditEx($id){
		$model = ExemplosSearch::model()->findByPk($id);
		$categorias = ExemplosSearchCategoria::model()->findAll();
		if(isset($_POST['ExemplosSearch'])){
			$model->attributes = $_POST['ExemplosSearch'];
			if($model->save()){
				Yii::app()->user->setFlash(self::SUCS_FLASH, 'Exemplo criado.');
				$this->redirect($this->createUrl('exemplos/index'));
			}
		}

		$this->render('novoEx', array(
			'model' => $model,
			'categorias' => CHtml::listData($categorias, 'id', 'nome'),
		));
	}


    public function actionExcEx($id){
		$model = ExemplosSearch::model()->findByPk($id);
		if($model->delete()){
			Yii::app()->user->setFlash(self::SUCS_FLASH, 'Exemplo excluído.');
		} else {
			Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao excluir.');
		}
		$this->redirect($this->createUrl('exemplos/index'));
	}

  public function actionPublicar($id){
    if($this->updateExemploEstado($id,1)){
      Yii::app()->user->setFlash(self::SUCS_FLASH,'Publicado.');
    } else {
      Yii::app()->user->setFlash(self::ERRO_FLASH,'Erro ao publicar.');
    }
    $this->redirect($this->createUrl('exemplos/index'));
  }

  public function actionDespublicar($id){
    if($this->updateExemploEstado($id,0)){
      Yii::app()->user->setFlash(self::SUCS_FLASH,'Despublicado.');
    } else {
      Yii::app()->user->setFlash(self::ERRO_FLASH,'Erro ao despublicar.');
    }
    $this->redirect($this->createUrl('exemplos/index'));
  }

  public function actionPublicarTodos($catId){
    $cat = ExemplosSearchCategoria::model()->findByPk($catId);
    foreach ($cat->todosExemplos as $e) {
      $this->updateExemploEstado($e->id,1);
    }
    Yii::app()->user->setFlash(self::SUCS_FLASH,'Categoria publicada.');
    $this->redirect($this->createUrl('exemplos/index'));
  }
  public function actionDespublicarTodos($catId){
    $cat = ExemplosSearchCategoria::model()->findByPk($catId);
    foreach ($cat->todosExemplos as $e) {
      $this->updateExemploEstado($e->id,0);
    }
    Yii::app()->user->setFlash(self::SUCS_FLASH,'Categoria despublicada.');
    $this->redirect($this->createUrl('exemplos/index'));
  }

  private function updateExemploEstado($id,$estado){
    $exemplo = ExemplosSearch::model()->findByPk($id);
    $exemplo->publicado = $estado;
    return $exemplo->update(array('publicado'));
  }


}

<?php

/**
 * This is the model class for table "organizacao".
 *
 * The followings are the available columns in table 'organizacao':
 * @property integer $id
 * @property string $nome
 * @property string $orgID
 * @property integer $rec_questoes
 * @property integer $rec_topicos
 *
 * The followings are the available model relations:
 * @property UserAutor[] $userAutors
 */
class InstOrganizacao extends CActiveRecord {

    public function getDbConnection() {
        return Yii::app()->orgDb;
    }

    public function tableName() {
        return 'organizacao';
    }

    public static function getTableName() {
        return 'organizacao';
    }

    public function rules() {
        return array(
            array('nome, orgID', 'required'),
            array('rec_questoes, rec_topicos', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 200),
            array('orgID', 'length', 'max' => 64),
            array('id, nome, orgID, rec_questoes, rec_topicos, traducoes', 'safe'),
            array('id, nome, orgID, rec_questoes, rec_topicos', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'orgID' => 'Org',
            'rec_questoes' => 'Rec Questoes',
            'rec_topicos' => 'Rec Topicos',
            'apresentacao' => 'Apresentação',
            'modoDeUsar' => 'Modo de usar',
            'plataformas' => 'Plataformas',
            'referencias' => 'Referências',
            'parceiros' => 'Parceiros',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('orgID', $this->orgID, true);
        $criteria->compare('rec_questoes', $this->rec_questoes);
        $criteria->compare('rec_topicos', $this->rec_topicos);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Retorna os recursos disponíveis para a organização.
     * Colunos iniciadas com 'rec_' em organização são tratadas como booleanos
     * para definir o uso ou não do recurso.
     * @return type
     */
    public function getRecursos() {
        $recursos = array();
        foreach ($this->attributes as $attr => $value) {
            if (substr($attr, 0, 4) == 'rec_' && $value == 1) {
                $recursos[] = substr($attr, 4);
            }
        }
        return $recursos;
    }

}

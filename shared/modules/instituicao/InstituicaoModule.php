<?php

class InstituicaoModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'instituicao.components.*',
            'instituicao.models.*',
            'shared.extensions.imperavi-redactor-widget.*'
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Arvore
            'perfil.index' => 'lerOrganizacao',
            'perfil.editar' => 'atualizarOrganizacao',
            'perfil.editarprocessamentobusca' => 'atualizarOrganizacao',
            'perfil.*' => false,
            'teclado.*' => 'atualizarOrganizacao',
            'tecladosimbolos.*' => 'atualizarOrganizacao',
            // Controller Instruções
            'instrucoes.*' => 'edicaoInstrucoes',
            'tecladocomandos.*' => 'atualizarOrganizacao',
            // Controller Resolutor
            'resolutor.*' => false,
            'widgets.*'=>'admin',
            'exemplos.*' => false,

        );
    }

}

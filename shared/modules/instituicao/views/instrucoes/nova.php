<?php
$this->breadcrumbs += array(
    'Instruções'
);
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('instrucoes/index')),
);
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'topico-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'descricao'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->textField($model, 'descricao', array('class' => 'medium-12')); ?>
    </div>
    <div class="column medium-6">
        <?php echo $form->error($model, 'descricao'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'keywords'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->textField($model, 'keywords', array('class' => 'medium-12')); ?>
        <span class="hint">Separados por vírgula.</span>
    </div>
    <div class="column medium-6">
        <?php echo $form->error($model, 'keywords'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'labels'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->textField($model, 'labels', array('class' => 'medium-12')); ?>
        <span class="hint">Separados por vírgula.</span>
    </div>
    <div class="column medium-6">
        <?php echo $form->error($model, 'labels'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'publicado'); ?>
    </div>
    <div class="column medium-4">
        <?php echo $form->dropDownList($model, 'publicado', array(0 => 'Não', 1 => 'Sim'), array('class' => 'medium-12')); ?>
    </div>
    <div class="column medium-6">
        <?php echo $form->error($model, 'publicado'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="column medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'span6 btn btn-success')); ?>
    </div>
</div>
<?php echo $form->error($model, 'nome'); ?>
<?php $this->endWidget(); ?>

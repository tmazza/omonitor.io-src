
<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('instrucoes/index', array('id' => $instrucao->id))),
);
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'templates-codigo-_form-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'id'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'id', array('class' => 'medium-12')); ?>
        <span class="hint">Palavra que será utilizada para a busca.</span>
    </div>
    <div class="medium-offset-2">
        <?php echo $form->error($model, 'id'); ?>
    </div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'entradas'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'entradas', array('class' => 'medium-12')); ?>
        <span class="hint">Exemplos para uso do comando. Separados por |</span>
    </div>
    <div class="medium-offset-2">
        <?php echo $form->error($model, 'entradas'); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
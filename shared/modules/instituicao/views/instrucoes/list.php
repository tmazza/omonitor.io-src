<?php
$this->breadcrumbs += array(
    'Instruções'
);
$this->menuContexto = array(
    ShCode::makeItem('Principal', $this->createUrl('perfil/EditarProcessamentoBusca')),
    ShCode::makeItem('Instruções', $this->createUrl('instrucoes/index')),
    ShCode::makeItem('Traduções', $this->createUrl('perfil/EditarTraducoes')),
    ShCode::makeItem('Nova instrução', $this->createUrl('instrucoes/nova')),
);
?>

<?php foreach ($inst as $i): ?>
    <div class="box box-primary">
        <div class="box-header" style='cursor:pointer;' onclick="$(this).next().slideToggle();">
            <?= $i->publicado == '1' ? null : '<i data-toggle="tooltip" title="Não publicado" class="fa fa-eye-slash"></i>'; ?>
            <h3 class="box-title"><?= $i->descricao ?></h3>
        </div>
        <div class="box-body" style='display: none;'>
            <?php echo ' ' . CHtml::link('Editar', $this->createUrl('instrucoes/editar', array('id' => $i->id)), array('class' => 'btn btn-sm btn-default')) . ' '; ?>
            <hr>
            <br>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1-<?= $i->id ?>" data-toggle="tab">Códigos</a></li>
                    <li><a href="#tab_2-<?= $i->id ?>" data-toggle="tab">Apelidos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1-<?= $i->id ?>">
                        <?php echo CHtml::link('Novo código', $this->createUrl('instrucoes/novoTemplate', array('id' => $i->id)), array('class' => 'btn btn-success btn-xs')) . '<br><br>'; ?>

                        <?php
                        $templates = $i->templates;
                        echo '<ul>';
                        foreach ($templates as $t) {
                            echo '<li><span style="display:inline-block;width: 200px;">' . $t->descricao . '</span>';
                            echo CHtml::link('Editar', $this->createUrl('instrucoes/editarTemplate', array('id' => $i->id, 'id2' => $t->id))) . ' | ';
                            echo CHtml::link('Excluir', $this->createUrl('instrucoes/excluirTemplate', array('id' => $i->id, 'id2' => $t->id))) . '<br>';
                            echo '</li>';
                        }
                        echo '</ul>';
                        ?>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2-<?= $i->id ?>">
                        <?php echo CHtml::link('Novo apelido', $this->createUrl('instrucoes/novoApelido', array('id' => $i->id)), array('class' => 'btn btn-success btn-xs')) . '<br><br>';  ?>
                        <?php
                        $apelidos = $i->nomes;
                        echo '<ul>';
                        foreach ($apelidos as $a) {
                            echo '<li><span style="display:inline-block;width: 200px;">' . $a->id . '</span>';
                            echo CHtml::link('Editar', $this->createUrl('instrucoes/editarApelido', array('id' => $i->id, 'id2' => $a->id))) . ' | ';
                            echo CHtml::link('Excluir', $this->createUrl('instrucoes/excluirApelido', array('id' => $i->id, 'id2' => $a->id))) . '<br>';
                            echo '</li>';
                        }
                        echo '</ul>';
                        ?>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php

$this->pageTitle = 'Templates de ' . $model->descricao;
$this->breadcrumbs += array(
    'Instruções'
);
$this->menuContexto = array(
    ShCode::makeItem('Principal', $this->createUrl('perfil/EditarProcessamentoBusca')),
    ShCode::makeItem('Instruções', $this->createUrl('instrucoes/index')),
    ShCode::makeItem('Nova instrução', $this->createUrl('instrucoes/nova')),
);

echo CHtml::link('Novo', $this->createUrl('instrucoes/novoTemplate', array('id' => $model->id)), array('class' => 'btn btn-primary')) . '<br><br>';
$templates = $model->templates;
foreach ($templates as $t) {
    echo $t->descricao;
    echo CHtml::link('Editar', $this->createUrl('instrucoes/editarTemplate', array('id' => $model->id, 'id2' => $t->id))) . '<br>';
}
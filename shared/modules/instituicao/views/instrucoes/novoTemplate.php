
<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('instrucoes/index', array('id' => $instrucao->id))),
);
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'templates-codigo-_form-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<div class="sh-row">
    <div class="medium-12 column">
        <?php echo $form->error($model, 'descricao'); ?><br>
        <?php echo $form->error($model, 'template'); ?><br>
    </div>
</div>
<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'descricao'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'descricao', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'tipo'); ?>
    </div>
    <div class="medium-7 column">
        <?php
        echo $form->dropDownList($model, 'tipo', array(
            InstrucaoCodigo::TipoAberto => 'Aberto',
            InstrucaoCodigo::TipoBotao => 'Botao',
                ), array('class' => 'medium-12'));
        ?>
    </div>
    <div class="medium-offset-2"></div>
</div>


<div class="sh-row">
    <div class="medium-12 column">
<?php echo $form->labelEx($model, 'template'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="medium-12 column">
        <textarea name="InstrucaoCodigo[template]" id="InstrucaoCodigo-template"  class="sage"><?= stripslashes($model->template); ?></textarea>
        <?php
        echo CHtml::ajaxLink('Carregar preview', $this->createUrl('instrucoes/preview'), array(
            'update' => '#preview-template',
            'type' => 'POST',
            'data' => array('code' => 'js: $("#InstrucaoCodigo-template").val() '),
            'beforeSend' => 'js: function() { $("#preview-template").html("Carregando...") } ',
                ), array('class' => 'btn btn-sm btn-primary', 'id' => 'main-update'))
        ?>
        <?php if(!$model->isNewRecord): ?>
          <button type="button" onclick="salvaContinua($(this))" class='btn btn-success'>
          Salvar e continuar
          </button>
        <?php endif;?>

    </div>
</div>

<?php $this->endWidget(); ?>
<hr>
<div id="preview-template"></div>
<hr>

<script>
    function getParametros() {
        data = {};
        $('#params input').each(function() {
            data[$(this).attr('data-id')] = $(this).val();
        });
        return JSON.stringify(data);
    }
    <?php if(!$model->isNewRecord): ?>
    function salvaContinua(elem) {
        $("#templates-codigo-_form-form").attr("action", "<?= $this->createUrl('instrucoes/EditarTemplate'); ?>/id/<?=$id?>/id2/<?=$id2?>/r/n").submit();
        elem.html('Salvando...');
    }
    <?php endif;  ?>

    setInterval(function(){

      alert('Lembre-se de salvar.')

    }, 7 * 60 * 1000);

</script>
<style>
    <!--
    .sagecell .CodeMirror-scroll {
        min-height: 40em;
        min-width: 100%;
    }
    -->
</style>

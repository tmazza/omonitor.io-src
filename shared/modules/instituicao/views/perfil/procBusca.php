<?php
$this->breadcrumbs += array(
    'Edição: ' . Yii::app()->user->orgName,
);
$this->menuContexto = array(
    ShCode::makeItem('Principal', $this->createUrl('perfil/EditarProcessamentoBusca')),
    ShCode::makeItem('Instruções', $this->createUrl('instrucoes/index')),
    ShCode::makeItem('Traduções', $this->createUrl('perfil/EditarTraducoes')),
);
echo CHtml::beginForm();
echo CHtml::submitButton('Atualizar', array('class' => 'btn btn-success')) . '<br><br>';
echo CHtml::label('Código para processamento da busca', 'cod-proc-busca');
echo "Considere a existência de uma variavel de nome 'a' que receberá o que for digitado na busca";
?>
<div class="sh-row">
    <div class="medium-12 column">
        <textarea name="cod" id="InstrucaoCodigo-template"  class="sage"><?= stripslashes($cod); ?></textarea>
        <?php
        echo CHtml::ajaxLink('Carregar preview', $this->createUrl('perfil/preview'), array(
            'update' => '#preview-template',
            'type' => 'POST',
            'data' => array('code' => 'js: $("#InstrucaoCodigo-template").val() '),
            'beforeSend' => 'js: function() { $("#preview-template").html("Carregando...") } ',
                ), array('class' => 'btn btn-sm btn-primary', 'id' => 'main-update-proc'))
        ?>
    </div>
</div>

<script>
    function getParametros() {
        data = {};
        $('#params input').each(function() {
            data[$(this).attr('data-id')] = $(this).val();
        });
        return JSON.stringify(data);
    }
</script>
<?php
echo CHtml::endForm();
?>
<hr>
<div id="preview-template"></div>
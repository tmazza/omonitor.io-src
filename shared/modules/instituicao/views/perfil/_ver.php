<h1>
    <span class="hint">
        <?= Organizacao::model()->getAttributeLabel($attr); ?>
        <?= CHtml::link('Editar', $this->createUrl('perfil/editar', array('pagina' => $attr)), array('class' => 'link-inside')); ?>
    </span>
</h1>
<div>
    <?php if (is_null($org->{$attr})): ?>
        <div class="hint">
            Não editado.
        </div>
    <?php else: ?>
        <?= $org->{$attr}; ?>
    <?php endif; ?>
</div>
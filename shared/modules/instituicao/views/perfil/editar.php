<?php
$this->breadcrumbs += array(
    'Edição: ' . Yii::app()->user->orgName => $this->createUrl('perfil/index'),
    'Editar'
);
$this->menuContexto = array(
    array('label' => 'Cancelar', 'url' => $this->createUrl('perfil/index')),
);
?>
<?php
$this->renderPartial('_form', array(
    'model' => $model,
    'attr' => $attr,
));
?>

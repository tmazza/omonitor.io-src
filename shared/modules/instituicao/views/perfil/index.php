<?php

$this->breadcrumbs += array(
    'Edição: ' . Yii::app()->user->orgName,
);
$this->menuContexto = array(
    ShCode::makeItem('Apresentação', $this->createUrl('perfil/index', array('pagina' => 'apresentacao'))),
    ShCode::makeItem('Modo de usar', $this->createUrl('perfil/index', array('pagina' => 'modoDeUsar'))),
    ShCode::makeItem('Plataformas', $this->createUrl('perfil/index', array('pagina' => 'plataformas'))),
    ShCode::makeItem('Referências', $this->createUrl('perfil/index', array('pagina' => 'referencias'))),
    ShCode::makeItem('Parceiros', $this->createUrl('perfil/index', array('pagina' => 'parceiros'))),
    ShCode::makeItem('Como utilizar?', $this->createUrl('perfil/index', array('pagina' => 'como_usar'))),
    ShCode::makeItem('Primeiras impressões', $this->createUrl('perfil/index', array('pagina' => 'comentarios'))),
);
?>
<?php $this->renderPartial('_ver', array('org' => $org, 'attr' => $attr)); ?>

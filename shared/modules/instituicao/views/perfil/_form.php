<h1><span class="hint">Editar: </span><?= Organizacao::model()->getAttributeLabel($attr); ?></h1>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'organizacao-_form-form',
    'enableAjaxValidation' => false,
        ));
?>
<div class="">
    <?php
    $this->widget('ImperaviRedactorWidget', array(
        'model' => $model,
        'attribute' => $attr,
        'options' => array(
            'lang' => 'pt_br',
            'plugins' => array(
                'bufferbuttons',
                'fullscreen',
                'video',
                'advanced',
            ),
            'replaceDivs' => false,
            'iframe' => true,
            'minHeight' => 300,
            'cleanSpaces' => false,
//                'minWidth' => 300,
            'linebreaks' => true,
            'removeEmpty' => array('strong', 'em', 'span', 'p'),
            'definedLinks' => $this->createUrl('camada/getDefinedLinks'),
            'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'link'),
        ),
    ));
    ?>

    <?php echo $form->error($model, $attr); ?>
</div>


<div class="textCenter">
    <?php echo CHtml::submitButton('Atualizar', array('class' => 'btn btn-success span6')); ?>
</div>

<?php $this->endWidget(); ?>

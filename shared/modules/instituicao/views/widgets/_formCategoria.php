
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'teclado-form-categoria',
    'enableAjaxValidation' => false,
        ));
?>
<?php
//if (!$model->isNewRecord) {
//    echo '<p class="textRight">';
//    echo CHtml::link('Excluir', $this->createUrl('teclado/excluirCategoria', array('id' => $model->id)), array(
//        'confirm' => 'Todas as teclas desta categoria também serão excluídas. Confirma exclusão de categoria?',
//    ));
//    echo '</p>';
//}
?>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'label'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textField($model, 'label', array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'label'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'descricao'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textArea($model, 'descricao', array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'descricao'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'categoria_pai'); ?>
    </div>
    <div class="column medium-10">
        <?php
        echo $form->dropDownList($model, 'categoria_pai', $cats, array('class' => 'medium-4', 'prompt' => 'Selecione...'));
        ?>
        <br>
        <span class="hint">Tamanho das teclas da categoria.</span>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'categoria_pai'); ?>
    </div>
</div>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<?php $this->endWidget(); ?>
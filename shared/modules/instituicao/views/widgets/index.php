<?php

$this->pageTitle = 'Widgets';
$this->menuContexto = array(
    ShCode::makeItem('Novo widget', $this->createUrl('widgets/novo')),
    ShCode::makeItem('Nova categoria', $this->createUrl('widgets/novaCategoria')),
);
?>
<?php

$this->renderPartial('renderTree', array(
    'categorias' => $categorias,
));


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'codes-codigo-_form-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>
<div class="sh-row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Salvar atualização', array('class' => 'btn btn-success')); ?>
</div>
<div class="sh-row">
    <div class="medium-12 column">
        <?php echo $form->error($model, 'nome'); ?><br>
        <?php echo $form->error($model, 'code'); ?><br>
    </div>
</div>
<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'nome', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'uso'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textArea($model, 'uso', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'labels'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'labels', array('class' => 'medium-12')); ?>
    </div>
    <div class="medium-offset-2"></div>
</div>
<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'doc'); ?>
    </div>
    <div class="medium-7 column">
        <?php echo $form->textField($model, 'doc', array('class' => 'medium-12')); ?>
        <span class="hint">Separdos por |</span>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'categoria_id'); ?>
    </div>
    <div class="medium-7 column">
        <?php
        echo $form->dropDownList($model, 'categoria_id', $cats, array('class' => 'medium-4', 'prompt' => 'Selecione...'));
        ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<div class="sh-row">
    <div class="medium-3 column">
        <?php echo $form->labelEx($model, 'code'); ?>
    </div>
    <div class="medium-7 column">
        <textarea name="Widget[code]" id="TemplatesCodigo-code"  class="sage"><?= stripslashes($model->code); ?></textarea>
        <?php
        echo CHtml::ajaxLink('Carregar preview', $this->createUrl('widgets/preview'), array(
            'update' => '#preview-template',
            'type' => 'POST',
            'data' => array('code' => 'js: $("#TemplatesCodigo-code").val() '),
            'beforeSend' => 'js: function() { $("#preview-code").html("Carregando...") } ',
                ), array('class' => 'btn btn-sm btn-primary', 'id' => 'main-update'))
        ?>
    </div>
    <div class="medium-offset-2"></div>
</div>

<?php $this->endWidget(); ?>
<hr>
<div id="preview-template"></div>
<hr>

<script>
    function getParametros() {
        data = {};
        $('#params input').each(function() {
            data[$(this).attr('name')] = $(this).val();
        });
        return JSON.stringify(data);
    }
</script>
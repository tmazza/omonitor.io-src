<?php if (count($categorias) > 0): ?>
    <ul>

        <?php foreach ($categorias as $c): ?>
            <li><?= $c->label; ?>
            <?php
            $this->renderPartial('renderTree', array('categorias' => $c->categorias));
            ?>
            <?php if (count($c->widgets) > 0): ?>
                <ul class="nav">
                    <?php
                    foreach ($c->widgets as $w) {
                        echo '<li>';
                        echo CHtml::link($w->nome, $this->createUrl('widgets/editar', array('id' => $w->id)));
                        echo '</li>';
                    }
                    ?>
                </ul>
            <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

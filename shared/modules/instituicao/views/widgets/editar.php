<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('widgets/index')),
);
?>
<div class="sh-row">
    <div class="medium-12 column">
        <?php
        $this->renderPartial('_form', array(
            'model' => $model,
            'cats' => $cats,
        ));
        ?>
    </div>
</div>
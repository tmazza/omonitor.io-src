<?php
$this->menuContexto = array(
	ShCode::makeItem('Finalizar edição de ordem', $this->createUrl('exemplos/index')),
);
$this->pageTitle = 'Ordem exemplos categoria ' . $model->nome;
?>
<form method='POST' action="<?=$this->createUrl('exemplos/editarOrdem',array('id'=>$model->id))?>">
  <?php foreach ($model->todosExemplos as $e): ?>
    <input name='ordem[<?=$e->id?>]' type='number' value='<?=$e->ordem?>' style='width:60px;' min=0 />&nbsp;&nbsp;&nbsp;
    <span title="<?=$e->valor;?>">$<?=$e->latex?>$</span><br>
  <?php endforeach; ?>
  <button type='submit'>Salvar</button>
</form>

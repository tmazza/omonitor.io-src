<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'comandos-teclado-_form-form',
    'enableAjaxValidation' => false,
        ));

?>

<?php
if (!$model->isNewRecord) {
    echo '<p class="textRight">';
    echo CHtml::link('Excluir', $this->createUrl('teclado/excluirTecla', array('id' => $model->id)), array(
        'confirm' => 'Confirma exclusão de tecla?',
    ));
    echo '</p>';
}
?>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'categoria_id'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->dropDownList($model, 'categoria_id', array(null=>null) + $categorias, array('class' => 'medium-4')); ?>
        <?php //echo CHtml::link('Nova categoria', ShCode::getModUrl('instituicao', 'teclado', 'novaCategoria', array('id' => $id, 'rt' => ShCode::geraLinkRetorno(array('id'))))) ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'latex'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textField($model, 'latex', array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'latex'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'valor'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textField($model, 'valor', array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'valor'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'layout'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->dropDownList($model, 'layout', ExemplosSearch::layoutTipos(), array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'layout'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
    </div>
</div>

<?php $this->endWidget(); ?>

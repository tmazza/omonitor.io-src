<div style='padding-left:40px;'>
  <?php
  foreach($categorias as $c) {
    echo '<table class="table">';
    echo '<tr><td colspan="3" class="">';
    echo "<h3 style='display:inline-block;'>{$c->nome}</h3>   ";
    echo CHtml::link('Editar', $this->createUrl('exemplos/EditCategoria', array('id' => $c->id)), array('class' => 'btn btn-xs btn-info'));
    echo CHtml::link('Ordem', $this->createUrl('exemplos/EditarOrdem', array('id' => $c->id)), array('class' => 'btn btn-xs btn-default'));
    echo CHtml::link('x', $this->createUrl('exemplos/excCategoria', array('id' => $c->id)), array('class' => 'btn btn-xs btn-red', 'confirm' => 'Certeza?'));
    echo '<br><br>';
    echo CHtml::link('Despublicar todos', $this->createUrl('exemplos/despublicarTodos', array('catId' => $c->id)), array('class' => 'btn btn-xs btn-primary'));
    echo CHtml::link('Publicar todos', $this->createUrl('exemplos/publicarTodos', array('catId' => $c->id)), array('class' => 'btn btn-xs btn-success'));
    echo '<br><br></td></tr>';

    foreach($c->todosExemplos as $e){

      echo '<tr>';
      echo "<td>$ {$e->latex} $</td>";
      echo "<td style='font-family:monospace; color: #333;'>{$e->valor}</td>";
      echo '<td>';
      echo CHtml::link('Editar', $this->createUrl('exemplos/EditEx', array('id' => $e->id)), array('class' => 'btn btn-xs btn-info'));

      if($e->publicado == 1){
         echo CHtml::link('Despublicar', $this->createUrl('exemplos/despublicar', array('id' => $e->id)), array('class' => 'btn btn-xs btn-primary'));
      } else {
         echo CHtml::link('Publicar', $this->createUrl('exemplos/publicar', array('id' => $e->id)), array('class' => 'btn btn-xs btn-success'));
      }

      echo CHtml::link('x', $this->createUrl('exemplos/excEx', array('id' => $e->id)), array('class' => 'btn btn-xs', 'confirm' => 'Certeza?'));
      echo '</td>';
      echo '</tr>';

    }

    echo '</table>';

    $this->renderPartial('linha',array('categorias'=>$c->filhas));
  }
  ?>
</div>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'teclado-form-categoria',
    'enableAjaxValidation' => false,
        ));
?>
<?php
if (!$model->isNewRecord) {
    echo '<p class="textRight">';
    echo CHtml::link('Excluir', $this->createUrl('teclado/excluirCategoria', array('id' => $model->id)), array(
        'confirm' => 'Todas as teclas desta categoria também serão excluídas. Confirma exclusão de categoria?',
    ));
    echo '</p>';
}
?>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'nome'); ?>
    </div>
    <div class="column medium-10">
        <?php echo $form->textField($model, 'nome', array('class' => 'medium-4')); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'nome'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-2">
        <?php echo $form->labelEx($model, 'pai_id'); ?>
    </div>
    <div class="column medium-10">
        <?php $pais = ExemplosSearchCategoria::model()->findAll(); ?>
        <?php echo $form->dropDownList($model, 'pai_id', CHtml::listData($pais,'id','nome')
        , array('class' => 'medium-4','prompt'=>false)); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo $form->error($model, 'pai_id'); ?>
    </div>
</div>
<div class="sh-row">
    <div class="column medium-12">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success')); ?>
    </div>
</div>

<?php $this->endWidget(); ?>

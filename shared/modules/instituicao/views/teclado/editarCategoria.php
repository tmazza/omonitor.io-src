<?php
$this->pageTitle = 'Editar categoria';
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('teclado/editar', array('id' => $id))),
);
$this->renderPartial('_formCategoria', array(
    'model' => $model,
    'id' => $id,));

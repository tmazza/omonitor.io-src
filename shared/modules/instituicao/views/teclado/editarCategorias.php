<?php
$this->pageTitle = 'Edição de categorias de teclado';
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('teclado/editar', array('id' => $id))),
    ShCode::makeItem('Nova categoria', $this->createUrl('teclado/novaCategoria', array('id' => $id))),
);
?>

<?php if (count($categorias) > 0): ?>
    <ul class="nav">
        <?php foreach ($categorias as $cat): ?>
            <li>
                <?= CHtml::link($cat->nome, $this->createUrl('teclado/editarCategoria', array('id' => $cat->id))); ?>
            </li>        
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    Nenhuma categoria criada.
<?php endif; ?>

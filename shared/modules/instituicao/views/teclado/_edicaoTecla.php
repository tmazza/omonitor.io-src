<div class="box">
    <div class="box-header">
        <div class="box-tools pull-right">
            <?= CHtml::link('Editar', $this->createUrl('teclado/editarTecla', array('id' => $tecla->id)), array('class' => '')) ?>
        </div>
    </div>
    <div class="box-body">
        <div class="textCenter">
            <button type==button" style="overflow: hidden;" class="btn btn-default btn-<?= $tecla->categoria->tamanho; ?>">
                <?= $tecla->label; ?>
            </button>
            <br>
            <span class="textCenter hint"><?= $tecla->code; ?></span>
        </div>
    </div>
</div>
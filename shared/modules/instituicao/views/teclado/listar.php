<?php
$this->pageTitle = 'Edição de teclados';
?>

<?php if (count($teclados) > 0): ?>
    <ul class="nav">
        <?php foreach ($teclados as $tec): ?>
            <li>
                <?= CHtml::link($tec->nome, $this->createUrl('teclado/editar', array('id' => $tec->id))); ?>
            </li>        
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    Nenhum teclado criado.
<?php endif; ?>

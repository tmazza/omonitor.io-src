<?php
$this->pageTitle = 'Ordem teclas: ' .  $categoria->nome;
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('teclado/editar',array('id'=>$categoria->teclado->id))),
);
$teclas = $categoria->teclas;
?>
<form method='POST' action="<?=$this->createUrl('teclado/ordemTeclas',array('id'=>$categoria->id))?>">
  <?php foreach ($teclas as $t): ?>
    <input name='ordem[<?=$t->id?>]' type='number' value='<?=$t->ordem?>' style='width:60px;' min=0 />&nbsp;&nbsp;&nbsp;
    <span title="<?=$t->code;?>"><?=$t->label?></span><br>
  <?php endforeach; ?>
  <button type='submit'>Salvar</button>
</form>

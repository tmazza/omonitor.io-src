<?php
$this->pageTitle = 'Edição de teclado: ' . $teclado->nome;
$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('teclado/index')),
    ShCode::makeItem('Editar categorias', $this->createUrl('teclado/editarCategorias', array('id' => $teclado->id))),
    ShCode::makeItem('Nova tecla', $this->createUrl('teclado/novaTecla', array('id' => $teclado->id))),
    ShCode::makeItem('Novo Comando', $this->createUrl('teclado/novoComando', array('id' => $teclado->id))),
);
$categorias = $teclado->categorias;
?>
<?php if (count($categorias) > 0): ?>
    <?php foreach ($categorias as $c): ?>
        <h4><?= $c->nome; ?> <?=CHtml::link('Editar ordem',$this->createUrl('teclado/ordemTeclas',array('id'=>$c->id)))?></h4>
        <?php if (count($c->teclas) > 0): ?>
            <ul class="medium-block-grid-4 small-block-grid-1 large-block-grid-5">
                <?php foreach ($c->teclas as $t): ?>
                    <li>
                        <?php $this->renderPartial('_edicaoTecla', array('tecla' => $t)); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            <span class="hint">Nenhum tecla nesta categoria.</span>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
    <h5><span class="hint">Nenhuma categoria criada.</span></h5>
<?php endif; ?>

<br><br><br>
<a onclick="$('#preveiew-tec').slideDown();">Pré-visualiar teclado</a>
<div id="preveiew-tec" style="display: none;">
    <?php $this->renderPartial('_verTeclado', array('id' => $teclado->id)); ?>
</div>

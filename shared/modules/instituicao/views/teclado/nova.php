<?php

$id = $model->isNewRecord ? $id : $model->categoria->teclado->id;
$this->pageTitle = 'Nova tecla';
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('teclado/editar', array('id' => $id))),
);
$this->renderPartial('_form', array(
    'model' => $model,
    'categorias' => $categorias,
    'id' => $id,));

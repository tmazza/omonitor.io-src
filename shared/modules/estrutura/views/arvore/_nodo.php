<?php $nivel++ ?>
<?php $filhos = is_null($item->filhos) ? array() : $item->filhos; ?>
<ul class="arvore">
    <li>
        <?php
        echo ShView::ajaxModal(
                $item->nome, 
                $this->createUrl('arvore/AtualizaItem', array('id' => $item->id,)), 
                'atualizar-nome-' . $item->id, 
                'Ops! Tivemos um probleminha, atualize a página.', 
                array('id' => 'atualizar-nome-' . $item->id, 'class' => ''));

        $this->renderPartial('_nodo_all', array(
            'item' => $item,
            'canEdit' => $canEdit,
            'simples' => $item->tipo == 0,
        ));
        if (count($filhos) > 0) {
            foreach ($filhos as $filho) {
                $this->renderPartial('_nodo', array(
                    'item' => $filho,
                    'nivel' => $nivel,
                    'hasDesconexos' => $hasDesconexos,
                    'canEdit' => $canEdit,
                ));
            }
        } else {
            $this->renderPartial('_nodo_folha', array(
                'item' => $item,
                'hasDesconexos' => $hasDesconexos,
            ));
        }
        ?>
    </li>
</ul>
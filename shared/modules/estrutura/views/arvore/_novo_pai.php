<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Inclusão de pai</h3>
</div>
<?php echo CHtml::beginForm(); ?>
<div class="modal-body">
    <?php echo CHtml::hiddenField('filho', $filho); ?>
    <?php echo CHtml::label('Nome: ', 'tax_nome'); ?>
    <?php echo CHtml::textField('tax_nome', ''); ?>
</div>
<div class="modal-footer">
    <?php echo CHtml::submitButton('Incluir', array('class' => 'btn btn-success right')); ?>
</div>
<?php echo CHtml::endForm(); ?>
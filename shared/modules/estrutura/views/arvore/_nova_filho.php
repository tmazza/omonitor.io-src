<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Inclusão de filho</h3>
</div>
<?php echo CHtml::beginForm(); ?>
<div class="modal-body">
    <?php echo CHtml::hiddenField('pai', $pai); ?>
    <?php if ($temFilhos == true): ?>
        <div>Este conteúdo já possui tópicos relacionados, o que fazer com eles após a inclusão do conteúdo filho?</div>
        <div class="textCenter">
            <?=
            CHtml::dropDownList('topicos', null, array(
                'DESC' => 'Desconectar tópicos',
                'ATAC' => 'Conecter tópicos ao filho criado',
                    ), array('prompt' => "Selecione...", 'style' => 'margin: 0 auto;'));
            ?>
        </div><br><br><br>
    <?php endif; ?>
    <div>
        <?php echo CHtml::label('Nome: ', 'tax_nome'); ?>
        <?php echo CHtml::textField('tax_nome', ''); ?>
    </div>
</div>
<div class="modal-footer">
    <?php echo CHtml::submitButton('Incluir', array('class' => 'btn btn-success right')); ?>
</div>
<?php echo CHtml::endForm(); ?>
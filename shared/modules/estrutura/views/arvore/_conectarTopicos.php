<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Conectar tópico</h3>
</div>
<?php if (count($topicos) > 0): ?>
    <?php echo CHtml::beginForm(); ?>
    <div class="modal-body">
        <div class="hint">Mostrando somente tópicos desconexos</div>
        <?php echo CHtml::hiddenField('item', $item); ?>
        <?php echo CHtml::label('Tópico: ', 'topico'); ?>
        <?php echo CHtml::dropDownList('topico', null, CHtml::listData($topicos, 'id', 'nome'), array('prompt' => 'Selecionar tópico...')); ?>
    </div>
    <div class="modal-footer">
        <?php echo CHtml::submitButton('Incluir', array('class' => 'btn btn-success right')); ?>
    </div>
    <?php echo CHtml::endForm(); ?>
<?php else: ?>
    <div class="modal-body">
        <p class="hint">Nenhum tópico desconexo.</p>
        <br><br>
    </div>
<?php endif; ?>

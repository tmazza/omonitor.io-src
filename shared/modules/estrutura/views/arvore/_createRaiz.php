<h1>Raiz de conteúdo</h1>
<br><br>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tax-item-_createRaiz-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'nome'); ?>
        <?php echo $form->textField($model, 'nome'); ?>
        <?php echo $form->error($model, 'nome'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Criar'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
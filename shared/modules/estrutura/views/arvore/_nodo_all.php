<?php if ($canEdit): ?>
    <span class="right nodo-edit">
        <?php
        echo ShView::ajaxModal('novo filho', $this->createUrl('arvore/novaConexao', array('pai' => $item->id)), 'novo-filho-' . $item->id);

        if (!isset($simples) || (isset($simples) && !$simples)) {
            echo '<span class="hint"> | </span>';
            echo ShView::ajaxModal('novo pai', $this->createUrl('arvore/novoPai', array('filho' => $item->id)), 'novo-pai-' . $item->id);
            echo '<span class="hint"> | </span>';
            echo CHtml::link('remover', $this->createUrl('arvore/removerNodo', array(
                        'id' => $item->id,
                    )), array(
                'confirm' => 'Todos os conteudos filhos serão excluidos e os tópicos encontrados serão desconectados. Tem certeza?',
                'class' => 'hint',
                'id' => 'remover' . $item->id,
            ));
        }
        ?>
    </span>
<?php endif; ?>
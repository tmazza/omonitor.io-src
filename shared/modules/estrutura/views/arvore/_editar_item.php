<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Editar nome</h3>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'topico-form',
    'enableAjaxValidation' => false,
        ));
?>

<div class="modal-body">
    <?php echo $form->labelEx($model, 'nome', array()); ?>
    <?php echo $form->textField($model, 'nome', array('class' => 'span5')); ?>
    <br><br>
    <?php echo $form->error($model, 'nome'); ?>
</div>
<div class="modal-footer">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Atualizar', array('class' => 'btn btn-success right')); ?>
</div>

<?php $this->endWidget(); ?>

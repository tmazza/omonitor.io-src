<?php

if ($hasDesconexos) {
    echo '<span class="hint"> | </span>';
    echo ShView::ajaxModal('conectar tópico', $this->createUrl('arvore/conectarTopico', array('id' => $item->id)), 'conecta-topico-' . $item->id, 'Falha ao incluir nodo, atualize a página');
}

$topicos = $item->topicos;
if (!is_null($topicos)) {
    echo '<ul>';
    foreach ($topicos as $topico) {
        echo "<li class='nodo-topico'>";
        echo $topico->nome;
        echo '<span class="hint"> | </span>';
        echo CHtml::link('desconectar', $this->createUrl('arvore/desconectar', array(
                    'id' => $topico->id,
                    'idItem' => $item->id,
                )), array('class' => 'hint'));

        echo "</li>";
    }
    echo '</ul>';
}

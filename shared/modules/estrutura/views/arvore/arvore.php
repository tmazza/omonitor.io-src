<?php
$this->pageTitle = 'Árvore de conteúdo';
$this->breadcrumbs += array(
    'Árvore de conteúdo'
);
$this->menuContexto[] = array('label' => 'Visualizar árvore no site', 'url' => Yii::app()->baseUrl . '/arvore/ver4');
?>

<!--<div id="modal-ajax" class="modal hide fade"></div>-->

<?php
// Dialog utilizado para carregamento de conteúdo ajax
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'nova-tax',
    'options' => array(
        'title' => 'Novo filho',
        'width' => 700,
        'height' => 300,
        'autoOpen' => false,
    ),
));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<div class="row">
    <div class="span12 arvore">
        <?php //endif; ?>
        <?php if (is_null($autor->raizPessoal)): ?>
            <span class="hint">
                Você ainda não possue uma árvore de conteúdo pessoal.
                <?php
                echo CHtml::ajaxLink('Criar', $this->createUrl('arvore/createRaiz', array()), array(
                    'success' => 'js:function(data){$("#nova-tax").html(data);$("#nova-tax").dialog("option","title", "Criação de árvore: ");$("#nova-tax").dialog("open");}',
                    'error' => 'js:function(data){alert("Falha ao criar raiz.");}',
                        ), array('class' => ''));
                ?>
            </span>
        <?php else: ?>
            <!--<span class="hint">(Somente alunos/classes atribuídas terão acesso.)</span><br>-->
            <?php
            $this->renderPartial('_nodo', array(
                'item' => $autor->raizPessoal,
                'nivel' => 0,
                'hasDesconexos' => count($topicos) > 0,
                'canEdit' => true,
            ));
            ?>
        <?php endif; ?>
    </div>
</div>
<br><br>
<div class="row">
    <div class="span12 textCenter">
        <h5>Tópicos desconexos</h5>
        <?php if (count($topicos) > 0): ?>
            <hr>
            <?php
            $first = true;
            foreach ($topicos as $top) {
                if (count($top->conexoes) == 0) {
                    if (!$first) {
                        echo '<span class="hint"> | </span>';
                    }
                    echo $top->nome;
                    $first = false;
                }
            }
            if ($first) {
                echo '<div class="textCenter hint">Todos os tópicos estão conectados. Muito bem!</div>';
            }
            ?>
        <?php else: ?>
            <div class="textCenter hint">Nenhum tópico desconexo</div>
        <?php endif; ?>
        <br>
    </div>
</div>
<style>
    a.hint:hover {
        color: blue;
    }
</style>
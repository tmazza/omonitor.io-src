<?php

define('TAX_RAIZ', 0);
define('TAX_ITEM', 1);

/**
 * Description of TaxonomiaHelper
 *
 * @author tiago
 */
trait TaxonomiaTrait {

    private function novaTax($nomeItem, $tipo = EstTaxTipo::Nodo) {
        $taxItem = new EstTaxItem();
        $taxItem->nome = $nomeItem;
        $taxItem->user_id = Yii::app()->user->id;
        $taxItem->tipo = $tipo;
        if (!$taxItem->save()) {
            echo '<pre>';
            print_r($taxItem->getErrors());
            exit;
        }
        return $taxItem->id;
    }

    private function conectaTax($pai, $filho) {
        // TODO: garantir que um nodo tenha somente um PAI!!!
        $modelPai = EstTaxItem::model()->doAutor()->findByPk($pai);
        $modelFilho = EstTaxItem::model()->doAutor()->findByPk($filho);
        if (!is_null($modelPai) && !is_null($modelFilho)) {
            $taxFilho = new EstTaxFilho();
            $taxFilho->pai = $pai;
            $taxFilho->filho = $filho;
            $taxFilho->user_id = Yii::app()->user->id;
            if (!$taxFilho->save()) {
                echo '<pre>';
                print_r($taxFilho->getErrors());
                exit;
            }
        } else {
            echo '<pre>';
            print_r('Itens invalidos, relação não incluida.');
            exit;
        }
        return true;
    }

    private function desconectaTax($pai, $filho) {
        return EstTaxFilho::model()->doAutor()->deleteByPk(array(
                    'pai' => $pai,
                    'filho' => $filho,
        ));
    }

    private function addFilho($nome, $pai, $op) {
        $filhoId = $this->novaTax($nome);

        if ($op == 'ATAC') {
            // Conecta tópicos ao 
            EstTopicoTax::model()->doAutor()->updateAll(array(
                'tax_item_id' => $filhoId,
                    ), array(
                'condition' => "tax_item_id = {$pai->id}"
            ));
        } else {
            EstTopicoTax::model()->doAutor()->deleteAll(array(
                'condition' => "tax_item_id = {$pai->id}",
            ));
        }

        if ($this->conectaTax($pai->id, $filhoId)) {
            Yii::app()->user->setFlash('flash-sucess', 'Filho adicionado.');
        } else {
            Yii::app()->user->setFlash('flash-sucess', 'Filho adicionado.');
        }
    }

    private function addPai($nome, $filho) {
        $modelFilho = EstTaxItem::model()->doAutor()->findByPk($filho);
        if ($modelFilho->tipo == 1) {

            $paiId = $this->novaTax($nome, EstTaxTipo::Nodo);
            $conexaoAntiga = EstTaxFilho::model()->doAutor()->findAllByAttributes(array(
                'filho' => $filho,
            ));
            foreach ($conexaoAntiga as $conexao) {
                $conexao->filho = $paiId;
                $conexao->save();
            }

            if ($this->conectaTax($paiId, $filho)) {
                Yii::app()->user->setFlash('flash-sucess', 'Filho adicionado.');
            } else {
                Yii::app()->user->setFlash('flash-error', 'Pai não pode ser criado adicionado.');
            }
        } else {
            Yii::app()->user->setFlash('flash-error', 'Pai não pode se adicionado à raiz de conteúdo.');
        }
    }

}

<?php

/**
 * Description of EditorController
 *
 * @author tiago
 */
class ArvoreController extends EstruturaController {

    use TaxonomiaTrait;

    public $layout = '//layouts/column1';

    public function actionIndex() {
        $this->render('arvore', array(
            'autor' => EstAutor::model()->findByAttributes(array('id' => Yii::app()->user->id)),
            'topicos' => EstTopico::model()->findAll("user_id = " . Yii::app()->user->id)
        ));
    }

    public function actionNovaConexao($pai) {
        $modelPai = EstTaxItem::model()->doAutor()->findByPk($pai);
        $temFilhos = count($modelPai->topicos) > 0;

        if (isset($_POST['tax_nome']) && isset($_POST['pai'])) {
            $nome = $_POST['tax_nome'];
            $pai = $_POST['pai'];

            if ($temFilhos) {
                $op = $_POST['topicos'];
            } else {
                $op = 'NOP';
            }

            $this->addFilho($nome, $modelPai, $op);
            $this->redirect($this->createUrl('arvore/index'));
        }


        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('_nova_filho', array(
                'pai' => $pai,
                'temFilhos' => $temFilhos,
                    ), true);
        } else {
            $this->render('_nova_filho', array(
                'pai' => $pai,
                'temFilhos' => $temFilhos,
            ));
        }
    }

    public function actionNovoPai($filho) {
        if (isset($_POST['tax_nome']) && isset($_POST['filho'])) {
            $nome = $_POST['tax_nome'];
            $this->addPai($nome, $filho);
            $this->redirect($this->createUrl('arvore/index'));
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('_novo_pai', array(
                'filho' => $filho,
                    ), true);
        } else {
            $this->render('_novo_pai', array(
                'filho' => $filho,
            ));
        }
    }

    public function actionDesconectar($id, $idItem) {
        $conexao = EstTopicoTax::model()
                ->doAutor()
                ->findByPk(array(
            'topico_id' => $id,
            'tax_item_id' => $idItem,
        ));
        if ($conexao->delete()) {
            Yii::app()->user->setFlash('flash-success', 'Tópico desconectado.');
        } else {
            Yii::app()->user->setFlash('flash-error', 'Falha ao salvar.');
        }
        $this->redirect($this->createUrl('arvore/index'));
    }

    public function actionConectarTopico($id) {
        if (isset($_POST['item']) && isset($_POST['topico'])) {
            $topico = EstTopico::model()->doAutor()->findByPk($_POST['topico']);
            $conexao = new EstTopicoTax();
            $conexao->topico_id = $topico->id;
            $conexao->tax_item_id = $_POST['item'];
            $conexao->user_id = Yii::app()->user->id;
            if ($conexao->save()) {
                Yii::app()->user->setFlash('flash-success', 'Tópico conectado.');
            } else {
                Yii::app()->user->setFlash('flash-error', 'Falha ao salvar.');
            }
            $this->redirect($this->createUrl('arvore/index'));
        }
        $topicosEmTax = EstTopicoTax::model()
                ->doAutor()
                ->findAll(array(
            'index' => 'topico_id',
        ));
        $criteria = new CDbCriteria();
        $criteria->condition = "excluido = 0";
        $criteria->addNotInCondition('id', array_keys($topicosEmTax));
        $topicos = EstTopico::model()->doAutor()->findAll($criteria);
        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('_conectarTopicos', array(
                'item' => $id,
                'topicos' => $topicos,
                    ), true);
        } else {
            $this->render('_conectarTopicos', array(
                'item' => $id,
                'topicos' => $topicos,
            ));
        }
    }

    public function actionAtualizaItem($id) {
        $model = EstTaxItem::model()->doAutor()->findByPk($id);
        if (!is_null($model)) {
            if (isset($_POST['EstTaxItem']['nome'])) {
                $model->nome = $_POST['EstTaxItem']['nome'];
                if ($model->save()) {
                    Yii::app()->user->setFlash('alert-success', 'Nome atualizado.');
                } else {
                    Yii::app()->user->setFlash('alert-error', 'Ops! Nome não pode ser atualizado.');
                }
                $this->redirect($this->createUrl('arvore/index'));
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo $this->renderPartial('_editar_item', array(
                    'model' => $model,
                        ), true);
            } else {
                $this->render('_editar_item', array(
                    'model' => $model,
                ));
            }
        } else {
            Yii::app()->user->setFlash('alert-error', 'Item não encontrado.');
        }
    }

    public function actionCreateRaiz() {
        $autor = Autor::model()->findByPk(Yii::app()->user->id);

        if (is_null($autor->raizPessoal)) {
            $model = new EstTaxItem();

            if (isset($_POST['EstTaxItem'])) {
                $model->attributes = $_POST['EstTaxItem'];
                $model->user_id = Yii::app()->user->id;
                $model->tipo = EstTaxTipo::RaizAutor;
                if ($model->save()) {
                    Yii::app()->user->setFlash('alert-success', 'Raiz de conteúdo criada.');
                    $this->redirect($this->createUrl('arvore/index'));
                }
            }

            echo $this->renderPartial('_createRaiz', array(
                'model' => $model,
                    ), true);
        } else {
            Yii::app()->user->setFlash('alert-error', 'Você já possui uma raiz de conteúdo pessoal.');
            $this->redirect($this->createUrl('arvore/index'));
        }
    }

    public function actionRemoverNodo($id) {
        $this->removeONodo($id);
        Yii::app()->user->setFlash('alert-success', 'Conteúdo (e subconteúdo(s)) removidos. Tópicos desconectados.');
        $this->redirect($this->createUrl('arvore/index'));
    }

    private function removeONodo($id) {
        $tax = EstTaxItem::model()
                ->doAutor()
                ->findByPk($id);
        // Apaga tópico conectados
        EstTopicoTax::model()
                ->doAutor()
                ->deleteAll("tax_item_id = {$id}");
        foreach ($tax->filhos as $filho) {
            $this->removeONodo($filho->id);
        }
        EstTaxFilho::model()->doAutor()->deleteAll("pai = {$id} || filho = {$id}"); // TODO: rever a necessidade do uso do filho
        // Apaga relação de filhos
        $tax->delete();
    }

}

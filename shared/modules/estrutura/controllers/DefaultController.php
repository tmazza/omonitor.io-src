<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends EstruturaController {

    public function actionIndex() {
        $this->redirect($this->createUrl('arvore/index'));
    }

}

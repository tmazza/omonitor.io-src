<?php

class EstruturaModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'estrutura.components.*',
            'estrutura.models.*',
            'estrutura.traits.*',
            'estrutura.helpers.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Arvore
            'arvore.index' => 'verArvore',
            'arvore.novaconexao' => 'criarNodoArvore',
            'arvore.novopai' => 'atualizarNodoArvore',
            'arvore.desconectar' => 'atualizarNodoArvore',
            'arvore.conectartopico' => 'atualizarNodoArvore',
            'arvore.atualizaitem' => 'atualizarNodoArvore',
            'arvore.createraiz' => 'criarNodoArvore',
            'arvore.removernodo' => 'excluirNodoArvore',
        );
    }

}

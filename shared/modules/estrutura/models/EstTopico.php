<?php

/**
 * This is the model class for table "topico".
 *
 * The followings are the available columns in table 'topico':
 * @property integer $id
 * @property string $nome
 */
class EstTopico extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'topico';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('nome', 'length', 'max' => 200),
            array('id, nome', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'EstAutor', 'user_id'),
            'conexoes' => array(self::MANY_MANY, 'EstTaxItem', EstTopicoTax::getTableName() . '(topico_id,tax_item_id)'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Topico the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

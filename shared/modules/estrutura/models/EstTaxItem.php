<?php

/**
 * This is the model class for table "tax_item".
 *
 * The followings are the available columns in table 'tax_item':
 * @property integer $id
 * @property string $nome
 * @property integer $tipo
 *
 * The followings are the available model relations:
 * @property EstTaxFilho[] $taxFilhos
 * @property EstTaxFilho[] $taxFilhos1
 */
class EstTaxItem extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tax_item';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('nome', 'required'),
            array('id, nome', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'EstAutor', 'user_id'),
            'filhos' => array(self::MANY_MANY, 'EstTaxItem', EstTaxFilho::getTableName() . '(pai,filho)', 'order' => 'id ASC'),
            'topicos' => array(self::MANY_MANY, 'EstTopico', EstTopicoTax::getTableName() . '(topico_id,tax_item_id)', 'order' => 'topicos_topicos.id ASC'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'tipo' => 'Tipo',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TaxItem the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

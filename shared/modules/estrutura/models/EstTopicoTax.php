<?php

/**
 * This is the model class for table "topico_tax".
 *
 * The followings are the available columns in table 'topico_tax':
 * @property integer $topico_id
 * @property integer $tax_item_id
 */
class EstTopicoTax extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'topico_tax';
    }

    public static function getTableName() {
        return 'topico_tax';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('topico_id, tax_item_id', 'required'),
            array('topico_id, tax_item_id', 'numerical', 'integerOnly' => true),
            array('topico_id, tax_item_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
        );
    }
    
    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            )
        );
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'topico_id' => 'Topico',
            'tax_item_id' => 'Tax Item',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('topico_id', $this->topico_id);
        $criteria->compare('tax_item_id', $this->tax_item_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TopicoTax the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "autor".
 *
 * The followings are the available columns in table 'autor':
 * @property integer $id
 * @property string $id
 * @property string $nome
 *
 * The followings are the available model relations:
 * @property AutorLivro[] $autorLivros
 */
class EstAutor extends CActiveRecord {

    public function tableName() {
        return 'user';
    }

    public function rules() {
        return array(
            array('id, nome', 'required'),
            array('nome', 'length', 'max' => 200),
            array('id, id, nome', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'raizPessoal' => array(self::HAS_ONE, 'EstTaxItem', 'user_id', 'condition' => "tipo = " . EstTaxTipo::RaizAutor),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
        );
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Autor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

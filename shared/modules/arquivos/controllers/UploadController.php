<?php

/**
 * Description of UploadController
 *
 * @author tiago
 */
class UploadController extends ArquivosController {

    /**
     * Lista arquivos do autor
     */
    public function actionListar() {
        $arquivos = new CActiveDataProvider(ArqArquivo::model()->doAutor(), array(
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
        $this->render('listar', array(
            'arquivos' => $arquivos,
        ));
    }

    /**
     * Inclusão de novo arquivo
     * @param type $r - url de retorno
     */
    public function actionNovo($rt = false) {
        $model = new ArqArquivo;
        if (isset($_POST['ArqArquivo'])) {
            $model->attributes = $_POST['ArqArquivo'];
            $model->file = CUploadedFile::getInstance($model, 'file');
            $model->user_id = Yii::app()->user->id;
            if ($model->validate()) {
                $model->alias = $model->generateAlias($model->file->getName());
                $updaloadPath = Yii::getPathOfAlias('application');
                if ($model->file->saveAs($updaloadPath . '/uploads/' . $model->alias) && $model->save()) {
                    ShCode::setFlashSucesso('Arquivo salvo.');
                } else {
                    ShCode::setFlashErro('Falha ao salvar arquivo.');
                }
                if ($rt) {
                    $this->redirect(ShCode::interpretaLinkRetorno($rt));
                } else {
                    $this->redirect($this->createUrl('upload/listar'));
                }
            }
        }
        $this->render('novo', array(
            'model' => $model,
        ));
    }

}

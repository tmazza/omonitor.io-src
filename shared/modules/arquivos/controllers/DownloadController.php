<?php

class DownloadController extends ArquivosController {

    public function actionGet($id) {
        $arquivo = $this->getArquivo($id);
        $filePath = Yii::getPathOfAlias('application') . '/uploads/' . $arquivo->alias;
        if (file_exists($filePath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filePath));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            ob_end_flush();
            readfile($filePath);
        }
    }

}

<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends ArquivosController {

    public function actionIndex() {
        $this->redirect($this->createUrl('upload/listar'));
    }
    
}

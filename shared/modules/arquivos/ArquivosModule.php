<?php

class ArquivosModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'arquivos.components.*',
            'arquivos.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Upload
            'upload.listar' => 'lerArquivo',
            'upload.novo' => 'criarArquivo',
            // Controller Download
            'download.get' => 'lerArquivo',
        );
    }

}

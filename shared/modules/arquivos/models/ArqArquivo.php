<?php

/**
 * This is the model class for table "arquivo".
 *
 * The followings are the available columns in table 'arquivo':
 * @property integer $id
 * @property string $alias
 * @property string $nome
 * @property string $user_id
 * @property integer $publicado
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class ArqArquivo extends CActiveRecord {

    public $file;

    public function tableName() {
        return 'arquivo';
    }

    public static function getTableName() {
        return 'arquivo';
    }

    public function rules() {
        return array(
            array('file', 'file', 'types' => 'jpg, gif, png, pdf, doc, docx, csv'),
            array('nome, user_id', 'required'),
            array('publicado,user_id', 'numerical', 'integerOnly' => true),
            array('alias', 'length', 'max' => 256),
            array('nome', 'length', 'max' => 512),
            array('id, alias, nome, user_id, publicado', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'alias' => 'Alias',
            'nome' => 'Nome',
            'user_id' => 'Autor',
            'publicado' => 'Publicado',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function generateAlias($uploadName) {
        $extension = pathinfo($uploadName, PATHINFO_EXTENSION);
        return md5($uploadName . microtime(true)) . '.' . $extension;
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

}

<?php

$this->menuContexto = array(
    ShCode::makeItemCondicionado('Cancelar', $this->createUrl('upload/listar')),
);
?>
<h1>Upload arquivo</h1>

<div class="sh-row">
    <div class="column medium-12">
        <?php
        $form = $this->beginWidget(
                'CActiveForm', array(
            'id' => 'upload-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                )
        );
        ?>

        <div class="sh-row">
            <div class="column medium-2">
                <?= $form->labelEx($model, 'nome'); ?>
            </div>
            <div class="column medium-3">
                <?= $form->textField($model, 'nome'); ?>
            </div>
            <div class="column medium-7">
                <?= $form->error($model, 'nome'); ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="column medium-2">
                <?= $form->labelEx($model, 'file'); ?>
            </div>
            <div class="column medium-3">
                <?= $form->fileField($model, 'file'); ?>
            </div>
            <div class="column medium-7">
                <?= $form->error($model, 'file'); ?>
            </div>
        </div>
        <br>
        <div class="sh-row">
            <div class="column medium-12">
                <?= CHtml::submitButton('Salvar', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <?php
        $this->endWidget();
        ?>
    </div>
</div>

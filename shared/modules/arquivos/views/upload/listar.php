<?php
$this->menuContexto = array(
    ShCode::makeItem('Novo arquivo', $this->createUrl('upload/novo')),
);
?>
<h1>Arquivos</h1>
<div class="sh-row">
    <div class="medium-12 columns">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $arquivos,
            'columns' => array(
                array(
                    'type' => 'html',
                    'name' => 'nome',
                    'value' => 'CHtml::link($data->nome . "." .pathinfo($data->alias, PATHINFO_EXTENSION), Yii::app()->controller->createUrl("download/get", array("id" => $data->id)))',
                ),
                'publicado',
            ),
        ));
        ?>
    </div>
</div>
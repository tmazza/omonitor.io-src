<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class ArquivosController extends EditorController {

    protected function beforeAction($action) {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.column1';
        return parent::beforeAction($action);
    }

    /**
     * Busca curso do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getArquivo($id) {
        $arquivo = ArqArquivo::model()
                ->doAutor()
                ->findByPk($id);
        if (is_null($arquivo)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Arquivo não econtrado.');
            $this->redirect($this->createUrl('upload/listar'));
        }
        return $arquivo;
    }

}

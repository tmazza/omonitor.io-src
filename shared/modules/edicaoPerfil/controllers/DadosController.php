<?php

/**
 * Description of DadosController
 *
 * @author tiago
 */
class DadosController extends EdicaoPerfilController {

    /**
     * Mostra informações do usuário
     */
    public function actionVer() {
        $user = $this->getUsuario();
        $this->render('ver', array(
            'user' => $user,
        ));
    }

    /**
     * Edição de nome de usuário.
     * Nome esta presente na tabela de perfil corresponde. Atualmente autor ou aluno.
     */
    public function actionEditarNome() {
        $user = $this->getUsuario();

        if (isset($_POST['nome'])) {
            $nome = $_POST['nome'];
            if ($user->atualizaNome($nome)) {
                Yii::app()->user->setFlash('alert-success', 'Atualizado.');
                $this->redirect($this->createUrl('dados/ver'));
            }
        }

        $this->render('editarNome', array(
            'model' => $user,
        ));
    }

    /**
     * Edição de apresentação de usuário.
     * Apresentação esta presente na tabela de perfil corresponde. Atualmente autor ou aluno.
     */
    public function actionEditarApresentacao() {
        $user = $this->getUsuario();

        if (isset($_POST['apresentacao'])) {
            $apresentacao = $_POST['apresentacao'];
            if ($user->atualizaApresentacao($apresentacao)) {
                Yii::app()->user->setFlash('alert-success', 'Atualizada.');
                $this->redirect($this->createUrl('dados/ver'));
            }
        }

        $this->render('editarApresentacao', array(
            'model' => $user,
        ));
    }

}

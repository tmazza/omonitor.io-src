<?php

/**
 * Description of SenhaController
 *
 * @author tiago
 */
class ContaController extends EdicaoPerfilController {

    /**
     * Edicão de senha
     */
    public function actionAlterarSenha() {
        $user = $this->getUsuario();
        $model = new AtualizarSenhaForm();

        if (isset($_POST['AtualizarSenhaForm'])) {
            $model->setUser($user);
            $model->attributes = $_POST['AtualizarSenhaForm'];
            if ($model->validate()) {
                if ($user->atualizaPassword($model->novaSenha)) {
                    Yii::log('Senha alterada. AUTOR: ' . Yii::app()->user->id . '. OLD: ' . $user->password, CLogger::LEVEL_WARNING, 'perfil.senha');
                    Yii::app()->user->setFlash(self::SUCS_FLASH, 'Senha atualizada.');
                } else {
                    Yii::log('Falaha ao alterar senha. AUTOR: ' . Yii::app()->user->id, CLogger::LEVEL_ERROR, 'perfil.senha');
                    Yii::app()->user->setFlash(self::ERRO_FLASH, 'Falha ao atualizar senha.');
                }
                $this->redirect($this->createUrl('dados/ver'));
            }
        }
        $this->render('alterarSenha', array(
            'model' => $model,
        ));
    }

}

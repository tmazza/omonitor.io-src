<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends EdicaoPerfilController {

    public function actionIndex() {
        $this->redirect($this->createUrl('dados/ver'));
    }

}

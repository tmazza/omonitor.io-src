<?php

class EdicaoPerfilModule extends CWebModule {

    public function init() {
        $this->setImport(array(
            'edicaoPerfil.components.*',
            'edicaoPerfil.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Controller Dados
            'dados.ver' => 'lerPerfil',
            'dados.editarnome' => 'atualizarPerfil',
            'dados.editarapresentacao' => 'atualizarPerfil',
            // Controller Conta
            'conta.alterarsenha' => 'atualizarPerfil',
        );
    }

}

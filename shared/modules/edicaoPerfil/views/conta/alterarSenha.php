<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar', $this->createUrl('dados/ver')),
);
?>
<h1>Alteração de senha</h1>
<div class="sh-row">
    <div class="columns medium-12">
        <p>Nova senha deve conter:
        <ul>
            <li>Entre <b>seis</b> e <b>dezesseis</b> caracteres</li>
            <li>ao menos <b>um</b> caracter <b>maiúsculo</b>.</li>
            <li>ao menos <b>um</b> caracter <b>minúsculo</b>.</li>
        </ul>
        </p>
        <br>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'alterar-senha-user',
        ));
        ?>

        <div class="sh-row">
            <div class="columns medium-2">
                <?= $form->label($model, 'senhaAtual') ?>
            </div>
            <div class="columns medium-3">
                <?php $model->senhaAtual = ''; ?>
                <?= $form->passwordField($model, 'senhaAtual') ?>
            </div>
            <div class="columns medium-7">
                <?= $form->error($model, 'senhaAtual') ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="columns medium-2">
                <?= $form->label($model, 'novaSenha') ?>
            </div>
            <div class="columns medium-3">
                <?= $form->passwordField($model, 'novaSenha') ?>
            </div>
            <div class="columns medium-7">
                <?= $form->error($model, 'novaSenha') ?>
            </div>
        </div>

        <div class="sh-row">
            <div class="columns medium-2">
                <?= $form->label($model, 'novaSenhaConfirma') ?>
            </div>
            <div class="columns medium-3">
                <?= $form->passwordField($model, 'novaSenhaConfirma') ?>
            </div>
            <div class="columns medium-7">
                <?= $form->error($model, 'novaSenhaConfirma') ?>
            </div>
        </div>
        <br>
        <div class="sh-row">
            <div class="medium-12 columns">
                <?php echo CHtml::submitButton('Salvar', array('class' => 'btn btn-success span6')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<?php
$this->menuContexto = array(
    ShCode::makeItem('Alterar nome', $this->createUrl('dados/editarNome')),
    ShCode::makeItem('Editar apresentação', $this->createUrl('dados/editarApresentacao')),
    ShCode::makeItem('Alterar senha', $this->createUrl('conta/alterarSenha')),
);
?>
<br>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $user,
    'attributes' => array(
        'username',
        'perfil.nome',
        'perfil.apresentacao:html',
    ),
));

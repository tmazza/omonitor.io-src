<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar edição de apresentação', $this->createUrl('dados/ver')),
    ShCode::makeItem('Editar nome', $this->createUrl('dados/editarNome')),
    ShCode::makeItem('Alterar senha', $this->createUrl('conta/alterarSenha')),
);
?>
<h1>Editar nome</h1>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo CHtml::beginForm(); ?>
        <div class="sh-row">
            <div class="columns medium-2">
                <?= CHtml::label('Apresentação: ', 'apresentacao-user'); ?>
            </div>
            <div class="columns medium-7">
                <?php
                $this->widget('ImperaviRedactorWidget', array(
                    'name' => 'apresentacao',
                    'value' => $model->perfil->apresentacao,
                    'options' => array(
                        'lang' => 'pt_br',
                        'plugins' => array(
                            'bufferbuttons',
                            'fullscreen',
                            'video',
                        ),
                        'replaceDivs' => false,
                        'iframe' => true,
                        'minHeight' => 300,
                        'cleanSpaces' => false,
                        'linebreaks' => true,
                        'removeEmpty' => array('strong', 'em', 'span', 'p'),
                        'buttons' => array('formatting', 'bold', 'italic', 'alignment', 'unorderedlist', 'orderedlist', 'link'),
                    ),
                    'htmlOptions' => array(
                        'id' => 'apresentacao',
                    )
                ));
                ?>
            </div>
            <div class="columns medium-3">
                <?php echo CHtml::submitButton('Atualizar', array('class' => 'btn btn-success span6')); ?>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
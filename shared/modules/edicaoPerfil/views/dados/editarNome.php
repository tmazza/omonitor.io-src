<?php
$this->menuContexto = array(
    ShCode::makeItem('Cancelar edição de nome', $this->createUrl('dados/ver')),
    ShCode::makeItem('Editar apresentação', $this->createUrl('dados/editarApresentacao')),
    ShCode::makeItem('Alterar senha', $this->createUrl('conta/alterarSenha')),
);
?>
<h1>Editar nome</h1>
<div class="sh-row">
    <div class="columns medium-12">
        <?php echo CHtml::beginForm(); ?>
        <div class="sh-row">
            <div class="columns medium-2">
                <?= CHtml::label('Nome: ', 'nome-user'); ?>
            </div>
            <div class="columns medium-7">
                <?= CHtml::textField('nome', $model->perfil->nome, array('id' => 'nome-user')); ?>
            </div>
            <div class="columns medium-3">
                <?php echo CHtml::submitButton('Atualizar', array('class' => 'btn btn-success span6')); ?>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password - sha256
 * @property string $tipo
 */
class PerfUser extends CActiveRecord {

    const TipoAutor = 'autor';
    const TipoAluno = 'aluno';

    protected function beforeSave() {
        if ($this->isNewRecord) {
            $this->password = CPasswordHelper::hashPassword($this->password);
        }
        return parent::beforeSave();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('username, password, tipo', 'required'),
            array('username, password', 'length', 'max' => 200),
            array('tipo', 'length', 'max' => 12),
            array('id, username, password, tipo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'tipo' => 'Tipo',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Incluir permissão de acesso para usuário no nodo $itemID
     * TODO: excluir árvore filha de permissões ?
     * @param type $itemID
     */
    public function addPermissao($itemID) {
        $permissao = new TaxUser();
        $permissao->user_id = $this->username;
        $permissao->tax_item_id = $itemID;
        $permissao->save();
    }

    /**
     * Retorna model referente ao perfil do usuário (aluno | autor)
     * @return type
     */
    public function getPerfil() {
        return $this;
    }

    /**
     * Atualiza nome do usuário
     * @param type $nome
     * @return boolean
     */
    public function atualizaNome($nome) {
        $this->nome = $nome;
        if ($this->validate()) {
            Yii::app()->user->setState('nome', $nome);
            return $this->update(array('nome'));
        }
        return false;
    }

    /**
     * Atualiza apresentacao do usuário
     * @param type $nome
     * @return boolean
     */
    public function atualizaApresentacao($apresentacao) {
        $this->apresentacao = $apresentacao;
        if ($this->validate()) {
            Yii::app()->user->setState('apresentacao', $apresentacao);
            return $this->update(array('apresentacao'));
        }
        return false;
    }

    public function atualizaPassword($novaSenha){
        $this->password = CPasswordHelper::hashPassword($novaSenha);
        return $this->update(array('password'));
    }
        
    
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AtualizarSenhaForm
 *
 * @author tiago
 */
class AtualizarSenhaForm extends CFormModel {

    private $_user;
    public $senhaAtual;
    public $novaSenha;
    public $novaSenhaConfirma;

    public function rules() {
        return array(
            array('senhaAtual, novaSenha, novaSenhaConfirma', 'required'),
            array('novaSenha', 'length', 'min' => 6, 'max' => 16),
            array('novaSenha', 'shared.extensions.SPasswordValidator.SPasswordValidator', 'digit' => 0,'min' => 6, 'up' => 0, 'low' => 2),
            array('novaSenha', 'compare', 'compareAttribute' => 'novaSenhaConfirma'),
            array('senhaAtual', 'validaSenhaAtual'),
        );
    }

    public function attributeLabels() {
        return array(
            'senhaAtual' => 'Senha Atual',
            'novaSenha' => 'Nova Senha',
            'novaSenhaConfirma' => 'Confirme a nova senha',
        );
    }

    public function validaSenhaAtual($attribute, $params) {
        if (!CPasswordHelper::verifyPassword($this->senhaAtual, $this->_user->password)) {
            $this->addError('senhaAtual', 'Senha atual incorreta.');
        }
    }

    public function setUser($user) {
        $this->_user = $user;
    }

}

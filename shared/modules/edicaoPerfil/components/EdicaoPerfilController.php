<?php

/**
 * Description of MonitorController
 *
 * @author tiago
 */
class EdicaoPerfilController extends EditorController {

    protected function beforeAction($action) {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.column1';
        return parent::beforeAction($action);
    }

    /**
     * Busca usuário de acordo com username da sessão
     * @param type $id
     * @return type
     */
    protected function getUsuario() {
        $user = PerfUser::model()->findByPk((int) Yii::app()->user->id);
        if (is_null($user)) {
            Yii::app()->user->setFlash('alert-error', 'Usuário não econtrado.');
            $this->redirect(ShCode::getModUrl('edicaoPerfil'));
        }
        return $user;
    }

}

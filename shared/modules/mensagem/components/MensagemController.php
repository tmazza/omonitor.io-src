<?php

/**
 * Description of ControlerAcessoController
 *
 * @author tiago
 */
class MensagemController extends EditorController {

    // Definidas em PublicacoesModule
    public $layoutMain;

    protected function beforeAction($action) {
        // Define layouts da aplicação
        $this->setLayouts();
        return parent::beforeAction($action);
    }

    /**
     * Baseado nas configurações do módulo define os layouts em uso.
     * Utiliza o layout dos módulo pai.
     */
    private function setLayouts() {
        $this->layout = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutMain;
    }

    /**
     * Busca curso do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getMensagemRecebida($id) {
        $msg = Mensagem::model()
                ->queRecebi()
                ->findByPk($id);
        if (is_null($msg)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Mensagem não econtrada.');
            $this->redirect($this->createUrl('default/index'));
        }
        return $msg;
    }
    /**
     * Busca curso do autor, caso não exista redireciona para action de erro
     * @param type $id
     * @return type
     */
    protected function getMensagemEnviei($id) {
        $msg = Mensagem::model()
                ->queEnvie()
                ->findByPk($id);
        if (is_null($msg)) {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Mensagem não econtrada.');
            $this->redirect($this->createUrl('default/index'));
        }
        return $msg;
    }

}

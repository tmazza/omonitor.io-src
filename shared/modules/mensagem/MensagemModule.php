<?php

class MensagemModule extends CWebModule {

    // Layout mais utilizado
    public $layoutMain= 'main';

    public function init() {
        $this->setImport(array(
            'mensagem.components.*',
//            'mensagem.models.*',
        ));
    }

    /**
     * Define controle de acesso para cada action, no formato controler.action => permissao
     * Asterisco para action significa que todas as actinons SEM nível de permissão atribuído
     * receberão tal permissão.
     */
    public function getControleDeAcesso() {
        return array(
            // Controller default
            'default.*' => false,
            // Conotroller Ler
            'ler.*' => false,
            // Controller enviar
            'enviar.*' => false,
        );
    }

}

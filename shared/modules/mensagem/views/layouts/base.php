<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <a href="<?= ShCode::getModUrl('mensagem', 'enviar', 'escrever') ?>" class="btn btn-primary btn-block margin-bottom">Escrever</a>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <!--<h3 class="box-title">Pastas</h3>-->
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="<?= in_array($this->action->id, array('listar', 'index')) ? 'active' : '' ?>">
                            <a href="<?= ShCode::getModUrl('mensagem', 'ler', 'listar') ?>">
                                <i class="fa fa-inbox"></i> Recebidas 
                                <?php $qtdNaoLidas = count(ShMsg::getNaoLidas()); ?>
                                <?php if ($qtdNaoLidas > 0): ?>
                                    <span class="label label-primary pull-right"><?= $qtdNaoLidas ?></span>
                                <?php endif; ?>
                            </a>
                        </li>
                        <li class="<?= in_array($this->action->id, array('enviadas', 'enviada')) ? 'active' : '' ?>"><a href="<?= ShCode::getModUrl('mensagem', 'ler', 'enviadas') ?>"><i class="fa fa-envelope-o"></i> Enviadas</a></li>
                        <!--<li class="<?//= in_array($this->action->id, array('rascunhos')) ? 'active' : '' ?>"><a href="<?= ShCode::getModUrl('mensagem', 'ler', 'rascunhos') ?>"><i class="fa fa-file-text-o"></i> Rascunhos</a></li>-->
                        <li class="<?= in_array($this->action->id, array('excluidas')) ? 'active' : '' ?>"><a href="<?= ShCode::getModUrl('mensagem', 'ler', 'excluidas') ?>"><i class="fa fa-trash-o"></i> Excluídas</a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /. box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Labels</h3>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <?php if (in_array($this->action->id, array('listar', 'index'))): ?>
                            <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i>  Não lida</a></li>
                        <?php elseif (in_array($this->action->id, array('enviadas', 'enviada'))): ?>
                            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Data envio</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-blue"></i> Data entrega</a></li>
                            <li><a href="#"><i class="fa fa-circle-o text-green"></i> Data leitura</a></li>
                            <li><span class="hint">Mantenho o mouse sob o círculo para visualizar a data</span></li>
                        <?php endif; ?>
                    </ul>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-md-9">
            <div class="box box-primary">
                <?php echo $content; ?>
            </div><!-- /. box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
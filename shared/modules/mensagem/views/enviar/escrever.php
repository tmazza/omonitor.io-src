<?php $this->beginContent('mensagem.views.layouts.base'); ?>
<?php $body = isset($body) ? $body : null; ?> 
<?php $options = isset($options) ? $options : array(); ?> 
<?php echo CHtml::beginForm($this->createUrl('enviar/escrever')); ?>
<!-- Main content -->
<div class="box-header with-border">
    <h3 class="box-title">Nova mensagem</h3>
</div><!-- /.box-header -->
<div class="box-body">
    <div class="form-group">
        <?php
        $this->widget('shared.extensions.select2.ESelect2', array(
            'name' => 'destinos',
            'data' => $users,
            'htmlOptions' => array(
                'options' => $options,
                'multiple' => 'multiple',
                'class' => 'medium-12',
                'placeholder' => 'Para: '
            ),
        ));
        ?>
    </div>
    <div class="form-group">
        <?php
        $this->widget('ImperaviRedactorWidget', array(
            'name' => 'mensagem',
            'value' => $body,
            'options' => array(
                'lang' => 'pt_br',
                'plugins' => array(
                    'bufferbuttons',
                ),
                'replaceDivs' => false,
                'minHeight' => 300,
                'removeEmpty' => array(),
                'cleanStyleOnEnter' => true,
                'linebreaks' => true,
                'pastePlainText' => true,
                'cleanOnPaste' => true,
                'focus' => true,
                'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
                'buttons' => array('formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'alignment', 'link'),
            ),
        ));
        ?>
    </div>
</div><!-- /.box-body -->
<div class="box-footer">
    <div class="form-group">
        <!--        <label>
                    <input name="replicaEmEmail" type="checkbox" class=""/>
                    Replicar mensagem por email.
                </label>-->
    </div>
</div>
<div class="box-footer">
    <div class="pull-right">
        <!--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Salvar</button>-->
        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Enviar</button>
    </div>
    <?=
    CHtml::link('<i class="fa fa-times"></i> Cancelar', $this->createUrl('ler/listar'), array(
        'class' => 'btn btn-default',
        'confirm' => 'A mensagem será perdida. Cancelar envio?',
    ));
    ?>
</div><!-- /.box-footer -->
<?php echo CHtml::endForm(); ?>
<?php $this->endContent(); ?>
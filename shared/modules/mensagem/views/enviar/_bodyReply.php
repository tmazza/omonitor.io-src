<br><br>
<div contenteditable="false">
    <hr>
    <?= $msg->mensagem; ?>
    <br><br>
    <span class="hint">
        Enviada em <?= date("d/m/Y H:i:s", $msg->dataEnvio); ?> (<?= ShMsg::tempoAteAgora($msg->dataEnvio); ?>).
    </span>
</div>
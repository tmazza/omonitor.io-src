<?php $this->beginContent('mensagem.views.layouts.base'); ?>
<div class="box-header with-border">
    <h3 class="box-title">Mensagem</h3>
    <div class="box-tools pull-right">
        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
    </div>
</div><!-- /.box-header -->
<div class="box-body no-padding">
    <div class="mailbox-read-info">
        <span class="mailbox-read-time pull-right">Enviada em: <?= date("d/m/Y H:i:s", $msg->dataEnvio); ?> (<?= ShMsg::tempoAteAgora($msg->dataEnvio); ?>)</span>
        <?php if ($msg->de == Yii::app()->user->id): ?>
            <h5>Para <b><?= $msg->destino->perfil->nome; ?></b>
                <!--para <?//= $msg->destino->perfil->nome; ?>-->
            </h5>
        <?php else: ?>
            <h5>De <b><?= $msg->origem->perfil->nome; ?></b>
                <!--para <?//= $msg->destino->perfil->nome; ?>-->
            </h5>
        <?php endif; ?>
    </div><!-- /.mailbox-read-info -->
    <hr>
    <div class="mailbox-read-message">
        <?= $msg->mensagem; ?>
    </div><!-- /.mailbox-read-message -->
</div><!-- /.box-body -->
<div class="box-footer">
    <?php if ($msg->de != Yii::app()->user->id): ?>
        <div class="pull-right">
            <?= CHtml::link("<i class='fa fa-reply'></i> Reply", $this->createUrl('enviar/reply', array('id' => $msg->id)), array('class' => 'btn btn-default')); ?>
            <!--<button class="btn btn-default"><i class="fa fa-share"></i> Forward</button>-->
        </div>
        <?= CHtml::link('<i class="fa fa-trash-o"></i> Delete', $this->createUrl('ler/excluir', array('id' => $msg->id)), array('class' => 'btn btn-default')); ?>
        <!--<button class="btn btn-default"><i class="fa fa-print"></i> Print</button>-->
    <?php endif; ?>
</div><!-- /.box-footer -->
<?php $this->endContent(); ?>
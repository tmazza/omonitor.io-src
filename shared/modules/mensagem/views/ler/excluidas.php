<?php $this->beginContent('mensagem.views.layouts.base'); ?>
<div class="box-header with-border">
    <h3 class="box-title">Excluídas</h3>
    <div class="box-tools pull-right">
        <div class="has-feedback">
            <input type="text" class="form-control input-sm" placeholder="Buscar mensagem"/>
            <span class="glyphicon glyphicon-search form-control-feedback"></span>
        </div>
    </div><!-- /.box-tools -->
</div><!-- /.box-header -->
<div class="box-body no-padding">
    <div class="mailbox-controls">
        <br><br>
    </div>
    <div class="table-responsive mailbox-messages">
        <table class="table table-hover table-striped">
            <tbody>
                <?php foreach ($msgs as $m): ?>
                    <tr>
                        <td class="mailbox-attachment">
                            
                        </td>
                        <td class="mailbox-name">
                            <?php if (isset($m->destino->perfil)): ?>
                                <a href="<?= ShCode::getModUrl('mensagem', 'ler', 'enviada', array('id' => $m->id)); ?>"><?= $m->destino->perfil->nome; ?></a>
                            <?php endif; ?>
                        </td>
                        <?php $msg = strip_tags($m->mensagem); ?>
                        <td class="mailbox-subject"><?= substr($msg, 0, 60) . (strlen($msg) > 60 ? '...' : '') ?></td>
                        <td class="mailbox-date"><div class="right"><?= ShMsg::tempoAteAgora($m->dataEnvio); ?></div></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table><!-- /.table -->
    </div><!-- /.mail-box-messages -->
</div><!-- /.box-body -->
<?php $this->endContent(); ?>
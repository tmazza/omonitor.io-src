<?php $this->beginContent('mensagem.views.layouts.base'); ?>
<div class="box-header with-border">
    <h3 class="box-title">Recebidas</h3>
    <div class="box-tools pull-right">
        <div class="has-feedback">
            <input type="text" class="form-control input-sm" placeholder="Buscar mensagem"/>
            <span class="glyphicon glyphicon-search form-control-feedback"></span>
        </div>
    </div><!-- /.box-tools -->
</div><!-- /.box-header -->
<div class="box-body no-padding">
    <div class="mailbox-controls">
        <!-- Check all button -->
<!--        <div class="btn-group">
            <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
            <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
            <button class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
        </div> /.btn-group -->
        <?= CHtml::link("<i class='fa fa-refresh'></i>", $this->createUrl('ler/listar'), array('class' => 'btn btn-default btn-sm')); ?>
        <div class="pull-right">
            1-50/200
            <div class="btn-group">
                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
            </div><!-- /.btn-group -->
        </div><!-- /.pull-right -->
        <br><br>
    </div>
    <div class="table-responsive mailbox-messages">
        <table class="table table-hover table-striped">
            <tbody>
                <?php foreach ($msgs as $m): ?>
                    <tr>
                        <td>
                            <?php if (!$m->lida): ?>
                                <a href="#"><i class="fa fa-circle-o text-blue"></i></a>
                            <?php endif; ?>
                        </td>
                        <td class="mailbox-name"><a href="<?= ShCode::getModUrl('mensagem', 'ler', 'index', array('id' => $m->id)); ?>"><?= $m->origem->perfil->nome; ?></a></td>
                        <?php $msg = strip_tags($m->mensagem); ?>
                        <td class="mailbox-subject"><?= substr($msg, 0, 60) . (strlen($msg) > 60 ? '...' : '') ?></td>
                        <td class="mailbox-date"><div class="right"><?= ShMsg::tempoAteAgora($m->dataEnvio); ?></div></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table><!-- /.table -->
    </div><!-- /.mail-box-messages -->
</div><!-- /.box-body -->
<div class="box-footer no-padding">
    <div class="mailbox-controls">
        <br>
        <div class="pull-right">
            1-50/200
            <div class="btn-group">
                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
            </div><!-- /.btn-group -->
        </div><!-- /.pull-right -->
        <br><br>
    </div>
</div>
<?php $this->endContent(); ?>
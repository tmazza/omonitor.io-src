<?php

/**
 * Description of VerController
 *
 * @author tiago
 */
class LerController extends MensagemController {

    /**
     * Mostra mensagem e atualiza para lida
     * @param type $id
     */
    public function actionIndex($id) {
        $msg = $this->getMensagemRecebida($id);
        if (!$msg->lida) {
            $msg->setAsLida();
        }
        $this->render('ler', array(
            'msg' => $msg,
        ));
    }

    /**
     * Mostra mensagem e atualiza para lida
     * @param type $id
     */
    public function actionEnviada($id) {
        $msg = $this->getMensagemEnviei($id);
        $this->render('ler', array(
            'msg' => $msg,
        ));
    }

    /**
     * Lista das as mensagens que o autor recebeu
     */
    public function actionListar() {
        $msgs = Mensagem::model()->queRecebi()->findAll(array('order' => 'dataEnvio DESC'));
        $this->render('recebidas', array(
            'msgs' => $msgs,
        ));
    }

    /**
     * Lista das as mensagens que o autor envio
     */
    public function actionEnviadas() {
        $msgs = Mensagem::model()->queEnvie()->findAll(array('order' => 'dataEnvio DESC'));
        $this->render('enviadas', array(
            'msgs' => $msgs,
        ));
    }

    public function actionExcluir($id) {
        $msg = Mensagem::model()->queRecebi()->findByPk((int) $id);
        if (!is_null($msg)) {
            if ($msg->setAsDelete()) {
                Yii::app()->user->setFlash(self::SUCS_FLASH, 'Mensagem movida para a lixeira.');
            } else {
                Yii::app()->user->setFlash(self::ERRO_FLASH, 'Não foi possível excluir mensagem.');
            }
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Mensagem não enconttrada na caixa de entrada.');
        }
        $this->redirect($this->createUrl('ler/listar'));
    }

    /**
     * Lista das as mensagens que o autor envio
     */
    public function actionExcluidas() {
        $msgs = Mensagem::model()->excluidas()->findAll(array('order' => 'dataEnvio DESC'));
        $this->render('excluidas', array(
            'msgs' => $msgs,
        ));
    }

//    /**
//     * Lista das as mensagens que o usuario salvou
//     */
//    public function actionRascunhos() {
//        $this->render('rascunhos', array(
//        ));
//    }
}

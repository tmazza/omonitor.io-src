<?php

/**
 * Description of EnviarController
 *
 * @author tiago
 */
class EnviarController extends MensagemController {

    public function actionEscrever() {
        if (isset($_POST['destinos']) && isset($_POST['mensagem'])) {
            foreach ($_POST['destinos'] as $d) {
                // TODO: validar se destino está entre os usuários "amigos"
                if (ShMsg::enviar($d, $_POST['mensagem'])) {
                    Yii::app()->user->setFlash(self::SUCS_FLASH, 'Mensagem enviada!');
                } else {
                    Yii::app()->user->setFlash(self::ERRO_FLASH, 'Mensagem não enviada!');
                }
            }
            if (isset($_POST['replicaEmEmail'])) {
                ShEmail::send($_POST['destinos'], 'Mensagem', $this->orgID, 'noreply@noreply.com', $_POST['mensagem']);
            }
        }

        $this->render('escrever', array(
            'users' => $this->getUsers(),
        ));
    }

    public function actionReply($id) {
        $mensagem = Mensagem::model()->queRecebi()->findByPk((int) $id);
        if (!is_null($mensagem)) {
            $this->render('escrever', array(
                'users' => $this->getUsers(),
                'options' => array($mensagem->de => array('selected' => true)),
                'body' => $this->renderPartial('_bodyReply', array('msg' => $mensagem), true),
            ));
        } else {
            Yii::app()->user->setFlash(self::ERRO_FLASH, 'Mensagem não encontrada.');
            $this->redirect($this->createUrl('ler/listar'));
        }
    }

    public function getUsers() {
        // TODO: não buscar todos! somente aqueles relacionados ao usuário
        return CHtml::listData(User::model()->findAll(), 'id', 'perfil.nome');
    }

}

<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends MensagemController {

    public function actionIndex() {
        $this->redirect($this->createUrl('ler/listar'));
    }
    
}

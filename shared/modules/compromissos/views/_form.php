<?php
/* @var $this CompromissoController */
/* @var $model Compromisso */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'compromisso-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'titulo'); ?>
		<?php echo $form->textField($model,'titulo',array('size'=>60,'maxlength'=>150,"class"=>"form-control")); ?>
		<?php echo $form->error($model,'titulo'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'descricao'); ?>
		<?php echo $form->textArea($model,'descricao',array('rows'=>6, 'cols'=>50,"class"=>"form-control")); ?>
		<?php echo $form->error($model,'descricao'); ?>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<?php echo $form->labelEx($model,'data_inicio'); ?>
				<?php echo $form->textField($model,'data_inicio',array('size'=>10,'maxlength'=>10,"class"=>"datepicker date-mask form-control")); ?>
				<?php echo $form->error($model,'data_inicio'); ?>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
					<?php echo $form->labelEx($model,'data_fim'); ?>
				<?php echo $form->textField($model,'data_fim',array('size'=>10,'maxlength'=>10,"class"=>"datepicker date-mask form-control")); ?>
				<?php echo $form->error($model,'data_fim'); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<?php echo $form->labelEx($model,'horario_inicio'); ?>
				<?php echo $form->textField($model,'horario_inicio',array('size'=>10,'maxlength'=>10,"class"=>"form-control time-mask")); ?>
				<?php echo $form->error($model,'horario_inicio'); ?>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<?php echo $form->labelEx($model,'horario_fim'); ?>
				<?php echo $form->textField($model,'horario_fim',array('size'=>10,'maxlength'=>10,"class"=>"form-control time-mask")); ?>
				<?php echo $form->error($model,'horario_fim'); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Criar' : 'Editar',array("class"=>"btn btn-primary pull-right")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
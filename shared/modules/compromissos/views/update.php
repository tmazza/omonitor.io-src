<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <h2>Compromisso: <?php echo $model->titulo; ?></h2>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>
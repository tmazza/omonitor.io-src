<?php  $baseUrl = Yii::app()->baseUrl."/js";?>
<link href='<?=$baseUrl?>/fullcalendar-2.3.2/fullcalendar.css' rel='stylesheet' />
<link href='<?=$baseUrl?>/fullcalendar-2.3.2/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?=$baseUrl?>/fullcalendar-2.3.2/lib/moment.min.js'></script>
<script src='<?=$baseUrl?>/fullcalendar-2.3.2/fullcalendar.min.js'></script>
<script src='<?=$baseUrl?>/fullcalendar-2.3.2/lang/pt-br.js'></script>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?=QQ::getFlashMessages();?>

        <h2>
            Meus Compromissos.
            <span>
                 <?php echo CHtml::link('<i class="fa fa-thumb-tack"></i> Novo Compromisso',array('compromisso/create'),array("class"=>"btn btn-primary pull-right"));?>
            </span>
        </h2>

        <p>*Lembrando que o portal te envia um email no dia do seu compromisso ;)</p>


        <div style="background-color: whitesmoke">
            <?php
                $this->widget('application.extensions.fullcalendar.EFullCalendarHeart', array(
                //'themeCssFile'=>'cupertino/jquery-ui.min.css',
                'options'=>array(
                    'lang'=>'pt-br',
                    'header'=>array(
                        'left'=>'prev,next,today',
                        'center'=>'title',
                        'right'=>'month,agendaWeek,agendaDay',
                    ),
                    'events'=>$this->createUrl('compromisso/GetCompromissos'), // URL to get event
                    'eventClick'=> 'js:function(calEvent, jsEvent, view) {
                        $("#compromisso_pessoa_titulo").html(calEvent.title);
                        $("#compromisso_pessoa_detalhe").load("'.Yii::app()->createUrl("compromisso/DetalheCompromisso").'?id="+calEvent.id+"");
                        $("#compromisso_pessoa").modal("show");
                        //alert(calEvent.title);
                    }',
                )));
            ?>
        </div>

   </div>
</div>


<!-- Modal -->
<div class="modal fade" id="compromisso_pessoa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="compromisso_pessoa_titulo"></h3>
            </div>

            <div id="compromisso_pessoa_detalhe"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>
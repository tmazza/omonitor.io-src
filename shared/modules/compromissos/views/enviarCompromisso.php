<div class="row">
    <div class="col-md-6 col-md-offset-3">

        <h2>Enviar Compromisso: <?=$model->titulo;?></h2>

        <form action="" method="post">
                <div class="form-group">
                    <label>Enviar para:</label>
                    <input type="text" name="EnviarCompromisso[email]" placeholder="email" class="form-control">
                </div>

                <div class="form-group">
                    <label>Mensagem Adicional</label>
                     <textarea rows="4" cols="50" name="EnviarCompromisso[mensagem]" class="form-control" placeholder="mensagem adicional"></textarea>
                </div>

            <input type="submit" value="Enviar" class="btn btn-primary pull-right">
        </form>
    </div>
</div>
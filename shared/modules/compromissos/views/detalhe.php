<div class="col-md-10 col-md-offset-1">
    <table class="table">
        <tbody>
        <tr><td>Descricao</td><td><?=$model->descricao?></td></tr>
        <tr><td>Data Início</td><td><?=$model->data_inicio?></td></tr>
        <tr><td>Data Fim</td><td><?=$model->data_fim?></td></tr>
        <tr><td>Horario Inicio</td><td><?=$model->horario_inicio?></td></tr>
        <tr><td>Horario Fim</td><td><?=$model->horario_fim?></td></tr>
        <tr><td>Criado em:</td><td><?=$model->dt_cadastro?></td></tr>
        <tr><td>Avisado pelo sistema em:</td><td><?=$model->dt_avisado?></td></tr>
        <tr>
            <td>Ações:</td>
            <td>
                <?=CHtml::link('Enviar por email',Yii::app()->createUrl('/'.QQ::getModuleName().'/compromisso/EnviarCompromisso',array("id"=>$model->compromisso_id)),array("class"=>"btn btn-xs btn-info"))?>
                <?=CHtml::link('Editar',Yii::app()->createUrl('/'.QQ::getModuleName().'/compromisso/update',array("id"=>$model->compromisso_id)),array("class"=>"btn btn-xs btn-primary"))?>
                <?=CHtml::link('Excluir',Yii::app()->getBaseUrl(true).Yii::app()->createUrl('/'.QQ::getModuleName().'/compromisso/delete',array("id"=>$model->compromisso_id)),array("class"=>"btn btn-xs btn-danger"))?>
            </td>
        </tr>
        </tbody>
    </table>
</div>



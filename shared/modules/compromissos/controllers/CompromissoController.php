<?php

class CompromissoController extends AgendaController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				//'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	//FullCalendar
	public function actionAgenda(){
		//$this->layout = "redactor";
		$this->render("agenda");
	}

	//lista os compromissos na tela inicial
	public function actionGetCompromissos(){
		$items = array();
		$compromissos=Compromisso::model()->findAllByAttributes(array("pessoa_id"=>QQ::getPessoaId()));

		foreach ($compromissos as $c) {
			$start = QQ::montaDataHoraUCT($c->data_inicio,$c->horario_inicio);
			$end = QQ::montaDataHoraUCT($c->data_fim,$c->horario_fim);

			$items[]=array(
				'id'=>$c->compromisso_id,
				'title'=>$c->titulo,
				'start'=>$start,
				'end'=>$end,
				//'color'=>'#CC0000',
				//'allDay'=>true,
				//'url'=>'http://google.com'
			);

			$start = null;
			$end = null;
		}
		echo CJSON::encode($items);
		Yii::app()->end();
	}

	//ao clicar sobre um compromisso mostra os detalhes
	public function actionDetalheCompromisso($id){
		$compromisso_id = intval($id);
		$model=Compromisso::model()->findByPk(array("compromisso_id"=>$compromisso_id));
		echo $this->renderPartial("detalhe",array("model"=>$model),true,true);
		Yii::app()->end();
	}

	public function actionEnviarCompromisso($id){
		$compromisso_id = intval($id);
		$model = Compromisso::model()->findByPk($compromisso_id);

		if(isset($_POST["EnviarCompromisso"])){
			$email = $_POST["EnviarCompromisso"]['email'];
			$assunto = "Lembrete de Compromisso.";

			$mensagem = $_POST["EnviarCompromisso"]['mensagem'];
			$mensagem .= "<br><br>".$this->renderPartial("detalheEmail",array("model"=>$model),true,true);
			$mensagem = $this->widget('application.widgets.EmailTemplate', array('msg'=>$mensagem),true);

			Email::enviar($email,$assunto,$mensagem);
			QQ::getFlashMessages("Lembrete enviado por email!");
			$this->redirect(array("compromisso/agenda"));
		}

		$this->render("enviarCompromisso",array("model"=>$model));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Compromisso;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Compromisso']))
		{
			$model->attributes=$_POST['Compromisso'];
			$model->pessoa_id = QQ::getPessoaId();
			$model->avisado = 0;
			$model->dt_cadastro = new CDbExpression('NOW()');

			if($model->save())
				$this->redirect(array('agenda'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Compromisso']))
		{
			$model->attributes=$_POST['Compromisso'];
			if($model->save())
				$this->redirect(array('compromisso/agenda'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->redirect(array('compromisso/agenda'));
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
		//	$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Compromisso');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Compromisso('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Compromisso']))
			$model->attributes=$_GET['Compromisso'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Compromisso the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Compromisso::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Compromisso $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='compromisso-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

/**
 * This is the model class for table "compromisso".
 *
 * The followings are the available columns in table 'compromisso':
 * @property integer $compromisso_id
 * @property integer $pessoa_id
 * @property string $titulo
 * @property string $descricao
 * @property string $data_inicio
 * @property string $data_fim
 * @property string $horario_inicio
 * @property string $horario_fim
 * @property integer $avisado
 * @property string $dt_avisado
 * @property string $dt_cadastro
 */
class Compromisso extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compromisso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pessoa_id, titulo, descricao, data_inicio, dt_cadastro', 'required'),
			array('pessoa_id, avisado', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>150),
			array('data_inicio, data_fim, horario_inicio, horario_fim', 'length', 'max'=>10),
			array('dt_avisado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('compromisso_id, pessoa_id, titulo, descricao, data_inicio, data_fim, horario_inicio, horario_fim, avisado, dt_avisado, dt_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'compromisso_id' => 'Compromisso',
			'pessoa_id' => 'Pessoa',
			'titulo' => 'Titulo',
			'descricao' => 'Descricao',
			'data_inicio' => 'Data Inicio',
			'data_fim' => 'Data Fim',
			'horario_inicio' => 'Horario Inicio',
			'horario_fim' => 'Horario Fim',
			'avisado' => 'Avisado',
			'dt_avisado' => 'Dt Avisado',
			'dt_cadastro' => 'Dt Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('compromisso_id',$this->compromisso_id);
		$criteria->compare('pessoa_id',$this->pessoa_id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('data_inicio',$this->data_inicio,true);
		$criteria->compare('data_fim',$this->data_fim,true);
		$criteria->compare('horario_inicio',$this->horario_inicio,true);
		$criteria->compare('horario_fim',$this->horario_fim,true);
		$criteria->compare('avisado',$this->avisado);
		$criteria->compare('dt_avisado',$this->dt_avisado,true);
		$criteria->compare('dt_cadastro',$this->dt_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Compromisso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

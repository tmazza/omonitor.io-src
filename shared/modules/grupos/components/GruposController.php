<?php

/**
 * Description of MonitorController
 *
 * @author tiago
 */
class GruposController extends EditorController {

    // Definidas em PublicacoesModule
    public $layoutComMenu;
    public $layoutSemMenu;

    protected function beforeAction($action) {
        // Define layouts da aplicação
        $this->setLayouts();
        return parent::beforeAction($action);
    }

    /**
     * Baseado nas configurações do módulo define os layouts em uso.
     * Utiliza o layout dos módulo pai.
     */
    private function setLayouts() {
        $this->layoutComMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutComMenu;
        $this->layoutSemMenu = $this->module->parentModule->getName() . '.views.layouts.' . $this->module->layoutSemMenu;
        $this->layout = $this->layoutSemMenu;
    }

}


/**
Tabelas criadas:
grupo: configuração do grupo
grupo_mensagem: mensagem postadas no grupo
grupo_user: membros do grupo.

Observações:
Qualquer usuário pode criar um grupo
Tabela grupo possui a coluna formaDeAcesso. Que define se qualquer um pode entrar no grupo ou se terá solicitação de entrada para posterior aprovação.
Table grupo_user possui o campo status que defini o estado da solicitação do usuário. Somente usuário com status "aprovada" podem acessar/postar no grupo.
Existe somente um "dono" do grupo por enquanto(manter). É o cara que criou o grupo.Só ele pode editar o titulo, a descrição, a forma de acesso, etc.
Todas as relações que marcam um registro como de alguem, como user_id, owner, etc, sempre se refere (apesar do nome) a tabela usuário.

Dica:
O layout 'timeline' (UI elements -> timeline) no modelo do layout (anexo), parece uma boa para a visualização de um forúm.
Dentro do módulo /meuEspaco  (módulo "pai") o usuário sempre está logado, seu id está em Yii::app()->user->id, se refere user(username).
 */
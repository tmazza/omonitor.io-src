<?php

/**
 * Description of VerController
 *
 * @author tiago
 */
class VerController extends GruposController {

    public function actionMembrosGrupo($id){
        $grupo_id = $id;

        $membros = GrupoUser::model()->findAllByAttributes(array("grupo_id"=>$id));
        $qtd_membros = count($membros);

        $usuarios = array();
        foreach($membros as $m){
            $usuario = null;
            $usuario['membro'] = User::model()->findByPk($m->user_id);
            $usuario['membro_desde'] = $m->dt_registro;
            $usuarios[] = $usuario;
        }

        $this->render('membros_grupo',array("membros"=>$membros,"qtd_membros"=>$qtd_membros,"grupo_id"=>$grupo_id,"usuarios"=>$usuarios));
    }

    public function actionPostsGrupo($id){
        $grupo = Grupo::model()->findByPk($id);
        $posts = GrupoMensagem::model()->findAllByAttributes(array("grupo_id"=>$id),array("order"=>"id DESC"));
        $this->render('posts_grupo',array("grupo"=>$grupo,"posts"=>$posts));
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 17/08/15
 * Time: 23:45
 */

class PostController extends GruposController {

    public function actionNovoPost(){

        if(isset($_POST['Post'])){
            $NovoPost = new GrupoMensagem();
            $NovoPost->mensagem = $_POST['Post']['mensagem'];
            $NovoPost->grupo_id = $_POST['Post']['grupo_id'];
            $NovoPost->user_id = Yii::app()->user->id;
            $NovoPost->dt_envio = new CDbExpression("NOW()");

            if($NovoPost->save()){
                $this->redirect($this->createUrl('ver/PostsGrupo',array('id'=>$_POST['Post']['grupo_id'])));
            }
        }
    }



}

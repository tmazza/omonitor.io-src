<?php

/**
 * Description of DefaultController
 *
 * @author tiago
 */
class DefaultController extends GruposController {

    public function actionIndex() {
        $grupos = Grupo::model()->findAll();
        $this->render('listar_grupos',array('grupos'=>$grupos));
    }

    public function actionNovoGrupo(){
        $grupo = new Grupo();

        if(isset($_POST['Grupo'])){
            $grupo->attributes=$_POST['Grupo'];

            if(Grupo::salvarNovoGrupo($grupo)){
                $this->redirect($this->createUrl('default/index'));
            }
        }

        $this->render('novo_grupo',array('grupo'=>$grupo));
    }

    public function actionMeusGrupos(){
        $grupos = Grupo::model()->findAllByAttributes(array("user_id"=>Yii::app()->user->id));
        $this->render('listar_grupos',array('grupos'=>$grupos));
    }

    //@TODO crud dos grupos
    public function actionEditarGrupo(){}
    public function actionExcluirGrupo(){}

}

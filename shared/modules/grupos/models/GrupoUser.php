<?php

/**
 * This is the model class for table "grupo_user".
 *
 * The followings are the available columns in table 'grupo_user':
 * @property integer $grupo_id
 * @property integer $user_id
 * @property integer $status
 * @property string $dt_registro
 */
class GrupoUser extends AplicationActiveRecord
{
	const StatusAguardando = 1;
	const StatusAprovado = 2;
	const StatusRejeitado = 3;

	public function tableName()
	{
		return 'grupo_user';
	}

        public static function getTableName()
	{
		return 'grupo_user';
	}

        
	public function rules()
	{
		return array(
			array('grupo_id, user_id, dt_registro', 'required'),
			array('grupo_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('grupo_id, user_id, status, dt_registro', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'grupo_id' => 'Grupo',
			'user_id' => 'User',
			'status' => 'Status',
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('grupo_id', $this->grupo_id);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public static function getStatusDeSolicitacao(){
		return array(
			self::StatusAguardando => 'Aguardando',
			self::StatusAprovado => 'Aprovada',
			self::StatusRejeitado => 'Rejeitada',
		);
	}

	public static function isMembroDoGrupo($grupo_id){
		$is_membro = false;

		$user_id = Yii::app()->user->id;
		$criteria = new CDbCriteria;
		$criteria->condition = " user_id=$user_id AND grupo_id=$grupo_id ";

		$membro = GrupoUser::model()->findByAttributes($criteria);
		if(count($membro) > 0){
			$is_membro = true;
		}

		return $is_membro;
	}

	public static function isMembroAprovado(){
		$aprovado = false;

		$user_id = Yii::app()->user->id;
		$criteria = new CDbCriteria;
		$criteria->condition = "user_id=$user_id AND status=".GrupoUser::StatusAprovado;

		$membro_aprovado = GrupoUser::model()->findByAttributes($criteria);
		if(count($membro_aprovado) > 0){
			$aprovado = true;
		}

		return $aprovado;
	}
}

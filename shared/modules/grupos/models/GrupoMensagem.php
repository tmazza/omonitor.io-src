<?php

/**
 * This is the model class for table "grupo_mensagem".
 *
 * The followings are the available columns in table 'grupo_mensagem':
 * @property integer $id
 * @property string $mensagem
 * @property string $dt_envio
 * @property integer $grupo_id
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property Grupo $grupo
 * @property User $user
 */
class GrupoMensagem extends CActiveRecord
{
	public function tableName()
	{
		return 'grupo_mensagem';
	}

	public static function getTableName()
	{
		return 'grupo_mensagem';
	}

	public function rules()
	{
		return array(
			array('mensagem, dt_envio, grupo_id, user_id', 'required'),
			array('grupo_id, user_id', 'numerical', 'integerOnly'=>true),
			array('id, mensagem, dt_envio, grupo_id, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'grupo' => array(self::BELONGS_TO, 'Grupo', 'grupo_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mensagem' => 'Mensagem',
			'dt_envio' => 'Data de Envio',
			'grupo_id' => 'Grupo',
			'user_id' => 'User',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mensagem',$this->mensagem,true);
		$criteria->compare('dt_envio',$this->dt_envio,true);
		$criteria->compare('grupo_id',$this->grupo_id);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "grupo".
 *
 * The followings are the available columns in table 'grupo':
 * @property integer $id
 * @property string $titulo
 * @property string $descricao
 * @property integer $forrmaDeAcesso
 * @property integer $user_id
 * @property string $dt_registro
 *
 * The followings are the available model relations:
 * @property User $user
 * @property GrupoMensagem[] $grupoMensagems
 * @property User[] $users
 */
class Grupo extends CActiveRecord
{
	const AcessoLiberado = 1;
	const AcessoPorSolicitacao = 2;

	public function tableName()
	{
		return 'grupo';
	}

	public static function getTableName()
	{
		return 'grupo';
	}

        
	public function rules()
	{
		return array(
			array('titulo, user_id, dt_registro', 'required'),
			array('forrmaDeAcesso, user_id', 'numerical', 'integerOnly'=>true),
			array('titulo', 'length', 'max'=>100),
			array('descricao', 'safe'),
			array('id, titulo, descricao, forrmaDeAcesso, user_id, dt_registro', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'dono' => array(self::BELONGS_TO, 'User', 'user_id'),
			'mensagens' => array(self::HAS_MANY, 'GrupoMensagem', 'grupo_id', 'order' => 'dataEnvio DESC'),
			'usuarios' => array(self::MANY_MANY, 'User', GrupoUser::getTableName() . '(grupo_id, user_id)'),
		);
	}

	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'titulo' => 'Titulo',
			'descricao' => 'Descricao',
			'forrmaDeAcesso' => 'Forrma De Acesso',
			'user_id' => 'Owner',
			'dt_registro' => 'Data Criacao',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('titulo',$this->titulo,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('forrmaDeAcesso',$this->forrmaDeAcesso);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('dt_registro',$this->dt_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getFormasDeAcesso() {
		return array(
			self::AcessoLiberado => 'Acesso livre',
			self::AcessoPorSolicitacao => 'Acesso por solicitação',
		);
	}

	public static function salvarNovoGrupo(Grupo $grupo){
		//inicia transacao
		$transaction = Yii::app()->db->beginTransaction();

		try{
			$grupo->user_id = Yii::app()->user->id;
			$grupo->forrmaDeAcesso = Grupo::AcessoLiberado;
			$grupo->dt_registro = new CDbExpression('NOW()');

			if(!$grupo->save()){
				throw new Exception;
			}

			$grupo_user = new GrupoUser;
			$grupo_user->grupo_id =  Yii::app()->db->getLastInsertID();
			$grupo_user->user_id = Yii::app()->user->id;
			$grupo_user->status = GrupoUser::StatusAprovado;

			if(!$grupo_user->save()){
				throw new Exception;
			}
			//apos criar grupo e incluir usuario criador como membro finalizar
			//@TODO metodos para distiguir membro owner ou *admin

			$transaction->commit();
			return true;

		}catch(Exception $e){
			$transaction->rollback();
			return false;
		}

	}

}

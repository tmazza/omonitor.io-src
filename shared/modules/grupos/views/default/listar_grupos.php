<?php
    $this->pageTitle = 'Grupos';
    $this->breadcrumbs += array(
    'Grupos'
    );

    $this->menuContexto = array(
        ShCode::makeItem('Grupos', $this->createUrl('default/index')),
        ShCode::makeItem('Meus Grupos', $this->createUrl('default/MeusGrupos')),
        ShCode::makeItem('Criar Grupo', $this->createUrl('default/NovoGrupo')),
    );

?>

<div class="box">
    <!--<div class="box-header">
        <h3 class="box-title">&nbsp;</h3>
        <div class="box-tools">
            <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
            </ul>
        </div>
    </div>--><!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table">
            <tbody>
            <tr>
                <th style="width: 10px">#</th>
                <th>Grupo</th>
                <th>Descrição</th>
                <th style="width: 40px">Posts</th>
            </tr>

            <?php $qtd = 0;?>
            <?php  foreach($grupos as $grupo):?>
            <?php
                $posts = null;
                $qtd_posts = null;

                $qtd++;
                $posts = GrupoMensagem::model()->findAllByAttributes(array("grupo_id"=>$grupo->id));
                $qtd_posts = count($posts);
            ?>
            <?php echo "<tr>
                    <td>$qtd</td>
                    <td>".CHtml::link($grupo->titulo, Yii::app()->controller->createUrl("ver/PostsGrupo", array("id" => $grupo->id)))."</td>
                    <td>".$grupo->descricao."</td>
                    <td><span class='badge bg-red'>$qtd_posts</span></td>
                </tr>";?>
            <?php endforeach;?>

            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>
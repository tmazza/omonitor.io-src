<?php
    $this->pageTitle = 'Novo Grupo';
    $this->breadcrumbs += array(
        'Grupos'
    );

    $this->menuContexto = array(
        ShCode::makeItem('Grupos', $this->createUrl('default/index')),
        ShCode::makeItem('Meus Grupos', $this->createUrl('default/MeusGrupos')),
        ShCode::makeItem('Criar Grupo', $this->createUrl('default/NovoGrupo')),
    );

?>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'grupo-form',
    'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($grupo); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">&nbsp;</h3>
    </div><!-- /.box-header -->
    <!-- form start -->

        <div class="box-body">


            <div class="form-group">
                <?php echo $form->labelEx($grupo,'titulo'); ?>
                <?php echo $form->textField($grupo,'titulo',array('class'=>'form-control','placeholder'=>'titulo')); ?>
                <?php echo $form->error($grupo,'titulo'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($grupo,'descricao'); ?>
                <?php echo $form->textField($grupo,'descricao',array('class'=>'form-control','placeholder'=>'descrição')); ?>
                <?php echo $form->error($grupo,'descricao'); ?>
            </div>

        </div><!-- /.box-body -->

        <div class="box-footer">
            <?php echo CHtml::submitButton('Salvar',array("class"=>"btn btn-success")); ?>
        </div>
</div>

<?php $this->endWidget(); ?>

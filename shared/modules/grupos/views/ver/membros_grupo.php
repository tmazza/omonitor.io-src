<?php
$this->pageTitle = 'Membros do Grupo';
$this->breadcrumbs += array(
    'Grupos'
);

$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
    ShCode::makeItem('Membros', $this->createUrl('ver/MembrosGrupo',array("id"=>$grupo_id))),
);?>

    <!-- USERS LIST -->
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">&nbsp;</h3>
            <div class="box-tools pull-right">
                <span class="label label-danger"><?=$qtd_membros;?> Membros</span>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="users-list clearfix">

               <?php foreach($usuarios as $u):?>
                    <li>
                        <img class="user-image" width="60" src="/monitor/assets/9478dcf0/dist/img/user1-128x128.jpg" alt="<?=$u['membro']->nome?>">
                        <a class="users-list-name" href="#"><?=$u['membro']->nome?></a>
                        <span class="users-list-date"><?=$u['membro_desde']?></span>
                    </li>
                <?php endforeach;?>

            </ul><!-- /.users-list -->
        </div><!-- /.box-body -->
        <div class="box-footer text-center">
            <a href="javascript:" class="uppercase">View All Users</a>
        </div><!-- /.box-footer -->
    </div><!--/.box -->

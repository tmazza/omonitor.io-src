<?php
$this->pageTitle = $grupo->titulo;
$this->breadcrumbs += array(
    "Grupos"
);

$this->menuContexto = array(
    ShCode::makeItem('Voltar', $this->createUrl('default/index')),
    ShCode::makeItem('Membros', $this->createUrl('ver/MembrosGrupo',array("id"=>$grupo->id))),
);?>

<div class="row">
    <div class="col-md-12">
        <?php //if(GrupoUser::isMembroDoGrupo($grupo->id) && GrupoUser::isMembroAprovado()): ?>
            <form action="<?=$this->createUrl("post/NovoPost")?>" method="post">
                <div class="col-md-6">
                    <input type="hidden" name="Post[grupo_id]" value="<?=$grupo->id?>">

                    <textarea placeholder="Escreva algo..."  id="mensagem" name="Post[mensagem]"></textarea>
                    <?php

                    $this->widget('ImperaviRedactorWidget', array(
                        'selector' => '#mensagem',
                        'options' => array(
                            'lang' => 'pt_br',
                            'plugins' => array(
                                'bufferbuttons',
                            ),
                            'replaceDivs' => false,
                            'minHeight' => 200,
                            'removeEmpty' => array(),
                            'cleanStyleOnEnter' => true,
                            'linebreaks' => true,
                            'pastePlainText' => true,
                            'cleanOnPaste' => true,
                            'focus' => true,
                            'pasteCallback' => 'js:function(html) { /*console.log(html); console.log($("<span>"+html+"</span>").text());*/ return $("<span>"+html+"</span>").text(); }',
                            'buttons' => array('formatting', 'bold', 'italic', 'unorderedlist', 'orderedlist', 'alignment', 'link'),

                            'fileUpload'=>Yii::app()->createUrl('site/uploadImageFeedback'),
                            'imageUpload' => Yii::app()->createUrl('site/uploadImageFeedback'),
                            'imageUploadErrorCallback' => new CJavaScriptExpression(
                                'function(obj,json) { alert(json.error); }'
                            ),
                            'clipboardUploadUrl' => Yii::app()->createUrl('site/uploadImageFeedback'),
                        ),
                    ));
                    ?>

                    <input class="btn btn-info pull-right" type="submit" value="Publicar">

                </div>
            </form>
        <?php //endif; ?>
    </div>


    <?php echo $this->renderPartial('time_line',array('posts'=>$posts)); ?>

</div>
<br>

<!--
post based layout
<div class="row">
    <div class="col-md-12">
        <div class="post">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" width="60" src="/monitor/assets/9478dcf0/dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#">Jonathan Burke Jr.</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                <span class="description">Shared publicly - 7:30 PM today</span>
            </div>
            <p>
                Lorem ipsum represents a long-held tradition for designers,
                typographers and the like. Some people hate it and argue for
                its demise, but others ignore the hate as they create awesome
                tools to help create filler text for everyone from bacon lovers
                to Charlie Sheen fans.
            </p>
            <ul class="list-inline">
                <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>
                <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (5)</a></li>
            </ul>

            <input class="form-control input-sm" placeholder="Type a comment" type="text">
        </div>
    </div>
</div>
-->
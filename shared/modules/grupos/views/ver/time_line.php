<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a aria-expanded="true" href="#activity" data-toggle="tab">Posts do Grupo</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="activity">

                <?php foreach($posts as $p): ?>
                <!-- Post -->
                <div class="post">
                    <div class="user-block">
                        <img class="img-circle img-bordered-sm" width="60" src="/monitor/assets/9478dcf0/dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#"><?php echo $p->user->nome; ?></a>
                        </span>
                        <span class="description">Publicado em - <?=$p->dt_envio;?></span>
                    </div><!-- /.user-block -->
                    <p>
                        <?php echo $p->mensagem; ?>
                    </p>
                    <ul class="list-inline">
                        <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>
                        <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (5)</a></li>
                    </ul>
                    <input class="form-control input-sm" placeholder="Type a comment" type="text">
                    <br>
                </div>
                <!-- /.post -->
                <?php endforeach; ?>

        </div><!-- /.tab-content -->
    </div><!-- /.nav-tabs-custom -->
</div>


<!--
<div class="row">
    <div class="col-md-12">
        <?php foreach($posts as $p): ?>
            <div class="post">
                <div class="user-block">
                    <img class="img-circle img-bordered-sm" width="60" src="/monitor/assets/9478dcf0/dist/img/user1-128x128.jpg" alt="user image">
                            <span class="username">
                              <a href="#"><?=$p->user->nome;?>.</a>
                            </span>
                    <span class="description">Publicado em - <?=$p->dt_envio;?></span>
                </div>
                <div class="user-block">
                    <p>
                        <?=$p->mensagem;?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>


--->
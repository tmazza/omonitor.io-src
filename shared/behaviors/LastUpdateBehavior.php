<?php

/**
 * Description of ExclusaoLogicaBehavior
 *
 * @author tiago
 */
class LastUpdateBehavior extends CActiveRecordBehavior {

    public $attrName = 'lastUpdate';

    /**
     * Conteúdo não pode ter sido excluído
     * @param type $event
     */
    public function beforeSave($event) {
        $this->owner->setAttribute($this->attrName, time());
        parent::beforeSave($event);
    }

}

<?php

/**
 * Description of ExclusaoLogicaBehavior
 *
 * @author tiago
 */
class ExclusaoLogicaBehavior extends CActiveRecordBehavior {

    public $attrName = 'excluido';

    /**
     * Conteúdo não pode ter sido excluído
     * @param type $event
     */
    public function beforeFind($event) {
        $this->owner->getDbCriteria()->mergeWith(array(
            'condition' => $this->owner->getTableAlias() . '.' . $this->attrName . ' = 0',
        ));
        parent::beforeFind($event);
    }

}

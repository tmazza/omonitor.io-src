<?php

/**
 * Description of ExclusaoLogicaBehavior
 *
 * @author tiago
 */
class RestrigeTipoBehavior extends CActiveRecordBehavior {

    public $attrName = 'tipo';
    public $tipos = null;

    /**
     * Conteúdo não pode ter sido excluído
     * @param type $event
     */
    public function beforeFind($event) {
        $this->owner->getDbCriteria()->mergeWith(array(
            'condition' => $this->owner->getTableAlias() . '.' . $this->attrName . ' IN (' . implode(',', $this->tipos) . ')',
        ));
        parent::beforeFind($event);
    }

}

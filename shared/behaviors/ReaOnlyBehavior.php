<?php

/**
 * Description of ExclusaoLogicaBehavior
 *
 * @author tiago
 */
class ReaOnlyBehavior extends CActiveRecordBehavior {

    /**
     * Conteúdo não pode ter sido excluído
     * @param type $event
     */
    public function beforeSave($event) {
        return false;
    }

}

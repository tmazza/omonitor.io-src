<?php

/**
 * Não permite que dados sejam criados, excluídos ou atualizados.
 * TODO: garantir condições descritas :), mais testes!
 *
 * @author tiago
 */
class AplicationActiveRecord extends CActiveRecord {

    public function behaviors() {
        return array(
            'somenteLeitura' => array(
                'class' => 'ReaOnlyBehavior',
            ),
        );
    }

}

<?php

/**
 * É o controller pai de todos os controller dos modulos de recursos
 *
 * @author tiago
 */
class EditorController extends MainController {

    protected $user = null;
    protected $assetsPath = null;

    /**
     * Permissões de acesso em nodo, topico, questionario e arquivo
     * @var type
     */
    private $permissoes = null;

    /**
     * Menu de aplicação
     * Agrupado por categorias
     * @var type array
     */
    public $menu = array();

    /**
     * Menu de contexto
     * @var type array
     */
    public $menuContexto = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function actions() {
        return array(
            'ParseInstrucao' => 'shared.actions.ParseInstrucao',
            'AutocompleteInstrucao' => 'shared.actions.AutocompleteInstrucao',
        );
    }

    protected function beforeAction($action) {
        // Controla acesso ao conteúdo
        $this->verificaPermissaoEmOperacao();
        // Carrega menu de módulos ativos
        $this->loadMenu();
        // Publica arquivos compartilhados
        $this->publicaArquivosCompartilhados();
        // Define breadcrumbs base
        $this->setBreadcrumbsRaiz();

        return parent::beforeAction($action);
    }

    public function actionLoadMathSolver() {
        echo $this->renderPartial('shared.views.Arquimedes.AjaxLoad', array(), true, true);
    }

    /**
     * Controle de acesso a action.
     * Permissão de acesso são vínculadas ao usuário na tabela authassgn.
     * Utilizado CDbAuthManager.
     * Se a action não possuir permissao definida recebe a permissão utilizada em controller.*
     * Se não houver permissão definida o acesso é bloquerado.     *
     * nível de permissão definido em
     */
    private function verificaPermissaoEmOperacao() {
        $placeID = strtolower($this->id . '.' . $this->action->id);
        // TODO: remvoer
        $permissoes = $this->module->getControleDeAcesso();
        if (isset($permissoes[$placeID])) {
            $operation = $permissoes[$placeID];
        } elseif (isset($permissoes[strtolower($this->id) . '.*'])) {
            $operation = $permissoes[strtolower($this->id) . '.*'];
        } else {
            if (YII_DEBUG) {
                echo '<pre>';
                print_r($placeID . ': permissão não definida.');
                exit;
            } else {
                Yii::log('Permissão em módulo não definida.', CLogger::LEVEL_ERROR, 'editor.permissoes');
            }
            ShSeg::bloqueiaAcesso();
        }
        if ($operation != false) {
            ShSeg::checkAccess($operation);
        }
    }

    /**
     * Carrega links do menu
     * Monta de acordo com o modulo atual e os recursos habilitados.
     */
    private function loadMenu() {
        if (isset($this->lite) && $this->lite) {
            $this->menu = array(
                array(
                    'label' => 'Área de estudos',
                    'items' => array(
                        array(
                            'label' => 'Links favoritos',
                            'url' => ShCode::getModUrl('notebook', 'Links', 'index'),
                            'icon' => 'link',
                            'visible' => ShSeg::hasAccess('notebook'),
//                        'items' => $this->linksMatriculas(),
                        ),
                        array(
                            'label' => 'Questionários',
                            'url' => ShCode::getModUrl('questoes'),
                            'icon' => 'question',
                            'visible' => ShSeg::hasAccess('notebook'),
//                        'items' => $this->linksMatriculas(),
                        ),
                    ),
                ),
                array(
                    'label' => 'Área criação',
                    'items' => array(
                        array(
                            'label' => 'Interações',
                            'url' => ShCode::getModUrl('notebook'),
                            'icon' => 'code',
                            'visible' => ShSeg::hasAccess('notebook'),
    //                        'items' => $this->linksMatriculas(),
                        ),
                      ),
                ),
                /*array(
                    'label' => 'Compatilhamento',
                    'items' => array(
                        $this->linkParaModulo('grupos', 'admin', 'Grupos', 'group'),
                    ),
                ),*/
                array(
                    'label' => 'Administração',
                    'items' => array(
                        array(
                            'label' => 'Exemplos busca',
                            'url' => ShCode::getModUrl('instituicao','exemplos','index'),
                            'visible' => ShSeg::hasAccess('organizacao'),
                        ),
                    ),
                ),
            );
        } else {

            $this->menu = array(
                array(
                    'label' => 'Área de estudos',
                    'items' => array(
                        array(
                            'label' => 'Cursos',
                            'url' => ShCode::getModUrl('meusCursos'),
                            'icon' => 'book',
                            'visible' => ShSeg::hasAccess('gerenciamentoCursos'),
//                        'items' => $this->linksMatriculas(),
                        ),
                        array(
                            'label' => 'Minhas Interações',
                            'url' => ShCode::getModUrl('notebook'),
                            'icon' => 'code-fork',
                            'visible' => ShSeg::hasAccess('notebook'),
//                        'items' => $this->linksMatriculas(),
                        ),
                    ),
                ),
                array(
                    'label' => 'Meu material',
                    'items' => array(
                        $this->linkParaModulo('publicacoes', 'edicaoTopico', 'Tópicos'),
                        array(
                            'label' => 'Compartilhados comigo',
                            'url' => ShCode::getModUrl('publicacoes', 'topico', 'listarComp'),
                            'visible' => ShSeg::hasAccess('edicaoTopico'),
                        ),
                        array(
                            'label' => 'Banco de questões',
                            'url' => ShCode::getModUrl('questionarios', 'questao', 'listar'),
                            'visible' => ShSeg::hasAccess('edicaoQuestionario'),
                        ),
                        $this->linkParaModulo('questionarios', 'edicaoQuestionario', 'Questionários'),
                        $this->linkParaModulo('arquivos', 'edicaoArquivo', 'Arquivos'),
                    ),
                ),
                array(
                    'label' => 'Organização de material',
                    'items' => array(
                        $this->linkParaModulo('gerenciamentoCursos', 'edicaoCurso', 'Gerenciar cursos'),
                        $this->linkParaModulo('estrutura', 'edicaoArvore', 'Árvore de conteúdo'),
                    ),
                ),
                array(
                    'label' => 'Compatilhamento',
                    'items' => array(
                        $this->linkParaModulo('grupos', 'gerenciamentoCursos', 'Grupos', 'group'),
                    ),
                ),
                array(
                    'label' => 'Administração',
                    'items' => array(
                        $this->linkParaModulo('instituicao', 'edicaoOrganizacao', 'Dados de Exatas.xyz'),
                        $this->linkParaModulo('controleAcesso', 'edicaoPermissao', 'Controle de acesso'),
                        array(
                            'label' => 'Processamento da busca',
                            'url' => ShCode::getModUrl('instituicao', 'perfil', 'EditarProcessamentoBusca'),
                            'visible' => ShSeg::hasAccess('edicaoInstrucoes'),
                        ),
                        array(
                            'label' => 'Teclados da aplicação',
                            'url' => ShCode::getModUrl('instituicao', 'teclado', 'index'),
                            'visible' => ShSeg::hasAccess('edicaoOrganizacao'),
                        ),
                        array(
                            'label' => 'Widgets',
                            'url' => ShCode::getModUrl('instituicao', 'widgets', 'index'),
                            'visible' => ShSeg::hasAccess('admin'),
                        ),
                        array(
                            'label' => 'Exemplos busca',
                            'url' => ShCode::getModUrl('instituicao','exemplos','index'),
                            'visible' => ShSeg::hasAccess('organizacao'),
                        ),
                    ),
                ),
            );
        }
    }

    /**
     * Retorna link para cursos no qual o usuário possuem matrícula.
     */
    private function linksMatriculas() {
        $matriculas = Curso::getCursosMatriculados();
        $links = array();
        foreach ($matriculas as $m) {
            $links[] = array(
                'label' => $m->nome,
                'url' => ShCode::getModUrl('meusCursos', 'default', 'ver', array('id' => $m->id)),
            );
        }
        return $links;
    }

    /**
     * Monta link para módulo testado se usuário tem permissão de acesso
     * @param type $moduloID
     * @param type $permissao
     * @param type $label
     * @return type
     */
    public function linkParaModulo($moduloID, $permissao, $label, $icon = null) {
        return array(
            'label' => $label,
            'icon' => $icon,
            'url' => ShCode::getModUrl($moduloID),
            'visible' => ShSeg::hasAccess($permissao),
        );
    }

    /**
     * Publica arquivos compartilhados
     */
    private function publicaArquivosCompartilhados() {
        $this->assetsPath = Yii::app()->assetManager->publish(Yii::getPathOfAlias('shared.webroot'), false, -1, true);
        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->clientScript->registerCssFile($this->assetsPath . '/css/editor-estilo-main.css');
        }
    }

    /**
     * Define breadcrumbs base
     */
    private function setBreadcrumbsRaiz() {
        if (!Yii::app()->user->isGuest) {
            $this->breadcrumbs = array(
                Yii::app()->user->orgName => Yii::app()->baseUrl,
                Yii::app()->user->nome => Yii::app()->baseUrl . '/meuEspaco',
            );
        }
    }

    /**
     * Retorna model de user logado
     * @return type
     */
    public function getUser() {
        if (is_null($this->user)) {
            $this->user = User::model()->findByAttributes(array(
                'username' => Yii::app()->user->id,
            ));
        }
        return $this->user;
    }

    /**
     * Retorna model de perfil(aluno, autor, admin, ...) de user logado
     * @return type
     */
    public function getUserPerfil() {
        return $this->getUser();
    }

    /**
     * Retorna todas as permissoes EM RECURSOS do usuário. Em nodos,
     * topicos, questionários e arquivos.
     */
    public function getPermissoes() {
        if (is_null($this->permissoes)) {
            $matriculas = Curso::model()->with('matriculas')
                    ->findAll(array(
                'condition' => "matriculas_matriculas.aluno_id = '" . Yii::app()->user->id . "'",
                'index' => 'id',
            ));
            $topicos = $questionarios = $arquivos = array();
            foreach ($matriculas as $m) {
//                $cursos[] = $m->id;
                foreach ($m->aulas as $a) {
//                    $aulas[] = $a->id;
                    foreach ($a->topicos as $t) {
                        $topicos[] = $t->id;
                    }
                    foreach ($a->questionarios as $q) {
                        $questionarios[] = $q->id;
                    }
                    foreach ($a->arquivos as $a) {
                        $arquivos[] = $a->id;
                    }
                }
            }
            $this->permissoes = array(
                'topicos' => $topicos,
                'questionarios' => $questionarios,
                'arquivos' => $arquivos,
            );
        }
        return $this->permissoes;
    }

    /**
     * Para cada nodo registra as permissões
     * no nodo, em topicos, em questionarios e em arquivos
     * @param type $raiz
     */
    private function carrgaPermissoes($raiz, &$nodos, &$topicos, &$questionarios, &$arquivos) {
        $topicos += CHtml::listData($raiz->topicos, 'id', 'id');
        $arquivos += CHtml::listData($raiz->arquivos, 'id', 'id');
        $questionarios += CHtml::listData($raiz->questionarios, 'id', 'id');

        $filhos = $raiz->filhos;
        foreach ($filhos as $f) {
            $nodos[$f->id] = $f->id;
            $this->carrgaPermissoes($f, $nodos, $topicos, $questionarios, $arquivos);
        }
    }

}

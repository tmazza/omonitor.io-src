<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class ShUserIdentity extends CUserIdentity {

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $user = User::model()->findByAttributes(array(
            'username' => $this->username,
        ));


        if (is_null($user)) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif (!CPasswordHelper::verifyPassword($this->password, $user->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->id;
            $this->setInfosNaSessao($user);

            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function setInfosNaSessao($user) {
        Yii::app()->user->setState('perfil', $user->tipo);
        if ($user->tipo == User::TipoAutor || $user->tipo == User::TipoAluno || User::TipoAdmin) {
            $organizacao = Organizacao::model()->findByAttributes(array(
                'orgID' => Yii::app()->controller->getOrgID(),
            ));
            $this->_id = $user->id;
            Yii::app()->user->setState('orgID', $organizacao->orgID);
            Yii::app()->user->setState('orgName', $organizacao->nome);
            Yii::app()->user->setState('nome', $user->nome);
            Yii::app()->user->setState('recursos', $organizacao->getRecursos());
        }
    }

    public function getId() {
        return $this->_id;
    }

}

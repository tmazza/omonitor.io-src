<?php
/**
 * Controller geral deve ter a organização definida.
 */
interface IRootController {

    public function getOrgID();
}

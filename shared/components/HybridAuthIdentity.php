<?php
/**
 * Created by PhpStorm.
 * User: davi
 * Date: 14/07/15
 * Time: 15:31
 */
//Hhttp://hybridauth.sourceforge.net/userguide/Configuration.html

class HybridAuthIdentity extends CUserIdentity
{
    const VERSION = '2.4.1';

    private $usuario;
    private $identifier;
    private $provider;
    private $_id;
    /**
     *
     * @var Hybrid_Auth
     */
    public $hybridAuth;

    /**
     *
     * @var Hybrid_Provider_Adapter
     */
    public $adapter;

    /**
     *
     * @var Hybrid_User_Profile
     */
    public $userProfile;

    public $allowedProviders = array('google', 'facebook', 'twitter',);

    protected $config;

    public function __construct()
    {
        $path = Yii::getPathOfAlias('shared.extensions').'/hybridauth-' . self::VERSION . '/hybridauth';
        require $path . '/Hybrid/Auth.php';  //path to the Auth php file within HybridAuth folder

        $this->config = require $path.'/config.php';

        $this->hybridAuth = new Hybrid_Auth($this->config);
    }

    /**
     *
     * @param string $provider
     * @return bool
     */
    public function validateProviderName($provider)
    {
        if (!is_string($provider))
            return false;
        if (!in_array($provider, $this->allowedProviders))
            return false;

        return true;
    }

    public function login($provider){
        //apenas loga usuario, se nao existir cria e loga
        $this->usuario = $this->userProfile->email;  //CUserIdentity
        $this->identifier = $this->userProfile->identifier;  //CUserIdentity
        $this->provider = $provider;

        $pessoaByEmail = Pessoa::model()->findByAttributes(array('usuario'=>$this->usuario));
        if(!is_null($pessoaByEmail)){
            //se exister, joga sessao tudo que temos

        }else{
            $pessoaByIdentifier = Pessoa::model()->findByAttributes(array('identifier'=>$this->identifier));
            if(!is_null($pessoaByIdentifier)){
                //se exister, joga sessao tudo que temos
            }else{
                //cria usuario identifier@provider.com
                //senha md5(identifier.rand(5) digitos alphanumericos

                //manda para pagina onde escolhe se prestador ou cliente
                //cria usuario pessoa fisica
                //joga sessao tudo que temos
                //     ou
                //cria pessoa fisica cliente
                //joga sessao tudo que temos
                //
            }

        }

        Yii::app()->user->login($this);
    }

    public function authenticate(){
        return true;
    }

    public function getId(){
        return $this->_id;
    }

    public function getSocialIdentifier(){
        return $this->identifier;
    }

    public function getSocialProvider(){
        return $this->provider;
    }

}
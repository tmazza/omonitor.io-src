<?php

/**
 * Description of AutocompleteInstrucao
 *
 * @author tiago
 */
class AutocompleteInstrucao extends CAction {

    public function run($term) {
        $data = array();

        $instrucoes = InstrucaoNome::model()->with(array(
                    'instrucao' => array(
                        'condition' => 'publicado = 1',
                    ),
                ))->findAll(array('alias' => 't'));
        foreach ($instrucoes as $i) {
            if (strstr(strtolower($i->id), strtolower($term))) {
                $data[] = $i->id;
            }
            $exemplos = explode('|', $i->entradas);
            foreach ($exemplos as $e) {
                if (strstr(strtolower($i->id . ' ' . $e), strtolower($term))) {
                    $data[] = $i->id . ' ' . $e;
                }
            }


        }
        // asort($data);
        echo json_encode(array_slice($data, 0, 5));
    }




}

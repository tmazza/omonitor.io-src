<?php if (Yii::app()->user->isGuest): ?>
  <p>Faça login para corrigir</p>
<?php else: ?>
  <?php if ($q->tipo == Questao::VerdadeiroOuFalseo): ?>
      <?php

      $cor = 'blue';
      if ($taxaAcerto == 1) {
          $msg = 'Correto! Parabéns.';
          $cor = 'green';
      } elseif ($taxaAcerto >= 0.7) {
          $msg = 'Ótimo desempenho! Tente denovo.';
      } elseif ($taxaAcerto >= 0.4) {
          $msg = 'Nada mal. Tente novamente.';
      } else {
          $msg = 'Tente novamente.';
          $cor = 'red';
      }
      ?>

      <div class="info-box bg-<?= $cor; ?>">
          <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
          <div class="info-box-content">
              <span class="info-box-number" style="font-weight: normal;color:green;"><?= $msg; ?></span>
              <span class="info-box-number"><?= number_format($taxaAcerto * 100, 2); ?>%.</span>
              <div class="progress">
                  <div class="progress-bar" style="width: <?= number_format($taxaAcerto * 100, 2); ?>%"></div>
              </div>
              <?php
              if (!is_null($q->comentarioGabarito && strlen($q->comentarioGabarito) > 0)) {
                  echo $q->comentarioGabarito;
              }
              ?>
          </div><!-- /.info-box-content -->
      </div>
  <?php else: ?>
      <?php if ($taxaAcerto == 1): ?>
          <div class="card-panel green lighten-2 white-text left" >
            <h5 style="color: green;">Correto! Parabéns.</h5>
          </div>
          <br>
      <?php else: ?>
          <br>
          <div class="card-panel red-text text-darken-2 z-depth-0">
              Tente novamente.
              <br>
              <div class="divider"></div>
              <span class="black-text">
                <?php
                if (!is_null($q->comentarioGabarito && strlen($q->comentarioGabarito) > 0)) {
                    echo $q->comentarioGabarito;
                }
                ?>
              </span>
          </div>
      <?php endif; ?>
  <?php endif; ?>
  <?php
  $data = $q->mediaAcertos();
  $taxa = 10 * $data['taxaGeral'];
  $total = $data['totalRespostasUsuatiosDistintos'];
  ?>
  <?php if($total > 10): ?>
  <a onclick='$(this).next().slideToggle();' href="#!">Estatísticas da questão</a>
  <div style='display:none'>
    <h4><b><?=(int) $taxa?></b> a cada 10 pessoas acertaram esta questão.</h4>
    Taxa acerto: <?=number_format(10*$taxa,2,',','.') . '%';?>
  </div>
  <?php endif; ?>
<?php endif; ?>

<div class='card-panel grey darken-3'>
  <p class='flow-text white-text'>
    Para verificar suas respostas <?=CHtml::link('cadastre-se',$this->createUrl('site/cadastro',array(
      'b' => base64_encode($this->createUrl('/questionarios/ver',array('id'=>Yii::app()->request->getParam('qstId')))),
    )),array('class'=>'btn indigo'))?>
    <br>
    <br>
    Já tem cadastro? <?=CHtml::link('faça login',$this->createUrl('site/login',array(
      'b' => base64_encode($this->createUrl('/questionarios/ver',array('id'=>Yii::app()->request->getParam('qstId')))),
    )),array('class'=>'btn indigo'))?>
  </p>
</div>

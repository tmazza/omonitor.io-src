<?php

/**
 * Description of ParsetString
 *
 * @author tiago
 */
class ParseString extends CAction {

    public function run() {
        if (isset($_POST['label']) && isset($_POST['template'])) {
            $data = isset($_POST['data']) ? $_POST['data'] : array();
            echo ShView::getTemplateTipo($_POST['label'], $_POST['template'], $data);
        } else {
            echo '<pre>';
            print_r($_POST);
            exit;
        }
    }

}

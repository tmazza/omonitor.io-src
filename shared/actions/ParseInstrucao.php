<?php

/**
 * Com id e data passados por POST, busca a instrução pelo e junta o template 
 * com os valores dos parâmetros definidos em data.
 *
 * @author tiago
 */
class ParseInstrucao extends CAction {

    // code: template
    // data: valores dos parametros do template
    public function run() {
        if (isset($_POST['id'])) {
            $id = (int) $_POST['id'];
            $inst = Instrucao::model()->publicada()->findByPk($id);
            if (!is_null($inst)) {
                $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
                echo $inst->montar($data, true, true);
            }
        }
    }

}

<?php

/**
 * Description of AutocompleteInstrucao
 *
 * @author tiago
 */
class CorrigirQuestao extends CAction {

    public $salvarResposta = false;
    // Busca o id do questionário a partir da url

    public function run($id, $resp, $qstId, $retorno = true, $sel = '') {
        if(Yii::app()->user->isGuest){
          echo Yii::app()->controller->renderPartial('shared.actions.views.correcaoCadastro', array(), true);
        } else {
          // TODO: corrigir -> Qualquer um pode verificar qualquer questão a qualquer momento!!
          $data = array();
          $q = Questao::model()->findByPk((int) $id);
          $resposta = json_decode($resp,true);

          if((is_array($resposta) && count($resposta) == 0) || (!is_array($resposta) && strlen($resposta) == 0) ){
            echo 'Responda antes de verificar.';
          } else {
            if($q->tipo == Questao::VerdadeiroOuFalseo && is_array($resposta)){
                $resposta = array_flip($resposta);
            } elseif($q->tipo == Questao::EntradaExpressao) {
                $data = json_decode(json_decode($sel),true);
            }

            $taxaAcerto = $q->taxaAcerto($resposta,$data);
            if(is_null($taxaAcerto)){
              echo '<br><br><b>Verifique erros sintáticos na sua resposta.</b><br><br><br><br>';
            } else {
              if($this->salvarResposta){
                $this->salvarResposta($q,$taxaAcerto,$qstId);
              }
              if($retorno){
                  echo Yii::app()->controller->renderPartial('shared.actions.views.correcaoQuestao', array(
                      'q' => $q,
                      'taxaAcerto' => $taxaAcerto,
                  ), true);
              } else {
                  echo $taxaAcerto;
              }
            }
          }
        }
    }

    private function salvarResposta($questao,$taxaAcerto,$qstId){
      // TODO: considerar $this->contexto e altera o local onde a resposta esta sendo saleav
      $resposta = new QuestaoResposta();
      $resposta->user_id = Yii::app()->user->id;
      $resposta->questao_id = $questao->id;
      $resposta->questionario_id = $qstId;
      $resposta->num_tentativa = QuestaoResposta::getTentativa($questao->id);
      $resposta->taxa_acerto = $taxaAcerto;
      $resposta->contexto = QuestaoResposta::ContextoQstLivre;
      if(!$resposta->save()){
        print_r( $resposta->getErrors());

      }
    }

}

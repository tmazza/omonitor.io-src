<?php

/**
 * Com code e data passados por POST, atribuí valores em data para os parâmetros
 * definidos em code.
 *
 * @author tiago
 */
class ParseTemplate extends CAction {

    // code: template
    // data: valores dos parametros do template
    public function run() {
        if (isset($_POST['code'])) {
            $data = isset($_POST['data']) ? json_decode($_POST['data'], true) : array();
            echo $this->widget('shared.widgets.Template.Template', array(
                'template' => $_POST['code'],
                'data' => $data,
                'conteudo' => ShView::mergeDataToTemplate($_POST['code'], $data),
                'url' => 'instrucoes/preview',
            ));
        }
    }

}

<?php

/**
 * Description of AutocompleteInstrucao
 *
 * @author tiago
 */
class QuestaoUpdateOpcao extends CAction {

    public function run($id,$idQst) {
      $questao = Questao::model()->findByPk($id);
      $this->controller->questionario = $questionario = Questionario::model()->findByPk($idQst);
      if(is_null($questao) || is_null($questionario)){
        echo 'Questão não encontrada.';
      } else {
        $q = new RespostaForm();
        $q->questao = $questao;
        echo $this->controller->renderPartial('shared.widgets.Questionario.views._templatePreencherExpressao',array(
          'q' => $q,
        ),true);
      }
    }

}

<?php

/**
 * This is the model class for table "turma".
 *
 * The followings are the available columns in table 'turma':
 * @property integer $id
 * @property string $nome
 * @property integer $inicioMatricula
 * @property integer $fimMatricula
 * @property integer $publicado
 * @property integer $lastUpdate
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class Curso extends AplicationActiveRecord {

    const MatriculaLivre = 0;
    const MatriculaPorSolicitacao = 1;
    const MatriculaPorSenhaDeAcesso = 2;
    const MatriculaPorPagamento = 3;
    const MatriculaPorPagamentoComFreeTrial = 4;

    public function tableName() {
        return 'curso';
    }

    public static function getTableName() {
        return 'curso';
    }

    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('inicioMatricula,user_id, fimMatricula, publicado, lastUpdate,publicado', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 256),
            array('id, nome, inicioMatricula, fimMatricula, publicado, lastUpdate, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'aulas' => array(self::HAS_MANY, 'CursoAula', 'curso_id','order'=>'ordem ASC'),
            'matriculas' => array(self::MANY_MANY, 'User', CursoAluno::getTableName() . '(aluno_id,curso_id)'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'inicioMatricula' => 'Início Matricula',
            'fimMatricula' => 'Fim Matricula',
            'publicado' => 'Publicado',
            'lastUpdate' => 'Última atualização',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function formasDeMatricula() {
        return array(
            self::MatriculaLivre => 'Matrícula livre',
            self::MatriculaPorSolicitacao => 'Matrícula por solicitação',
            self::MatriculaPorSenhaDeAcesso => 'Matrícula com senha de acesso',
            self::MatriculaPorPagamento => 'Matrícua com pagamento',
            self::MatriculaPorPagamentoComFreeTrial => 'Matrícula com pagamento e free trial',
        );
    }

    /**
     * Inclui um aluno(user) no curso
     */
    public function matriculaAluno($alunoID) {
        // Adiciona permissão de acesso ao nodo raiz do curso
        $aluno = User::model()->findByAttributes(array(
            'id' => $alunoID,
        ));
        // Registra matrícula de aluno no curso
        $matricula = new CursoAluno();
        $matricula->user_id = Yii::app()->user->id;
        $matricula->aluno_id = $alunoID;
        $matricula->curso_id = $this->id;
        return $matricula->save();
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    public function getLabelformaDeMatricula() {
        if ($this->formaDeMatricula == self::MatriculaLivre) {
            return 'aberta';
        } elseif ($this->formaDeMatricula == self::MatriculaPorPagamento) {
            return 'paga';
        } elseif ($this->formaDeMatricula == self::MatriculaPorPagamentoComFreeTrial) {
            return 'paga';
        } elseif ($this->formaDeMatricula == self::MatriculaPorSenhaDeAcesso) {
            return 'por senha';
        } elseif ($this->formaDeMatricula == self::MatriculaPorSolicitacao) {
            return 'por solicitacao';
        } else {
            return '';
        }
    }

    public static function getCursosMatriculados() {
        return Curso::model()->with('matriculas')
                        ->findAll(array(
                            'condition' => "matriculas_matriculas.aluno_id = '" . Yii::app()->user->id . "'",
                            'index' => 'id',
        ));
    }

}

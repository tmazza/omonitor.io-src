<?php

/**
 * This is the model class for table "curso_aula".
 *
 * The followings are the available columns in table 'curso_aula':
 * @property integer $id
 * @property string $nome
 * @property integer $curso_id
 *
 * The followings are the available model relations:
 * @property Curso $curso
 * @property CursoAulaArquivo[] $cursoAulaArquivos
 */
class CursoAula extends CActiveRecord {

    public function tableName() {
        return 'curso_aula';
    }

    public static function getTableName() {
        return 'curso_aula';
    }

    public function rules() {
        return array(
            array('nome, curso_id', 'required'),
            array('curso_id', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 512),
            array('nome, curso_id,urlVideo', 'safe'),
        );
    }

    public function relations() {
        return array(
            'curso' => array(self::BELONGS_TO, 'Curso', 'curso_id'),
            'arquivos' => array(self::MANY_MANY, 'Arquivo', CursoAulaArquivo::getTableName() . '(aula_id,arquivo_id)'),
            'topicos' => array(self::MANY_MANY, 'Topico', CursoAulaTopico::getTableName() . '(aula_id,topico_id)'),
            'questionarios' => array(self::MANY_MANY, 'Questionario', CursoAulaQuestionario::getTableName() . '(aula_id,questionario_id)', 'condition' => 'publicado = 1'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'curso_id' => 'Curso',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('curso_id', $this->curso_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Conecta um arquivo a árvore
     * @param type $contID
     */
    public function conectarMaterialDeApoio($contID) {
        $conexao = new CursoAulaArquivo();
        $conexao->aula_id = $this->id;
        $conexao->arquivo_id = $contID;
        $conexao->user_id = Yii::app()->user->id;
        return $conexao->save();
    }

    /**
     * Conecta um tópico a árvore
     * @param type $contID
     */
    public function desconectarMaterialDeApoio($contID) {
        return CursoAulaArquivo::model()->deleteByPk(array(
                    'user_id' => Yii::app()->user->id,
                    'aula_id' => $this->id,
                    'arquivo_id' => $contID,
                )) > 0;
    }

    /**
     * Conecta um tópico a árvore
     * @param type $contID
     */
    public function conectarConteudo($contID) {
        $top = new CursoAulaTopico();
        $top->aula_id = $this->id;
        $top->topico_id = $contID;
        $top->user_id = Yii::app()->user->id;
        return $top->save();
    }

    /**
     * Conecta um tópico a árvore
     * @param type $contID
     */
    public function desconectarConteudo($contID) {
        return CursoAulaTopico::model()->deleteByPk(array(
                    'user_id' => Yii::app()->user->id,
                    'topico_id' => $contID,
                    'aula_id' => $this->id,
                )) > 0;
    }

    /**
     * Conecta um arquivo a árvore
     * @param type $contID
     */
    public function conectarQuestionario($contID) {
        $conexao = new CursoAulaQuestionario();
        $conexao->aula_id = $this->id;
        $conexao->questionario_id = $contID;
        $conexao->user_id = Yii::app()->user->id;
        return $conexao->save();
    }

    /**
     * Conecta um tópico a árvore
     * @param type $contID
     */
    public function desconectarQuestionario($contID) {
        return CursoAulaQuestionario::model()->deleteByPk(array(
                    'aula_id' => $this->id,
                    'user_id' => Yii::app()->user->id,
                    'questionario_id' => $contID,
                )) > 0;
    }

    public function desconectarTudo() {
      $this->desconectarTodosQuestionarios();
      $this->desconectarTodosTopicos();
      $this->desconectarTodosMaterias();
      $this->desconectarTodosAlunos();
    }

    public function desconectarTodosQuestionarios() {
      return CursoAulaQuestionario::model()->deleteAll([
        'condition'=>"user_id = " . Yii::app()->user->id . " AND aula_id = {$this->id}",
        ]) > 0;
    }

    public function desconectarTodosTopicos() {
      return CursoAulaTopico::model()->deleteAll([
        'condition'=>"user_id = " . Yii::app()->user->id . " AND aula_id = {$this->id}",
        ]) > 0;
    }

    public function desconectarTodosMaterias() {
      return CursoAulaArquivo::model()->deleteAll([
        'condition'=>"user_id = " . Yii::app()->user->id . " AND aula_id = {$this->id}",
        ]) > 0;
    }

    public function desconectarTodosAlunos() {
      return AlunoAula::model()->deleteAll([
        'condition'=>"aula_id = {$this->id}",
        ]) > 0;
    }

    public function addVideo($url) {
        $this->urlVideo = $url;
        return $this->update(array('urlVideo'));
    }

    public function getIframe($width = '800px', $height = '450px') {
        $url = $this->urlVideo;
        $matches = array();
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
        $id = $matches[1];
        return "<iframe "
                . "id='ytplayer' "
                . "type='text/html' "
                . "width='{$width}' "
                . "height='{$height}' "
                . "src='https://www.youtube.com/embed/{$id}?rel=0&showinfo=0&color=white&iv_load_policy=3'
                    frameborder='0'
                    allowfullscreen></iframe>";
    }

    public function temTopicos() {
        return count($this->topicos) > 0;
    }

    public function temQuestionarios() {
        return count($this->questionarios) > 0;
    }

    public function temArquivos() {
        return count($this->arquivos) > 0;
    }

    public function getPrincipaisInteracoes(){
        $ints = array();
        if($this->temTopicos()){
            foreach ($this->topicos as $t){
                $ints = array_merge($ints, $t->interacoesComDestaque);
            }
        }
        return $ints;
    }


    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

}

<?php

/**
 * This is the model class for table "camada_apendice".
 *
 * The followings are the available columns in table 'camada_apendice':
 * @property integer $id
 * @property string $valor
 * @property integer $tipo
 * @property integer $camada_id
 */
class CamadaApendice extends AplicationActiveRecord {

    const TipoTitulo = 1;
    const TipoTextoLink = 2;
    const TipoInteracaoComDestaque = 3;
    const TipoResumoInteracao = 4;
    // Parametro definido em camadaTipApendico
    const TipoParametroDeCodigo = 5;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'camada_apendice';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('valor, tipo, camada_id', 'required'),
            array('tipo, camada_id', 'numerical', 'integerOnly' => true),
            array('id, valor, tipo, camada_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'camada' => array(self::BELONGS_TO, 'Camada', 'camada_id'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'valor' => 'Valor',
            'tipo' => 'Tipo',
            'camada_id' => 'Camada',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('valor', $this->valor, true);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('camada_id', $this->camada_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CamadaApendice the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

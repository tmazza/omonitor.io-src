<?php

/**
 * This is the model class for table "mensagem".
 *
 * The followings are the available columns in table 'mensagem':
 * @property integer $id
 * @property string $de
 * @property string $para
 * @property string $mensagem
 * @property integer $tipo
 * @property integer $entregue
 * @property integer $lida
 * @property integer $excluida
 *
 * The followings are the available model relations:
 * @property User $para0
 * @property User $de0
 */
class Mensagem extends CActiveRecord {

    const TipoDoSistema = 0; // Notficações enviadas para o usuário
    const TipoPessoal = 1; // De usuário para usuário, idenpendente de perfil
    const TipoDoGrupo = 2; // Mensagem envia através de um grupo

    const ParaGustavoMendes = 19; //gestor conteudo
    const ParaTiagoMazzarollo = 0;//gestor desenvolvimento
    const ParaDaviDosSantos = 12; //gestor de inovação :)

    public function tableName() {
        return 'mensagem';
    }

    public static function getTableName() {
        return 'mensagem';
    }

    public function rules() {
        return array(
            array('de, para, mensagem', 'required'),
            array('de, para,tipo, entregue, lida', 'numerical', 'integerOnly' => true),
            array('id, de, para, mensagem, tipo, entregue, lida, excluida', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'destino' => array(self::BELONGS_TO, 'User', 'para'),
            'origem' => array(self::BELONGS_TO, 'User', 'de'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'de' => 'De',
            'para' => 'Para',
            'mensagem' => 'Mensagem',
            'tipo' => 'Tipo',
            'entregue' => 'Entregue',
            'lida' => 'Lida',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('de', $this->de, true);
        $criteria->compare('para', $this->para, true);
        $criteria->compare('mensagem', $this->mensagem, true);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('entregue', $this->entregue);
        $criteria->compare('lida', $this->lida);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * altera status para entregue
     * Alterado para entregue quando o destinario se logou no sistema e a mensagem foi baixada.
     */
    public function setAsEntregue() {
        $this->entregue = 1;
        $this->dataEntrega = time();
        if (!$this->update(array('entregue', 'dataEntrega'))) {
            Yii::log('Falha ao marcar mensagem como entregue. ID ' . $this->id, CLogger::LEVEL_ERROR, 'msg.lida');
        }
    }

    /**
     * altera status para lida
     */
    public function setAsLida() {
        $this->lida = 1;
        $this->dataLeitura = time();
        if (!$this->update(array('lida', 'dataLeitura'))) {
            Yii::log('Falha ao marcar mensagem como lida. ID ' . $this->id, CLogger::LEVEL_ERROR, 'msg.lida');
        }
    }

    public function scopes() {
        return array(
            'queEnvie' => array(
                'condition' => "de = '" . Yii::app()->user->id . "'"
            ),
            'queRecebi' => array(
                'condition' => "para = '" . Yii::app()->user->id . "' and excluida = 0"
            ),
            'excluidas' => array(
                'condition' => "para = '" . Yii::app()->user->id . "' and excluida = 1"
            ),
            'entregues' => array(
                'condition' => "entregue = 1",
            ),
            'naoEntregues' => array(
                'condition' => "entregue = 0",
            ),
            'lidas' => array(
                'condition' => "lida = 1",
            ),
            'naoLidas' => array(
                'condition' => "lida = 0",
            ),
        );
    }

    public function setAsDelete() {
        $this->excluida = 1;
        return $this->update(array('excluida')) > 0;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "widget".
 *
 * The followings are the available columns in table 'widget':
 * @property integer $id
 * @property string $nome
 * @property string $code
 * @property string $labels
 * @property integer $categoria_id
 * @property string $doc
 * @property string $uso
 *
 * The followings are the available model relations:
 * @property WidgetCategoria $categoria
 */
class Widget extends CActiveRecord {

    public function tableName() {
        return 'widget';
    }

    public static function getTableName() {
        return 'widget';
    }

    public function rules() {
        return array(
            array('nome, code, labels, categoria_id', 'required'),
            array('categoria_id', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 128),
            array('nome, code, labels, categoria_id,doc,uso', 'safe'),
        );
    }

    public function relations() {
        return array(
            'categoria' => array(self::BELONGS_TO, 'WidgetCategoria', 'categoria_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'code' => 'Code',
            'labels' => 'Labels',
            'categoria_id' => 'Categoria',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('labels', $this->labels, true);
        $criteria->compare('categoria_id', $this->categoria_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getLabel($paramName) {
        $s = trim($paramName);
        $id = substr($s, 1, strlen($s) - 1);
        $params = explode(',', $this->labels);
        return isset($params[$id]) ? $params[$id] : $paramName;
    }

    public function getDoc($paramName) {
        $s = trim($paramName);
        $id = substr($s, 1, strlen($s) - 1);
        $params = explode('|', $this->doc);
        return isset($params[$id]) ? $params[$id] : $paramName;
    }

}

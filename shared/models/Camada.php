<?php

include_once __DIR__ . '/../vendors/simple_html_dom.php';

/**
 * @property integer $id
 * @property string $conteudo
 * @property integer $tipo
 */
class Camada extends AplicationActiveRecord {

    protected $pathViewCamadas = 'shared.views.subCamadas.';
    // Ao processar para visualização salva os subtopicos encontrados em ordem
    private $subtopicos = array();

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'camada';
    }

    public static function getTableName() {
        return 'camada';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('tipo_id', 'required'),
            array('conteudo', 'default', 'setOnEmpty' => true, 'value' => null),
            array('tipo_id', 'numerical', 'integerOnly' => true),
            array('id, conteudo, tipo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'topico' => array(self::BELONGS_TO, 'Topico', 'topico_id'),
            'tipo' => array(self::BELONGS_TO, 'CamadaTipo', 'tipo_id'),
            // Busca em camada apendice pelo tipo TITULO, relacionamento de ser
            // usado somente nos tipos que possuem titulo associado.
            'titulo' => array(self::HAS_ONE, 'CamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoTitulo),
            'descricao' => array(self::HAS_ONE, 'CamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoResumoInteracao),
            'isDestaque' => array(self::HAS_ONE, 'CamadaApendice', 'camada_id', 'condition' => "tipo = " . CamadaApendice::TipoInteracaoComDestaque),
            // Todos os apendices da camada
            'apendices' => array(self::HAS_MANY, 'CamadaApendice', 'camada_id'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Camada the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Cria dependencia para cache
     * @param type $id
     * @return \CDbCacheDependency
     */
    public static function cacheDependecy($id) {
        return new CDbCacheDependency("SELECT last_update FROM " . self::getTableName() . " WHERE id={$id}");
    }

    /**
     * Prepara camadas para visualização.
     * @param type $conteudo
     * @return type
     * @throws Exception
     */
    public function expandeCamadasVisualizacao($conteudo) {
        if (!is_null($conteudo) && $conteudo != '') {
            $html = str_get_html($conteudo);
            $camadas = $html->find('div.camada');
              // Restrutura documento
            foreach ($camadas as &$camada) {
                if (isset($camada->id)) {
                    $modelCamada = Camada::model()->findByPk($camada->id);


                    if (is_null($modelCamada)) {
                        Yii::log('Camada não encontrada. AUTOR: ' . Yii::app()->user->id . '. ID: ' . $modelCamada->id, CLogger::LEVEL_ERROR, 'publicacoes.camada');
                    } else {
                        $camada->outertext = $this->getTipoView($modelCamada);
                    }
                } else {
                    Yii::log('Classe camada salva, sem id associado. AUTOR: ' . Yii::app()->user->id . '. CONTEUDO: ' . $camada->outertext, CLogger::LEVEL_ERROR, 'publicacoes.camada.ver');
                }
            }
            return $this->processaConteudo($html->outertext);
        }
    }

    /**
     * Cria link ajax para conteudo.
     * O título do link esta condicionado ao tipo da camada.
     * - Caso o tipo possua um titulo associado, este é usado.
     * - Senão o titulo padrão definido no tipo da camada é usado.
     * - Em último caso, um texto padrão da aplicação é adicionado.
     * @param type $camada
     * @param type $texto - @TODO: buscar de cadastro no banco.
     * @return type
     */
    public function getConteudoEscondido($camada) {
        $texto = $camada->tipo->texto_link_expandir;
        if ($camada->tipo->possui_titulo) {
            $tiulo = $camada->titulo;
            if (!is_null($tiulo)) {
                $texto = $camada->titulo->valor;
            }
        } elseif ($texto == '') {
            $texto = 'Mais';
        }
        return Yii::app()->controller->renderPartial($this->pathViewCamadas . 'conteudoEscondido', array(
                    'texto' => $texto,
                    'camada' => $camada,
                        ), true);
    }

    /**
     * Monta conteúdo para visualização baseado no tipo da camada.
     *  -- Escondido: se o conteúdo deve vir carregado aberto ou como um link ajax
     *  -- Recursivo: se as subcamadas serão montadas ou não.
     * @param type $camada - CActiveRecord Camada
     * @param type $forceOpen - Anula comportamento de Escondido
     * @return string (html)
     * @throws Exception - Tipo de camada não cadastrado.
     */
    public function getTipoView($camada, $forceOpen = false, $proccessOut = false) {
        if (is_null($camada->tipo)) {
            throw new Exception("Camada cadastrada com tipo inváilido!", 500);
        }


        $conteudoEscondido = $camada->tipo->escondido;
        if ($conteudoEscondido && !$forceOpen) {
            return $this->getConteudoEscondido($camada);
        } else {

            if ($camada->tipo->recursivo) { // Processa sub camadas da camada
                $conteudo = $this->expandeCamadasVisualizacao($camada->conteudo);
            } else { // Camada não possui sub camadas
                $conteudo = $camada->conteudo;
            }

            // Salva subtópicos encontrados
            if ($camada->tipo->categoria == 'subtopico') {
                $this->subtopicos[] = $camada;
            }
            return Yii::app()->controller->renderPartial($this->pathViewCamadas . $camada->tipo->view_name, array(
                        'camada' => $camada,
                        'class' => 'camada', // attr html_class removido de camada_tipo
                        'conteudo' => $conteudo,
                            ), true, $proccessOut);
        }
    }

    /**
     * @param type $conteudo
     * @return type
     */
    private function processaConteudo($conteudo) {
        return $this->processaLinks($conteudo);
    }

    /**
     * Procura por formato {link-topipo:[0-9]*} e muda para o link do tópico.
     * @param type $contuedo
     * @return type
     */
    private function processaLinks($contuedo) {
        return preg_replace('/{link-topico:([0-9]*)}/', Yii::app()->controller->createUrl("/topico/ver") . "/id/$1", $contuedo);
    }

    /**
     * Retorna subtópicos encontrados ao expandir camadas para visualização
     * @return type
     */
    public function getSubtopicos() {
        return $this->subtopicos;
    }

    public function behaviors() {
        return array(
            'exclusaoLogica' => array(
                'class' => 'ExclusaoLogicaBehavior',
            ),
        );
    }

    public static function getJSON($html) {
        return json_decode(str_replace(" ", "", strip_tags($html)), true);
    }

    public function buscaConteudoDoTipo($tipo) {
        $criteria = new CDbCriteria();
        $criteria->compare('conteudo', $this->conteudo);
        $criteria->condition = "tipo_id = {$tipo} AND excluido = 0 AND user_id = '" . Yii::app()->user->id . "'";
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

}

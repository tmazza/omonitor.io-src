<?php

/**
 * This is the model class for table "camada_tipo_apendice".
 *
 * The followings are the available columns in table 'camada_tipo_apendice':
 * @property integer $id
 * @property string $nome
 * @property string $label
 * @property integer $tipo
 * @property integer $camada_tipo_id
 *
 * The followings are the available model relations:
 * @property CamadaTipo $camadaTipo
 */
class CamadaTipoApendice extends CActiveRecord {

    public function tableName() {
        return 'camada_tipo_apendice';
    }

    public static function getTableName() {
        return 'camada_tipo_apendice';
    }

    public function rules() {
        return array(
            array('nome, tipo, camada_tipo_id', 'required'),
            array('tipo, camada_tipo_id', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 100),
            array('label', 'length', 'max' => 128),
            array('id, nome, label, tipo, camada_tipo_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'camadaTipo' => array(self::BELONGS_TO, 'CamadaTipo', 'camada_tipo_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'label' => 'Label',
            'tipo' => 'Tipo',
            'camada_tipo_id' => 'Camada Tipo',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('label', $this->label, true);
        $criteria->compare('tipo', $this->tipo);
        $criteria->compare('camada_tipo_id', $this->camada_tipo_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

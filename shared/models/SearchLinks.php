<?php

/**
 * This is the model class for table "search_links".
 *
 * The followings are the available columns in table 'search_links':
 * @property integer $id
 * @property string $valor
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
class SearchLinks extends CActiveRecord
{
	public function tableName()
	{
		return 'search_links';
	}

  public static function getTableName()
	{
		return 'search_links';
	}


	public function rules()
	{
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('valor', 'safe'),
			array('id, valor, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'valor' => 'Valor',
			'user_id' => 'User',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function inclui($v){
		$model = new SearchLinks();
		$model->valor = $v;
		$model->user_id = Yii::app()->user->id;
		return $model->save();
	}

	public static function exclui($id){
		$model = SearchLinks::model()->doAutor()->findByPk($id);
		return $model->delete();
	}

	public function scopes(){
		return array(
			'doAutor' => array(
				'condition' => "user_id = " . Yii::app()->user->id,
			),
		);
	}

  public static function remover($id){
			return SearchLinks::model()->doAutor()->deleteByPk((int) $id) > 0;
	}

}

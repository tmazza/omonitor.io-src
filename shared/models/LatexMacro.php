<?php

/**
 * This is the model class for table "latex_macro".
 *
 * The followings are the available columns in table 'latex_macro':
 * @property integer $id
 * @property string $valor
 * @property string $alias
 * @property string $tipo
 * @property string $user_id
 */
class LatexMacro extends CActiveRecord
{
	const MacroNewCommand = "newCommand";
	const MAcroDef = "def";

	public function tableName()
	{
		return 'latex_macro';
	}

        public static function getTableName()
	{
		return 'latex_macro';
	}

        
	public function rules()
	{
		return array(
			array('valor, alias, tipo, user_id', 'required'),
			array('valor, alias, tipo', 'length', 'max'=>255),
			array('id, valor, alias, tipo, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'valor' => 'Valor',
			'alias' => 'Alias',
			'tipo' => 'Tipo',
			'user_id' => 'Autor',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('user_id',$this->user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getMacros(){
		return array(
			LatexMacro::MacroNewCommand => '\newcommand',
			LatexMacro::MAcroDef => '\def',
		);
	}

        public function getMacroMathJaxSintax(){

		$macro="";
		switch($this->tipo){
			case LatexMacro::MacroNewCommand:
				$macro = '\newcommand {'.$this->alias.'} {'.$this->valor.'}';
				break;
                            
			case LatexMacro::MAcroDef:
				$macro = '\def ' . $this->alias. '{' . $this->valor .'}';
				break;

			default:
		}

		return $macro;
	}

	public function getPreview(){
		return "$$" . $this->valor . "$$";
	}
        
        public function scopes(){
            return array(
                'doAutor' => array(
			'condition' => "user_id = :user_id",
			'params' => array(':user_id' => Yii::app()->user->id),
		),
            );
        }
        
}

<?php

/**
 * This is the model class for table "questionario_questao".
 *
 * The followings are the available columns in table 'questionario_questao':
 * @property integer $id
 * @property integer $questionario_id
 * @property integer $questao_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 * @property Questao $questao
 * @property Questionario $questionario
 */
class QuestionarioQuestao extends AplicationActiveRecord {

    public function tableName() {
        return 'questionario_questao';
    }

    public static function getTableName() {
        return 'questionario_questao';
    }

    public function rules() {
        return array(
            array('questionario_id, questao_id, user_id', 'required'),
            array('questionario_id,user_id, questao_id', 'numerical', 'integerOnly' => true),
            array('id, questionario_id, questao_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'questao' => array(self::BELONGS_TO, 'Questao', 'questao_id'),
            'questionario' => array(self::BELONGS_TO, 'Questionario', 'questionario_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'questionario_id' => 'Questionario',
            'questao_id' => 'Questao',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

}

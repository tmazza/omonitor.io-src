<?php

/**
 * This is the model class for table "curso_aula_questionario".
 *
 * The followings are the available columns in table 'curso_aula_questionario':
 * @property integer $aula_id
 * @property integer $questionario_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Questionario $questionario
 * @property CursoAula $aula
 * @property User $autor
 */
class CursoAulaQuestionario extends CActiveRecord {

    public function tableName() {
        return 'curso_aula_questionario';
    }

    public static function getTableName() {
        return 'curso_aula_questionario';
    }

    public function rules() {
        return array(
            array('aula_id, questionario_id, user_id', 'required'),
            array('aula_id,user_id, questionario_id', 'numerical', 'integerOnly' => true),
            array('aula_id, questionario_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'questionario' => array(self::BELONGS_TO, 'Questionario', 'questionario_id'),
            'aula' => array(self::BELONGS_TO, 'CursoAula', 'aula_id'),
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'aula_id' => 'Aula',
            'questionario_id' => 'Questionario',
            'user_id' => 'Autor',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('aula_id', $this->aula_id);
        $criteria->compare('questionario_id', $this->questionario_id);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

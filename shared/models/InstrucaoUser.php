<?php

/**
 * This is the model class for table "instrucao_user".
 *
 * The followings are the available columns in table 'instrucao_user':
 * @property integer $id
 * @property string $descricao
 * @property string $template
 */
class InstrucaoUser extends CActiveRecord
{
	
	public $tipo = 0;
	
	public function tableName()
	{
		return 'instrucao_user';
	}

        public static function getTableName()
	{
		return 'instrucao_user';
	}

        
	public function rules()
	{
		return array(
			array('template,alias,user_id', 'required'),
			array('descricao', 'safe'),
			array('id, descricao, template', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'template' => 'Template',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('template',$this->template,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	} 
	
	public function montar($data, $mostrarParametros, $execOut = false) {
        $baseViewPath = 'shared.widgets.Arquimedes.views.';
        $params = ShView::extraiParametros($this->template);

        foreach ($params as $p => $v) {
            if (!isset($data[$p])) {
                $data[$p] = $v;
            }
        }
        // Parâmetros em uso
        $output = Yii::app()->controller->renderPartial($baseViewPath . '_execSage', true, $execOut);

        if ($mostrarParametros) {
            $paramInput = Yii::app()->controller->renderPartial($baseViewPath . '_params', array(
                'params' => $data,
                'inst' => $this,
                    ), true);
            $output .= $paramInput;
        }
        $org = Organizacao::model()->findByAttributes(array(
            'orgID' => 'monitor',
        ));

        // Template usados na instrução
		$output = Yii::app()->controller->renderPartial($baseViewPath . '_template', array(
			'conteudo' => stripslashes(unserialize($org->traducoes)) . "\r\n" . ShView::mergeDataToTemplate($this->template, $data),
			'comando' => $this,
			'n' => $this->id == 6 || $this->id == 7,
				), true);
			
        return $output;
    }
	
	
	public function scopes(){
		return array(
			'doAutor' => array(
				'condition' => "user_id = " .  Yii::app()->user->id,
			),
		);		
	}
	
}

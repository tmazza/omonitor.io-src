<?php

/**
 * This is the model class for table "notebook".
 *
 * The followings are the available columns in table 'notebook':
 * @property integer $id
 * @property string $user_id
 * @property integer $linguagem
 * @property string $codigo
 * @property integer $dataCriacao
 * @property integer $dataEdicao
 * @property integer $favorito
 * @property integer $sharing
 * @property text $publicId
 */
class Notebook extends CActiveRecord {

    const Sage = 1;
    const Singular = 2;
    const R = 3;
    const Python = 4;

    public function tableName() {
        return 'notebook';
    }

    public static function getTableName() {
        return 'notebook';
    }

    public function rules() {
        return array(
            array('user_id, linguagem, dataCriacao, dataEdicao', 'required'),
            array('user_id,linguagem, dataCriacao, dataEdicao, favorito', 'numerical', 'integerOnly' => true),
            array('linguagem', 'in', 'range' => array(self::Sage, self::Singular, self::R, self::Python)),
            array('nome,sharing', 'safe'),
            array('id, user_id, linguagem, codigo, dataCriacao, sharing, dataEdicao, favorito', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'Autor',
            'linguagem' => 'Linguagem',
            'codigo' => 'Codigo',
            'dataCriacao' => 'Data Criacao',
            'dataEdicao' => 'Data Edicao',
            'favorito' => 'Favorito',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('linguagem', $this->linguagem);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('dataCriacao', $this->dataCriacao);
        $criteria->compare('dataEdicao', $this->dataEdicao);
        $criteria->compare('favorito', $this->favorito);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function criarNova($tipo, $codigo = '') {
        $note = new Notebook();
        $note->user_id = Yii::app()->user->id;
        $note->linguagem = $tipo;
        $note->codigo = $codigo;
        $note->sharing = 0;
        $note->publicId = hash('crc32', Yii::app()->user->id) . hash('crc32', microtime(true) . 'OMontior');
        $note->dataCriacao = $note->dataEdicao = time();
        return $note;
    }

    public function scopes() {
        return array(
            'doAutor' => array('condition' => "user_id = '" . Yii::app()->user->id . "'"),
        );
    }

    public function getLanguage() {
        switch ($this->linguagem) {
            case self::Sage: return 'sage';
            case self::Singular: return 'singular';
            case self::R: return 'r';
            case self::Python: return 'python';
        }
    }

    public function paraPublica() {
        $this->sharing = 1;
        return $this->update(array('sharing'));
    }

    public function paraPrivada() {
        $this->sharing = 0;
        return $this->update(array('sharing'));
    }

    public function getLinkPublico() {
        return 'http://' . $_SERVER['SERVER_NAME'] . Yii::app()->baseUrl . '/play/' . $this->publicId;
    }

}

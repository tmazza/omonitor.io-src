<?php

/**
 * This is the model class for table "topico".
 *
 * The followings are the available columns in table 'topico':
 * @property integer $id
 * @property string $nome
 */
class Topico extends AplicationActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'topico';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('nome', 'length', 'max' => 200),
            array('id, nome', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'camadaRaiz' => array(self::HAS_ONE, 'Camada', 'topico_id', 'condition' => 'tipo_id = 0'), // Camada tópico/raiz
            'relacionados' => array(self::HAS_MANY, 'TopicosRelacionados', 'pai_id'),
            'conexoes' => array(self::MANY_MANY, 'TaxItem', TopicoTax::getTableName() . '(topico_id,tax_item_id)'),
            'aulas' => array(self::MANY_MANY, 'CursoAula', CursoAulaTopico::getTableName() . '(topico_id,aula_id)'),
            // Tipos de camadas
            'interacoes' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('tipo'), 'condition' => 'tipo.categoria = "sage"'), // Camada sage/interação
            'interacoesComDestaque' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('isDestaque' => array('alias' => 'dest'), 'tipo'), 'condition' => 'tipo.categoria = "sage" AND dest.valor="S"', 'alias' => 'cam'), // Camada sage/interação
            'subtopicos' => array(self::HAS_MANY, 'Camada', 'topico_id', 'with' => array('tipo'), 'condition' => 'tipo.categoria = "subtopico"'), // Camada sage/interação
        );
    }

    public function scopes() {
        return array(
            'publico' => array(
                'condition' => 'publico = 1',
            ),
            'publicado' => array(
                'condition' => 'publicado = 1',
            ),
            'comDestaque' => array(
                'condition' => 'comDestaque = 1'
            ),
            'semestaque' => array(
                'condition' => 'comDestaque = 0'
            ),
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'user_id' => 'Autor',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('nome', $this->nome, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Topico the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Tópicos do autor
     * públicos e privados.
     * @return \Topico
     */
    public function autor($autor) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "user_id = :user_id",
            'params' => array(':user_id' => $autor),
        ));
        return $this;
    }

    /**
     * Busca somente os tópicos publicados respeitando o acesso a conteúdos
     * públicos e privados.
     * @return \Topico
     */
    public function publicados() {
        $somentePublico = null;
        if (Yii::app()->user->isGuest) {
            $somentePublico = " AND publico = 1";
        }
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "publicado = 1" . $somentePublico,
        ));
        return $this;
    }

    /**
     * Somente tópicos publicados por autores
     * @return \Topico
     */
    public function somenteDeAutores() {
        $this->getDbCriteria()->mergeWith(
                array(
                    'with' => 'autor',
                    'condition' => 'autor.tipo = "autor" || autor.tipo = "admin"',
                )
        );
        return $this;
    }

    /**
     * Somente tópicos publicados por alunos
     * @return \Topico
     */
    public function somenteDeAlunos() {
        $this->getDbCriteria()->mergeWith(
                array(
                    'with' => 'autor',
                    'condition' => 'autor.tipo = "aluno"',
                )
        );
        return $this;
    }

    /**
     * Adiciona condições de acesso aos tópicos de um usuário não identificada
     * @return \Topico
     */
    public function paraConvidado() {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "publico = 1",
            // 'condition' => "publicado = 1 AND publico = 1",
        ));
        return $this;
    }

    /**
     * Adiciona condições de acesso aos tópicos de um usuário identificado e do tipo AUTOR
     * @return \Topico
     */
    public function paraAluno($autorID) {
        $this->getDbCriteria()->mergeWith(array(
            // 'condition' => "publicado = 1 OR (t.user_id = '{$autorID}')",
            'condition' => "t.user_id = '{$autorID}'",
        ));
        return $this;
    }

    /**
     * Adiciona condições de acesso aos tópicos de um usuário identificado e do tipo AUTOR
     * @return \Topico
     */
    public function paraAutor($autorID) {
        $this->getDbCriteria()->mergeWith(array(
            'alias' => 't',
            'condition' => "publicado = 1 OR (t.user_id = '{$autorID}')",
        ));
        return $this;
    }

    public function buscarPublicados($query) {
        $this->publicados();
        $this->filtrarTopicos($query);
        return $this;
    }

    public function filtrarTopicos($query) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "nome LIKE '%$query%' ",
        ));
        return $this;
    }

    public static function buscaPorNome($nome) {
        $criteria = new CDbCriteria();
        // Realiza busca com comparação não exata (LIKE) e dado normalziado no nome do tópico
        $criteria->compare('nome', $nome, true);

        $criteria->compare('publicado', '1', false);
        // Realiza busca com comparação não exata (LIKE) e dado normalziado no nome conteúdo da camada raiz
        $criteria->order = 'comDestaque DESC';
        $dataProvaider = new CActiveDataProvider(Topico::model(), array(
            'criteria' => $criteria,
            'pagination' => false
        ));
        return $dataProvaider->getData();
    }

    public static function buscaTituloSubTopico($nome) {
        $criteria = new CDbCriteria();
        $criteria->with = array('titulo', 'tipo', 'topico');
        // Realiza busca com comparação não exata (LIKE) e dado normalziado no nome do tópico
        $criteria->condition = "tipo.categoria = 'subtopico'";
        $criteria->compare('titulo.valor', $nome, true);
        // Realiza busca com comparação não exata (LIKE) e dado normalziado no nome conteúdo da camada raiz
        $dataProvaider = new CActiveDataProvider(Camada::model(), array(
            'criteria' => $criteria,
            'pagination' => false
        ));
        $camadas = $dataProvaider->getData();
        $topicos = array();
        foreach ($camadas as $camada) {
            $topico = $camada->topico;
            if (!is_null($topico)) {
                if ($topico->publicado == '1' && $topico->excluido == '0') {
                    $topicos[] = $topico;
                }
            }
        }
        return $topicos;
    }

    public function behaviors() {
        return array(
            'exclusaoLogica' => array(
                'class' => 'ExclusaoLogicaBehavior',
            ),
        );
    }

    public static function getDestaquesAleatorios($max = 15) {
        // Definção de menu de contexto geral. Deve sobreescrito, caso o menu seja outro
        $destaques = Topico::model()
                ->cache(300)
                ->comDestaque()
                ->publicados()
                ->findAll(array('index' => 'id'));

        // Para PHP < 5.4
        if (!function_exists('cmp')) {

            function cmp($a, $b) {
                return rand(0, 1) < 1;
            }

        }

        usort($destaques, "cmp");

        return array_slice($destaques, 0, $max);
    }

    public function getLeitura() {
        $camada = $this->camadaRaiz;
        if (is_null($camada)) {
            return '';
        } else {
            return $camada->expandeCamadasVisualizacao($camada->conteudo);
        }
    }

    public function loadLatexMacros() {
        $macros = LatexMacro::model()->findAll(array(
            'condition' => "user_id = '{$this->user_id}'",
        ));
        $content = '<div style="display:none;">\\[';
        foreach ($macros as $m) {
            $content .= $m->getMacroMathJaxSintax();
        }
        $content .= '\]</div>';
        return $content;
    }

}

<?php

/**
 * This is the model class for table "topicos_relacionados".
 *
 * The followings are the available columns in table 'topicos_relacionados':
 * @property integer $pai_id
 * @property integer $filho_id
 */
class TopicosRelacionados extends AplicationActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'topico_relacionado';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('pai_id, filho_id', 'required'),
            array('pai_id, filho_id', 'numerical', 'integerOnly' => true),
            array('pai_id, filho_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'pai' => array(self::BELONGS_TO, 'Topico', 'pai_id'),
            'filho' => array(self::BELONGS_TO, 'Topico', 'filho_id'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = " . Yii::app()->user->id,
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pai_id' => 'Pai',
            'filho_id' => 'Filho',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('pai_id', $this->pai_id);
        $criteria->compare('filho_id', $this->filho_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TopicosRelacionados the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "tax_user".
 *
 * The followings are the available columns in table 'tax_user':
 * @property integer $id
 * @property integer $tax_item_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 * @property TaxItem $taxItem
 */
class TaxUser extends AplicationActiveRecord {

    public function tableName() {
        return 'tax_user';
    }

    public static function getTableName() {
        return 'tax_user';
    }

    public function rules() {
        return array(
            array('tax_item_id, user_id', 'required'),
            array('tax_item_id', 'numerical', 'integerOnly' => true),
            array('user_id', 'length', 'max' => 100),
            array('id, tax_item_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'taxItem' => array(self::BELONGS_TO, 'TaxItem', 'tax_item_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tax_item_id' => 'Tax Item',
            'user_id' => 'User',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

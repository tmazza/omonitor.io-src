<?php

/**
 * This is the model class for table "curso_aula_arquivo".
 *
 * The followings are the available columns in table 'curso_aula_arquivo':
 * @property integer $aula_id
 * @property integer $arquivo_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 * @property Arquivo $arquivo
 * @property CursoAula $aula
 */
class CursoAulaArquivo extends CActiveRecord {

    public function tableName() {
        return 'curso_aula_arquivo';
    }

    public static function getTableName() {
        return 'curso_aula_arquivo';
    }

    public function rules() {
        return array(
            array('aula_id, arquivo_id, user_id', 'required'),
            array('aula_id,user_id, arquivo_id', 'numerical', 'integerOnly' => true),
            array('aula_id, arquivo_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'arquivo' => array(self::BELONGS_TO, 'Arquivo', 'arquivo_id'),
            'aula' => array(self::BELONGS_TO, 'CursoAula', 'aula_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'aula_id' => 'Aula',
            'arquivo_id' => 'Arquivo',
            'user_id' => 'Autor',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('aula_id', $this->aula_id);
        $criteria->compare('arquivo_id', $this->arquivo_id);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

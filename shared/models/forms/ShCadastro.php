<?php

/**
 * Description of ShCadastro
 *
 * @author tiago
 */
class ShCadastro extends CFormModel {

    public $nome;
    public $email;
    public $senha;
    public $senhaConfirma;

    //social integration
    public $social_identifier;
    public $social_provider;

    public function rules() {
        return array(
            array('nome, email, senha, senhaConfirma', 'required'),
            array('email', 'email'),
            array('email', 'validaEmailChoice'),
            array('senha', 'length', 'min' => 6, 'max' => 16),
            // array('senha', 'shared.extensions.SPasswordValidator.SPasswordValidator', 'min' => 6, 'up' => 0, 'low' => 2),
            array('senhaConfirma', 'compare', 'compareAttribute' => 'senha', 'message' => 'Senhas diferentes.'),
        );
    }

    public function validaEmailChoice($attribute, $params) {
        $existentes = Yii::app()->db->createCommand()
                ->select('email')
                ->from(User::getTableName())
                ->queryColumn();

        if (in_array($this->{$attribute}, $existentes)) {
            $this->addError('email', 'Email ' . $this->{$attribute} . ' já esta sendo utilizado. <br>' . CHtml::link('Recupere sua senha', Yii::app()->controller->createUrl('senha/recuperar')));
        }
    }

    public function attributeLabels() {
        return array(
            'nome' => 'Nome',
            'email' => 'Email',
            'senha' => 'Senha',
            'senhaConfirma' => 'Confirme a senha',
        );
    }

    public function logaUsuario($user) {
        $identity = new ShUserIdentity($this->email, $this->senha);
        $identity->errorCode = ShUserIdentity::ERROR_NONE;
        $identity->setInfosNaSessao($user);
        return Yii::app()->user->login($identity, 60 * 60 * 24 * 7);
    }

    public static function getForm() {
        return array(
            'elements' => array(
                'nome' => array(
                    'type' => 'text',
                    'maxlength' => 32,
                ),
                'email' => array(
                    'type' => 'text',
                ),
                'senha' => array(
                    'type' => 'password',
                    'minlength' => 6,
                    'maxlength' => 16,
                ),
                'senhaConfirma' => array(
                    'type' => 'password',
                    'minlength' => 6,
                    'maxlength' => 16,
                ),
                '<br>',
            ),
            'buttons' => array(
                'cadastro' => array(
                    'type' => 'submit',
                    'label' => 'Criar conta',
                    'class' => 'btn btn-success',
                ),
        ));
    }

}

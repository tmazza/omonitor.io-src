<?php

/**
 * Description of ShCadastro
 *
 * @author tiago
 */
class RespostaForm extends CFormModel {

    public $questao;
    public $resposta;
    public $correta = null;

    public function rules() {
        return array(
            array('questao, resposta', 'required', 'message' => 'Resposta não preenchida.'),
            array('resposta', 'validaCincoAlt'),
            array('resposta', 'validaVouF'),
//            array('resposta', 'validaDiscursiva'),
        );
    }

    public function validaCincoAlt($attribute) {
        if ($this->questao->tipo == Questao::CincoAlternativas) {
            if (!$this->isRespostaInAlternativas($this->resposta)) {
                $this->addError($attribute, 'Resposta inválida, selecione uma das alternativas.');
            }
        }
    }

    public function validaVouF($attribute) {
        if ($this->questao->tipo == Questao::VerdadeiroOuFalseo && is_array($this->resposta)) {
            foreach (array_keys($this->resposta) as $r) {
                if (!$this->isRespostaInAlternativas($r)) {
                    $this->addError($attribute, 'Resposta inválida, selecione uma alternativa ou mais.');
                }
            }
        }
    }
    
//    public function validaDiscursiva($attribute) {
//        if ($this->questao->tipo == Questao::EntradaSimples || $this->questao->tipo == Questao::EntradaExpressao) {
//            if (strlen($this->resposta) <= 0) {
//                $this->addError($attribute, 'Resposta inválida, selecione uma das alternativas.');
//            }
//        }
//    }

    private function isRespostaInAlternativas($resp) {
        $alternativas = CHtml::listData($this->questao->alternativas, 'id', 'id');
        return in_array($resp, $alternativas);
    }

    public function attributeLabels() {
        return array(
            'resposta' => 'Resposta',
        );
    }

}

<?php

/**
 * This is the model class for table "arquivo".
 *
 * The followings are the available columns in table 'arquivo':
 * @property integer $id
 * @property string $alias
 * @property string $nome
 * @property string $user_id
 * @property integer $publicado
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class Arquivo extends AplicationActiveRecord {

    public $file;

    public function tableName() {
        return 'arquivo';
    }

    public static function getTableName() {
        return 'arquivo';
    }

    public function rules() {
        return array(
            array('file', 'file', 'types' => 'jpg, gif, png, pdf, doc, docx, csv'),
            array('nome, user_id', 'required'),
            array('user_id,publicado', 'numerical', 'integerOnly' => true),
            array('alias', 'length', 'max' => 256),
            array('nome', 'length', 'max' => 512),
            array('id, alias, nome, user_id, publicado', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'alias' => 'Alias',
            'nome' => 'Nome',
            'user_id' => 'Autor',
            'publicado' => 'Publicado',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function generateAlias($uploadName) {
        $extension = pathinfo($uploadName, PATHINFO_EXTENSION);
        return md5($uploadName . microtime(true)) . '.' . $extension;
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    public function showNome() {
        return $this->nome . '.' . pathinfo($this->alias, PATHINFO_EXTENSION);
    }

    public function getExt() {
        return pathinfo($this->alias, PATHINFO_EXTENSION);
    }

    //usada no widget Feedback para salvar no assets o screenshot da tela
    //html2canvas
    public static function uploadFeedbackScreenShot($src){
        $fileName = "feedback_imagem_".date("d-m-Y-H:i");
        $fileName = md5($fileName . microtime(true)).".png";

        $updaloadPath = Yii::getPathOfAlias('webroot');
        if(!file_exists($updaloadPath."/assets/feedback/")){
            mkdir($updaloadPath."/assets/feedback/",0777,true);
        }
        $filePath =  $updaloadPath."/assets/feedback/".$fileName;

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $src));
        file_put_contents($filePath, $data);

        return $fileName;
    }

}

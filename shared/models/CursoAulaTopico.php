<?php

/**
 * This is the model class for table "curso_aula_topico".
 *
 * The followings are the available columns in table 'curso_aula_topico':
 * @property integer $aula_id
 * @property integer $topico_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Topico $topico
 * @property CursoAula $aula
 * @property User $autor
 */
class CursoAulaTopico extends CActiveRecord {

    public function tableName() {
        return 'curso_aula_topico';
    }

    public static function getTableName() {
        return 'curso_aula_topico';
    }

    public function rules() {
        return array(
            array('aula_id, topico_id, user_id', 'required'),
            array('aula_id,user_id, topico_id', 'numerical', 'integerOnly' => true),
            array('aula_id, topico_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'topico' => array(self::BELONGS_TO, 'Topico', 'topico_id'),
            'aula' => array(self::BELONGS_TO, 'CursoAula', 'aula_id'),
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'aula_id' => 'Aula',
            'topico_id' => 'Topico',
            'user_id' => 'Autor',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('aula_id', $this->aula_id);
        $criteria->compare('topico_id', $this->topico_id);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

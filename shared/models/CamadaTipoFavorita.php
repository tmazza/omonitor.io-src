<?php

/**
 * This is the model class for table "camada_tipo_favorita".
 *
 * The followings are the available columns in table 'camada_tipo_favorita':
 * @property string $user_id
 * @property integer $tipo_id
 */
class CamadaTipoFavorita extends CActiveRecord {

    public function tableName() {
        return 'camada_tipo_favorita';
    }

    public static function getTableName() {
        return 'camada_tipo_favorita';
    }

    public function rules() {
        return array(
            array('user_id, tipo_id', 'required'),
            array('tipo_id,user_id', 'numerical', 'integerOnly' => true),
            array('user_id, tipo_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'user_id' => 'Autor',
            'tipo_id' => 'Tipo',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('tipo_id', $this->tipo_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    public static function favoritar($id) {
        $camdaTipo = CamadaTipo::model()->findByPk((int) $id, array(
            'condition' => "de_aplicacao = 1 || user_id = '" . Yii::app()->user->id . "'",
        ));
        $favorita = CamadaTipoFavorita::model()->findByPk(array(
            'user_id' => Yii::app()->user->id,
            'tipo_id' => $id,
        ));
        if (is_null($camdaTipo)) {
            return false;
        } else {
            if (is_null($favorita)) {
                $model = new CamadaTipoFavorita();
                $model->user_id = Yii::app()->user->id;
                $model->tipo_id = $id;
                return $model->save();
            } else {
                return true;
            }
        }
    }

    public static function desfavoritar($id) {
        $favorita = CamadaTipoFavorita::model()->findByPk(array(
            'user_id' => Yii::app()->user->id,
            'tipo_id' => $id,
        ));
        if(is_null($favorita)){
            return true;
        } else {
            return $favorita->delete();
        }
    }

}

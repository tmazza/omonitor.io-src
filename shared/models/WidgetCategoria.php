<?php

/**
 * This is the model class for table "widget_categoria".
 *
 * The followings are the available columns in table 'widget_categoria':
 * @property integer $id
 * @property string $label
 * @property string $descricao
 * @property integer $categoria_pai
 *
 * The followings are the available model relations:
 * @property Widget[] $widgets
 * @property WidgetCategoria $categoriaPai
 * @property WidgetCategoria[] $widgetCategorias
 */
class WidgetCategoria extends CActiveRecord {

    public function tableName() {
        return 'widget_categoria';
    }

    public static function getTableName() {
        return 'widget_categoria';
    }

    public function rules() {
        return array(
            array('label', 'required'),
            array('categoria_pai', 'numerical', 'integerOnly' => true),
            array('label', 'length', 'max' => 128),
            array('descricao', 'safe'),
            array('id, label, descricao, categoria_pai', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'widgets' => array(self::HAS_MANY, 'Widget', 'categoria_id'),
            'categoriaPai' => array(self::BELONGS_TO, 'WidgetCategoria', 'categoria_pai'),
            'categorias' => array(self::HAS_MANY, 'WidgetCategoria', 'categoria_pai'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'label' => 'Label',
            'descricao' => 'Descricao',
            'categoria_pai' => 'Categoria Pai',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('label', $this->label, true);
        $criteria->compare('descricao', $this->descricao, true);
        $criteria->compare('categoria_pai', $this->categoria_pai);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

<?php

/**
 * This is the model class for table "aluno_aula".
 *
 * The followings are the available columns in table 'aluno_aula':
 * @property integer $user_id
 * @property integer $aula_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property CursoAula $aula
 * @property User $user
 */
class AlunoAula extends CActiveRecord
{
	public function tableName()
	{
		return 'aluno_aula';
	}

        public static function getTableName()
	{
		return 'aluno_aula';
	}

        
	public function rules()
	{
		return array(
			array('user_id, aula_id, status', 'required'),
			array('user_id, aula_id, status', 'numerical', 'integerOnly'=>true),
			array('user_id, aula_id, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'aula' => array(self::BELONGS_TO, 'CursoAula', 'aula_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'aula_id' => 'Aula',
			'status' => 'Status',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('aula_id',$this->aula_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "questionario".
 *
 * The followings are the available columns in table 'questionario':
 * @property integer $id
 * @property string $nome
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class Questionario extends AplicationActiveRecord {

    public function tableName() {
        return 'questionario';
    }

    public static function getTableName() {
        return 'questionario';
    }

    public function rules() {
        return array(
            array('nome, user_id', 'required'),
            array('nome,publicado', 'length', 'max' => 256),
            array('nome, user_id,publicado'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
            'questoes' => array(self::MANY_MANY, 'Questao', QuestionarioQuestao::getTableName() . '(questionario_id,questao_id)'),
        );
    }

    public function attributeLabels() {
        return array(
            'nome' => 'Nome',
            'user_id' => 'Autor',
            'publicado' => 'Publicado?'
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

    public function constroiForm($respostas = array()) {
        $questoes = $this->questoes;
        $models = array();
        foreach ($questoes as $q) {
            $resposta = new RespostaForm();
            $resposta->questao = $q;
            if (isset($respostas[$q->id]['resposta'])) {
                $resposta->resposta = $respostas[$q->id]['resposta'];
                $resposta->correta = $q->taxaAcerto($respostas[$q->id]['resposta']);
            }
            $models[$q->id] = $resposta;
        }
        return $models;
    }

    public function meuDesempenho(){
      return $this->desempenhoDeUsuario(Yii::app()->user->id);
    }

    public function desempenhoDeUsuario($id){
      $respostas = $this->respostasDoUsuario($id);
      $totalDeQuestoes = count($this->questoes);

      $data = array();
      $data['respondido'] = count($respostas) / $totalDeQuestoes;

      $peso = 1;
      // Somatorio de acertos
      $acertos = $pesos = 0;
      foreach ($respostas as $r) {
        $acertos += $peso * $r['taxa_acerto'];
        $pesos += $peso;
      }
      $data['desempenho'] = $acertos / ($pesos == 0 ? 1 : $pesos);
      return $data;
    }

    public function minhasRespostas(){
      return $this->respostasDoUsuario(Yii::app()->user->id);
    }

    public function respostasDoUsuario($id){

      $ultimaTentativa = 'SELECT max(num_tentativa) FROM ' . QuestaoResposta::getTableName() . ' WHERE questao_id = qr.questao_id AND user_id = ' . $id;
      $data = Yii::app()->db->createCommand()
        ->select('*')
        ->from(QuestionarioQuestao::getTableName() . ' q')
        ->join(QuestaoResposta::getTableName() . ' qr', "qr.questionario_id = q.questionario_id AND qr.questao_id = q.questao_id AND num_tentativa = ($ultimaTentativa) AND qr.user_id = " . $id)
        ->where("q.questionario_id = " . $this->id)
        ->queryAll();
      return $data;
    }

}

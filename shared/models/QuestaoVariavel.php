<?php

/**
 * This is the model class for table "questao_variavel".
 *
 * The followings are the available columns in table 'questao_variavel':
 * @property integer $questao_id
 * @property string $variavel_id
 * @property string $valor
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Questao $questao
 */
class QuestaoVariavel extends CActiveRecord
{

	public $max = 0;

	public function tableName()
	{
		return 'questao_variavel';
	}

        public static function getTableName()
	{
		return 'questao_variavel';
	}


	public function rules()
	{
		return array(
			array('questao_id, variavel_id, valor, user_id', 'required'),
			array('questao_id, user_id', 'numerical', 'integerOnly'=>true),
			array('variavel_id', 'length', 'max'=>32),
			array('valor', 'length', 'max'=>128),
			array('questao_id, variavel_id, valor, user_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'questao' => array(self::BELONGS_TO, 'Questao', 'questao_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'questao_id' => 'Questao',
			'variavel_id' => 'Variavel',
			'valor' => 'Valor',
			'user_id' => 'User',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('questao_id',$this->questao_id);
		$criteria->compare('variavel_id',$this->variavel_id,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function scopes(){
		return array(
			'doAutor' => array(
				'condition' => 'user_id = ' . Yii::app()->user->id,
			),
		);
	}

  public static function excluir($questaoId,$varSequencia){
		return QuestaoVariavel::model()->deleteAllByAttributes(array(
			'user_id' => Yii::app()->user->id,
			'questao_id' => (int) $questaoId,
			'sequencia' => $varSequencia,
		));
	}

  public function getNext(){
		$model = QuestaoVariavel::model()->find(array(
			'select' => "max(sequencia) as max",
			'condition' => "questao_id = {$this->questao_id}",
		));
		if(is_null($model) || strlen($model->max) == 0){
			return 0;
		} else {
			return $model->max + 1;
		}
	}

  public static function novaSequencia($questaoId){
		$model = QuestaoVariavel::model()->find(array(
			'select' => "max(sequencia) as max",
			'condition' => "questao_id = {$questaoId}",
		));
		if(is_null($model) || strlen($model->max) == 0){
			return 0;
		} else {
			return $model->max + 1;
		}
	}

}

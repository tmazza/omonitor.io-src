<?php

/**
 * This is the model class for table "questao".
 *
 * The followings are the available columns in table 'questao':
 * @property integer $id
 * @property string $titulo
 * @property string $conteudo
 * @property integer $tipo
 * @property string $user_id
 * @property integer $excluido
 *
 * The followings are the available model relations:
 * @property UserAutor $autor
 * @property QuestaoAlternativa[] $questaoAlternativas
 * @property QuestaoGabarito[] $questaoGabaritos
 */
class Questao extends AplicationActiveRecord {

    const CincoAlternativas = 1;
    const VerdadeiroOuFalseo = 2;
    const EntradaSimples = 3;
    const EntradaExpressao = 4;
//    const EntradaNumerica = 5;

    const Nive0 = 0;
    const Nive1 = 1;
    const Nive2 = 2;
    const Nive3 = 3;
    const Nive4 = 4;
    const Nive5 = 5;
    const Nive6 = 6;
    const Nive7 = 7;
    const Nive8 = 8;
    const Nive9 = 9;

    public function tableName() {
        return 'questao';
    }

    public static function getTableName() {
        return 'questao';
    }

    public function rules() {
        return array(
            array('tipo, enunciado, user_id', 'required'),
            array('tipo,user_id,excluido', 'numerical', 'integerOnly' => true),
            array('tipo', 'in', 'range' => array_keys(Questao::getTipos())),
            array('comentarioGabarito, dica,interacao ', 'safe'),
            array('id, tipo, enunciado, comentarioGabarito, dica, user_id,excluido', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'user', 'user_id'),
            'alternativas' => array(self::HAS_MANY, 'QuestaoAlternativa', 'questao_id'),
            'gabaritoCincoAlt' => array(self::HAS_ONE, 'QuestaoAlternativa', 'questao_id', 'condition' => "verdadeira = 1"),
            'gabaritoEmTexto' => array(self::HAS_ONE, 'QuestaoAlternativa', 'questao_id'),
            'questaoGabaritos' => array(self::HAS_MANY, 'QuestaoGabarito', 'questao_id'),
            'variaveis' => array(self::HAS_MANY, 'QuestaoVariavel', 'questao_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'tipo' => 'Tipo',
            'enunciado' => 'Enunciado',
            'comentarioGabarito' => 'Comentario Gabarito',
            'dica' => 'Dica',
            'user_id' => 'Autor',
            'excluido' => 'Excluido',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

    public static function getTipos() {
        return array(
            self::CincoAlternativas => 'Cinco alternativas',
            self::VerdadeiroOuFalseo => 'Verdadeiro ou falso',
            self::EntradaSimples => 'Resposta em texto digitada',
            self::EntradaExpressao => 'Resposta matemática digitada',
//            self::EntradaNumerica => 'Resposta numérica digitada pelo aluno',
        );
    }

    public static function getLabelTipo($tipo) {
        $tipos = QstQuestao::getTipos();
        return $tipos[$tipo];
    }

    /**
     * Compara resposta com gabarito.
     * Retorna percentual de acerto entre 0 e 1
     * @param type $resposta
     */
    public function taxaAcerto($resposta, $selecionados = array()) {
        $gabarito = $this->getGabrito();
        if (!is_null($gabarito)) {
            if ($this->tipo == self::CincoAlternativas) {
                return $gabarito->id == $resposta ? 1 : 0;
            } elseif ($this->tipo == self::VerdadeiroOuFalseo) {
                $marcadasComoVerdadeiro = array_keys($resposta);
                $acertos = $corretas = 0;
                foreach ($gabarito as $g) {
                    if ($g->verdadeira) {
                      $corretas++;
                      if(in_array($g->id, $marcadasComoVerdadeiro)) {
                        $acertos++;
                      }
                    }
                }
                return ($acertos / ($corretas > 0 ? $corretas : 1)) * ($acertos / (count($marcadasComoVerdadeiro) > 0 ? count($marcadasComoVerdadeiro) : 1));
            } elseif ($this->tipo == self::EntradaSimples) {
                return ShCode::paraBusca($resposta) == ShCode::paraBusca($gabarito->valor) ? 1 : 0;
            } elseif ($this->tipo == self::EntradaExpressao) {
              $respostaCorreta = ShView::mergeDataToTemplate($gabarito->valor,$selecionados);
              $funcao = ShView::mergeDataToTemplate(stripslashes($gabarito->chave),$selecionados);
              $correcao =  ShSage::isEqual($resposta, $respostaCorreta,$funcao);
              if(is_null($correcao)){
                return null;
              } else {
                return (int) $correcao;
              }

            }
        } else {
            return 0;
        }
    }

    private function getGabrito() {
        if ($this->tipo == self::CincoAlternativas) {
            return $this->gabaritoCincoAlt;
        } elseif ($this->tipo == self::VerdadeiroOuFalseo) {
            return $this->alternativas;
        } elseif ($this->tipo == self::EntradaSimples) {
            return $this->gabaritoEmTexto;
        } elseif ($this->tipo == self::EntradaExpressao) {
            return $this->gabaritoEmTexto;
        }
    }

    public static function getPesos() {
        return array(
            self::Nive1 => 'Fácil',
            self::Nive4 => 'Médio',
            self::Nive8 => 'Difícil',
        );
    }

    // old
    public function getVariavelOptions($id){
          $opcoes = QuestaoVariavel::model()->findAll(array(
            'condition' => "questao_id = " . $this->id . " AND variavel_id = '$id'",
            'order' => 'sequencia DESC',
            'index' => 'sequencia',
          ));
          return $opcoes;
    }

    public function getSequencias(){
      $opcoes = QuestaoVariavel::model()->findAll(array(
        'condition' => "questao_id = " . $this->id,
        'order' => 'sequencia DESC',
      ));

      $sequencias = array();
      $atual = null;
      foreach ($opcoes as $o) {
        if($o->sequencia != $atual){
          $atual = $o->sequencia;
          $sequencias[$atual] = array();
        }
        $sequencias[$atual][$o->variavel_id] = $o;
      }
      return $sequencias;
    }

    public function mediaAcertos(){

      $data = Yii::app()->db->createCommand()
        ->select('sum(taxa_acerto)/count(*) as taxaAcerto,count(*) as total')
        ->from('questao_resposta')
        ->where('questao_id = ' . $this->id)
        ->group('user_id')
        ->queryAll();
      // TODO: montar SQL. com muitos usuário ficará lento
      $somaTaxa = $totalRespostas = 0;
      foreach ($data as $d) {
        $somaTaxa += $d['taxaAcerto'];
        $totalRespostas += $d['total'];
      }
      $respostasUsuatiosDistintos = (count($data) > 0 ? count($data) : 1);
      return array(
        'taxaGeral' => ($somaTaxa/$respostasUsuatiosDistintos),
        'totalRespostas' => $totalRespostas,
        'totalRespostasUsuatiosDistintos' => $respostasUsuatiosDistintos,
      );
    }

}

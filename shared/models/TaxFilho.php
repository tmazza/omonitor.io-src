<?php

/**
 * This is the model class for table "tax_filho".
 *
 * The followings are the available columns in table 'tax_filho':
 * @property integer $pai
 * @property integer $filho
 *
 * The followings are the available model relations:
 * @property TaxItem $filho0
 * @property TaxItem $pai0
 */
class TaxFilho extends AplicationActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tax_filho';
    }

    public static function getTableName() {
        return 'tax_filho';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('pai, filho', 'required'),
            array('pai, filho', 'numerical', 'integerOnly' => true),
            array('pai, filho', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'itemPai' => array(self::BELONGS_TO, 'TaxItem', 'pai'),
            'itemFilho' => array(self::BELONGS_TO, 'TaxItem', 'filho'),
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'pai' => 'Pai',
            'filho' => 'Filho',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('pai', $this->pai);
        $criteria->compare('filho', $this->filho);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TaxFilho the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

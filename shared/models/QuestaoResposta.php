<?php

/**
 * This is the model class for table "questao_resposta".
 *
 * The followings are the available columns in table 'questao_resposta':
 * @property integer $user_id
 * @property integer $questao_id
 * @property integer $num_tentativa
 * @property double $taxa_acerto
 * @property string $resposta
 * @property integer $contexto
 *
 * The followings are the available model relations:
 * @property Questao $questao
 * @property User $user
 */
class QuestaoResposta extends CActiveRecord
{

	const ContextoQstLivre = 0;
	const ContextoEmCurso = 1;

	public $max;

	public function tableName()
	{
		return 'questao_resposta';
	}

        public static function getTableName()
	{
		return 'questao_resposta';
	}


	public function rules()
	{
		return array(
			array('user_id, questao_id,questionario_id, num_tentativa, taxa_acerto', 'required'),
			array('user_id, questao_id, num_tentativa, contexto', 'numerical', 'integerOnly'=>true),
			array('taxa_acerto', 'numerical'),
			array('resposta', 'safe'),
			array('user_id, questao_id, num_tentativa, taxa_acerto, resposta, contexto', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'questao' => array(self::BELONGS_TO, 'Questao', 'questao_id'),
			'questionario' => array(self::BELONGS_TO, 'Questionario', 'questionario_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'questao_id' => 'Questao',
			'num_tentativa' => 'Num Tentativa',
			'taxa_acerto' => 'Taxa Acerto',
			'resposta' => 'Resposta',
			'contexto' => 'Contexto',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('questao_id',$this->questao_id);
		$criteria->compare('num_tentativa',$this->num_tentativa);
		$criteria->compare('taxa_acerto',$this->taxa_acerto);
		$criteria->compare('resposta',$this->resposta,true);
		$criteria->compare('contexto',$this->contexto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTentativa($id){
		$resposta = self::model()->find(array(
			'select' => 'max(num_tentativa) as max',
			'condition' => "questao_id = {$id} and user_id = " . Yii::app()->user->id,
		));
		if(is_null($resposta) || strlen($resposta->max) == 0){
			return 0;
		} else {
			return $resposta->max + 1;
		}
	}

}

<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $nome
 * @property string $apresentacao
 * @property string $email
 * @property string $password
 * @property string $tipo
 * @property string $social_identifier
 * @property string $social_provider
 * @property string $dt_cadastro
 *
 * The followings are the available model relations:
 * @property AlunoQuestaoQuestionario[] $alunoQuestaoQuestionarios
 * @property Arquivo[] $arquivos
 * @property Camada[] $camadas
 * @property CamadaApendice[] $camadaApendices
 * @property CamadaTipo[] $camadaTipos
 * @property Curso[] $cursos
 * @property CursoAluno[] $cursoAlunos
 * @property CursoAluno[] $cursoAlunos1
 * @property CursoAula[] $cursoAulas
 * @property CursoAulaArquivo[] $cursoAulaArquivos
 * @property CursoAulaQuestionario[] $cursoAulaQuestionarios
 * @property CursoAulaTopico[] $cursoAulaTopicos
 * @property Grupo[] $grupos
 * @property GrupoMensagem[] $grupoMensagems
 * @property Grupo[] $grupos1
 * @property LatexMacro[] $latexMacros
 * @property Mensagem[] $mensagems
 * @property Mensagem[] $mensagems1
 * @property Notebook[] $notebooks
 * @property QuestaoAlternativa[] $questaoAlternativas
 * @property QuestaoGabarito[] $questaoGabaritos
 * @property Questionario[] $questionarios
 * @property QuestionarioQuestao[] $questionarioQuestaos
 * @property SegAuthitem[] $segAuthitems
 * @property TaxFilho[] $taxFilhos
 * @property TaxItem[] $taxItems
 * @property TaxUser[] $taxUsers
 * @property Teclado[] $teclados
 * @property TecladoSimbolos[] $tecladoSimboloses
 * @property TemplatesLatex[] $templatesLatexes
 * @property Topico[] $topicos
 * @property TopicoRelacionado[] $topicoRelacionados
 */
class User extends CActiveRecord
{
	const TipoAdmin = 'admin';
	const TipoAutor = 'autor';
	const TipoAluno = 'aluno';

	protected function beforeSave() {
		if ($this->isNewRecord) {
			$this->password = CPasswordHelper::hashPassword($this->password);
		}
		return parent::beforeSave();
	}

	public function tableName()
	{
		return 'user';
	}

	public static function getTableName()
	{
		return 'user';
	}


	public function rules()
	{
		return array(
			array('nome, email, password, tipo, dt_cadastro', 'required'),
			array('email, password', 'length', 'max'=>300),
			array('tipo', 'length', 'max'=>12),
			array('social_identifier, social_provider', 'length', 'max'=>255),
			array('apresentacao', 'safe'),
			array('id, nome, apresentacao, email, password, tipo, social_identifier, social_provider, dt_cadastro', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'permissoes' => array(self::MANY_MANY, 'TaxItem', TaxUser::getTableName() . '(tax_item_id, user_id)'),
			'matriculas' => array(self::MANY_MANY, 'Curso', CursoAluno::getTableName() . '(curso_id,aluno_id)'),
			'topicos' => array(self::HAS_MANY, 'Topico', 'user_id', 'condition' => "publicado = 1", 'order' => 'nome'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'apresentacao' => 'Apresentacao',
			'email' => 'Email',
			'password' => 'Password',
			'tipo' => 'Tipo',
			'social_identifier' => 'Social Identifier',
			'social_provider' => 'Rede Social',
			'dt_cadastro' => 'Data de Cadastro',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('apresentacao',$this->apresentacao,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('social_identifier',$this->social_identifier,true);
		$criteria->compare('social_provider',$this->social_provider,true);
		$criteria->compare('dt_cadastro',$this->dt_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	/**
	 * Retirar a permissão de acesso do usuário nodo $itemID
	 * TODO: retirar permissão de nodos filhos?
	 * @param type $itemID
	 */
	public function removePermissao($itemID){
		return TaxUser::model()->deleteAll(array(
			'condition' => "user_id = '{$this->username}' AND tax_item_id = {$itemID}"
		)) > 0;
	}
	/**
	 * Inclui permissao
	 * @param type $itemID
	 */
	public function addPermissao($itemID){
		$am = Yii::app()->authManager->assign($itemID,$this->id);
	}

	/**
	 * Retorna model referente ao perfil do usuário (aluno | autor)
	 * @return type
	 */
	public function getPerfil(){
		return $this;
	}

	// Cria registro de usuário e de aluno
	public static function salvaNovoAluno($cadastro){
		$transaction = Yii::app()->db->beginTransaction();
		$novoUsuario = new User();
		$novoUsuario->nome = $cadastro->nome;
		$novoUsuario->email = $cadastro->email;
		$novoUsuario->username = $cadastro->email;
		$novoUsuario->password = $cadastro->senha;
		$novoUsuario->tipo = User::TipoAluno;
		$novoUsuario->social_identifier = isset($cadastro->social_identifier)?$cadastro->social_identifier:null;
		$novoUsuario->social_provider = isset($cadastro->social_provider)?$cadastro->social_provider:null;
		$novoUsuario->dt_cadastro = New CDbExpression('NOW()');

		if ($novoUsuario->save()) {
			$novoUsuario->addPermissao('aluno');
			$transaction->commit();
			return true;
		} else {
			$transaction->rollback();
			return false;
		}
	}

	// Testa se o email já existe no banco, util para quem vem das redes sociais
	public static function usuarioExistente($username){
		$cadastro = User::model()->findByPk((int) $username);
		if(!is_null($cadastro)){
			return true;
		}else{
			return false;
		}
	}

	public static function salvaAlunoImportadoDeRedeSocial(HybridAuthIdentity $hybrid){
		$cadastro = new ShCadastro;
		$cadastro->email = $hybrid->userProfile->email;
		$cadastro->nome = $hybrid->userProfile->displayName;
		$cadastro->senha = $hybrid->userProfile->identifier;
		$cadastro->social_identifier = $hybrid->userProfile->identifier;
		$cadastro->social_provider =  $hybrid->adapter->id;

		if(User::salvaNovoAluno($cadastro)){
			return true;
		}else{
			Yii::app()->user->setFlash('erro',"Algo deu errado :( , tente novamente!");
			return false;
		}
	}

	public static function logaUsuarioSocial($username) {
		$user = User::model()->findByAttributes(array(
			'email' => $username,
		));

		$identity = new ShUserIdentity($user->email, $user->password);
		$identity->setInfosNaSessao($user);
		return Yii::app()->user->login($identity, 60 * 60 * 24 * 7);
	}

	public function getMatriculas(){
		return $this->user->matriculas;
	}

}

<?php

/**
 * This is the model class for table "camada_tipo".
 *
 * The followings are the available columns in table 'camada_tipo':
 * @property integer $id
 * @property integer $nome
 * @property integer $escondido
 * @property integer $recursivo
 * @property string $view_name
 */
class CamadaTipo extends AplicationActiveRecord {

    const VisiEditNula = 0; // Não aparece para ser incluida na edição
    const VisiEditPessoal = 1; // Aparece na área do usuário como opção de camada
    const VisiEditAplicacao = 2; // Aprece no topo do conteúdo como opção de camada

    /**
     * @return string the associated database table name
     */

    public function tableName() {
        return 'camada_tipo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('nome, escondido, recursivo, view_name, user_id', 'required'),
            array('escondido, recursivo, em_modal', 'numerical', 'integerOnly' => true),
            array('view_name, texto_ao_criar', 'length', 'max' => 100),
            array('nome, texto_ao_criar, escondido, em_modal, recursivo, , view_name, texto_link_expandir, cor_fundo, cor_texto', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'camada' => array(self::HAS_MANY, 'Camada', 'tipo_id', 'condition' => "excluido = 0"),
            'camadasDoAutor' => array(self::HAS_MANY, 'Camada', 'tipo_id', 'condition' => "excluido = 0 AND user_id = '" . Yii::app()->user->id . "'"),
            'apendices' => array(self::HAS_MANY, 'CamadaTipoApendice', 'camada_tipo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'escondido' => 'Escondido',
            'recursivo' => 'Recursivo',
            'view_name' => 'View Name',
        );
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'"
            ),
            'editaveis' => array(
                'condition' => "editavel = 1",
            ),
            'comAcesso' => array(
                'condition' => "de_aplicacao = 1 OR user_id = '" . Yii::app()->user->id . "'"
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CamadaTipo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

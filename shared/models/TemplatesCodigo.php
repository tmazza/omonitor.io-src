<?php

/**
 * This is the model class for table "templates_codigo".
 *
 * The followings are the available columns in table 'templates_codigo':
 * @property integer $id
 * @property string $template
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property User $autor
 */
class TemplatesCodigo extends AplicationActiveRecord {

    public function tableName() {
        return 'templates_codigo';
    }

    public static function getTableName() {
        return 'templates_codigo';
    }

    public function rules() {
        return array(
            array('template', 'required'),
            array('id, template, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'autor' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'template' => 'Código',
            'user_id' => 'Autor',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('template', $this->template, true);
        $criteria->compare('user_id', $this->user_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'doAutor' => array(
                'condition' => "user_id = '" . Yii::app()->user->id . "'",
            ),
        );
    }

}

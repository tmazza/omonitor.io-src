<?php

/**
 * This is the model class for table "curso_aluno".
 *
 * The followings are the available columns in table 'curso_aluno':
 * @property integer $id
 * @property string $aluno_id
 * @property integer $curso_id
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property Curso $curso
 * @property User $aluno
 */
class CursoAluno extends AplicationActiveRecord {

    public function tableName() {
        return 'curso_aluno';
    }

    public static function getTableName() {
        return 'curso_aluno';
    }

    public function rules() {
        return array(
            array('aluno_id, curso_id, user_id', 'required'),
            array('curso_id', 'numerical', 'integerOnly' => true),
            array('aluno_id, user_id', 'length', 'max' => 100),
            array('id, aluno_id, curso_id, user_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'curso' => array(self::BELONGS_TO, 'Curso', 'curso_id'),
            'aluno' => array(self::BELONGS_TO, 'User', 'aluno_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'aluno_id' => 'Aluno',
            'curso_id' => 'Curso',
            'user_id' => 'Autor',
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

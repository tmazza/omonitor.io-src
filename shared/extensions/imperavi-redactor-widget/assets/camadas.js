if (!RedactorPlugins)
    var RedactorPlugins = {};
RedactorPlugins.advanced = function()
{
    return {
        init: function() {
            var dropdown = {};
            dropdown.point1 = {title: 'Afirmação', func: this.advanced.camadaAfirmacao};
            dropdown.point6 = {title: 'Definição', func: this.advanced.camadaDefinicao};
            dropdown.point2 = {title: 'Detalhe', func: this.advanced.camadaDetalhe};
            dropdown.point3 = {title: 'Dica', func: this.advanced.camadaDica};
            dropdown.point4 = {title: 'Explicação', func: this.advanced.camadaExplicacao};
            dropdown.point5 = {title: 'Interação', func: this.advanced.camadaSage};
            dropdown.point9 = {title: 'Prova', func: this.advanced.camadaProva};
            dropdown.point7 = {title: 'Teorema', func: this.advanced.camadaTeorema};
            dropdown.point8 = {title: 'Subtópico', func: this.advanced.camadaSubtopico};

            var button = this.button.add('advanced', 'Nova camada');
            this.button.addDropdown(button, dropdown);
        },
        camadaSage: function() {
            this.insert.html("<div class='camada' tipo='3'>Conteúdo interativo será editado no próximo passo.</div><br>", false);
        },
        camadaAfirmacao: function() {
            this.insert.html("<div class='camada' tipo='4'>Afirmação:</div><br>", false);
        },
        camadaDetalhe: function() {
            this.insert.html("<div class='camada' tipo='5'><div class='apendice' tipo='1'>Mais detalhes</div><br></div><br>", false);
        },
        camadaDica: function() {
            this.insert.html("<div class='camada' tipo='7'>Dica!</div><div class='separador'></div><br>", false);
        },
        camadaExplicacao: function() {
            this.insert.html("<div class='camada' tipo='8'>Explicação!</div><br>", false);
        },
        camadaProva: function() {
            this.insert.html("<div class='camada' tipo='13'>Prova</div><br>", false);
        },
        camadaTeorema: function() {
            this.insert.html("<div class='camada' tipo='11'>Teorema</div><br>", false);
        },
        camadaDefinicao: function() {
            this.insert.html("<div class='camada' tipo='10'>Definição: </div><br>", false);
        },
        camadaSubtopico: function() {
            this.insert.html("<div class='camada' tipo='12'><div class='apendice' tipo='1'>Título</div><br></div><br>", false);
        },
    };
};
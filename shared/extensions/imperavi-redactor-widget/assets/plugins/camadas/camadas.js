

if (!RedactorPlugins)
    var RedactorPlugins = {};
RedactorPlugins.advanced = function()
{
    return {
        init: function() {
            var dropdown = {};
            dropdown.point1 = {title: 'SageCell', func: this.advanced.camadaSage};
            dropdown.point2 = {title: 'Afirmacao', func: this.advanced.camadaAfirmacao};
            dropdown.point3 = {title: 'Detalhe', func: this.advanced.camadaDetalhe};
            dropdown.point4 = {title: 'Dica', func: this.advanced.camadaDica};
            dropdown.point5 = {title: 'Explicacao', func: this.advanced.camadaExplicacao};
            var button = this.button.add('advanced', 'Nova camada');
            this.button.addDropdown(button, dropdown);
        },
        camadaSage: function() {
            this.insert.html("<div class='camada' tipo='3'><div class='apendice' tipo='1'>Título</div><p>SageCell!<br></p></div><br>", false);
        },
        camadaAfirmacao: function() {
            this.insert.html("<div class='camada' tipo='4'><p>Afirmação!<br></p></div><br>", false);
        },
        camadaDetalhe: function() {
            this.insert.html("<div class='camada' tipo='5'><div class='apendice' tipo='1'>Título</div><p>Detalhes aqui!<br></p></div><br>", false);
        },
        camadaDica: function() {
            this.insert.html("<div class='camada' tipo='7'><p>Dica!<br></p></div><br>", false);
        },
        camadaExplicacao: function() {
            this.insert.html("<div class='camada' tipo='8'>Explicação!</div><br>", false);
        },
    };
};
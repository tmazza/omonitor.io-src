<?php
/**kaws
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------
//dakldhasdhaslkacnawkldskl
return
	array(
		"base_url" => Yii::app()->getBaseUrl(true)."/index.php/site/socialLogin",

		"providers" => array (

			"Facebook" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "494674024023508", "secret" => "9f75a95ce6ce415ab75df6ec051f6b5c" ),
				"scope"   => "email", 
				"trustForwarded" => true,
			),

			"Google" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "550563254267-38eemljv01q3s52kkh7ve2nicrgf5491.apps.googleusercontent.com", "secret" => "cjUu76XW0Jx_xSHpeAah_aCY" )
			),

			"Twitter" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "qTCjLuCPO0AJoHsSXwj0xBw15", "secret" => " 3RfNy4HRHkM0ijWjWap8PEeBpSy1ALiEQg92RPma0ePA4ejkeq" )
			),

			// openid providers
			"OpenID" => array (
				"enabled" => false
			),

			"AOL"  => array (
				"enabled" => false
			),

			"Yahoo" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			// windows live
			"Live" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			"MySpace" => array (
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"LinkedIn" => array (
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"Foursquare" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => true,

		"debug_file" => Yii::getPathOfAlias('application.runtime')."/social"
	);

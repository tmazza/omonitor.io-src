<div 
    class='camada' <?= $attrReuso; ?> 
    contenteditable='false' 
    onclick="edicaoInt($(this))" 
    dataType="<?= $modelCamada->tipo->view_name; ?>" 
    dataCode="<?= base64_encode($modelCamada->conteudo); ?>" 
    dataId="interacao-<?= $modelCamada->id; ?>"      
    tipo='<?= $modelCamada->tipo_id; ?>' 
    <?php if ($attrId): ?> 
        id='<?= $modelCamada->id; ?>'
    <?php endif; // Para possibilitar o duplicamento de camadas ?>
    >
        <?php $this->renderPartial('shared.views.edicaoCamadas.controleCamada', array('spaces' => false)); ?>
        <?= ucfirst($modelCamada->tipo->nome); ?>
</div>
<br>
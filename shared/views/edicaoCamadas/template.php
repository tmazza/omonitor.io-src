<div class='camada' contenteditable='false' 
     onclick="edicaoTemplate(event, $(this));"
     dataType="template"
     dataCode="<?= base64_encode($modelCamada->conteudo); ?>" 
     dataId="template-<?= hash('md4', microtime(true)); ?>" 
     tipo='<?= $modelCamada->tipo_id; ?>' 

     <?php if ($attrId): ?> 
         id='<?= $modelCamada->id; ?>'
     <?php endif; ?> 
     >
         <?php $this->renderPartial('shared.views.edicaoCamadas.controleCamada', array('spaces' => false)); ?>
         <?= ucfirst($widget->nome); ?>
</div>
<br>
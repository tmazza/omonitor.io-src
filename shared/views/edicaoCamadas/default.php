<?php
$titulo = '';
// Testa se camada possui um titulo
if ($camada->tipo->possui_titulo) {
    if (!is_null($camada->titulo)) {
        $titulo = "<div class='apendice' id='{$camada->titulo->id}' tipo='{$camada->titulo->tipo}'>" . $camada->titulo->valor . '</div>';
    }
}
?>
<div 
    class='camada' 
    <?= $attrReuso; ?> 
    tipo='<?= $camada->tipo_id; ?>' 
    <?php if ($attrId): ?> 
        id='<?= $camada->id; ?>'
    <?php endif;  // Para possibilitar o duplicamento de camadas?> 
    >
        <?php $this->renderPartial('shared.views.edicaoCamadas.controleCamada', array('spaces' => true)); ?>
        <?= $titulo; ?>
        <?php echo $thisModel->expandeCamadasEdicao($camada->conteudo, $attrId); ?>
</div>
<br>
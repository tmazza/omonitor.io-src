<script>
    sagecell.makeSagecell({
        inputLocation: '.sage-code',
        evalButtonText: 'Atualizar',
        autoeval: true,
        hide: ['permalink'],
        languages: ["singular"],
    });
    function toggleCodeInteracao(e) {
        if (e.parent().find('.sagecell_input').hasClass('open-code')) {
            e.parent().find('.sagecell_input').removeClass('open-code');
        } else {
            e.parent().find('.sagecell_input').addClass('open-code');
        }
    }
</script>
<?php $style = isset($camada) ? "style='background-color: #{$camada->tipo->cor_fundo}; color:  #{$camada->tipo->cor_texto};'" : null; ?>
<div class='camada camada-sage' <?= $style; ?>>
    <div class='sage-hide-code'>
        <div class='fake-link' onclick="toggleCodeInteracao($(this));">Como foi feito?</div>
        <div class='fake-link-content'>
            <div class='sage-code'>
                <script type='text/x-sage'><?php echo CHtml::decode(stripslashes($conteudo)); ?></script>
            </div>
        </div>
    </div>
</div>
<style>
    .open-code {
        height: auto!important;        
    }    
</style>
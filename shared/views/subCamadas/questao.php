<script>
    sagecell.makeSagecell({
        inputLocation: '.sage-questao',
        autoeval: true,
        hide:["editor","language","evalButton","permalink","done","sessionFiles"],
    });
</script>
<?php $style = isset($camada) ? "style='background-color: #{$camada->tipo->cor_fundo}; color:  #{$camada->tipo->cor_texto};'" : null; ?>
<div class='camada camada-questao' <?= $style; ?>>
    <div class='sage-questao'>
        <script type='text/x-sage'><?php echo CHtml::decode(stripslashes($conteudo)); ?></script>
    </div>
</div>

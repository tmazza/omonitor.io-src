<?php

$sharedAssets = Yii::app()->assetManager->publish(Yii::getPathOfAlias('shared.webroot'));
/*
 *  Monta link com dados definidos no tipo da camada.
 *  Utiliza o campo text_para_expandir como conteúdo da do link para expansão
 *  Se o campo em_modal for true, ao invez de incluir o conteudo no meio da visualização
 *  este é colocado dentro de uma componente dialog o qual é aberto
 */
$linkID = "link{$camada->id}";
$expandirID = "expande{$camada->id}";

// Url para atualização
$url = Yii::app()->controller->createUrl('site/expandeCamada', array('id' => $camada->id,));

// Conteúdo do ajax
$ajaxOptions = array(
    'update' => "#{$expandirID}",
    'complete' => 'js:function(){'
    . '$("#' . $expandirID . '").slideDown();'
    . 'var math = document.getElementById("' . $expandirID . '");'
    . 'MathJax.Hub.Queue(["Typeset",MathJax.Hub,math]);'
);

$renderContent = 'asd';

if ($camada->tipo->em_modal) {
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => $expandirID,
        'htmlOptions' => array('class' => 'camada-modal'),
//        'themeUrl' =>  Yii::app()->baseUrl . '/webroot/monitor/css/',
//        'cssFile' => 'dialog.css',
        'options' => array(
            'title' => isset($camada->titulo) ? $camada->titulo->valor : null,
            'width' => "80%",
//            'height' => 600,
            'autoOpen' => false,
            'resizable' => false,
            'modal' => true,
            'overlay' => array(
                'backgroundColor' => '#000',
                'opacity' => '0.5'
            ),
            'buttons' => array(
                'Ok, entendi.' => 'js:function(){$(this).dialog("close");}',
            ),
        ),
    ));
    $this->endWidget('zii.widgets.jui.CJuiDialog');
    // Adiciona instrução para abertura de dialog ao complete do ajax
    $ajaxOptions['complete'] .= '$("#' . $expandirID . '").dialog("open"); return false;';
} else {
    $renderContent = "<div id='{$expandirID}'></div>";
    $ajaxOptions['beforeSend'] = 'js:function() { $("#btn-link' . $camada->id . '").html(\'' . CHtml::image($sharedAssets . '/img/loading.gif') . '\'); }';
    $ajaxOptions['complete'] .= '$("#btn-link' . $camada->id . '").slideUp(0).html("' . $texto . '");';
    $ajaxOptions['complete'] .= '$("#btn-close-link' . $camada->id . '").slideDown();';
}
$ajaxOptions['complete'] .= '}';


echo "<div id='link{$camada->id}'>";
echo CHtml::ajaxLink($texto, $url, $ajaxOptions, array('id' => 'btn-link' . $camada->id, 'class' => 'btn btn-default link-camada btn-' . $camada->tipo->categoria));
echo "<button style='display:none;' id='btn-close-link{$camada->id}' onClick='$(\"#{$expandirID}\").html(\"\");$(this).slideUp(0);$(\"#btn-link{$camada->id}\").slideDown();' class='btn red'>&#10008;</button>";
echo "</div>";

echo $renderContent;


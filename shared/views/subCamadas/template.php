<?php
$data = Camada::getJSON($camada->conteudo);
$template = Widget::model()->findByPk($data['tipo']);
$realData = array();
foreach ($data['params'] as $key => $value) {
  $realData[$key] = array('valor' => $value);
}
$code = ShView::mergeDataToTemplate($template->code,$realData);
?>
<script>
    sagecell.makeSagecell({
        inputLocation: '.sage-template',
        autoeval: true,
        hide: ["editor", "language", "evalButton", "permalink", "done", "sessionFiles"]
    });
</script>
<div class="sage-template">
    <script type="text/x-sage"><?= stripslashes($code); ?></script>
</div>

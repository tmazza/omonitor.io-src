<div class="<?php echo $class; ?>" style='background-color: #<?= $camada->tipo->cor_fundo; ?>; color:  #<?= $camada->tipo->cor_texto; ?>;'>
    <?php if (!is_null($camada->titulo)): ?>
        <h4 class="h4" id="<?= ViewHelper::makeID($camada->titulo->valor); ?>"><?= $camada->titulo->valor; ?></h4>
    <?php endif; ?>
    <?php echo $conteudo; ?>
</div>
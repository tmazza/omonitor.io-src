<?php if ($isList): ?>
    <?php if (is_array($value)): ?>
        <?php foreach ($value as $v): ?>
            <input type='number' class="span2 param" value="<?= $v ?>" name='<?= $id ?>[]' />
        <?php endforeach; ?>
    <?php else: ?>
        <input type='number' class="span2 param" value="<?= $value ?>" name='<?= $id ?>[]' />
    <?php endif; ?>
<?php else: ?>
        <input type='number' value="<?= $value ?>" class="param" name='<?= $id ?>' />
<?php endif; ?>
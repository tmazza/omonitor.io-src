<?php

$inline = isset($inline) ? $inline : false;
$data = isset($this->data) ? $this->data : array();
foreach ($params as $id => $p) {
    if ($inline) {
        echo '<div style="display: inline-block; border: 1px dotted #777; margin: 10px;padding:10px;">';
    } else {
        echo '<div style="border: 1px dotted #777; margin: 10px;padding:10px;">';
    }
    echo $id . ' ';
    if ($p['isList']) {
        echo CHtml::ajaxLink('+', Yii::app()->controller->createUrl('loadType'), array(
            'method' => 'POST',
            'data' => array(
                'label' => $id,
                'template' => $p,
//                'data' => 'js: $("#widget-view form").serialize()',
            ),
            'success' => 'js: function(html) {'
            . "$('#np-{$id}').append(html);"
            . '}',
                ), array(
            'id' => 'ajax-p' . hash('crc32',microtime(true)),
            'class' => 'btn btn-primary btn-small ',
        )) . '<br>';
    }
    echo ShView::getTemplateTipo($id, $p, $data);
    if ($p['isList']) {
        echo "<div id='np-$id' style='display: inline-block;'></div>";
    }

    echo '</div>';
}
